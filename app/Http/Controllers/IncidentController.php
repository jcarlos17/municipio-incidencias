<?php

namespace App\Http\Controllers;

use App\Http\Requests\Incident\IncidentRequest;
use App\Models\Admin\Obligation;
use App\Models\Archive;
use App\Models\IncidentArchive;
use App\Models\IncidentChange;
use App\Models\IncidentFile;
use App\Models\IncidentForm;
use App\Models\Level;
use App\Models\CollectObligation;
use App\Models\CollectObligationDetail;
use App\Models\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Process;
use App\Models\Incident;
use App\Models\DepartmentUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class IncidentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function guide($id)
    {
        $incident = Incident::findOrFail($id);

        $pdf = PDF::loadView('incidents.guide-pdf', ['incident' => $incident])
            ->setPaper('a4');

        return $pdf->stream('guia-'.$incident->id.'.pdf');
    }
    public function show($id)
    {
        $incident = Incident::findOrFail($id);
        $incident_histories = IncidentChange::where('incident_id', $id)->get();

        $user = auth()->user();
        $department_id = $user->selected_department_id;

        $first = $incident->process->levels()->first(['id']);
        $lastId = $incident->process->levels()->orderByDesc('id')->first(['id'])->id;

        $take_incident = Incident::where('id', $id)
            ->where('support_id', $user->id)
            ->where('department_id', $department_id)
            ->whereIn('level_id', [$first->id, $lastId])
            ->exists();

        $messages = $incident->messages;

        $existsCollectObligation = $incident->collect_obligations()
            ->exists();

        $pending = $incident->collect_obligations()
            ->whereNull('payment_date')
            ->exists();

        return view('incidents.show', compact(
            'incident', 'messages', 'incident_histories', 'take_incident', 'first',
            'existsCollectObligation', 'pending'
        ));
    }

    public function create()
    {
        $user = auth()->user();

        if ($user->is(User::CLIENT)) {
            $processes = Process::where('department_id', $user->selected_department_id)->get([
                'id', 'name'
            ]);
        } else {
            $firstLevelIds = Level::where('department_id', $user->selected_department_id)
                ->select(['process_id', DB::raw('MIN(id) as id')])
                ->groupBy('process_id')
                ->pluck('id')
                ->toArray();

            $processIds = DepartmentUser::where('department_id', $user->selected_department_id)
                ->where('user_id', $user->id)
                ->whereIn('level_id', $firstLevelIds)
                ->pluck('process_id')
                ->toArray();

            $processes = Process::whereIn('id', $processIds)
                ->get([
                    'id', 'name'
                ]);
        }

        $oldNames = old('names');
        $oldArchiveIds = old('archive_ids');

        $archiveUrls = [];

        if($oldArchiveIds) {
            foreach ($oldArchiveIds as $oldArchiveId) {
                $archive = Archive::find($oldArchiveId);
                $archiveUrls[] = $archive ? $archive->url : '';
            }
        }

        return view('incidents.create', compact(
            'user', 'processes', 'oldNames', 'oldArchiveIds', 'archiveUrls'
        ));
    }

    public function store(IncidentRequest $request)
    {
        $processId = $request->input('process_id');
        $incident = new Incident();
        $incident->process_id = $processId;
        $incident->severity = $request->input('severity');
        $incident->description = $request->input('description');

        $incident->date = now()->format('Ymd');

        $count = Incident::whereBetween('created_at', [now()->startOfDay(), now()->endOfDay()])
            ->count();

        $incident->correlative = $count+1;

        $user = auth()->user();

        $incident->creator_id = $user->id;
        $incident->client_id = $request->input('client_id');
//        $incident->obligation_id = $request->input('obligation_id');
        $incident->department_id = $user->selected_department_id;
        $incident->level_id = Process::findOrFail($processId)->first_level_id;

        $incident->save();

        $history = new IncidentChange();
        $history->type = 'registry';
        $history->incident_id = $incident->id;
        $history->user_id = $user->id;
        $history->save();

        // Forms
        $forms = $incident->process->forms;
        foreach ($forms as $form) {
            IncidentForm::updateOrCreate([
                'form_id' => $form->id,
                'incident_id' => $incident->id
            ]);
        }

        // archives
        $names = $request->names;
        $archiveIds = $request->archive_ids;

        if ($names) {
            foreach ($names as $key => $name) {
                $archiveId = $archiveIds[$key];
                IncidentArchive::create([
                    'name' => $name,
                    'user_id' => $user->id,
                    'archive_id' => $archiveId > 0 ? $archiveId : null,
                    'incident_id' => $incident->id
                ]);
            }
        }

        return redirect("/vista/$incident->id")->with('notification', 'Incidencia registrada correctamente.');
    }

    public function preview(Incident $incident)
    {
        $count_days = $count_hours = $count_minutes = 0;

        $incident_count = Incident::active()
                ->where('department_id', $incident->department_id)
                ->where('id', '<', $incident->id)
                ->count() + 1;

        $process = $incident->process;
        foreach ($process->levels as $level){
            $count_days += $level->days;
            $count_hours += $level->hours;
            $count_minutes += $level->minutes;
            if ($count_minutes >= 60){
                $count_minutes -= 60;
                $count_hours += 1;
            }
            if ($count_hours >= 24){
                $count_hours -= 24;
                $count_days += 1;
            }
        }

        return view ('incidents.preview', compact(
            'incident', 'count_days', 'count_hours', 'count_minutes', 'incident_count'
        ));
    }

    public function edit(Incident $incident)
    {
        $user = auth()->user();
        $firstLevelId = $incident->process->firstLevelId;

        if ($user->is(User::CLIENT)) {
            $processes = Process::where('department_id', $user->selected_department_id)->get([
                'id', 'name'
            ]);
        } else {
            $firstLevelIds = Level::where('department_id', $user->selected_department_id)
                ->select(['process_id', DB::raw('MIN(id) as id')])
                ->groupBy('process_id')
                ->pluck('id')
                ->toArray();

            $processIds = DepartmentUser::where('department_id', $user->selected_department_id)
                ->where('user_id', $user->id)
                ->whereIn('level_id', $firstLevelIds)
                ->pluck('process_id')
                ->toArray();

            $processes = Process::whereIn('id', $processIds)
                ->get([
                    'id', 'name'
                ]);
        }

        return view('incidents.edit', compact(
            'incident', 'user', 'processes', 'firstLevelId'
        ));
    }

    public function update(Incident $incident, IncidentRequest $request)
    {
        $user = auth()->user();
        $firstLevelId = $incident->process->firstLevelId;

        if ($user->id == $incident->creator_id && $incident->level_id == $firstLevelId && $incident->support_id == NULL) {

            $incident->process_id = $request->input('process_id');
            $incident->severity = $request->input('severity');
            $incident->description = $request->input('description');

            $incident->save();

            $user_id = auth()->id();
            $history = new IncidentChange();
            $history->type = 'edit';
            $history->incident_id = $incident->id;
            $history->user_id = $user_id;
            $history->save();

            return redirect("/ver/$incident->id");
        }else{
            return redirect("/incidencia/$incident->id/editar");
        }
    }

    public function take(Incident $incident)
    {
        $user = auth()->user();

        if (! $user->is(User::SUPPORT))
            return back();

        // There is a relationship between user and department?
        // Is assigned to this level?
        $assigned = DepartmentUser::where('level_id', $incident->level_id)
            ->where('user_id', $user->id)
            ->exists();

        if (!$assigned)
            return back();

        $incident->support_id = $user->id;
        $incident->save();

        $history = new IncidentChange();
        $history->type = 'attention';
        $history->incident_id = $incident->id;
        $history->user_id = $incident->support_id;
        $history->save();

        return back();
    }

    public function solve(Incident $incident)
    {
        $user_id = auth()->id();
        // Is the user authenticated the author of the incident?
        if ($incident->support_id != $user_id)
            return back();

        $incident->active = 0; // false
        $incident->save();

        $history = new IncidentChange();
        $history->type = 'resolved';
        $history->incident_id = $incident->id;
        $history->user_id = $user_id;
        $history->save();

        return back();
    }

    public function open(Incident $incident)
    {
        // Is the user authenticated the author of the incident?
        if ($incident->support_id != auth()->user()->id)
            return back();

        $incident->active = 1; // true
        $incident->save();

        $history = new IncidentChange();
        $history->type = 'open';
        $history->incident_id = $incident->id;
        $history->user_id = auth()->id();
        $history->save();

        return back();
    }

    public function level(Incident $incident, $option)
    {
        $level_id = $incident->level_id;

        $process = $incident->process;
        $levels = $process->levels()
            ->pluck('id')
            ->toArray();

        $futureId = $this->getLevelId($level_id, $option, $levels);

        if ($futureId) {
            $incident->level_id = $futureId;
            $incident->support_id = null;
            $incident->save();

            $history = new IncidentChange();
            $history->type = 'derive';
            $history->incident_id = $incident->id;
            $history->user_id = auth()->id();
            $history->save();

            return back();
        }

        return back()->with('notification', 'No es posible derivar porque no hay un siguiente nivel.');
    }

    public function getLevelId($level_id, $option, $levels)
    {
        if (count($levels) <= 1)
            return null;

        $key = array_search($level_id, $levels);

        if ($key === false)
            return null;

        return $levels[$option == 'siguiente' ? $key+1 : $key-1];
    }

    public function download(IncidentFile $incidentFile)
    {
        return Storage::disk('public')->download($incidentFile->name);
    }

    public function upload($id, Request $request)
    {
        // files
        $files = $request->file('files');

        if ($files) {
            foreach ($files as $file) {
                $name = $file->getClientOriginalName();
                Storage::disk('public')->put($name, File::get($file));

                IncidentFile::create([
                    'name' => $name,
                    'user_id' => auth()->id(),
                    'incident_id' => $id
                ]);
            }
        }

        return back();
    }
}
