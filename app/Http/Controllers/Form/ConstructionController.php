<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\Form\FormConstructionRequest;
use App\Models\Form\FormConstruction;
use App\Models\Incident;
use App\Models\IncidentForm;
use App\Models\Requirement;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ConstructionController extends Controller
{
    public function create(IncidentForm $incidentForm)
    {
        $incident = $incidentForm->incident;

        return view('incidents.forms.constructions.create', compact(
            'incident'
        ));
    }

    public function store(IncidentForm $incidentForm, FormConstructionRequest $request)
    {
        $incident = $incidentForm->incident;

        $futureId = FormConstruction::whereDate('created_at', now())->count() + 1;
        $code = 'PDC-'.date('Ymd').'-'.$futureId;

        FormConstruction::create([
            'code' => $code,
            'client_name' => $incident->client->name,
            'description' => $request->description,
            'observation' => $request->observation,
            'incident_id' => $incident->id,
            'incident_form_id' => $incidentForm->id,
            'support_id' => auth()->id()
        ]);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha registrado correctamente.');
    }

    public function edit(FormConstruction $form)
    {
        return view('incidents.forms.constructions.edit', compact(
            'form'
        ));
    }

    public function update(FormConstruction $form, FormConstructionRequest $request)
    {
        $form->update([
            'description' => $request->description,
            'observation' => $request->observation,
            'support_id' => auth()->id()
        ]);

        $route = route('form.index', ['incident' => $form->incident_id]);

        return redirect($route)->with('notification', 'El formulario se ha actualizado correctamente.');
    }

    public function download(FormConstruction $form)
    {
        $pdf = PDF::loadView('incidents.forms.constructions.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(FormConstruction $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-construction-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(FormConstruction $form)
    {
        return Storage::disk('public')->download('pdf-construction-forms/'.$form->document);
    }
}
