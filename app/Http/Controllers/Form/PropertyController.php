<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\Form\FormPropertyRequest;
use App\Models\Form\FormProperty;
use App\Models\Incident;
use App\Models\IncidentForm;
use App\Models\Requirement;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PropertyController extends Controller
{
    public function create(IncidentForm $incidentForm)
    {
        $incident = $incidentForm->incident;

        return view('incidents.forms.properties.create', compact(
            'incident'
        ));
    }

    public function store(IncidentForm $incidentForm, FormPropertyRequest $request)
    {
        $incident = $incidentForm->incident;

        $futureId = FormProperty::whereDate('created_at', now())->count() + 1;
        $code = 'CAP-'.date('Ymd').'-'.$futureId;

        FormProperty ::create([
            'code' => $code,
            'client_name' => $incident->client->name,
            'client_document' => $incident->client->document,
            'owner_name' => $request->owner_name,
            'owner_document' => $request->owner_document,
            'department' => $request->department,
            'reason' => $request->reason,
            'street' => $request->street,
            'district' => $request->district,
            'sector' => $request->sector,
            'parish' => $request->parish,
            'observation' => $request->observation,
            'incident_id' => $incident->id,
            'incident_form_id' => $incidentForm->id,
            'support_id' => auth()->id()
        ]);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha registrado correctamente.');
    }

    public function edit(FormProperty $form)
    {
        return view('incidents.forms.properties.edit', compact(
            'form'
        ));
    }

    public function update(FormProperty $form, FormPropertyRequest $request)
    {
        $form->update([
            'owner_name' => $request->owner_name,
            'owner_document' => $request->owner_document,
            'department' => $request->department,
            'reason' => $request->reason,
            'street' => $request->street,
            'district' => $request->district,
            'parish' => $request->parish,
            'observation' => $request->observation,
            'support_id' => auth()->id()
        ]);

        $route = route('form.index', ['incident' => $form->incident_id]);

        return redirect($route)->with('notification', 'El formulario se ha actualizado correctamente.');
    }

    public function download(FormProperty $form)
    {
        $pdf = PDF::loadView('incidents.forms.properties.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(FormProperty $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-property-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(FormProperty $form)
    {
        return Storage::disk('public')->download('pdf-property-forms/'.$form->document);
    }
}
