<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Models\Form\FormCadastre;
use App\Models\Form\FormCadastreLocation;
use App\Models\Incident;
use App\Models\IncidentForm;
use App\Models\Requirement;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CadastreController extends Controller
{
    public function create(IncidentForm $incidentForm)
    {
        $incident = $incidentForm->incident;

        $form = $incidentForm->form;

        if (!$form) {
            $futureId = FormCadastre::whereDate('created_at', now())->count() + 1;
            $code = 'CPB-'.date('Ymd').'-'.$futureId;

            $form = FormCadastre::create([
                'code' => $code,
                'client_name' => $incident->client->name,
                'incident_id' => $incident->id,
                'incident_form_id' => $incidentForm->id,
                'support_id' => auth()->id()
            ]);
        }

        return redirect(route($incidentForm->requirement->form_type.'.edit', ['form' => $form->id]));
    }

    public function edit(FormCadastre $form)
    {
        return view('incidents.forms.cadastres.edit', compact(
            'form'
        ));
    }

    public function download(FormCadastre $form)
    {
        $pdf = PDF::loadView('incidents.forms.cadastres.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(FormCadastre $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-cadastres-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
                'support_id' => auth()->id()
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(FormCadastre $form)
    {
        return Storage::disk('public')->download('pdf-cadastres-forms/'.$form->document);
    }


    public function addLocation(Request $request)
    {
        FormCadastreLocation::create([
            'state' => $request->state,
            'parish' => $request->parish,
            'location' => $request->location,
            'key' => $request->key,
            'commercial' => $request->commercial,
            'form_cadastre_id' => $request->form_id,
        ]);

        return redirect('/certificado-avaluos-catastros/'.$request->form_id.'/editar');
    }

    public function updateLocation(Request $request)
    {
        $location = FormCadastreLocation::findOrFail($request->location_id);

        $location->update([
            'state' => $request->state,
            'parish' => $request->parish,
            'location' => $request->location,
            'key' => $request->key,
            'commercial' => $request->commercial,
        ]);

        return redirect('/certificado-avaluos-catastros/'.$location->form_cadastre_id.'/editar');
    }
}
