<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\Form\FormMunicipalityRequest;
use App\Models\Form\FormMunicipality;
use App\Models\IncidentForm;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class MunicipalityController extends Controller
{
    public function create(IncidentForm $incidentForm)
    {
        $incident = $incidentForm->incident;
        $officials = $incident->department->officials()->orderBy('name')
            ->get(['id', 'name']);

        $futureId = FormMunicipality::whereDate('created_at', now())->count() + 1;
        $code = 'CNA-'.date('Ymd').'-'.$futureId;

        return view('incidents.forms.municipalities.create', compact(
            'incident', 'officials', 'code'
        ));
    }

    public function store(IncidentForm $incidentForm, FormMunicipalityRequest $request)
    {
        $incident = $incidentForm->incident;
        $futureId = FormMunicipality::whereDate('created_at', now())->count() + 1;
        $code = 'CNA-'.date('Ymd').'-'.$futureId;

        FormMunicipality::create([
            'code' => $code,
            'client_name' => $request->client_name,
            'client_document' => $request->client_document,
            'address' => $request->address,
            'email' => $request->email,
            'phone' => $request->phone,
            'process_name' => $incident->process->name,
            'incident_form_id' => $incidentForm->id,
            'incident_id' => $incident->id,
            'code_qr' => '16311217278734971230',
            'support_id' => auth()->id()
        ]);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha registrado correctamente.');
    }

    public function edit(FormMunicipality $form)
    {
        return view('incidents.forms.municipalities.edit', compact(
            'form'
        ));
    }

    public function update(FormMunicipality $form, FormMunicipalityRequest $request)
    {
        $incident = $form->incident;

        $form->update([
            'client_name' => $request->client_name,
            'client_document' => $request->client_document,
            'address' => $request->address,
            'email' => $request->email,
            'phone' => $request->phone,
            'process_name' => $incident->process->name,
            'support_id' => auth()->id()
        ]);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha actualizado correctamente.');
    }

    public function download(FormMunicipality $form)
    {
        $pdf = PDF::loadView('incidents.forms.municipalities.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(FormMunicipality $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-municipality-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(FormMunicipality $form)
    {
        return Storage::disk('public')->download('pdf-municipality-forms/'.$form->document);
    }
}
