<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\Form\FormBlueprintRequest;
use App\Models\Form\FormBlueprint;
use App\Models\Incident;
use App\Models\IncidentForm;
use App\Models\Requirement;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BlueprintController extends Controller
{
    public function create(IncidentForm $incidentForm)
    {
        $incident = $incidentForm->incident;
        $futureId = FormBlueprint::whereDate('created_at', now())->count() + 1;
        $code = 'SAP-'.date('Ymd').'-'.$futureId;

        return view('incidents.forms.blueprints.create', compact(
            'incident', 'code'
        ));
    }

    public function store(IncidentForm $incidentForm, FormBlueprintRequest $request)
    {
        $incident = $incidentForm->incident;
        $futureId = FormBlueprint::whereDate('created_at', now())->count() + 1;
        $code = 'SAP-'.date('Ymd').'-'.$futureId;

        $data = $request->except('_token');
        $data['code'] = $code;
        $data['name'] = $incident->client->name;
        $data['incident_id'] = $incident->id;
        $data['incident_form_id'] = $incidentForm->id;
        $data['support_id'] = auth()->id();

        FormBlueprint::create($data);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha registrado correctamente.');
    }

    public function edit(FormBlueprint $form)
    {
        return view('incidents.forms.blueprints.edit', compact(
            'form'
        ));
    }

    public function update(FormBlueprint $form, FormBlueprintRequest $request)
    {
        $incident = $form->incident;

        $data = $request->except('_token');
        $data['name'] = $incident->client->name;
        $data['support_id'] = auth()->id();

        $form->update($data);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha actualizado correctamente.');
    }

    public function download(FormBlueprint $form)
    {
        $pdf = PDF::loadView('incidents.forms.blueprints.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(FormBlueprint $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-blueprint-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(FormBlueprint $form)
    {
        return Storage::disk('public')->download('pdf-blueprint-forms/'.$form->document);
    }
}
