<?php

namespace App\Http\Controllers\Form;

use App\Models\Incident;
use App\Http\Controllers\Controller;
use App\Models\IncidentForm;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(Incident $incident)
    {
        $incidentForms = $incident->incident_forms;

        $existsForms = $incident->incident_forms()
            ->whereHas('requirement', function ($query) {
                $query->whereIn('form_type', ['certificado-no-adeudar-municipio', 'solicitud-todo-tramite']);
            })
        ->exists();

        return view('incidents.forms.index', compact(
            'incident', 'incidentForms', 'existsForms'
        ));
    }

    public function add(Incident $incident)
    {
        $incidentForms = $incident->incident_forms()
            ->select('form_id')
            ->groupBy('form_id')
            ->whereHas('requirement', function ($query) {
                $query->whereIn('form_type', ['certificado-no-adeudar-municipio', 'solicitud-todo-tramite']);
            })
            ->get();

        return view('incidents.forms.add', compact(
            'incident', 'incidentForms'
        ));
    }

    public function updateAdd(Incident $incident, Request $request)
    {
        IncidentForm::create([
            'form_id' => $request->form_id,
            'incident_id' => $incident->id
        ]);

        return redirect('incidencia/'.$incident->id.'/formularios');
    }
}
