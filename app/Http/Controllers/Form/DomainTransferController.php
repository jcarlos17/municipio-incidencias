<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\Form\FormDomainTransferRequest;
use App\Models\Form\FormDomainTransfer;
use App\Models\Incident;
use App\Models\IncidentForm;
use App\Models\Requirement;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class DomainTransferController extends Controller
{
    public function create(IncidentForm $incidentForm)
    {
        $incident = $incidentForm->incident;
        $futureId = FormDomainTransfer::whereDate('created_at', now())->count() + 1;
        $code = 'TDD-'.date('Ymd').'-'.$futureId;

        return view('incidents.forms.domain-transfers.create', compact(
            'incident', 'code'
        ));
    }

    public function store(IncidentForm $incidentForm, FormDomainTransferRequest $request)
    {
        $incident = $incidentForm->incident;
        $futureId = FormDomainTransfer::whereDate('created_at', now())->count() + 1;
        $code = 'TDD-'.date('Ymd').'-'.$futureId;

        $data = $request->except('_token');
        $data['code'] = $code;
        $data['incident_id'] = $incident->id;
        $data['incident_form_id'] = $incidentForm->id;
        $data['support_id'] = auth()->id();

        FormDomainTransfer::create($data);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha registrado correctamente.');
    }

    public function edit(FormDomainTransfer $form)
    {
        return view('incidents.forms.domain-transfers.edit', compact(
            'form'
        ));
    }

    public function update(FormDomainTransfer $form, FormDomainTransferRequest $request)
    {
        $incident = $form->incident;

        $data = $request->except('_token');
        $data['support_id'] = auth()->id();
        $form->update($data);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha actualizado correctamente.');
    }

    public function download(FormDomainTransfer $form)
    {
        $pdf = PDF::loadView('incidents.forms.domain-transfers.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(FormDomainTransfer $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-domain-transfer-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(FormDomainTransfer $form)
    {
        return Storage::disk('public')->download('pdf-domain-transfer-forms/'.$form->document);
    }
}
