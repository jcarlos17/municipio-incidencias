<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\Form\FormCreditRequest;
use App\Models\Form\FormCredit;
use App\Models\Incident;
use App\Models\IncidentForm;
use App\Models\Official;
use App\Models\Requirement;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CreditController extends Controller
{
    public function create(IncidentForm $incidentForm)
    {
        $incident = $incidentForm->incident;
        $officials = Official::orderBy('name')
            ->get(['id', 'name']);

        $futureId = FormCredit::whereDate('created_at', now())->count() + 1;
        $code = 'ETC-'.date('Ymd').'-'.$futureId;

        return view('incidents.forms.credits.create', compact(
            'incident', 'officials', 'code'
        ));
    }

    public function store(IncidentForm $incidentForm, FormCreditRequest $request)
    {
        $incident = $incidentForm->incident;
        $fromOfficial = Official::where('name', $request->from_official_name)->first(['name', 'position']);
        $toOfficial = Official::where('name', $request->to_official_name)->first(['name', 'position']);

        $futureId = FormCredit::whereDate('created_at', now())->count() + 1;
        $code = 'ETC-'.date('Ymd').'-'.$futureId;

        FormCredit::create([
            'code' => $code,
            'from_official_name' => $fromOfficial->name,
            'from_official_position' => $fromOfficial->position,
            'to_official_name' => $toOfficial->name,
            'to_official_position' => $toOfficial->position,
            'client_name' => $incident->client->name,
            'client_document' => $incident->client->document,
            'quantity' => $request->quantity,
            'concept' => $request->concept,
            'street' => $request->street,
            'district' => $request->district,
            'parish' => $request->parish,
            'incident_id' => $incident->id,
            'incident_form_id' => $incidentForm->id,
            'support_id' => auth()->id()
        ]);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha registrado correctamente.');
    }

    public function edit(FormCredit $form)
    {
        $officials = Official::orderBy('name')
            ->get(['id', 'name']);

        return view('incidents.forms.credits.edit', compact('form', 'officials'));
    }

    public function update(FormCredit $form, FormCreditRequest $request)
    {
        $fromOfficial = Official::where('name', $request->from_official_name)->first(['name', 'position']);
        $toOfficial = Official::where('name', $request->to_official_name)->first(['name', 'position']);

        $incident = $form->incident;

        $form->update([
            'from_official_name' => $fromOfficial->name,
            'from_official_position' => $fromOfficial->position,
            'to_official_name' => $toOfficial->name,
            'to_official_position' => $toOfficial->position,
            'client_name' => $incident->client->name,
            'client_document' => $incident->client->document,
            'quantity' => $request->quantity,
            'concept' => $request->concept,
            'street' => $request->street,
            'district' => $request->district,
            'parish' => $request->parish,
            'support_id' => auth()->id()
        ]);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha actualizado correctamente.');
    }

    public function download(FormCredit $form)
    {
        $pdf = PDF::loadView('incidents.forms.credits.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(FormCredit $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-credit-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(FormCredit $form)
    {
        return Storage::disk('public')->download('pdf-credit-forms/'.$form->document);
    }
}
