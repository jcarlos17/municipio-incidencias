<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\Form\FormEstateRequest;
use App\Models\Form\FormEstate;
use App\Models\Incident;
use App\Models\IncidentForm;
use App\Models\Requirement;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class EstateController extends Controller
{
    public function create(IncidentForm $incidentForm)
    {
        $incident = $incidentForm->incident;

        return view('incidents.forms.estates.create', compact(
            'incident'
        ));
    }

    public function store(IncidentForm $incidentForm, FormEstateRequest $request)
    {
        $incident = $incidentForm->incident;

        $futureId = FormEstate::whereDate('created_at', now())->count() + 1;
        $code = 'CNP-'.date('Ymd').'-'.$futureId;

        FormEstate ::create([
            'code' => $code,
            'client_name' => $incident->client->name,
            'year' => $request->year,
            'incident_id' => $incident->id,
            'incident_form_id' => $incidentForm->id,
            'support_id' => auth()->id()
        ]);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha registrado correctamente.');
    }

    public function edit(FormEstate $form)
    {
        return view('incidents.forms.estates.edit', compact(
            'form'
        ));
    }

    public function update(FormEstate $form, FormEstateRequest $request)
    {
        $form->update([
            'year' => $request->year,
            'support_id' => auth()->id()
        ]);

        $route = route('form.index', ['incident' => $form->incident_id]);

        return redirect($route)->with('notification', 'El formulario se ha actualizado correctamente.');
    }

    public function download(FormEstate $form)
    {
        $pdf = PDF::loadView('incidents.forms.estates.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(FormEstate $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-estate-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(FormEstate $form)
    {
        return Storage::disk('public')->download('pdf-estate-forms/'.$form->document);
    }
}
