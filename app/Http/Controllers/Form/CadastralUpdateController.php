<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\Form\FormCadastralUpdateRequest;
use App\Models\Form\FormCadastralUpdate;
use App\Models\Incident;
use App\Models\IncidentForm;
use App\Models\Requirement;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CadastralUpdateController extends Controller
{
    public function create(IncidentForm $incidentForm)
    {
        $incident = $incidentForm->incident;
        $futureId = FormCadastralUpdate::whereDate('created_at', now())->count() + 1;
        $code = 'CAA-'.date('Ymd').'-'.$futureId;

        return view('incidents.forms.cadastral-updates.create', compact(
            'incident', 'code'
        ));
    }

    public function store(IncidentForm $incidentForm, FormCadastralUpdateRequest $request)
    {
        $incident = $incidentForm->incident;

        $futureId = FormCadastralUpdate::whereDate('created_at', now())->count() + 1;
        $code = 'CAA-'.date('Ymd').'-'.$futureId;

        $data = $request->except('_token');
        $data['code'] = $code;
        $data['client_name'] = $incident->client->name;
        $data['client_document'] = $incident->client->document;
        $data['incident_id'] = $incident->id;
        $data['incident_form_id'] = $incidentForm->id;
        $data['support_id'] = auth()->id();

        FormCadastralUpdate::create($data);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha registrado correctamente.');
    }

    public function edit(FormCadastralUpdate $form)
    {
        return view('incidents.forms.cadastral-updates.edit', compact(
            'form'
        ));
    }

    public function update(FormCadastralUpdate $form, FormCadastralUpdateRequest $request)
    {
        $incident = $form->incident;

        $data = $request->except('_token');
        $data['client_name'] = $incident->client->name;
        $data['client_document'] = $incident->client->document;
        $data['support_id'] = auth()->id();

        $form->update($data);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha actualizado correctamente.');
    }

    public function download(FormCadastralUpdate $form)
    {
        $pdf = PDF::loadView('incidents.forms.cadastral-updates.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(FormCadastralUpdate $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-cadastral-update-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(FormCadastralUpdate $form)
    {
        return Storage::disk('public')->download('pdf-cadastral-update-forms/'.$form->document);
    }
}
