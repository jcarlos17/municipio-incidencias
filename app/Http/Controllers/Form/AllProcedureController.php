<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\Incident\IncidentFormRequest;
use App\Models\Form;
use App\Models\Incident;
use App\Models\IncidentForm;
use App\Models\Official;
use App\Models\Requirement;
use App\Models\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AllProcedureController extends Controller
{
    public function create(IncidentForm $incidentForm, $type)
    {
        $incident = $incidentForm->incident;

        $typeForm = $type == 'informe-certificado' ? 'IOC' : 'SOL';
        $officials = $incident->department->officials()->orderBy('name')
            ->get(['id', 'name']);

        $futureId = Form::where('type', $typeForm)->whereDate('created_at', now())->count() + 1;
        $code = $typeForm.'-'.date('Ymd').'-'.$futureId;

        return view('incidents.forms.all-procedures.create', compact(
            'incident', 'officials', 'code', 'typeForm'
        ));
    }

    public function store(IncidentForm $incidentForm, $type, IncidentFormRequest $request)
    {
        $incident = $incidentForm->incident;
        $official = Official::where('name', $request->official_name)
            ->first(['name', 'position']);
        $typeForm = $type == 'informe-certificado' ? 'IOC' : 'SOL';

        $futureId = Form::where('type', $typeForm)->whereDate('created_at', now())->count() + 1;
        $code = $typeForm.'-'.date('Ymd').'-'.$futureId;
        $clientId = $request->client_id;
        $client = User::findOrFail($clientId);

        Form::create([
            'code' => $code,
            'official_name' => $official->name,
            'official_position' => $official->position,
            'client_name' => $client->name,
            'client_document' => $client->document,
            'detail' => $request->detail,
            'type' => $typeForm,
            'incident_id' => $incident->id,
            'incident_form_id' => $incidentForm->id,
            'support_id' => auth()->id(),
        ]);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha registrado correctamente.');
    }

    public function edit(Form $form)
    {
        $officials = $form->incident->department->officials()->orderBy('name')
            ->get(['id', 'name']);

        $client = User::where('document', $form->client_document)->first('id');

        return view('incidents.forms.all-procedures.edit', compact(
            'form', 'officials', 'client'
        ));
    }

    public function update(Form $form, IncidentFormRequest $request)
    {
        $official = Official::where('name', $request->official_name)
            ->first(['name', 'position']);

        $incident = $form->incident;
        $clientId = $request->client_id;
        $client = User::findOrFail($clientId);

        $form->update([
            'official_name' => $official->name,
            'official_position' => $official->position,
            'client_name' => $client->name,
            'client_document' => $client->document,
            'support_name' => $incident->support->name,
            'detail' => $request->detail,
            'support_id' => auth()->id()
        ]);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha actualizado correctamente.');
    }

    public function download(Form $form)
    {
        $pdf = PDF::loadView('incidents.forms.all-procedures.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(Form $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(Form $form)
    {
        return Storage::disk('public')->download('pdf-forms/'.$form->document);
    }
}
