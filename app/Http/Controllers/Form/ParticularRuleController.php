<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\Form\FormParticularRuleRequest;
use App\Models\Form\FormParticularRule;
use App\Models\Incident;
use App\Models\IncidentForm;
use App\Models\Official;
use App\Models\Requirement;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ParticularRuleController extends Controller
{
    public function create(IncidentForm $incidentForm)
    {
        $incident = $incidentForm->incident;
        $officials = Official::orderBy('name')
            ->get(['id', 'name']);

        $futureId = FormParticularRule::whereDate('created_at', now())->count() + 1;
        $code = 'FNP-'.date('Ymd').'-'.$futureId;

        return view('incidents.forms.particular-rules.create', compact(
            'incident', 'code', 'officials'
        ));
    }

    public function store(IncidentForm $incidentForm, FormParticularRuleRequest $request)
    {
        $incident = $incidentForm->incident;

        $official = Official::where('name', $request->official_name)->first(['name', 'position']);

        $futureId = FormParticularRule::whereDate('created_at', now())->count() + 1;
        $code = 'FNP-'.date('Ymd').'-'.$futureId;

        // file
        $sketch = $request->file('sketch');

        $name = $sketch->getClientOriginalName();
        Storage::disk('public')->put('images/sketches/'.$name, File::get($sketch));

        $data = $request->except('_token');
        $data['code'] = $code;
        $data['official_name'] = $official->name;
        $data['official_position'] = $official->position;
        $data['client_name'] = $incident->client->name;
        $data['incident_id'] = $incident->id;
        $data['incident_form_id'] = $incidentForm->id;
        $data['sketch'] = $name;
        $data['support_id'] = auth()->id();

        FormParticularRule::create($data);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha registrado correctamente.');
    }

    public function edit(FormParticularRule $form)
    {
        $officials = Official::orderBy('name')
            ->get(['id', 'name']);

        return view('incidents.forms.particular-rules.edit', compact(
            'form', 'officials'
        ));
    }

    public function update(FormParticularRule $form, FormParticularRuleRequest $request)
    {
        $incident = $form->incident;

        $official = Official::where('name', $request->official_name)->first(['name', 'position']);

        $data = $request->except('_token');
        $data['official_name'] = $official->name;
        $data['official_position'] = $official->position;
        $data['client_name'] = $incident->client->name;
        $data['support_id'] = auth()->id();

        // file
        $sketch = $request->file('sketch');

        if ($sketch) {
            $name = $sketch->getClientOriginalName();
            Storage::disk('public')->put('images/sketches/'.$name, File::get($sketch));

            $data['sketch'] = $name;
        }

        $form->update($data);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha actualizado correctamente.');
    }

    public function download(FormParticularRule $form)
    {
        $pdf = PDF::loadView('incidents.forms.particular-rules.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(FormParticularRule $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-particular-rule-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(FormParticularRule $form)
    {
        return Storage::disk('public')->download('pdf-particular-rule-forms/'.$form->document);
    }
}
