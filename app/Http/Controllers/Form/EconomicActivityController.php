<?php

namespace App\Http\Controllers\Form;

use App\Http\Controllers\Controller;
use App\Http\Requests\Form\FormEconomicActivityRequest;
use App\Models\Form\FormEconomicActivity;
use App\Models\Incident;
use App\Models\IncidentForm;
use App\Models\Requirement;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class EconomicActivityController extends Controller
{
    public function create(IncidentForm $incidentForm)
    {
        $incident = $incidentForm->incident;
        $futureId = FormEconomicActivity::whereDate('created_at', now())->count() + 1;
        $code = 'DIA-'.date('Ymd').'-'.$futureId;

        return view('incidents.forms.economic-activities.create', compact(
            'incident', 'code'
        ));
    }

    public function store(IncidentForm $incidentForm, FormEconomicActivityRequest $request)
    {
        $incident = $incidentForm->incident;

        $futureId = FormEconomicActivity::whereDate('created_at', now())->count() + 1;
        $code = 'DIA-'.date('Ymd').'-'.$futureId;


        $data = $request->except('_token');
        $data['code'] = $code;
        $data['client_name'] = $incident->client->name;
        $data['client_document'] = $incident->client->document;
        $data['incident_id'] = $incident->id;
        $data['incident_form_id'] = $incidentForm->id;
        $data['support_id'] = auth()->id();

        FormEconomicActivity::create($data);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha registrado correctamente.');
    }

    public function edit(FormEconomicActivity $form)
    {
        return view('incidents.forms.economic-activities.edit', compact(
            'form'
        ));
    }

    public function update(FormEconomicActivity $form, FormEconomicActivityRequest $request)
    {
        $incident = $form->incident;

        $data = $request->except('_token');
        $data['client_name'] = $incident->client->name;
        $data['client_document'] = $incident->client->document;
        $data['support_id'] = auth()->id();

        $form->update($data);

        $route = route('form.index', ['incident' => $incident->id]);

        return redirect($route)->with('notification', 'El formulario se ha actualizado correctamente.');
    }

    public function download(FormEconomicActivity $form)
    {
        $pdf = PDF::loadView('incidents.forms.economic-activities.download-pdf', ['form' => $form])
            ->setPaper('a4');

        return $pdf->stream($form->code.'.pdf');
    }

    public function upload(FormEconomicActivity $form, Request $request)
    {
        // file
        $document = $request->file('document');

        if ($document) {
            $name = $document->getClientOriginalName();
            Storage::disk('public')->put('pdf-economic-activity-forms/'.$name, File::get($document));

            $form->update([
                'document' => $name,
            ]);
        }

        return back()->with('notification', 'Se subió correctamente el documento.');
    }

    public function finalDownload(FormEconomicActivity $form)
    {
        return Storage::disk('public')->download('pdf-economic-activity-forms/'.$form->document);
    }
}
