<?php

namespace App\Http\Controllers;

use App\Models\CollectObligation;
use App\Models\Payment;
use App\Models\Transaction;
use App\Models\TransactionCollectObligation;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WebServiceController extends Controller
{
    // /Transbank/rest/transf/consulta/{document}
    public function consultation($document)
    {
        $client = User::where('document', $document)->first();

        if (!$client) {
            return response()->json([
                'codigo_respuesta' => '01',
                'mensaje_respuesta' => 'No existe'
            ]);
        }

        $clientId = $client->id;

        $obligations = CollectObligation::whereHas('obligation', function ($query) use($clientId) {
                $query->where('client_id', $clientId);
            })
            ->where('status', CollectObligation::ISSUED)
            ->orderByDesc('id')
            ->get();

        $details = [];

        foreach ($obligations as $obligation) {
            $details[] = [
                'emision' => $obligation->entry_order,
                'anio' => $obligation->period,
                'estadoe' => $obligation->status,
                'id_sgtributo' => $obligation->obligation_id,
                'grupo' => $obligation->income->income_group->name,
                'fecha_emi' => Carbon::parse($obligation->issue_date)->format('d-m-Y'),
                'tributos' => $obligation->income->name,
                'total' => number_format($obligation->total, 2),
            ];
        }

        $data = [
            'contribuyente' => $client->id,
            'cedula' => $client->document,
            'ruc' => $client->ruc,
            'nombres' => $client->name,
            'estadoc' => $client->status,
            'codigo_respuesta' => $details ? '00' : '03',
            'mensaje_respuesta' => $details ? 'CONSULTA EXITOSA' : 'No tiene deudas pendientes',
        ];

        if ($details) {
            $data['emission'] = $details;
        }

        return response()->json($data);
    }

    public function payment(Request $request)
    {
        $clientId = $request->contribuyente;
        $transactionId = $request->transaccion;
        $date = $request->fechaTx; // ddmmyyyy
        $time = $request->horaTx; // hh:mm
        $paymentType = $request->fPago; // EF - DE - CH
        $total = $request->valorpTx;
        $transactionStatus = $request->estadoTx;
        $emissionDetails = $request->emisiondet;

        $start = '07:00';
        $end = '19:00';

        // Validation
        // contribuyente
        $clientValidation = $this->clientValidation($clientId);
        if ($clientValidation) {
            return response()->json($clientValidation);
        }

        $client = User::find($clientId);

        // fechaTx
        $dateValidation = $this->dateValidation($date);
        if ($dateValidation) {
            return response()->json($dateValidation);
        }

        // HoraTx
        $timeValidation = $this->timeValidation($time, $start, $end);
        if ($timeValidation) {
            return response()->json($timeValidation);
        }
        // fPago
        $typeValidation = $this->typePaymentValidation($paymentType);
        if ($typeValidation) {
            return response()->json($typeValidation);
        }

        // estadoTx
        $transactionValidation = $this->transactionValidation($transactionStatus);
        if ($transactionValidation) {
            return response()->json($transactionValidation);
        }

        // emisiondet
        $sumDetails = array_sum(array_column($emissionDetails, 'total'));

        if ($sumDetails != $total) {
            return response()->json([
                'codigo_respuesta' => '04',
                'mensaje_respuesta' => 'Error al registrar el cobro'
            ]);
        }

        // exists collect obligations
        foreach ($emissionDetails as $detail) {
            $obligation = CollectObligation::whereHas('obligation', function ($query) use($clientId) {
                    $query->where('client_id', $clientId);
                })
                ->where('entry_order', $detail['emision'])
                ->first();

            if (!$obligation) {
                return response()->json([
                    'codigo_respuesta' => '04',
                    'mensaje_respuesta' => 'Error al registrar el cobro'
                ]);
            }

            if ($obligation->status != $detail['estadoe'] || $obligation->status != CollectObligation::ISSUED) {
                return response()->json([
                    'codigo_respuesta' => '04',
                    'mensaje_respuesta' => 'Error al registrar el cobro'
                ]);
            }
        }

        // passed validation

        //Register Transaction
        $transaction = Transaction::updateOrCreate(
            [
                'contribuyente' => $clientId,
                'transaccion' => $transactionId,
                'fechaTx' => $date,
                'horaTx' => $time,
                'fPago' => $paymentType,
                'valorpTx' => number_format($total, 2),
                'estadoTx' => Transaction::VERIFICATION,
                'codigo_respuesta' => '00',
                'mensaje_respuesta' => 'Proceso concluido exitosamente',
                'user_id' => $clientId
            ],
            [
                'fecConsiliacion' => now()->format('dmY')
            ]
        );

        $newEmissionDetails = [];

        foreach ($emissionDetails as $detail) {
            $obligation = CollectObligation::whereHas('obligation', function ($query) use($clientId) {
                    $query->where('client_id', $clientId);
                })
                ->where('entry_order', $detail['emision'])
                ->first();

            $obligation->update([
                'payment_date' => Carbon::createFromFormat("dmY H:i", $date.' '.$time),
                'status' => CollectObligation::COLLECTED
            ]);

            TransactionCollectObligation::updateOrCreate([
                'transaction_id' => $transaction->id,
                'collect_obligation_id' => $obligation->id
            ]);

            $newEmissionDetails[] = [
                'emision' => $obligation->entry_order,
                'estadoe' => CollectObligation::COLLECTED,
                'total' => number_format($obligation->total, 2)
            ];
        }

        $transaction->update([
            'emisiondet' => json_encode($newEmissionDetails)
        ]);

        return response()->json([
            'contribuyente' => $clientId,
            'transaccion' => $transactionId,
            'fechaTx' => $date,
            'horaTx' => $time,
            'fPago' => $paymentType,
            'valorpTx' => number_format($total, 2),
            'estadoTx' => $transaction->estadoTx,
            'codigo_respuesta' => '00',
            'mensaje_respuesta' => 'Proceso concluido exitosamente',
            'fecConsiliacion' => now()->format('dmY'),
            'emisiondet' => $newEmissionDetails
        ]);
    }

    public function reverse(Request $request)
    {
        $clientId = $request->contribuyente;
        $transactionId = $request->transaccion;
        $date = $request->fechaTx; // ddmmyyyy
        $total = $request->valorpTx;
        $transactionStatus = $request->estadoTx;

        // Validation
        // contribuyente
        $clientValidation = $this->clientValidation($clientId);
        if ($clientValidation) {
            return response()->json($clientValidation);
        }

        // fechaTx
        $dateValidation = $this->dateValidation($date);
        if ($dateValidation) {
            return response()->json($dateValidation);
        }

        // estadoTx
        $transactionValidation = $this->transactionValidation($transactionStatus);
        if ($transactionValidation) {
            return response()->json($transactionValidation);
        }

        // conciliated
        $transaction = Transaction::where('contribuyente', $clientId)
            ->where('transaccion', $transactionId)
            ->where('fechaTx', $date)
            ->where('valorpTx', $total)
            ->where('estadoTx', Transaction::PAID)
            ->first();

        if (!$transaction || $transaction->conciliated) {
            return response()->json([
                'codigo_respuesta' => '01',
                'mensaje_respuesta' => 'No se encontró la transacción para realizar el reverso o la transacción ya se encuentra conciliada'
            ]);
        }

        $transaction->collect_obligations()
            ->update([
                'payment_date' => NULL,
                'status' => CollectObligation::ISSUED
            ]);
        $transaction->update([
            'estadoTx' => Transaction::REVERSE
        ]);

        return response()->json([
            'contribuyente' => $clientId,
            'transaccion' => $transactionId,
            'fechaTx' => $date,
            'valorpTx' => number_format($total, 2),
            'estadoTx' => $transaction->estadoTx,
            'codigo_respuesta' => '00',
            'mensaje_respuesta' => 'PROCESO EXISTOSO'
        ]);
    }

    public function confirm(Request $request)
    {
        $clientId = $request->contribuyente;
        $transactionId = $request->transaccion;
        $date = $request->fechaTx; // ddmmyyyy
        $time = $request->horaTx; // hh:mm
        $paymentType = $request->fPago; // EF - DE - CH
        $total = $request->valorpTx;
        $transactionStatus = $request->estadoTx;

        // Validation
        // contribuyente
        $clientValidation = $this->clientValidation($clientId);
        if ($clientValidation) {
            return response()->json($clientValidation);
        }

        // fechaTx
        $dateValidation = $this->dateValidation($date);
        if ($dateValidation) {
            return response()->json($dateValidation);
        }

        // HoraTx
        $timeValidation = $this->timeValidation($time);
        if ($timeValidation) {
            return response()->json($timeValidation);
        }

        // fPago
        $typeValidation = $this->typePaymentValidation($paymentType);
        if ($typeValidation) {
            return response()->json($typeValidation);
        }

        // estadoTx
        $transactionValidation = $this->transactionValidation($transactionStatus);
        if ($transactionValidation) {
            return response()->json($transactionValidation);
        }

        // Exists transaction
        $transaction = Transaction::where('contribuyente', $clientId)
            ->where('transaccion', $transactionId)
            ->where('fechaTx', $date)
            ->where('horaTx', $time)
            ->where('fPago', $paymentType)
            ->where('valorpTx', $total)
            ->where('estadoTx', Transaction::VERIFICATION)
            ->first();

        if (!$transaction) {
            return response()->json([
                'codigo_respuesta' => '01',
                'mensaje_respuesta' => 'No existe'
            ]);
        }

        // passed validation
        $transaction->update([
            'estadoTx' => Transaction::PAID
        ]);

        return response()->json([
            'contribuyente' => $clientId,
            'transaccion' => $transactionId,
            'fechaTx' => $date,
            'horaTx' => $time,
            'fPago' => $paymentType,
            'valorpTx' => number_format($total, 2),
            'estadoTx' => $transaction->estadoTx,
            'codigo_respuesta' => '00',
            'mensaje_respuesta' => 'Proceso concluido exitosamente"'
        ]);
    }

    // Validation
    protected function clientValidation($id)
    {
        $client = User::find($id);

        if (!$client) {
            return [
                'codigo_respuesta' => '01',
                'mensaje_respuesta' => 'No existe'
            ];
        }

        return false;
    }

    protected function dateValidation($date)
    {
        if (strlen($date) != 8) {
            return [
                'codigo_respuesta' => '10',
                'mensaje_respuesta' => 'Error en el formato de las fecha de transacción o la fecha no puede ser de días anteriores.'
            ];
        }

        $d = Carbon::createFromFormat("!dmY", $date)->format('dmY');

        if ($d != $date) {
            return [
                'codigo_respuesta' => '10',
                'mensaje_respuesta' => 'Error en el formato de las fecha de transacción o la fecha no puede ser de días anteriores.'
            ];
        }

        return false;
    }

    protected function transactionValidation($status)
    {
        $transactions = [
            Transaction::PAID,
            Transaction::CANCELED,
            Transaction::REVERSE,
            Transaction::VERIFICATION
        ];

        $transactionCondition = in_array($status, $transactions);

        if (!$transactionCondition) {
            return [
                'codigo_respuesta' => '12',
                'mensaje_respuesta' => 'Error en el formato de los estados de transacción'
            ];
        }

        return false;
    }

    protected function timeValidation($time, $start = null, $end = null)
    {
        if (strlen($time) != 5) {
            return [
                'codigo_respuesta' => '11',
                'mensaje_respuesta' => 'Error en el formato de las horas de transacción'
            ];
        }

        $t = Carbon::createFromFormat("!H:i", $time)->format('H:i');

        if ($t != $time) {
            return [
                'codigo_respuesta' => '11',
                'mensaje_respuesta' => 'Error en el formato de las horas de transacción'
            ];
        }

        if ($start && $end && ($time < $start || $time > $end)) {
            return [
                'codigo_respuesta' => '11',
                'mensaje_respuesta' => 'Servicio de pagos o reversos solo funciona de 07:00 a 19:00'
            ];
        }

        return false;
    }

    protected function typePaymentValidation($type)
    {
        $payments = [
            Payment::CASH,
            Payment::PLUX,
            Payment::BANK_CHECK
        ];

        $paymentTypeCondition = in_array($type, $payments);

        if (!$paymentTypeCondition) {
            return [
                'codigo_respuesta' => '12',
                'mensaje_respuesta' => 'Error en el formato en las formas de pago'
            ];
        }

        return false;
    }
}
