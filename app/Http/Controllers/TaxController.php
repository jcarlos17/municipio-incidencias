<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TaxController extends Controller
{
    public function payable(Request $request)
    {
        $documentInput = $request->documentInput;
        $pendingValues = [];
        $format = $request->input('format');

        if ($documentInput) {
            $waterResponse = Http::get('http://186.46.195.20/gadm_unica/gadm_agua.php', [
                'cdl' => $documentInput
            ]);
            $waterJson = $waterResponse->getBody()->getContents();
            if ($waterJson) {
                $waterArray = json_decode($waterJson);
                if ($waterArray) {
                    foreach ($waterArray as $water) {
                        $water->rubro = 'Agua';
                        $pendingValues[] = $water;
                    }
                }
            }

            $localResponse = Http::get('http://186.46.195.20/gadm_unica/gadm_locales.php', [
                'cdl' => $documentInput
            ]);
            $localJson = $localResponse->getBody()->getContents();
            if ($localJson) {
                $localArray = json_decode($localJson);
                if ($localArray) {
                    foreach ($localArray as $local) {
                        $local->rubro = 'Locales';
                        $pendingValues[] = $local;
                    }
                }
            }

            $ruralResponse = Http::get('http://186.46.195.20/gadm_unica/gadm_urru.php', [
                'cdl' => $documentInput
            ]);
            $ruralJson = $ruralResponse->getBody()->getContents();
            if ($ruralJson) {
                $ruralArray = json_decode($ruralJson);
                if ($ruralArray) {
                    foreach ($ruralArray as $rural) {
                        $rural->rubro = 'Urbano Rural';
                        $pendingValues[] = $rural;
                    }
                }
            }

            $betterResponse = Http::get('http://186.46.195.20/gadm_unica/gadm_mejoras.php', [
                'cdl' => $documentInput
            ]);
            $betterJson = $betterResponse->getBody()->getContents();
            if ($betterJson) {
                $betterArray = json_decode($betterJson);
                if ($betterArray) {
                    foreach ($betterArray as $better) {
                        $better->rubro = 'Mejoras';
                        $pendingValues[] = $better;
                    }
                }
            }

            $response = Http::get('http://186.46.195.20/gadm_unica/gadm_alcantarillado.php', [
                'cdl' => $documentInput
            ]);
            $sewerJson = $response->getBody()->getContents();
            if ($sewerJson) {
                $sewerArray = json_decode($sewerJson);
                if ($sewerArray) {
                    foreach ($sewerArray as $sewer) {
                        $sewer->rubro = 'Alcantarillado';
                        $pendingValues[] = $sewer;
                    }
                }
            }

            $royaltyResponse = Http::get('http://186.46.195.20/gadm_unica/gadm_regalias.php', [
                'cdl' => $documentInput
            ]);
            $royaltyJson = $royaltyResponse->getBody()->getContents();
            if ($royaltyJson) {
                $royaltyArray = json_decode($royaltyJson);
                if ($royaltyArray) {
                    foreach ($royaltyArray as $royalty) {
                        $royalty->rubro = 'Regalías';
                        $pendingValues[] = $royalty;
                    }
                }
            }

            $cemeteryResponse = Http::get('http://186.46.195.20/gadm_unica/gadm_cementerio.php', [
                'cdl' => $documentInput
            ]);

            $cemeteryJson = $cemeteryResponse->getBody()->getContents();
            if ($cemeteryJson) {
                $cemeteryArray = json_decode($cemeteryJson);
                if ($cemeteryArray) {
                    foreach ($cemeteryArray as $cemetery) {
                        $cemetery->rubro = 'Cementerio';
                        $pendingValues[] = $cemetery;
                    }
                }
            }

        }
        if ($format === 'json') {
            return json_encode($pendingValues);
        }

        return view('tax.payable', compact('documentInput', 'pendingValues'));
    }
}
