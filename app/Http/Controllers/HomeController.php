<?php

namespace App\Http\Controllers;

use App\Models\Incident;
use App\Models\DepartmentUser;
use App\Models\Process;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = auth()->user();
        $selected_department_id = $user->selected_department_id;

        if($user->is(User::ADMIN)) {
            return redirect('incidencias');
        }

        if ($user->is(User::SUPPORT)) {
            return redirect('assigned-incidents');
        }

        $incidents_client = [];
        $processId = $request->process_id;
        $startDate = $request->start_date;
        $endingDate = $request->ending_date;

        if ($selected_department_id) {
            $processes = Process::where('department_id', $selected_department_id)->get([
                'id', 'name'
            ]);

            $query = Incident::where('client_id', $user->id)
                ->where('department_id', $selected_department_id);

            if ($processId) {
                $query = $query->where('process_id', $processId);
            }

            if ($startDate && $endingDate) {
                $query = $query->whereBetween('created_at', [$startDate, $endingDate]);
            }

            $incidents_client = $query->paginate(10);
        }

        return view('home', compact(
            'incidents_client', 'processes', 'processes', 'processId', 'startDate', 'endingDate'
        ));
    }

    public function selectDepartment($id)
    {
        // Validar que el usuario esté asociado con el departamento
        $user = auth()->user();
        $user->selected_department_id = $id;
        $user->save();

        return back();
    }
}
