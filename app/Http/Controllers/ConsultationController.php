<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Incident;
use App\Models\Process;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ConsultationController extends Controller
{
    public function procedure(Request $request)
    {
        $procedureInput = $request->procedureInput;
        $incident = [];
        $format = $request->input('format');

        $array = explode("-", $procedureInput);

        if (count($array) == 2) {
            $dateString = $array[0];
            $id = $array[1];

            if (strlen($dateString) == 8) {
                $year = substr($dateString, 0, 4);
                $month = substr($dateString, 4, 2);
                $day = substr($dateString, 6, 2);
                $date = "$year-$month-$day";

                $incident = Incident::where('id', $id)
                    ->where('created_at', 'like', $date.'%')
                    ->first();
            }
        }

        $notification = !$incident ? 'No se encontraron resultados' : [];

        if ($format === 'json') {

            if ($incident) {
//                $histories = [];
//                foreach ($incident->histories as $history) {
//                    $histories[] = [
//                        'description' => $history->description,
//                        'created_at' => $history->created_at
//                    ];
//                }

                return json_encode([
                    'code' => $incident->code,
                    'department_name' => $incident->department->name,
                    'process_name' => $incident->process_name,
                    'created_at' => $incident->created_at,
                    'support_name' => $incident->support_name,
                    'level_name' => $incident->level->name,
                    'state' => $incident->state,
                    'severity_full' => $incident->severity_full,
                    'client_name' => $incident->client->name,
                    'description' => $incident->description,
//                    'histories' => $histories
                ]);
            } else {
                return json_encode(null);
            }
        }

        return view('consultation.procedure', compact('procedureInput', 'incident', 'notification'));
    }

    public function guide(Request $request)
    {
        $requestName = $request->requestName;
        $departmentId = $request->departmentId;

        $departments = Department::all(['id', 'name']);

        $query = Process::query();

        if ($departmentId) {
            $query = $query->where('department_id', $departmentId);
        }

        if ($requestName) {
            $query = $query->where('name', 'like', "$requestName%");
        }

        $processes = $query->paginate(10);

        return view('consultation.guide', compact(
            'departments', 'requestName', 'departmentId', 'processes'
        ));
    }

    public function download(Process $process)
    {
        return Storage::disk('public')->download('pdf-processes/'.$process->file);
    }

    public function requirementPrint(Process $process)
    {
        $pdf = PDF::loadView('consultation.requirements-pdf', ['process' => $process])
            ->setPaper('a4');

        return $pdf->stream('requisitos-'.$process->id.'.pdf');
    }
}
