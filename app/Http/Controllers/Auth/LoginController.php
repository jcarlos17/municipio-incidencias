<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'document';
    }

    public function redirectTo()
    {
        if (auth()->user()->is(User::CLIENT)) {
            return route('welcome');
        }

        return route('home');
    }

    protected function authenticated()
    {
        $user = auth()->user();

        if (! $user->selected_department_id) {
            if (!$user->is(User::SUPPORT)) {
                $user->selected_department_id = Department::first()->id;

            } else { // is_support
                // y si el usuario de soporte no está asociado a ningún proyecto?
                $first_department = $user->departments->first();

                if ($first_department)
                    $user->selected_department_id = $first_department->id;
            }

            $user->save();
        }
    }
}
