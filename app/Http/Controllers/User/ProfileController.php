<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    public function postImage(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image'
        ]);

        $image = $request->file('image');

        $user = Auth::user();
        $extension = $image->getClientOriginalExtension();
        $fileName = $user->id . '.' . $extension;

        // Store image
        $stored = Storage::disk('avatars')->put($fileName, File::get($image));

        if (!$stored) {
            return [
                'success' => false
            ];
        }

        $user->image = $fileName;
        $user->save();

        $publicPath = public_path('images/users/'.$fileName);

        // Resize to 128x128
        Image::make($publicPath)->fit(128, 128)->save();

        $data['success'] = true;
        $data['file_name'] = $fileName;
        return $data;
    }
}
