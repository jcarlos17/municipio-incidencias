<?php

namespace App\Http\Controllers\Admin;

use App\Models\Department;
use App\Models\DepartmentUser;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $inputSearch = $request->inputSearch;

        $query = User::where('role', User::SUPPORT);

        if ($inputSearch) {
            $query = $query->where('name', 'like', "%$inputSearch%")
                ->orWhere('document', 'like', "%$inputSearch%");
        }

    	$users = $query
            ->orderBy('name')
            ->paginate(10);

    	return view('admin.users.index')->with(compact('users', 'inputSearch'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
    	$rules = [
    		'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'document' => 'required|max:255|unique:users',
            'password' => 'required|min:6'
    	];
    	$messages = [
    		'name.required' => 'Es necesario ingresar el nombre del usuario.',
    		'name.max' => 'El nombre es demasiado extenso.',
            'email.required' => 'Es necesario ingresar el e-mail del usuario.',
    		'email.email' => 'El e-mail ingresado no es válido.',
    		'email.max' => 'El e-mail es demasiado extenso.',
            'document.required' => 'Es indispensable ingresar la cédula del usuario.',
            'document.max' => 'La cédula es demasiado extenso.',
            'document.unique' => 'Este cédula ya se encuentra en uso.',
    		'password.required' => 'Olvidó ingresar una contraseña.',
    		'password.min' => 'La contraseña debe presentar al menos 6 caracteres.'
    	];
    	$this->validate($request, $rules, $messages);

    	$user = new User();
        $user->name = $request->input('name');
        $user->cellphone = $request->input('cellphone');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->document = $request->input('document');
        $user->password = bcrypt($request->input('password'));
    	$user->role = User::SUPPORT;
        $user->incident_cashier = $request->has('incident_cashier');
        $user->incident_obligation = $request->has('incident_obligation');
    	$user->save();

    	return redirect('users')->with('notification', 'Usuario registrado exitosamente.');
    }

    public function edit($id)
    {
    	$user = User::find($id);
        $departments = Department::all();

        $departments_user = DepartmentUser::where('user_id', $user->id)
            ->where('department_id', auth()->user()->selected_department_id)
            ->get();

    	return view('admin.users.edit')->with(compact(
    	    'user', 'departments', 'departments_user'
        ));
    }

    public function update($id, Request $request)
    {
    	$rules = [
    		'name' => 'required|max:255',
            'email' => 'email|max:255',
            'password' => 'nullable|min:6'
    	];
    	$messages = [
    		'name.required' => 'Es necesario ingresar el nombre del usuario.',
    		'name.max' => 'El nombre es demasiado extenso.',
            'email.email' => 'El e-mail ingresado no es válido.',
            'email.max' => 'El e-mail es demasiado extenso.',
    		'password.min' => 'La contraseña debe presentar al menos 6 caracteres.'
    	];
    	$this->validate($request, $rules, $messages);

    	$user = User::find($id);
        $user->name = $request->input('name');
        $user->cellphone = $request->input('cellphone');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
    	$password = $request->input('password');
        $user->incident_cashier = $request->has('incident_cashier');
        $user->incident_obligation = $request->has('incident_obligation');
    	if ($password) {
            $user->password = bcrypt($password);
        }

    	$user->save();

    	return redirect('users')->with('notification', 'Usuario modificado exitosamente.');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();

        return back()->with('notification', 'El usuario se ha dado de baja correctamente.');
    }
}
