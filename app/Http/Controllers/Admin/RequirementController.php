<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RequirementRequest;
use App\Models\Requirement;

class RequirementController extends Controller
{
    public function store(RequirementRequest $request)
    {
        Requirement::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'type' => $request->input('type'),
            'form_type' => $request->input('form_type'),
            'process_id' => $request->input('process_id'),
            'department_id' => $request->input('department_id'),
        ]);

        return back();
    }

    public function update(RequirementRequest $request)
    {
        $requirement_id = $request->input('requirement_id');

        $requirement = Requirement::findOrFail($requirement_id);

        $type = $request->input('type');
        $formType = $type ? $request->input('form_type') : NULL;

        $requirement->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'type' => $type,
            'form_type' => $formType
        ]);

        return back();
    }

    public function delete(Requirement $requirement)
    {
        $requirement->delete();

        return back();
    }
}
