<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ItemRequest;
use App\Models\Admin\Obligation;
use App\Models\Admin\Income;
use App\Models\Admin\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function index(Income $income, Request $request)
    {
        $inputSearch = $request->input('inputSearch');

        $query = $income->items();

        if ($inputSearch) {
            $query = $query->where('name', 'like', '%'.$inputSearch.'%');
        }
        $items = $query->paginate(10);

        return view('admin.items.index', compact(
            'income', 'items', 'inputSearch'
        ));
    }

    public function create(Income $income)
    {
        $obligations = $income->obligations()->orderBy('code')->get(['id', 'code']);

        return view('admin.items.create', compact(
             'income', 'obligations'
        ));
    }

    public function store(Income $income, ItemRequest $request)
    {
        $data = $request->except('_token');
        $data['income_id'] = $income->id;

        Item::create($data);

        return redirect('incomes/'.$income->id.'/items')->with('notification', 'Item registrado exitosamente.');
    }

    public function edit(Item $item)
    {
        return view('admin.items.edit', compact(
            'item'
        ));
    }

    public function update(Item $item, ItemRequest $request)
    {
        $item->update($request->all());

        return redirect('incomes/'.$item->income_id.'/items')->with('notification', 'Item modificado exitosamente.');
    }

    public function delete(Item $item)
    {
        $item->delete();

        return back()->with('notification', 'Item eliminado exitosamente.');
    }
}
