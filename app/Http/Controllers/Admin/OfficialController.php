<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OfficialRequest;
use App\Models\Department;
use App\Models\Official;
use Illuminate\Http\Request;

class OfficialController extends Controller
{
    public function index()
    {
        $officials = Official::paginate(10);

        return view('admin.officials.index', compact('officials'));
    }

    public function create()
    {
        $departments = Department::get(['id', 'name']);

        return view('admin.officials.create', compact('departments'));
    }

    public function store(OfficialRequest $request)
    {
        Official::create($request->all());

        return redirect('officials')->with('notification', 'El funcionario se ha registrado correctamente.');
    }

    public function edit(Official $official)
    {
        $departments = Department::get(['id', 'name']);

        return view('admin.officials.edit', compact(
            'official', 'departments'
        ));
    }

    public function update(Official $official, OfficialRequest $request)
    {
        $official->update($request->all());

        return redirect('officials')->with('notification', 'El funcionario se ha actualizado correctamente.');
    }

    public function delete(Official $official)
    {
        $official->delete();

        return back()->with('notification', 'El funcionario se ha eliminado correctamente.');
    }
}
