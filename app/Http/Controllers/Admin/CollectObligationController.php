<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Obligation;
use App\Models\CollectObligation;
use App\Models\CollectObligationDetail;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CollectObligationController extends Controller
{
    public function index(Request $request)
    {
        $optionSearch = $request->input('optionSearch');
        $inputSearch = $request->input('inputSearch');

        $query = CollectObligation::whereNull('payment_date');

        if ($optionSearch == 'OI') {
            $query = $query->where('entry_order', 'like', '%'.$inputSearch.'%');
        } elseif ($optionSearch == 'T') {
            $query = $query->whereHas('obligation', function ($query) use ($inputSearch) {
                $query->whereHas('income', function ($query) use ($inputSearch) {
                    $query->where('name', 'like', "%$inputSearch%");
                });
            });
        } elseif ($optionSearch == 'FE') {
            $query = $query->where('issue_date', $inputSearch);
        } elseif ($optionSearch == 'U') {
            $query = $query->whereHas('user', function ($query) use ($inputSearch) {
                $query->where('name', 'like', "%$inputSearch%");
            });
        }

        $collectObligations = $query->orderByDesc('id')->paginate(10);

        return view('admin.collect_obligations.index', compact(
            'collectObligations', 'inputSearch', 'optionSearch'
        ));
    }

    public function create()
    {
        $futureId = CollectObligation::whereDate('created_at', now())->count() + 1;

        $clientIds = Obligation::distinct('client_id')
            ->pluck('client_id')
            ->toArray();

        $clients = User::whereIn('id', $clientIds)->get(['id', 'name']);

        return view('admin.collect_obligations.create', compact(
            'clients', 'futureId'
        ));
    }

    public function store(Request $request)
    {
        $incomeId = $request->income_id;
        $clientId = $request->client_id;
        $values = $request->values;

        $obligation = Obligation::where('income_id', $incomeId)
            ->where('client_id', $clientId)
            ->first(['id']);

        $issueDate = new Carbon($request->issue_date);

        $futureId = CollectObligation::whereDate('created_at', $issueDate)->count() + 1;
        $code = $issueDate->format('Ymd').'-'.$futureId;

        $collectObligation = CollectObligation::create([
            'issue_date' => $request->issue_date,
            'entry_order' => $code,
            'obligation_id' => $obligation->id,
            'period' => $issueDate->format('Ym'),
            'year' => $issueDate->format('Y'),
            'total' => array_sum($values),
            'income_id' => $incomeId,
            'user_id' => auth()->id(),
        ]);

        $itemIds = $request->item_ids;

        foreach ($itemIds as $key => $itemId) {
            CollectObligationDetail::create([
                'value' => $values[$key],
                'item_id' => $itemId,
                'income_id' => $incomeId,
                'collect_obligation_id' => $collectObligation->id,
            ]);
        }

        return redirect('collect_obligations')->with('notification', 'Obligación por recaudar registrado exitosamente.');
    }

    public function edit(CollectObligation $collectObligation)
    {
        $clientIds = Obligation::distinct('client_id')->pluck('client_id')->toArray();

        $clients = User::whereIn('id', $clientIds)->get(['id', 'name']);

        return view('admin.collect_obligations.edit', compact(
            'collectObligation','clients'
        ));
    }

    public function update(CollectObligation $collectObligation, Request $request)
    {
        $incomeId = $request->income_id;
        $clientId = $request->client_id;
        $obligation = Obligation::where('income_id', $incomeId)
            ->where('client_id', $clientId)
            ->first(['id']);

        $collectObligation->update([
            'obligation_id' => $obligation->id,
            'income_id' => $incomeId,
            'user_id' => auth()->id(),
        ]);

        $itemIds = $request->item_ids;
        $values = $request->values;

        if ($itemIds) {
            $collectObligation->details()
                ->whereNotIn('item_id', $itemIds)
                ->delete();

            foreach ($itemIds as $key => $itemId) {
                CollectObligationDetail::updateOrCreate(
                    [
                        'item_id' => $itemId,
                        'collect_obligation_id' => $collectObligation->id,
                        'income_id' => $incomeId,
                    ],
                    ['value' => $values[$key]]
                );
            }
        } else {
            $collectObligation->details()
                ->delete();
        }

        return redirect('collect_obligations')->with('notification', 'Obligación por recaudar modificado exitosamente.');
    }

    public function delete(CollectObligation $collectObligation)
    {
        $collectObligation->update([
            'status' => CollectObligation::DISCHARGED
        ]);

        $collectObligation->delete();

        return back()->with('notification', 'Obligación por recaudar eliminado exitosamente.');
    }

    public function show(CollectObligation $collectObligation)
    {
        return view('admin.collect_obligations.show', compact(
            'collectObligation'
        ));
    }

    public function download(Request $request)
    {
        $date = $request->fecConsiliacion;
        $query = Transaction::where('fecConsiliacion', $date);
        $transactions = $query->get();
        $transCount = $query->count();
        $transTotal = number_format($query->sum('valorpTx'), 2);

        $collector = auth()->user();

        $txt = "$date;$collector->document;$transCount;$transTotal\n";

        foreach ($transactions as $transaction) {
            $txt .= $transaction->contribuyente.';';
//            $txt .= $transaction->.';';
            $txt .= $transaction->transaccion.';';
            $txt .= $transaction->fechaTx.';';
            $txt .= $transaction->horaTx.';';
            $txt .= $transaction->valorpTx.';';
            $txt .= "\n";
        }

        $name = 'REC'.$collector->document.'_'.$date;
        //offer the content of txt as a download (name.txt)
        return response($txt)
            ->withHeaders([
                'Content-Type' => 'text/plain',
                'Cache-Control' => 'no-store, no-cache',
                'Content-Disposition' => 'attachment; filename="'.$name.'.txt',
            ]);
    }
}
