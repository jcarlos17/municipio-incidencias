<?php

namespace App\Http\Controllers\Admin;

use App\Models\DepartmentUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentUserController extends Controller
{
    public function store(Request $request)
    {
        $rules = [
            'department_id' => 'exists:departments,id',
            'process_id' => 'exists:processes,id',
            'user_id' => 'exists:users,id',
            'level_id' => 'exists:levels,id'
        ];
        $this->validate($request, $rules);
        // Seguridad adicional:
    	// Verificar que el nivel pertenezca al departamento.

    	$departmentId = $request->input('department_id');
        $processId = $request->input('process_id');
        $levelId = $request->input('level_id');
        $userId = $request->input('user_id');

		$departmentLevelUser = DepartmentUser::where('department_id', $departmentId)
            ->where('process_id', $processId)
            ->where('user_id', $userId)
            ->where('level_id', $levelId)
            ->exists();

		if ($departmentLevelUser)
			return back()->with('notification', 'El usuario ya pertenece a este nivel del departamento.');

    	$department_user = new DepartmentUser();
    	$department_user->department_id = $departmentId;
        $department_user->process_id = $processId;
    	$department_user->user_id = $userId;
    	$department_user->level_id = $levelId;
    	$department_user->save();

    	return back();
    }

    public function delete($id)
    {
    	DepartmentUser::find($id)->delete();

    	return back();
    }
}
