<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ObligationRequest;
use App\Models\Admin\Obligation;
use App\Models\Admin\Income;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ObligationController extends Controller
{
    public function index(Request $request)
    {
        $inputSearch = $request->input('inputSearch');

        $query = Obligation::query();

        if ($inputSearch) {
            $query = $query->whereHas('client', function ($query) use ($inputSearch) {
                    $query->where('name', 'like', '%'.$inputSearch.'%')
                        ->orWhere('document', 'like', '%'.$inputSearch.'%');
                })
                ->orWhereHas('income', function ($query) use ($inputSearch) {
                    $query->where('name', 'like', '%'.$inputSearch.'%');
                });
        }
        $obligations = $query->orderByDesc('id')->paginate(10);

        return view('admin.obligations.index', compact(
            'obligations', 'inputSearch'
        ));
    }

    public function create()
    {
        $incomes = Income::orderBy('name')->get(['id', 'name']);

        $correlative = Obligation::whereDate('created_at', today())
            ->count();

        $code = now()->format('Ymd').'-'.($correlative+1);

        return view('admin.obligations.create', compact(
            'incomes', 'code'
        ));
    }

    public function store(ObligationRequest $request)
    {
        Obligation::create($request->all());

        return redirect('obligations')->with('notification', 'Obligación registrada exitosamente.');
    }

    public function edit(Obligation $obligation)
    {
        $incomes = Income::orderBy('name')->get(['id', 'name']);
        $clients = User::where('role', User::CLIENT)
            ->orderBy('name')
            ->get(['id', 'name']);

        return view('admin.obligations.edit', compact(
            'obligation', 'incomes', 'clients'
        ));
    }

    public function update(Obligation $obligation, ObligationRequest $request)
    {
        $obligation->update($request->all());

        return redirect('obligations')->with('notification', 'Obligación modificada exitosamente.');
    }

    public function delete(Obligation $obligation)
    {
        if ($obligation->collect_obligations()->exists()) {
            return back()->with('notification', 'No se puede eliminar la obligación porque tiene obligaciones recaudadas registrados');
        }

        $obligation->delete();

        return back()->with('notification', 'Obligación eliminada exitosamente.');
    }
}
