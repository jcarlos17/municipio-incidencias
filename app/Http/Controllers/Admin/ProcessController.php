<?php

namespace App\Http\Controllers\Admin;

use App\Models\Department;
use App\Models\Process;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProcessController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $departmentId = $user->selected_department_id;

        $processes = Process::where('department_id', $departmentId)
            ->paginate(10);

        return view('admin.processes.index', compact('processes'));
    }

    public function create()
    {
        $departments = Department::all(['id', 'name']);

        return view('admin.processes.create', compact('departments'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required',
            'department_id' => 'required'
    	], [
    		'name.required' => 'Es necesario ingresar un nombre para el proceso.',
            'department_id.required' => 'Es necesario seleccionar un departamento'
    	]);

    	$process = Process::create([
            'name' => $request->name,
            'department_id' => $request->department_id
        ]);

        // files
        $file = $request->file('file');

        if ($file) {
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('pdf-processes/'.$name, File::get($file));

            $process->update([
                'file' => $name,
            ]);
        }

        return redirect('processes')->with('notification', 'Proceso registrado exitosamente.');
    }

    public function edit(Process $process)
    {
        $departments = Department::all(['id', 'name']);
        $levels = $process->levels;
        $requirements = $process->requirements;

        return view('admin.processes.edit', compact(
            'process', 'departments', 'levels', 'requirements'
        ));
    }

    public function update(Request $request, Process $process)
    {
        $this->validate($request, [
            'name' => 'required',
            'department_id' => 'required'
        ], [
            'name.required' => 'Es necesario ingresar un nombre para el proceso.',
            'department_id.required' => 'Es necesario seleccionar un departamento'
        ]);

        $process->update([
            'name' => $request->name,
            'department_id' => $request->department_id
        ]);

        // files
        $file = $request->file('file');

        if ($file) {
            $name = $file->getClientOriginalName();
            Storage::disk('public')->put('pdf-processes/'.$name, File::get($file));

            $process->update([
                'file' => $name,
            ]);
        }

        return redirect('processes')->with('notification', 'Proceso modificado exitosamente.');
    }

    public function delete(Process $process)
    {
        $process->delete();

        return back();
    }

    public function show(Process $process)
    {
        $levels = $process->levels;

        $sumTotalHours = number_format($process->levels()
            ->sum(DB::raw("days*24 + hours + minutes/60")), 2);

        return view('admin.processes.show', compact(
            'process', 'levels', 'sumTotalHours'
        ));
    }
}
