<?php

namespace App\Http\Controllers\Admin;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    public function index()
    {
        $departments = Department::withTrashed()->paginate(10);

    	return view('admin.departments.index', compact('departments'));
    }

    public function create()
    {
        return view('admin.departments.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, Department::$rules, Department::$messages);

        Department::create($request->all());

        return back()->with('notification', 'El departamento se ha registrado correctamente.');
    }

	public function edit(Department $department)
    {
        return view('admin.departments.edit', compact(
            'department'
        ));
    }

    public function update(Department $department, Request $request)
    {
        $this->validate($request, Department::$rules, Department::$messages);

        $department->update($request->all());

        return back()->with('notification', 'El departamento se ha actualizado correctamente.');
    }

    public function delete(Department $department)
    {
        $department->delete();

        return back()->with('notification', 'El departamento se ha deshabilitado correctamente.');
    }
    public function restore($id)
    {
        Department::withTrashed()->findOrFail($id)->restore();

        return back()->with('notification', 'El departamento se ha habilitado correctamente.');
    }

}
