<?php

namespace App\Http\Controllers\Admin;

use App\Models\Incident;
use App\Models\Level;
use App\Models\Process;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LevelController extends Controller
{
    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required'
    	], [
    		'name.required' => 'Es necesario ingresar un nombre para el nivel.'
    	]);

    	$processId = $request->input('process_id');

    	$level = new Level();
    	$level->name = $request->input('name');
        $level->days = $request->input('days');
        $level->hours = $request->input('hours');
        $level->minutes = $request->input('minutes');
        $level->process_id = $processId;
        $level->department_id = $request->input('department_id');
        $level->save();

        $process = Process::findOrFail($processId);

        $process->update(['levels_count' => $process->levels_count + 1]);

    	return back();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ], [
            'name.required' => 'Es necesario ingresar un nombre para el nivel.'
        ]);


        $level_id = $request->input('level_id');

        $level = Level::find($level_id);
        $level->name = $request->input('name');
        $level->days = $request->input('days');
        $level->hours = $request->input('hours');
        $level->minutes = $request->input('minutes');
        $level->save();

        return back();
    }

    public function delete($id)
    {
        if (Incident::where('level_id', $id)->exists()) {
            return back()->with('notification', 'El nivel no se puede eliminar porque está en uso.');
        }

        $level = Level::findOrFail($id);

        $process = $level->process;
        $process->update(['levels_count' => $process->levels_count - 1]);

        $level->delete();

        return back();
    }
}
