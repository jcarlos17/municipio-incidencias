<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\IncomeRequest;
use App\Models\Admin\Income;
use App\Models\Admin\IncomeGroup;
use App\Models\Process;
use Illuminate\Http\Request;

class IncomeController extends Controller
{
    public function index(Request $request)
    {
        $inputSearch = $request->input('inputSearch');

        $query = Income::query();

        if ($inputSearch) {
            $query = $query->where('name', 'like', '%'.$inputSearch.'%');
        }
        $incomes = $query->paginate(10);

        return view('admin.incomes.index', compact(
            'incomes', 'inputSearch'
        ));
    }

    public function create()
    {
        $groups = IncomeGroup::get(['id', 'name']);
        $processes = Process::orderBy('name')->get(['id', 'name']);

        return view('admin.incomes.create', compact(
            'groups', 'processes'
        ));
    }

    public function store(IncomeRequest $request)
    {
        Income::create($request->all());

        return redirect('incomes')->with('notification', 'Ingreso registrado exitosamente.');
    }

    public function edit(Income $income)
    {
        $groups = IncomeGroup::get(['id', 'name']);
        $processes = Process::orderBy('name')->get(['id', 'name']);

        return view('admin.incomes.edit', compact(
            'income', 'groups', 'processes'
        ));
    }

    public function update(Income $income, IncomeRequest $request)
    {
        $income->update($request->all());

        return redirect('incomes')->with('notification', 'Ingreso modificado exitosamente.');
    }

    public function delete(Income $income)
    {
        if ($income->obligations()->exists()) {
            return back()->with('notification', 'No se puede eliminar el ingreso porque tiene obligaciones registrados');
        }
        $income->delete();

        return back()->with('notification', 'Ingreso eliminado exitosamente.');
    }
}
