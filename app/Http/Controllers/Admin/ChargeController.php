<?php

namespace App\Http\Controllers\Admin;

use App\Models\CollectObligation;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Http;

class ChargeController extends Controller
{
    public function index(Request $request)
    {
        $inputSearch = $request->input('inputSearch');

        $query = CollectObligation::whereNotNull('payment_date');

        if ($inputSearch) {
            $query = $query->whereHas('obligation', function ($query) use ($inputSearch) {
                $query->whereHas('client', function ($query) use ($inputSearch) {
                    $query->where('name', 'like', "%$inputSearch%")
                        ->orWhere('document', 'like', "%$inputSearch%");
                });
            });
        }

        $collectObligations = $query->paginate(10);

        return view('admin.charges.index', compact('collectObligations', 'inputSearch'));
    }

    public function create()
    {
        if (!Auth::user()->is(User::CASHIER)) {
            return redirect('charges');
        }
        return view('admin.charges.create');
    }

    public function store(Request $request)
    {
        $collectObligationId = $request->collect_obligation_id;

        $collectObligation = CollectObligation::find($collectObligationId);

        $collectObligation->update([
            'payment_date' => now(),
            'cashier_id' => auth()->id(),
            'status' => CollectObligation::COLLECTED
        ]);

        if (!$request->token) {
            $collectObligation->update([
                'payment_type' => $request->payment_type
            ]);

            return redirect('charges')->with('notification', 'Se realizó el cobro correctamente');
        }

        $code = $this->getCode();
        $url = 'https://apipre.pagoplux.com/transv1/transaction/validationTokenDateResource';

        $response = Http::withHeaders([
            'Content-type' => 'application/json',
            'X-Second' => 'bar'
        ])->withToken($code)
            ->post($url, [
                'token' => $request->token,
                'amount' => $request->amount,
                'date' => $request->date
            ]);

        $body = $response->body();
        $decode = json_decode($body);

        $decodeResponse = json_decode($request->response);
        $detail = $decodeResponse->detail;

        $payment = Payment::create([
            'id_transaccion' => $detail->id_transaccion,
            'token' => $detail->token,
            'amount' => $detail->amount,
            'cardType' => $detail->cardType,
            'cardIssuer' => $detail->cardIssuer,
            'cardInfo' => $detail->cardInfo,
            'clientID' => $detail->clientID,
            'clientName' => $detail->clientName,
            'state' => $detail->state,
            'fecha' => $detail->fecha,
            'acquirer' => $detail->acquirer,
            'deferred' => $detail->deferred,
            'interests' => $detail->interests,
            'interestValue' => $detail->interestValue,
            'amountWoTaxes' => $detail->amountWoTaxes,
            'amountWTaxes' => $detail->amountWTaxes,
            'taxesValue' => $detail->taxesValue,
            'tipoPago' => $detail->tipoPago,
            'response' => $request->response,
            'collect_obligation_id' => $collectObligationId
        ]);

        $collectObligation->update([
            'payment_type' => Payment::PLUX
        ]);

        // success payment
        if (!empty($decode->status) && $decode->status === 'succeeded') {
            $payment->update([
                'succeeded' => true
            ]);
        }

        return redirect('charges')->with('notification', 'Se realizó el cobro correctamente');
    }

    protected  function getCode()
    {
        $secretKey = env('SECRET_KEY_PLUX');
        $length = rand()/getrandmax() * mb_strlen($secretKey);
        $string = '';
        while (mb_strlen($string) < $length) {
            $string .= substr($secretKey, intval((rand()/getrandmax()*$length)), 1);
        }

        $number = microtime(1)*1000 * 30;

        return base64_encode($string . 'PPX_' . $secretKey . 'PPX_' . $number . 'AWS');
    }

    public function show(CollectObligation $collectObligation)
    {
        $obligation = $collectObligation->obligation;

        return view('admin.charges.show', compact(
            'collectObligation', 'obligation'
        ));
    }

    public function reverse(CollectObligation $collectObligation)
    {
        $collectObligation->update([
            'payment_date' => null,
            'cashier_id' => null
        ]);

        return redirect('charges')->with('notification', 'Se realizó el reverso del cobro correctamente');
    }
}
