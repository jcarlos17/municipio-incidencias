<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Incident;
use Illuminate\Http\Request;

class BoardController extends Controller
{
    public function index()
    {
        $departments = Department::pluck('name')
            ->toArray();

        $incidentsCount = Department::withCount('incidents')
            ->pluck('incidents_count')
            ->toArray();

        $activeCount = Incident::where('active', 0)->count();
        $assignedCount = Incident::whereNotNull('support_id')->count();
        $notAssignedCount = Incident::whereNull('support_id')->count();

        $state = [
            $activeCount,
            $assignedCount,
            $notAssignedCount
        ];

        return view('board', compact('departments', 'incidentsCount', 'state'));
    }
}
