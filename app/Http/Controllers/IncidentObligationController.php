<?php

namespace App\Http\Controllers;

use App\Http\Requests\Incident\CollectObligationRequest;
use App\Models\Admin\Obligation;
use App\Models\CollectObligation;
use App\Models\CollectObligationDetail;
use App\Models\Incident;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IncidentObligationController extends Controller
{
    public function index(Incident $incident)
    {
        $collectObligations = $incident->collect_obligations()->paginate(5);

        return view('incidents.collect_obligations.index', compact(
            'incident','collectObligations'
        ));
    }

    public function show(CollectObligation $collectObligation)
    {
        $obligation = $collectObligation->obligation;

        return view('incidents.collect_obligations.show', compact(
            'collectObligation', 'obligation'
        ));
    }

    public function create(Incident $incident)
    {
        $futureId = CollectObligation::whereDate('created_at', now())->count() + 1;

        $incomes = $incident->process->incomes;

        return view('incidents.collect_obligations.create', compact(
            'incident', 'futureId', 'incomes'
        ));
    }

    public function store(Incident $incident, CollectObligationRequest $request)
    {
        $itemIds = $request->item_ids;
        $values = $request->values;

        $correlative = Obligation::whereDate('created_at', today())
            ->count();

        $codeObligation = now()->format('Ymd').'-'.($correlative+1);

        $obligation = Obligation::firstOrCreate(
            [
                'client_id' => $request->client_id,
                'income_id' => $request->income_id
            ],
            [
                'code' => $codeObligation
            ]
        );

        $issueDate = new Carbon($request->issue_date);

        $futureId = CollectObligation::whereDate('created_at', $issueDate)->count() + 1;
        $code = $issueDate->format('Ymd').'-'.$futureId;

        $collectObligation = CollectObligation::create([
            'issue_date' => $request->issue_date,
            'entry_order' => $code,
            'obligation_id' => $obligation->id,
            'period' => $issueDate->format('Ym'),
            'year' => $issueDate->format('Y'),
            'total' => array_sum($values),
            'income_id' => $obligation->income_id,
            'user_id' => auth()->id(),
            'incident_id' => $incident->id
        ]);

        if ($itemIds) {
            $collectObligation->details()
                ->whereNotIn('item_id', $itemIds)
                ->delete();

            foreach ($itemIds as $key => $itemId) {
                CollectObligationDetail::updateOrCreate(
                    [
                        'item_id' => $itemId,
                        'collect_obligation_id' => $collectObligation->id,
                        'income_id' => $collectObligation->income_id,
                    ],
                    ['value' => $values[$key]]
                );
            }
        } else {
            $collectObligation->details()
                ->delete();
        }

        return redirect('incidencia/'.$incident->id.'/pago')->with('notification', 'Obligación por recaudar registrado exitosamente.');
    }

    public function edit(CollectObligation $collectObligation)
    {
        $obligation = $collectObligation->obligation;

        return view('incidents.collect_obligations.edit', compact(
            'collectObligation', 'obligation'
        ));
    }

    public function update(CollectObligation $collectObligation, Request $request)
    {
        $itemIds = $request->item_ids;
        $values = $request->values;

        if ($itemIds) {
            $collectObligation->details()
                ->whereNotIn('item_id', $itemIds)
                ->delete();

            $collectObligation->update([
                'total' => array_sum($values)
            ]);

            foreach ($itemIds as $key => $itemId) {
                CollectObligationDetail::updateOrCreate(
                    [
                        'item_id' => $itemId,
                        'collect_obligation_id' => $collectObligation->id,
                        'income_id' => $collectObligation->obligation->income_id,
                    ],
                    ['value' => $values[$key]]
                );
            }
        } else {
            $collectObligation->details()
                ->delete();
        }

        return redirect('incidencia/'.$collectObligation->incident_id.'/pago')->with('notification', 'Obligación por recaudar modificado exitosamente.');
    }

    public function delete(CollectObligation $collectObligation)
    {
        $collectObligation->details()->delete();
        $collectObligation->delete();

        return back()->with('notification', 'Obligación por recaudar eliminado exitosamente.');
    }
}
