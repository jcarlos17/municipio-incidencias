<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use App\Models\CollectObligation;
use App\Models\Incident;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class IncidentChargeController extends Controller
{
    public function index(Incident $incident, Request $request)
    {
        $inputSearch = $request->input('inputSearch');

        $query = $incident->collect_obligations()->whereNotNull('payment_date');

        if ($inputSearch) {
            $query = $query->whereHas('obligation', function ($query) use ($inputSearch) {
                $query->whereHas('client', function ($query) use ($inputSearch) {
                    $query->where('name', 'like', "%$inputSearch%")
                        ->orWhere('document', 'like', "%$inputSearch%");
                });
            });
        }

        $collectObligations = $query->paginate(10);

        return view('support.incidents.charges.index', compact(
            'incident', 'collectObligations', 'inputSearch'
        ));
    }

    public function create(Incident $incident)
    {
        $client = $incident->client;

        return view('support.incidents.charges.create', compact(
            'incident', 'client'
        ));
    }

    public function store(Incident $incident, Request $request)
    {
        $collectObligationId = $request->collect_obligation_id;

        $collectObligation = CollectObligation::find($collectObligationId);

        $collectObligation->update([
            'payment_date' => now(),
            'cashier_id' => auth()->id(),
            'status' => CollectObligation::COLLECTED
        ]);

        if (!$request->token) {
            $collectObligation->update([
                'payment_type' => $request->payment_type
            ]);

            return redirect('incidents/'.$incident->id.'/charges')->with('notification', 'Se realizó el cobro correctamente');
        }

        $code = $this->getCode();
        $url = 'https://apipre.pagoplux.com/transv1/transaction/validationTokenDateResource';

        $response = Http::withHeaders([
            'Content-type' => 'application/json',
            'X-Second' => 'bar'
        ])->withToken($code)
            ->post($url, [
                'token' => $request->token,
                'amount' => $request->amount,
                'date' => $request->date
            ]);

        $body = $response->body();
        $decode = json_decode($body);

        $decodeResponse = json_decode($request->response);
        $detail = $decodeResponse->detail;

        $payment = Payment::create([
            'id_transaccion' => $detail->id_transaccion,
            'token' => $detail->token,
            'amount' => $detail->amount,
            'cardType' => $detail->cardType,
            'cardIssuer' => $detail->cardIssuer,
            'cardInfo' => $detail->cardInfo,
            'clientID' => $detail->clientID,
            'clientName' => $detail->clientName,
            'state' => $detail->state,
            'fecha' => $detail->fecha,
            'acquirer' => $detail->acquirer,
            'deferred' => $detail->deferred,
            'interests' => $detail->interests,
            'interestValue' => $detail->interestValue,
            'amountWoTaxes' => $detail->amountWoTaxes,
            'amountWTaxes' => $detail->amountWTaxes,
            'taxesValue' => $detail->taxesValue,
            'tipoPago' => $detail->tipoPago,
            'response' => $request->response,
            'collect_obligation_id' => $collectObligationId
        ]);

        $collectObligation->update([
            'payment_type' => Payment::PLUX
        ]);

        // success payment
        if (!empty($decode->status) && $decode->status === 'succeeded') {
            $payment->update([
                'succeeded' => true
            ]);
        }

        return redirect('incidents/'.$incident->id.'/charges')->with('notification', 'Se realizó el cobro correctamente');
    }

    protected  function getCode()
    {
        $secretKey = env('SECRET_KEY_PLUX');
        $length = rand()/getrandmax() * mb_strlen($secretKey);
        $string = '';
        while (mb_strlen($string) < $length) {
            $string .= substr($secretKey, intval((rand()/getrandmax()*$length)), 1);
        }

        $number = microtime(1)*1000 * 30;

        return base64_encode($string . 'PPX_' . $secretKey . 'PPX_' . $number . 'AWS');
    }

    public function show(Incident $incident, CollectObligation $collectObligation)
    {
        $obligation = $collectObligation->obligation;

        return view('support.incidents.charges.show', compact(
            'incident', 'collectObligation', 'obligation'
        ));
    }

    public function reverse(Incident $incident, CollectObligation $collectObligation)
    {
        $collectObligation->update([
            'payment_date' => null,
            'cashier_id' => null
        ]);

        return redirect('incidents/'.$incident->id.'/charges')->with('notification', 'Se realizó el reverso del cobro correctamente');
    }
}
