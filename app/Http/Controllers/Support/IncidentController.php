<?php

namespace App\Http\Controllers\Support;

use App\Http\Controllers\Controller;
use App\Models\DepartmentUser;
use App\Models\Incident;
use App\Models\Process;
use App\Models\User;
use Illuminate\Http\Request;

class IncidentController extends Controller
{
    public function assigned(Request $request)
    {
        $user = auth()->user();
        $selected_department_id = $user->selected_department_id;

        $processId = $request->process_id;
        $startDate = $request->start_date;
        $endingDate = $request->ending_date;

        $myIncidents = [];

        if ($selected_department_id) {
            $processes = Process::where('department_id', $selected_department_id)->get([
                'id', 'name'
            ]);

            $query = Incident::assignedToMe($selected_department_id, $user->id);

            if ($processId) {
                $query = $query->where('process_id', $processId);
            }

            if ($startDate && $endingDate) {
                $query = $query->whereBetween('created_at', [$startDate, $endingDate]);
            }

            $myIncidents = $query->orderByDesc('created_at')->paginate(10);
        }

        if (count($myIncidents) == 0 && !$processId && !$startDate && !$endingDate) {
            return redirect('pending-incidents');
        }

        return view('support.incidents.assigned', compact(
            'myIncidents', 'processes',
            'processId', 'startDate', 'endingDate'
        ));
    }

    public function pending(Request $request)
    {
        $user = auth()->user();
        $selected_department_id = $user->selected_department_id;
        $processId = $request->process_id;
        $startDate = $request->start_date;
        $endingDate = $request->ending_date;

        $pending_incidents = $processes = [];

        if ($selected_department_id) {
            $processes = Process::where('department_id', $selected_department_id)->get([
                'id', 'name'
            ]);
            $departmentUserLevels = DepartmentUser::where('user_id', $user->id)
//                ->where('department_id', $selected_department_id)
                ->pluck('level_id')
                ->toArray();

            if (count($departmentUserLevels) > 0) {
                $query = Incident::pendingToAttend(
//                    $selected_department_id,
                    $departmentUserLevels
                );

                if ($processId) {
                    $query = $query->where('process_id', $processId);
                }

                if ($startDate && $endingDate) {
                    $query = $query->whereBetween('created_at', [$startDate, $endingDate]);
                }

                $pending_incidents = $query->orderByDesc('created_at')->paginate(10);
            }
        }

        return view('support.incidents.pending', compact(
            'pending_incidents', 'processes',
            'processId', 'startDate', 'endingDate'
        ));
    }

    public function reported(Request $request)
    {
        $user = auth()->user();
        $selected_department_id = $user->selected_department_id;

        $processId = $request->process_id;
        $startDate = $request->start_date;
        $endingDate = $request->ending_date;

        $incidents_by_me = [];

        if ($selected_department_id) {
            $processes = Process::where('department_id', $selected_department_id)->get([
                'id', 'name'
            ]);

            $query = Incident::reportedByMe($user->id, $selected_department_id);

            if ($processId) {
                $query = $query->where('process_id', $processId);
            }

            if ($startDate && $endingDate) {
                $query = $query->whereBetween('created_at', [$startDate, $endingDate]);
            }

            $incidents_by_me = $query->paginate(10);
        }

        return view('support.incidents.reported', compact(
            'incidents_by_me', 'processes',
            'processId', 'startDate', 'endingDate'
        ));
    }

    public function index(Request $request)
    {
        $searchIncident = $request->input('document');
        $searchState = $request->input('state');

        $query = Incident::query();

        if ($searchState) {
            if ($searchState == 'Resuelto')
                $query = $query->where('active', 0);
            elseif ($searchState == 'Asignado')
                $query = $query->where('active', 1)
                    ->whereNotNull('support_id');
            else
                $query = $query = $query->where('active', 1)
                    ->whereNull('support_id');
        }
        if ($searchIncident) { // search by document
            $userIds = User::where('document', 'like', "%$searchIncident%")->pluck('id');
            $query = $query->whereIn('client_id', $userIds);
        }

        $incidents = $query->orderByDesc('created_at')->paginate(5);

        // always paginate
        return view('support.incidents.index')
            ->with(compact('incidents', 'searchIncident', 'searchState'));
    }

    public function show($id)
    {
        $incident = Incident::findOrFail($id);
        $incident_histories = $incident->histories;

        return view('support.incidents.show', compact(
            'incident', 'incident_histories'
        ));
    }
}
