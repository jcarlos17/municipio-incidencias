<?php

namespace App\Http\Controllers;

use App\Models\CollectObligation;
use App\Models\Incident;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class IncidentPaymentController extends Controller
{
    public function create(Incident $incident)
    {
        $client = $incident->client;

        return view('incidents.charges.create', compact(
            'incident', 'client'
        ));
    }

    public function store(Incident $incident, Request $request)
    {
        $collectObligationId = $request->collect_obligation_id;

        $collectObligation = CollectObligation::find($collectObligationId);

        if (!$request->token) {
            return redirect('ver/'.$incident->id)->with('notification', 'No se realizo el pago correctamente.');
        }

        $code = $this->getCode();
        $url = 'https://apipre.pagoplux.com/transv1/transaction/validationTokenDateResource';

        $response = Http::withHeaders([
            'Content-type' => 'application/json',
            'X-Second' => 'bar'
        ])->withToken($code)
            ->post($url, [
                'token' => $request->token,
                'amount' => $collectObligation->total,
                'date' => $request->date
            ]);

        $body = $response->body();
        $decode = json_decode($body);

        $decodeResponse = json_decode($request->response);
        $detail = $decodeResponse->detail;

        $payment = Payment::create([
            'id_transaccion' => $detail->id_transaccion,
            'token' => $detail->token,
            'amount' => $detail->amount,
            'cardType' => $detail->cardType,
            'cardIssuer' => $detail->cardIssuer,
            'cardInfo' => $detail->cardInfo,
            'clientID' => $detail->clientID,
            'clientName' => $detail->clientName,
            'state' => $detail->state,
            'fecha' => $detail->fecha,
            'acquirer' => $detail->acquirer,
            'deferred' => $detail->deferred,
            'interests' => $detail->interests,
            'interestValue' => $detail->interestValue,
            'amountWoTaxes' => $detail->amountWoTaxes,
            'amountWTaxes' => $detail->amountWTaxes,
            'taxesValue' => $detail->taxesValue,
            'tipoPago' => $detail->tipoPago,
            'response' => $request->response,
            'collect_obligation_id' => $collectObligationId
        ]);

        $collectObligation->update([
            'payment_type' => Payment::PLUX,
            'payment_date' => now(),
            'cashier_id' => auth()->id(),
            'status' => CollectObligation::COLLECTED
        ]);

        // success payment
        if (!empty($decode->status) && $decode->status === 'succeeded') {
            $payment->update([
                'succeeded' => true
            ]);
        }

        return redirect('ver/'.$incident->id)->with('notification', 'Se realizó el cobro correctamente');
    }

    protected  function getCode()
    {
        $secretKey = env('SECRET_KEY_PLUX');
        $length = rand()/getrandmax() * mb_strlen($secretKey);
        $string = '';
        while (mb_strlen($string) < $length) {
            $string .= substr($secretKey, intval((rand()/getrandmax()*$length)), 1);
        }

        $number = microtime(1)*1000 * 30;

        return base64_encode($string . 'PPX_' . $secretKey . 'PPX_' . $number . 'AWS');
    }
}
