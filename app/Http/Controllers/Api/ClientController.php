<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Admin\Obligation;
use App\Models\Admin\Income;
use App\Models\CollectObligation;
use App\Models\Incident;
use App\Models\User;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function search(Request $request)
    {
        $type = $request->type;
        $inputSearch = $request->input_search;

        return User::where('role', User::CLIENT)
            ->where($type, 'like', "%$inputSearch%")
            ->take(10)
            ->get();
    }

    public function clients(Request $request)
    {
        $type = $request->type;
        $inputSearch = $request->input_search;

        $obligationIds = CollectObligation::whereNull('payment_date')
            ->distinct('obligation_id')
            ->pluck('obligation_id')
            ->toArray();

        $clientIds = Obligation::whereIn('id', $obligationIds)
            ->distinct('client_id')
            ->pluck('client_id')
            ->toArray();

        return User::whereIn('id', $clientIds)
            ->where($type, 'like', "%$inputSearch%")
            ->take(10)
            ->get(['id', 'document', 'email', 'name', 'address']);
    }

    public function collectObligation($id)
    {
        $obligations = CollectObligation::whereNull('payment_date')
            ->whereHas('obligation', function ($query) use ($id) {
                $query->where('client_id', $id);
            })
            ->get();

        foreach ($obligations as $obligation) {
            $obligation->income_name = $obligation->income->name;
        }

        return $obligations;
    }

    public function items(CollectObligation $collectObligation)
    {
        $data['id'] = $collectObligation->id;
        $data['income_id'] = $collectObligation->income_id;
        $data['income_name'] = $collectObligation->income->name;
        $data['period'] = $collectObligation->period;
        $data['total'] = $collectObligation->total;
        $data['details'] = $collectObligation->details()->with('item')->get();

        return $data;
    }
}
