<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Admin\Obligation;
use App\Models\Admin\Income;
use App\Models\User;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function byIncome($incomeId)
    {
        $income = Income::where('id', $incomeId)
            ->first();

        return $income->items;
    }
}
