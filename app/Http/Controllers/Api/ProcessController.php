<?php

namespace App\Http\Controllers\Api;

use App\Models\Process;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Department;

class ProcessController extends Controller
{
    public function byDepartment(Request $request)
    {
    	$department_id = $request->input('department_id');
    	$data['processes'] = Department::find($department_id)->processes;

    	return $data;
    }

    // Web API
    public function levels(Process $process)
    {
        return $process->levels()->get([
            'id', 'name'
        ]);
    }

    public function archives(Process $process)
    {
        return $process->archives()->get();
    }
}
