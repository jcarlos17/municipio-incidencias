<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Incident;
use App\Models\Department;

class IncidentController extends Controller
{
    public function stateCount()
    {
    	$incidents = Incident::all();

    	$pending = 0;
    	$solved = 0;
    	$assigned = 0;

		foreach ($incidents as $incident) {
			switch ($incident->state) {
				case 'Resuelto':
					++$solved;
					break;
				case 'Asignado':
					++$assigned;
					break;
				default:
					++$pending;
					break;
			}
		}

		$data['pending'] = $pending;
		$data['solved'] = $solved;
		$data['assigned'] = $assigned;
    	return $data;
    }

    public function store(Request $request)
    {
    	$incident = new Incident();

    	$incident->title = $request->input('title');
    	$incident->description = $request->input('description');
    	$incident->severity = $request->input('severity');

 		$incident->process_id = $request->input('process_id');
 		$incident->department_id = $request->input('department_id');

 		$incident->client_id = $request->input('client_id');
        $incident->level_id = Department::find($request->input('department_id'))->first_level_id;

        $success = $incident->save();

        $data['error'] = !$success;
        return $data;
    }

    public function lists(Request $request)
    {
        $user = $request->user();
        $data = $incidents = [];

        if ($user->is(User::CLIENT)) {
            $incidents = Incident::where('client_id', $user->id)->get();
        } elseif ($user->is(User::SUPPORT)) {
            $incidents = Incident::where('support_id', $user->id)->get();
        }

        foreach ($incidents as $incident) {
            $data[] = [
                'code' => $incident->id,
                'department' => $incident->department->name,
                'process' => $incident->process->name,
                'created_at' => $incident->created_at,
                'level' => $incident->level->name,
                'state' => $incident->state,
                'severity_full' => $incident->severityFull,
                'client' => $incident->client->name,
                'description' => $incident->description
            ];
        }

        return response()->json($data);
    }

    public function show($id, Request $request)
    {
        $user = $request->user();
        $incident = [];

        $query = Incident::where('id', $id);

        if ($user->is(User::CLIENT)) {
            $incident = $query->where('client_id', $user->id)->first();
        } elseif ($user->is(User::SUPPORT)) {
            $incident = $query->where('support_id', $user->id)->first();
        }

        if (!$incident)
            return response()->json([
                'message' => 'No tiene acceso a la incidencia.'
            ], 401);

        return response()->json($incident);
    }
}
