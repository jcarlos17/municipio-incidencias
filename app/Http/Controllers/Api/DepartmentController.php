<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Department;
use App\Models\Incident;

class DepartmentController extends Controller
{
    public function all()
    {
    	$data['departments'] = Department::all();
    	return $data;
    }

    public function incidentCount()
    {
    	$departments = Department::all();

		$incidents_by_department = [];

    	foreach ($departments as $department) {
    		$current_department['name'] = $department->name;
    		$count = Incident::where('department_id', $department->id)->count();
    		$current_department['count'] = $count;

    		$incidents_by_department[] = $current_department;
    	}

    	$data['incidents_by_department'] = $incidents_by_department;
    	return $data;
    }

    // Web API
    public function processes(Department $department)
    {
        return $department->processes()->get([
            'id', 'name'
        ]);
    }
}
