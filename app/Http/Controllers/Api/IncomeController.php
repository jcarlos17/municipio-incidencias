<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class IncomeController extends Controller
{
    public function byClient(User $user)
    {
        return $user->incomes()
            ->get();
    }
}
