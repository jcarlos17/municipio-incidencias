<?php

namespace App\Http\Controllers;

use App\Models\Archive;
use App\Models\IncidentArchive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ArchiveController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('archive');

        $originalName = $file->getClientOriginalName();
        $extension = '.'.$file->getClientOriginalExtension();
        $basename = basename($originalName, $extension);
        $name = $basename.uniqid().$extension;

        Storage::disk('public')->put('archives/' . $name, File::get($file));

        $id = $request->incident_archive_id;
        $archive = Archive::firstOrCreate([
            'name' => $name
        ]);

        if ($id) {
            $incidentArchive = IncidentArchive::find($id);

            $incidentArchive->update([
                'user_id' => auth()->id(),
                'archive_id' => $archive->id
            ]);
        }

        return $archive;
    }

    public function download(Archive $archive)
    {
        return Storage::disk('public')->download('archives/'.$archive->name);
    }
}
