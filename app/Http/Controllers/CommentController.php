<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        return Comment::create([
            'name' => $request->input('name'),
            'document' => $request->input('document'),
            'email' => $request->input('email'),
            'cellphone' => $request->input('cellphone'),
            'description' => $request->input('description'),
            'permission' => $request->has('permission'),
            'responsibility' => $request->has('responsibility')
        ]);
    }
}
