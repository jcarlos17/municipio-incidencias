<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'comments',
        // Test
        'http://municipio-incidencias.test.com/Transbank/rest/transf/pago',
        'http://municipio-incidencias.test.com/Transbank/rest/transf/reverso',
        'http://municipio-incidencias.test.com/Transbank/rest/transf/confirm',
        // Production
        'http://165.227.217.138/Transbank/rest/transf/pago',
        'http://165.227.217.138/Transbank/rest/transf/reverso',
        'http://165.227.217.138/Transbank/rest/transf/confirm'
    ];
}
