<?php

namespace App\Http\Requests\Incident;

use Illuminate\Foundation\Http\FormRequest;

class IncidentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'official_name' => 'required',
            'detail' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'official_name.required' => 'Es necesario seleccionar un funcionario.',
            'detail.required' => 'Es necesario el campo detalle.'
        ];
    }
}
