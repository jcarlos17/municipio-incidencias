<?php

namespace App\Http\Requests\Incident;

use App\Models\Incident;
use Illuminate\Foundation\Http\FormRequest;

class IncidentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'process_id' => 'required|sometimes|exists:processes,id',
            'severity' => 'required|in:M,N,A',
            'client_id' => 'required|exists:users,id',
            'description' => 'required|min:15'
        ];
    }

    public function messages()
    {
        return [
            'process_id.exists' => 'El proceso seleccionado no existe en nuestra base de datos.',
            'client_id.required' => 'Es necesario seleccionar un cliente.',
            'client_id.exists' => 'El cliente ingresado no se encuentra registrado',
            'description.required' => 'Es necesario ingresar una descripción para la incidencia.',
            'description.min' => 'La descripción debe presentar al menos 15 caracteres.'
        ];
    }

    public function withValidator($validator)
    {
//        $validator->after(function($validator) {
//            $query = Incident::where('obligation_id', $this->obligation_id);
//
//            $incident = $this->incident;
//
//            if ($incident) {
//                $query = $query->where('id', '<>', $incident->id);
//            }
//            $exists = $query->exists();
//
//            if ($exists)
//                $validator->errors()->add('code', 'Ya existe una obligación registrada en este proceso');
//        });
    }
}
