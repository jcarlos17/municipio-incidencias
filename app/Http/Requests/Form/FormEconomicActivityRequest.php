<?php

namespace App\Http\Requests\Form;

use Illuminate\Foundation\Http\FormRequest;

class FormEconomicActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ruc' => 'required',
            'business_name' => 'required',
            'initial_date' => 'required',
            'company_street' => 'required',
            'company_parish' => 'required',
            'company_sector' => 'required',
            'company_floor' => 'required',
            'legal_street' => 'required',
            'legal_parish' => 'required',
            'legal_sector' => 'required',
            'legal_floor' => 'required',
            'main' => 'required',
            'secondary' => 'required',
            'box' => 'required',
            'commodity' => 'required',
            'machinery' => 'required',
            'furniture' => 'required',
            'vehicles' => 'required',
            'local_type' => 'required',
            'local' => 'required',
            'other_assets' => 'required',
            'total_assets' => 'required',
            'debs_pay' => 'required',
            'dsts_pay' => 'required',
            'other_passives' => 'required',
            'total_passives' => 'required',
            'asset_passive' => 'required',
            'total' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'ruc.required' => 'Es necesario el campo RUC',
            'business_name.required' => 'Es necesario el campo Razón social',
            'initial_date.required' => 'Es necesario el campo Fecha de inicio',
            'company_street.required' => 'Es necesario el campo Empresa Calle(s) y N°',
            'company_parish.required' => 'Es necesario el campo Empresa parroquia',
            'company_sector.required' => 'Es necesario el campo Empresa sector',
            'company_floor.required' => 'Es necesario el campo Empresa piso',
            'legal_street.required' => 'Es necesario el campo Representate Legal Calle(s) y N°',
            'legal_parish.required' => 'Es necesario el campo Representate Legal parroquia',
            'legal_sector.required' => 'Es necesario el campo Representate Legal sector',
            'legal_floor.required' => 'Es necesario el campo Representate Legal piso',
            'main.required' => 'Es necesario el campo Principal',
            'secondary.required' => 'Es necesario el campo Secundaria',
            'box.required' => 'Es necesario el campo Caja-bancos',
            'commodity.required' => 'Es necesario el campo Mercaderia',
            'machinery.required' => 'Es necesario el campo Maquinaria y equipo',
            'furniture.required' => 'Es necesario el campo Muebles y enseres',
            'vehicles.required' => 'Es necesario el campo Vehículos',
            'local_type.required' => 'Es necesario el campo Tipo Local',
            'local.required' => 'Es necesario el campo Local',
            'other_assets.required' => 'Es necesario el campo Otros activos',
            'total_assets.required' => 'Es necesario el campo TOTAL ACTIVOS',
            'debs_pay.required' => 'Es necesario el campo Cuentas por pagar',
            'dsts_pay.required' => 'Es necesario el campo Dsts por pagar',
            'other_passives.required' => 'Es necesario el campo Otros pasivos',
            'total_passives.required' => 'Es necesario el campo TOTAL PASIVOS',
            'asset_passive.required' => 'Es necesario el campo	Activos-pasivos',
            'total.required' => 'Es necesario el campo TOTAL',
        ];
    }
}
