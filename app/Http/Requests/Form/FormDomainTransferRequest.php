<?php

namespace App\Http\Requests\Form;

use Illuminate\Foundation\Http\FormRequest;

class FormDomainTransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'notary' => 'required',
            'seller' => 'required',
            'buyer' => 'required',
            'parish' => 'required',
            'sector' => 'required',
            'street' => 'required',
            'old_key' => 'required',
            'current_key' => 'required',
            'old_property' => 'required',
            'perimeter' => 'required',
            'acquirer' => 'required',
            'acquired_date' => 'required',
            'property_extension' => 'required',
            'sale_extension' => 'required',
            'old_price' => 'required',
            'current_sale' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'notary.required' => 'Es necesario el campo Notaría',
            'seller.required' => 'Es necesario el campo Vendedor',
            'buyer.required' => 'Es necesario el campo Comprador',
            'parish.required' => 'Es necesario el campo Parroquia',
            'sector.required' => 'Es necesario el campo Sector',
            'street.required' => 'Es necesario el campo Calle',
            'old_key.required' => 'Es necesario el campo Clave catastral anterior',
            'current_key.required' => 'Es necesario el campo Clave catastral actual',
            'old_property.required' => 'Es necesario el campo Dueño anterior',
            'perimeter.required' => 'Es necesario el campo Perímetro',
            'acquirer.required' => 'Es necesario el campo Adquirido por',
            'acquired_date.required' => 'Es necesario el campo Fecha adquirida',
            'property_extension.required' => 'Es necesario el campo Extensión del predio',
            'sale_extension.required' => 'Es necesario el campo Venta totañ',
            'old_price.required' => 'Es necesario el campo Precio compra anterior',
            'current_sale.required' => 'Es necesario el campo Precio venta actual'
        ];
    }
}
