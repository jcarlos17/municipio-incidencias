<?php

namespace App\Http\Requests\Form;

use Illuminate\Foundation\Http\FormRequest;

class FormParticularRuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'official_name' => 'required',
            'reason' => 'required',
            'address_location' => 'required',
            'address_between' => 'required',
            'intersection' => 'required',
            'key' => 'required',
            'sector' => 'required',
            'district' => 'required',
            'lot' => 'required',
            'square' => 'required',
            'applicant' => 'required',
            'via_address' => 'required',
            'via_width' => 'required',
            'via_address2' => 'required',
            'via_width2' => 'required',
            'frontal' => 'required',
            'side' => 'required',
            'bottom' => 'required',
//            'wall_side' => 'required',
//            'wall_later' => 'required',
            'wall' => 'required',
            'type' => 'required',
            'cos' => 'required',
            'cus' => 'required',
            'other_floor' => 'required',
            'land_use' => 'required',
            'number_floor' => 'required',
            'max_height' => 'required',
//            'water' => 'required',
//            'road' => 'required',
//            'sewerage' => 'required',
//            'curb' => 'required',
//            'electric' => 'required',
//            'pavement' => 'required',
//            'phone' => 'required',
//            'situation_total' => 'required',
//            'situation_partial' => 'required',
//            'enclosure' => 'required',
//            'factory_line' => 'required',
            'add_report' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'official_name.required' => 'Es necesario el campo Para',
            'reason.required' => 'Es necesario el campo Motivo',
            'address_location.required' => 'Es necesario el campo Propiedad ubicada en',
            'address_between.required' => 'Es necesario el campo Entre',
            'intersection.required' => 'Es necesario el campo Intersección',
            'key.required' => 'Es necesario el campo Clave catastral',
            'sector.required' => 'Es necesario el campo Sector',
            'district.required' => 'Es necesario el campo Barrio o parcelación',
            'lot.required' => 'Es necesario el campo Lote N°',
            'square.required' => 'Es necesario el campo Manzana N°',
            'applicant.required' => 'Es necesario el campo Solicitante',
            'via_address.required' => 'Es necesario el campo Nombre de la calle o avenida (1)',
            'via_width.required' => 'Es necesario el campo Ancho Mt. (1)',
            'via_address2.required' => 'Es necesario el campo Nombre de la calle o avenida (2)',
            'via_width2.required' => 'Es necesario el campo Ancho Mt. (2)',
            'frontal.required' => 'Es necesario el campo Frontal',
            'side.required' => 'Es necesario el campo Lateral',
            'bottom.required' => 'Es necesario el campo Fondo',
//            'wall_side.required' => 'Es necesario el campo A las medianeras laterales',
//            'wall_later.required' => 'Es necesario el campo A las medianera posterior',
            'wall.required' => 'Es necesario el campo A la medianera',
            'type.required' => 'Es necesario el campo Tipo',
            'cos.required' => 'Es necesario el campo C.O.S.',
            'cus.required' => 'Es necesario el campo C.U.S.',
            'other_floor.required' => 'Es necesario el campo Otros pisos',
            'land_use.required' => 'Es necesario el campo Uso Suelo',
            'number_floor.required' => 'Es necesario el campo N° de pisos',
            'max_height.required' => 'Es necesario el campo Atura máxima mt.',
//            'water.required' => 'Es necesario el campo Agua Potable',
//            'road.required' => 'Es necesario el campo Calzada',
//            'sewerage.required' => 'Es necesario el campo Alcantarillado',
//            'curb.required' => 'Es necesario el campo Bordillos',
//            'electric.required' => 'Es necesario el campo Luz eléctrica',
//            'pavement.required' => 'Es necesario el campo Aceras',
//            'phone.required' => 'Es necesario el campo Teléfono',
//            'situation_total.required' => 'Es necesario el campo Afectada totalmente',
//            'situation_partial.required' => 'Es necesario el campo Parcialmente',
//            'enclosure.required' => 'Es necesario el campo Tiene cerramiento',
//            'factory_line.required' => 'Es necesario el campo En línea de fábrica',
            'add_report.required' => 'Es necesario el campo Informes adicionales',
        ];
    }
}
