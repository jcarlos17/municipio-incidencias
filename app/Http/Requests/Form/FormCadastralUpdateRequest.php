<?php

namespace App\Http\Requests\Form;

use Illuminate\Foundation\Http\FormRequest;

class FormCadastralUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'constancy' => 'required',
            'year' => 'required',
            'canton' => 'required',
            'parish' => 'required',
            'registration' => 'required',
            'estate' => 'required',
            'value' => 'required',
            'notary' => 'required',
            'amount' => 'required',
            'seller_name' => 'required',
            'seller_document' => 'required',
            'buyer_name' => 'required',
            'buyer_document' => 'required',
            'cadastral_canton' => 'required',
            'cadastral_parish' => 'required',
            'sector' => 'required',
            'property_name' => 'required',
            'inscription_date' => 'required',
            'total_area' => 'required',
            'total_sale' => 'required',
            'partial_sale' => 'required',
            'reserved_area' => 'required',
            'domain_limitation' => 'required',
            'north_sale' => 'required',
            'south_sale' => 'required',
            'west_sale' => 'required',
            'east_sale' => 'required',
            'north_reservation' => 'required',
            'south_reservation' => 'required',
            'west_reservation' => 'required',
            'east_reservation' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'constancy.required' => 'Es necesario el campo Consta en el catastro',
            'year.required' => 'Es necesario el campo Consta en el catastro (año)',
            'canton.required' => 'Es necesario el campo Cantón',
            'parish.required' => 'Es necesario el campo Parroquia',
            'registration.required' => 'Es necesario el campo Registro',
            'estate.required' => 'Es necesario el campo Predio',
            'value.required' => 'Es necesario el campo Avaluo Catastral',
            'notary.required' => 'Es necesario el campo Notaría',
            'amount.required' => 'Es necesario el campo Cuantía',
            'seller_name.required' => 'Es necesario el campo Vendedor',
            'seller_document.required' => 'Es necesario el campo Cédula N°',
            'buyer_name.required' => 'Es necesario el campo Comprador',
            'buyer_document.required' => 'Es necesario el campo Cédula N°',
            'cadastral_canton.required' => 'Es necesario el campo Cantón',
            'cadastral_parish.required' => 'Es necesario el campo Parroquia',
            'sector.required' => 'Es necesario el campo Sector o barrio',
            'property_name.required' => 'Es necesario el campo Nombre del predio',
            'inscription_date.required' => 'Es necesario el campo Fecha de la inscripción de la escritura',
            'total_area.required' => 'Es necesario el campo Superficie total',
            'total_sale.required' => 'Es necesario el campo Venta total (Has.)',
            'partial_sale.required' => 'Es necesario el campo Venta parcial (Has.)',
            'reserved_area.required' => 'Es necesario el campo Superficie reservada',
            'domain_limitation.required' => 'Es necesario el campo Limitación de dominio (USUFRUCTO)',
            'north_sale.required' => 'Es necesario el campo Norte',
            'south_sale.required' => 'Es necesario el campo Sur',
            'west_sale.required' => 'Es necesario el campo Oeste',
            'east_sale.required' => 'Es necesario el campo Este',
            'north_reservation.required' => 'Es necesario el campo Norte',
            'south_reservation.required' => 'Es necesario el campo Sur',
            'west_reservation.required' => 'Es necesario el campo Oeste',
            'east_reservation.required' => 'Es necesario el campo Este',
        ];
    }
}
