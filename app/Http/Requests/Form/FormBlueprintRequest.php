<?php

namespace App\Http\Requests\Form;

use Illuminate\Foundation\Http\FormRequest;

class FormBlueprintRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location' => 'required',
            'key' => 'required',
            'street' => 'required',
            'ground_surface' => 'required',
            'ground_floor_surface' => 'required',
            'total_construction' => 'required',
            'rooms' => 'required',
            'floors' => 'required',
            'retreat' => 'required',
            'foundation' => 'required',
            'structure' => 'required',
            'wall' => 'required',
            'cover' => 'required',
            'unit_cost' => 'required',
            'total_cost' => 'required',
            'financing' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'location.required' => 'Es necesario el campo Ubicada en',
            'key.required' => 'Es necesario el campo Clave catastral',
            'street.required' => 'Es necesario el campo Street',
            'ground_surface.required' => 'Es necesario el campo Superficie del terreno',
            'ground_floor_surface.required' => 'Es necesario el campo Superficie en planta baja',
            'total_construction.required' => 'Es necesario el campo Superficie total de la construcción',
            'rooms.required' => 'Es necesario el campo Número de cuartos',
            'floors.required' => 'Es necesario el campo Número de pisos',
            'retreat.required' => 'Es necesario el campo Propiedad Retiros',
            'foundation.required' => 'Es necesario el campo Cimientos',
            'structure.required' => 'Es necesario el campo Estructura',
            'wall.required' => 'Es necesario el campo Fecha Paredes',
            'cover.required' => 'Es necesario el campo Extensión Cubiertas',
            'unit_cost.required' => 'Es necesario el campo Costo unitario de construcción',
            'total_cost.required' => 'Es necesario el campo Costo total',
            'financing.required' => 'Es necesario el campo Financiamiento con',
        ];
    }
}
