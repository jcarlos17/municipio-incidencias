<?php

namespace App\Http\Requests\Form;

use Illuminate\Foundation\Http\FormRequest;

class FormConstructionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'observation' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'description.required' => 'Es necesario el campo Descripción.',
            'observation.required' => 'Es necesario el campo Observaciones.'
        ];
    }
}
