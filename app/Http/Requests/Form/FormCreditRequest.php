<?php

namespace App\Http\Requests\Form;

use Illuminate\Foundation\Http\FormRequest;

class FormCreditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from_official_name' => 'required',
            'to_official_name' => ['required', 'distinct:from_official_name'],
            'quantity' => 'required',
            'concept' => 'required',
            'street' => 'required',
            'district' => 'required',
            'parish' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'from_official_name.required' => 'Es necesario completar el campo De',
            'to_official_name.required' => 'Es necesario completar el campo Para',
            'quantity.required' => 'Es necesario completar el campo Por la cantidad de:',
            'concept.required' => 'Es necesario completar el campo Por concepto de:',
            'street.required' => 'Es necesario completar el campo Calle:',
            'district.required' => 'Es necesario completar el campo Barrio:',
            'parish.required' => 'Es necesario completar el campo Parroquia:'
        ];
    }

//    public function withValidator($validator)
//    {
//        $validator->after(function($validator) {
//            if ($this->from_official_name == $this->to_official_name)
//                $validator->errors()->add('code', 'Los funcionarios deben ser distintos');
//        });
//    }
}
