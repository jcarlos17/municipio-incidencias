<?php

namespace App\Http\Requests\Form;

use Illuminate\Foundation\Http\FormRequest;

class FormPropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'owner_name' => 'required',
            'owner_document' => 'required',
            'department' => 'required',
            'reason' => 'required',
            'street' => 'required',
            'district' => 'required',
            'sector' => 'required',
            'parish' => 'required',
            'observation' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'owner_name.required' => 'Es necesario el campo Nombre Propietario.',
            'owner_document.required' => 'Es necesario el campo Cédula Propietario.',
            'department.required' => 'Es necesario seleccionar el campo Observaciones.',
            'reason.required' => 'Es necesario seleccionar el campo Motivos.',
            'street.required' => 'Es necesario el campo Calle.',
            'district.required' => 'Es necesario el campo Barrio.',
            'sector.required' => 'Es necesario el campo Sector.',
            'parish.required' => 'Es necesario el campo Parroquia.',
            'observation.required' => 'Es necesario el campo Observaciones.'
        ];
    }
}
