<?php

namespace App\Http\Requests\Admin;

use App\Models\Admin\Income;
use Illuminate\Foundation\Http\FormRequest;

class IncomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'income_group_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Es necesario ingresar un nombre.',
            'name.unique' => 'El nombre ya se encuentra registro.',
            'income_group_id.required' => 'Es necesario seleccionar un grupo.',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function($validator) {
            $exists = false;
            if ($this->process_id) {
                $query = Income::where('process_id', $this->process_id)
                    ->where('name', $this->name);

                $income = $this->income;

                if ($income) {
                    $query = $query->where('id', '<>', $income->id);
                }

                $exists = $query->exists();
            }

            if ($exists) {
                $validator->errors()->add('name', 'El ingreso y proceso seleccionado ya se encuentra registrado');
            }
        });
    }
}
