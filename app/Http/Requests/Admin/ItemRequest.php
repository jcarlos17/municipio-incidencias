<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'code' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'description.required' => 'Es necesario ingresar la descripción del item.',
            'code.required' => 'Es necesario ingresar cta contable del item.'
        ];
    }
}
