<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RequirementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Es necesario ingresar un nombre para el nivel.',
            'type.required' => 'Es necesario seleccionar un tipo para el requisito.'
        ];
    }
}
