<?php

namespace App\Http\Requests\Admin;

use App\Models\Admin\Obligation;
use Illuminate\Foundation\Http\FormRequest;

class ObligationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required',
            'income_id' => 'required',
            'code' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'client_id.required' => 'Es necesario selecionar un cliente.',
            'income_id.required' => 'Es necesario selecionar un ingreso.',
            'code.required' => 'Es necesario ingresar un código.',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function($validator) {
            $query = Obligation::where('client_id', $this->client_id)
                ->where('income_id', $this->income_id)
                ->where('code', $this->code);

            $obligation = $this->obligation;

            if ($obligation) {
                $query = $query->where('id', '<>', $obligation->id);
            }
            $exists = $query->exists();

            if ($exists)
                $validator->errors()->add('code', 'Ya existe una obigación en el ingreso con este código');
        });
    }
}
