<?php

namespace App\Models;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\IncidentChange
 *
 * @property int $id
 * @property string $type
 * @property int|null $incident_id
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $attention_seconds
 * @property-read mixed $attention_time
 * @property-read mixed $description
 * @property-read mixed $standby_seconds
 * @property-read mixed $standby_time
 * @property-read \App\Models\Incident|null $incident
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentChange newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentChange newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentChange query()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentChange whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentChange whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentChange whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentChange whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentChange whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentChange whereUserId($value)
 * @mixin \Eloquent
 */
class IncidentChange extends Model
{
    protected $dates = [
        'created_at'
    ];

    public function incident()
    {
        return $this->belongsTo(Incident::class, 'incident_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    public function getDescriptionAttribute()
    {
        if ($this->type == 'registry')
            return 'La incidencia fue registrada por '.$this->user->name;
        if ($this->type == 'edit')
            return 'La incidencia fue editada por '.$this->user->name;
        if ($this->type == 'attention')
            return 'La incidencia fue atendida por '.$this->user->name;
        if ($this->type == 'derive')
            return 'La incidencia fue derivada por '.$this->user->name;
        if ($this->type == 'resolved')
            return 'La incidencia fue resuelta por '.$this->user->name;
        if ($this->type == 'open')
            return 'La incidencia fue reabierta por '.$this->user->name;
    }

    public function getStandbySecondsAttribute()
    {
        $incident = $this->incident;
        if ($this->type == 'derive') {
            $attention = $incident->histories()
                ->where('type', 'attention')
                ->where('id', '>', $this->id)
                ->first(['created_at']);

            if ($attention) {
                return $attention->created_at->diffInSeconds($this->created_at);
            }
        } else if ($this->type == 'registry') {
            $first = $incident->histories()
                ->where('type', 'attention')
                ->where('id', '>', $this->id)
                ->first(['created_at']);

            if ($first) {
                return $first->created_at->diffInSeconds($this->created_at);
            }
        }

        return 0;
    }

    public function getStandbyTimeAttribute()
    {
        if ($this->standbySeconds) {
            return gmdate("H:i:s", $this->standbySeconds);
        }

        return '';
    }

    public function getAttentionSecondsAttribute()
    {
        $incident = $this->incident;
        if ($this->type == 'attention') {
            $derive = $incident->histories()
                ->where('type', 'derive')
                ->where('id', '>', $this->id)
                ->first(['created_at']);

            if ($derive) {
                return $derive->created_at->diffInSeconds($this->created_at);
            } else {
                $resolved = $incident->histories()
                    ->where('type', 'resolved')
                    ->where('id', '>', $this->id)
                    ->first(['created_at']);

                if ($resolved) {
                    return $resolved->created_at->diffInSeconds($this->created_at);
                }
            }
        }

        return 0;
    }

    public function getAttentionTimeAttribute()
    {
        if ($this->attentionSeconds) {
            return gmdate("H:i:s", $this->attentionSeconds);
        }

        return '';
    }

}
