<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Level
 *
 * @property int $id
 * @property string $name
 * @property int $days
 * @property int $hours
 * @property int $minutes
 * @property int $process_id
 * @property int $department_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $total_hours
 * @property-read \App\Models\Process $process
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Level newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Level newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Level onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Level query()
 * @method static \Illuminate\Database\Eloquent\Builder|Level whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Level whereDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Level whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Level whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Level whereHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Level whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Level whereMinutes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Level whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Level whereProcessId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Level whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Level withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Level withoutTrashed()
 * @mixin \Eloquent
 */
class Level extends Model
{
	use SoftDeletes;

    protected $fillable = ['name', 'process_id'];

    public function process()
    {
        return $this->belongsTo(Process::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'department_user', 'level_id', 'user_id');
    }

    public function getTotalHoursAttribute()
    {
        return $this->days*24 + $this->hours + $this->minutes/60;
    }
}
