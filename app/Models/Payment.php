<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Payment
 *
 * @property int $id
 * @property string $id_transaccion
 * @property string $token
 * @property string $amount
 * @property string $cardType
 * @property string $cardIssuer
 * @property string $cardInfo
 * @property string $clientID
 * @property string $clientName
 * @property string $state
 * @property string $fecha
 * @property string $acquirer
 * @property string $deferred
 * @property string $interests
 * @property string $interestValue
 * @property string $amountWoTaxes
 * @property string $amountWTaxes
 * @property string $taxesValue
 * @property string $tipoPago
 * @property string $response
 * @property int $succeeded
 * @property int $collect_obligation_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereAcquirer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereAmountWTaxes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereAmountWoTaxes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCardInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCardIssuer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCardType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereClientID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCollectObligationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereDeferred($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereFecha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereIdTransaccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereInterestValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereInterests($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereSucceeded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereTaxesValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereTipoPago($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Payment extends Model
{
    use HasFactory;

    const CASH = 'EF';
    const PLUX = 'DE';
    const BANK_CHECK = 'CH';

    protected $fillable = [
        'id_transaccion', 'token', 'amount', 'cardType', 'cardIssuer', 'cardInfo', 'clientID', 'clientName', 'state',
        'fecha', 'acquirer', 'deferred', 'interests', 'interestValue', 'amountWoTaxes', 'amountWTaxes', 'taxesValue', 'tipoPago',
        'response', 'succeeded', 'collect_obligation_id'
    ];
}
