<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TransactionCollectObligation
 *
 * @property int $id
 * @property int $transaction_id
 * @property int $collect_obligation_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCollectObligation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCollectObligation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCollectObligation query()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCollectObligation whereCollectObligationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCollectObligation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCollectObligation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCollectObligation whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCollectObligation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TransactionCollectObligation extends Model
{
    use HasFactory;

    protected $fillable = [
        'transaction_id', 'collect_obligation_id'
    ];
}
