<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Official
 *
 * @property int $id
 * @property string $name
 * @property string $position
 * @property int $department_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Department $department
 * @method static \Illuminate\Database\Eloquent\Builder|Official newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Official newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Official query()
 * @method static \Illuminate\Database\Eloquent\Builder|Official whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Official whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Official whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Official whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Official wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Official whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Official extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'position', 'department_id'
    ];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
