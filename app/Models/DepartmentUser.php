<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DepartmentUser
 *
 * @property int $id
 * @property int $department_id
 * @property int $process_id
 * @property int $level_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Department $department
 * @property-read \App\Models\Level $level
 * @property-read \App\Models\Process $process
 * @method static \Illuminate\Database\Eloquent\Builder|DepartmentUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DepartmentUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DepartmentUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|DepartmentUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepartmentUser whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepartmentUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepartmentUser whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepartmentUser whereProcessId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepartmentUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DepartmentUser whereUserId($value)
 * @mixin \Eloquent
 */
class DepartmentUser extends Model
{
    protected $table = 'department_user';

    public function department()
    {
    	return $this->belongsTo(Department::class);
    }

    public function process()
    {
        return $this->belongsTo(Process::class);
    }

    public function level()
    {
    	return $this->belongsTo(Level::class);
    }
}
