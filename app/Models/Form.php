<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Form
 *
 * @property int $id
 * @property string|null $code
 * @property string $official_name
 * @property string $official_position
 * @property string $client_name
 * @property string $client_document
 * @property string $detail
 * @property string|null $type
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $date_complete
 * @property-read \App\Models\Incident $incident
 * @property-read \App\Models\User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|Form newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Form newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Form query()
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereClientDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereOfficialName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereOfficialPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Form extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'official_name', 'official_position',
        'client_name', 'client_document', 'type',
        'detail', 'document', 'incident_id', 'incident_form_id', 'support_id'
    ];

    protected $dates = [
        'created_at'
    ];

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getDateCompleteAttribute()
    {
        $month = $this->getMonthName(($this->created_at->format('n')) - 1);

        return 'San Miguel de Salcedo a, '.$this->created_at->format('d').' de '.$month.' del '.$this->created_at->format('Y');
    }

    public function getMonthName($key)
    {
        $months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

        return $months[$key];
    }
}
