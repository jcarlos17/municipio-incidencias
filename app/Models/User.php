<?php

namespace App\Models;

use App\Models\Admin\Income;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property string $password
 * @property string $document
 * @property string|null $cellphone
 * @property string|null $address
 * @property int $role
 * @property string|null $image
 * @property int|null $selected_department_id
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $incident_cashier
 * @property int $incident_obligation
 * @property string|null $ruc
 * @property string $status
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Laravel\Passport\Client> $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\CollectObligation> $collect_obligations
 * @property-read int|null $collect_obligations_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Department> $departments
 * @property-read int|null $departments_count
 * @property-read mixed $avatar_path
 * @property-read mixed $image_url
 * @property-read mixed $is_support_level_one
 * @property-read mixed $list_of_departments
 * @property-read mixed $updated_format
 * @property-read mixed $status_text
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Income> $incomes
 * @property-read int|null $incomes_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection<int, \Illuminate\Notifications\DatabaseNotification> $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Laravel\Passport\Token> $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCellphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIncidentCashier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIncidentObligation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRuc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSelectedDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User withoutTrashed()
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'ruc',
        'status'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Rol
    const CLIENT = 2;
    const SUPPORT = 1;
    const ADMIN = 0;
    const CASHIER = 3;

    // Status
    const LOCKED = 'B';
    const ACTIVE = 'O';
    const DELETED = 'E';

    // relationships

    public function departments()
    {
        return $this->belongsToMany(Department::class);
    }

    public function incomes()
    {
        return $this->belongsToMany(Income::class, 'obligations', 'client_id', 'income_id')
            ->withCount('items')
            ->having('items_count', '>', 0);
    }

    public function collect_obligations()
    {
        return $this->hasMany(CollectObligation::class);
    }

    public function canTake(Incident $incident)
    {
        return DepartmentUser::where('user_id', $this->id)
            ->where('level_id', $incident->level_id)
            ->exists();
    }

    // accessors
    public function getAvatarPathAttribute()
    {
        if ($this->is(User::CLIENT))
            return '/images/client.png';

        return '/images/support.png';
    }

    public function getImageUrlAttribute()
    {
        if ($this->image)
            return '/images/users/'.$this->image;

        return '/images/users/0.jpg';
    }

    public function getListOfDepartmentsAttribute()
    {
        if ($this->role == User::SUPPORT)
            return $this->departments()->distinct()->get();

        return Department::all();
    }

    public function is($role)
    {
        return $this->role == $role;
    }

    public function getIsSupportLevelOneAttribute()
    {
        $first = Level::where('department_id', $this->selected_department_id)
            ->select(['id', 'process_id'])
            ->distinct('process_id')
            ->orderBy('id')
            ->get();

        $firstLevelIds = Level::where('department_id', $this->selected_department_id)
            ->select(['process_id', DB::raw('MIN(id) as id')])
            ->groupBy('process_id')
            ->pluck('id')
            ->toArray();

        $exists_level = DepartmentUser::where('department_id', $this->selected_department_id)
            ->where('user_id', $this->id)
            ->whereIn('level_id', $firstLevelIds)
            ->exists();

        return ($this->role == User::SUPPORT and $exists_level);
    }

    public function getUpdatedFormatAttribute()
    {
        return $this->updated_at->format('d-m-Y');
    }

    public function getstatusTextAttribute()
    {
        $arr = [
            self::LOCKED => 'Bloqueado',
            self::ACTIVE => 'Activo',
            self::DELETED => 'Eliminado'
        ];

        return $arr[$this->status];
    }
}
