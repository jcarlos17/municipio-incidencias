<?php

namespace App\Models;

use App\Models\Form\FormBlueprint;
use App\Models\Form\FormCadastralUpdate;
use App\Models\Form\FormCadastre;
use App\Models\Form\FormConstruction;
use App\Models\Form\FormCredit;
use App\Models\Form\FormDomainTransfer;
use App\Models\Form\FormEconomicActivity;
use App\Models\Form\FormEstate;
use App\Models\Form\FormMunicipality;
use App\Models\Form\FormParticularRule;
use App\Models\Form\FormProperty;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\IncidentForm
 *
 * @property int $id
 * @property int $form_id
 * @property int $incident_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Incident $incident
 * @property-read \App\Models\Requirement $requirement
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentForm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentForm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentForm query()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentForm whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentForm whereFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentForm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentForm whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentForm whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class IncidentForm extends Model
{
    use HasFactory;

    protected $fillable = [
        'incident_id', 'form_id'
    ];

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }

    public function requirement()
    {
        return $this->belongsTo(Requirement::class, 'form_id');
    }

    public function form()
    {
        $requirement = $this->requirement;

        if ($requirement->form_type == 'certificado-no-adeudar-municipio') {
            return $this->hasOne(FormMunicipality::class, 'incident_form_id');

        } else if ($requirement->form_type == 'solicitud-todo-tramite') {
            return $this->hasOne(Form::class, 'incident_form_id')
                ->where('type', 'SOL');

        } else if ($requirement->form_type == 'informe-certificado') {
            return $this->hasOne(Form::class, 'incident_form_id')
                ->where('type', 'IOC');

        } else if ($requirement->form_type == 'certificado-avaluos-catastros') {
            return $this->hasOne(FormCadastre::class, 'incident_form_id');

        } else if ($requirement->form_type == 'certificado-avaluos-actualizacion-catastral') {
            return $this->hasOne(FormCadastralUpdate::class, 'incident_form_id');

        } else if ($requirement->form_type == 'normas-particulares') {
            return $this->hasOne(FormParticularRule::class, 'incident_form_id');

        } else if ($requirement->form_type == 'permiso-construccion') {
            return $this->hasOne(FormConstruction::class, 'incident_form_id');

        } else if ($requirement->form_type == 'solicitud-aprobacion-planos') {
            return $this->hasOne(FormBlueprint::class, 'incident_form_id');

        } else if ($requirement->form_type == 'certificado-atencion-propiedad') {
            return $this->hasOne(FormProperty::class, 'incident_form_id');

        } else if ($requirement->form_type == 'transferencia-dominio-compra-venta') {
            return $this->hasOne(FormDomainTransfer::class, 'incident_form_id');

        } else if ($requirement->form_type == 'certificado-no-poseer-bienes') {
            return $this->hasOne(FormEstate::class, 'incident_form_id');
        } else if ($requirement->form_type == 'actividad-economica') {
            return $this->hasOne(FormEconomicActivity::class, 'incident_form_id');
        } else if ($requirement->form_type == 'emision-titulo-credito') {
            return $this->hasOne(FormCredit::class, 'incident_form_id');
        }
    }
}
