<?php

namespace App\Models;

use App\Models\Admin\Item;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CollectObligationDetail
 *
 * @property int $id
 * @property float $value
 * @property int $item_id
 * @property int $income_id
 * @property int $collect_obligation_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Item $item
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligationDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligationDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligationDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligationDetail whereCollectObligationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligationDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligationDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligationDetail whereIncomeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligationDetail whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligationDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligationDetail whereValue($value)
 * @mixin \Eloquent
 */
class CollectObligationDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'value', 'item_id', 'income_id', 'collect_obligation_id'
    ];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
