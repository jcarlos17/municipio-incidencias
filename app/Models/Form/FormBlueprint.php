<?php

namespace App\Models\Form;

use App\Models\Incident;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Form\FormBlueprint
 *
 * @property int $id
 * @property string|null $code
 * @property string $name
 * @property string $location
 * @property string $key
 * @property string $street
 * @property float $ground_surface
 * @property float $ground_floor_surface
 * @property float $total_construction
 * @property int $rooms
 * @property int $floors
 * @property int $retreat
 * @property int $foundation
 * @property int $structure
 * @property int $wall
 * @property int $cover
 * @property float $unit_cost
 * @property float $total_cost
 * @property int $financing
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $client_name
 * @property-read mixed $date_complete
 * @property-read Incident $incident
 * @property-read User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereFinancing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereFloors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereFoundation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereGroundFloorSurface($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereGroundSurface($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereRetreat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereRooms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereStructure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereTotalConstruction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereTotalCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereUnitCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormBlueprint whereWall($value)
 * @mixin \Eloquent
 */
class FormBlueprint extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'name', 'location', 'key', 'street', 'ground_surface', 'ground_floor_surface', 'total_construction', 'rooms',
        'floors', 'retreat', 'foundation', 'structure', 'wall', 'cover', 'unit_cost', 'total_cost', 'financing',
        'document', 'incident_id', 'incident_form_id', 'support_id'
    ];

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getClientNameAttribute()
    {
        return $this->name;
    }

    public function getDateCompleteAttribute()
    {
        $months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre"];

        $month = $months[($this->created_at->format('n')) - 1];

        return 'San Miguel de Salcedo, a ' . $this->created_at->format('d').' de '.$month.' del '.$this->created_at->format('Y');
    }
}
