<?php

namespace App\Models\Form;

use App\Models\Incident;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Form\FormCredit
 *
 * @property int $id
 * @property string|null $code
 * @property string $from_official_name
 * @property string $from_official_position
 * @property string $to_official_name
 * @property string $to_official_position
 * @property string $client_name
 * @property string $client_document
 * @property string $quantity
 * @property string $concept
 * @property string $street
 * @property string $district
 * @property string $parish
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $date_complete
 * @property-read Incident $incident
 * @property-read User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereClientDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereConcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereFromOfficialName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereFromOfficialPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereParish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereToOfficialName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereToOfficialPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCredit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FormCredit extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'from_official_name', 'from_official_position', 'to_official_name', 'to_official_position', 'client_name',
        'client_document', 'quantity', 'concept', 'street', 'district', 'parish', 'document', 'incident_id', 'incident_form_id',
        'support_id'
    ];

    protected $dates = [
        'created_at'
    ];

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getDateCompleteAttribute()
    {
        $month = $this->getMonthName(($this->created_at->format('n')) - 1);

        return 'San Miguel de Salcedo, '.$this->created_at->format('d').' de '.$month.' del '.$this->created_at->format('Y');
    }

    public function getMonthName($key)
    {
        $months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

        return $months[$key];
    }
}
