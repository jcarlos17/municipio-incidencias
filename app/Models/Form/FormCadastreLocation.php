<?php

namespace App\Models\Form;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Form\FormCadastreLocation
 *
 * @property int $id
 * @property string $state
 * @property string $parish
 * @property string $location
 * @property string $key
 * @property string $commercial
 * @property int $form_cadastre_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation whereCommercial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation whereFormCadastreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation whereParish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastreLocation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FormCadastreLocation extends Model
{
    use HasFactory;

    protected $fillable = [
        'state', 'parish', 'location', 'key', 'commercial', 'form_cadastre_id'
    ];
}
