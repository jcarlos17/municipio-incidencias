<?php

namespace App\Models\Form;

use App\Models\Incident;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\Form\FormParticularRule
 *
 * @property int $id
 * @property string|null $code
 * @property string $official_name
 * @property string $official_position
 * @property string $client_name
 * @property string $reason
 * @property string $address_location
 * @property string $address_between
 * @property string $intersection
 * @property string $key
 * @property string $sector
 * @property string $district
 * @property string $lot
 * @property string $square
 * @property string $applicant
 * @property string $sketch
 * @property string $via_address
 * @property string $via_width
 * @property string $via_address2
 * @property string $via_width2
 * @property string $frontal
 * @property string $side
 * @property string $bottom
 * @property string|null $wall_side
 * @property string|null $wall_later
 * @property string $wall
 * @property string $type
 * @property string $cos
 * @property string $cus
 * @property string $other_floor
 * @property string $land_use
 * @property string $number_floor
 * @property string $max_height
 * @property string|null $water
 * @property string|null $road
 * @property string|null $sewerage
 * @property string|null $curb
 * @property string|null $electric
 * @property string|null $pavement
 * @property string|null $phone
 * @property string|null $situation_total
 * @property string|null $situation_partial
 * @property string|null $enclosure
 * @property string|null $factory_line
 * @property string $add_report
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $date_complete
 * @property-read mixed $sketch_url
 * @property-read Incident $incident
 * @property-read User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereAddReport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereAddressBetween($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereAddressLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereApplicant($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereBottom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereCos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereCurb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereCus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereElectric($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereEnclosure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereFactoryLine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereFrontal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereIntersection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereLandUse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereLot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereMaxHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereNumberFloor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereOfficialName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereOfficialPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereOtherFloor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule wherePavement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereRoad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereSector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereSewerage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereSide($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereSituationPartial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereSituationTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereSketch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereSquare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereViaAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereViaAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereViaWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereViaWidth2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereWall($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereWallLater($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereWallSide($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormParticularRule whereWater($value)
 * @mixin \Eloquent
 */
class FormParticularRule extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'official_name', 'official_position', 'client_name', 'reason', 'address_location', 'address_between',
        'intersection', 'key', 'sector', 'district', 'lot', 'square', 'applicant', 'sketch', 'via_address', 'via_width',
        'via_address2', 'via_width2', 'frontal', 'side', 'bottom', 'wall_side', 'wall_later', 'wall', 'type', 'cos', 'cus',
        'other_floor', 'land_use', 'number_floor', 'max_height', 'water', 'road', 'sewerage', 'curb', 'electric', 'pavement',
        'phone', 'situation_total', 'situation_partial', 'enclosure', 'factory_line', 'add_report',
        'document', 'incident_id', 'incident_form_id', 'support_id',
    ];

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getSketchUrlAttribute()
    {
        return Storage::disk('public')->url('images/sketches/'.$this->sketch);
    }

    public function getDateCompleteAttribute()
    {
        $month = $this->getMonthName(($this->created_at->format('n')) - 1);

        return 'San Miguel de Salcedo a, '.$this->created_at->format('d').' de '.$month.' del '.$this->created_at->format('Y');
    }

    public function getMonthName($key)
    {
        $months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

        return $months[$key];
    }
}
