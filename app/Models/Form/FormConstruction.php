<?php

namespace App\Models\Form;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Form\FormConstruction
 *
 * @property int $id
 * @property string|null $code
 * @property string $client_name
 * @property string $description
 * @property string $observation
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $date_complete
 * @property-read User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction whereObservation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormConstruction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FormConstruction extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'client_name', 'description', 'observation', 'document', 'incident_id', 'incident_form_id', 'support_id'
    ];

    protected $dates = [
        'created_at'
    ];

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getDateCompleteAttribute()
    {
        $month = $this->getMonthName(($this->created_at->format('n')) - 1);

        return 'San Miguel de Salcedo a, '.$this->created_at->format('d').' de '.$month.' del '.$this->created_at->format('Y');
    }

    public function getMonthName($key)
    {
        $months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

        return $months[$key];
    }
}
