<?php

namespace App\Models\Form;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Form\FormCadastre
 *
 * @property int $id
 * @property string|null $code
 * @property string $client_name
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $created_at_format
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Form\FormCadastreLocation> $locations
 * @property-read int|null $locations_count
 * @property-read User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastre whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FormCadastre extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'client_name', 'document', 'incident_id', 'incident_form_id', 'support_id'
    ];

    protected $dates = [
        'created_at'
    ];

    public function locations()
    {
        return $this->hasMany(FormCadastreLocation::class, 'form_cadastre_id');
    }

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getCreatedAtFormatAttribute()
    {
        $months = ["ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SETIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"];

        $month = $months[($this->created_at->format('n')) - 1];

        return 'SALCEDO A ' . $this->created_at->format('d').' DE '.$month.' DEL '.$this->created_at->format('Y');
    }
}
