<?php

namespace App\Models\Form;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Form\FormEstate
 *
 * @property int $id
 * @property string|null $code
 * @property string $client_name
 * @property string $year
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $date_complete
 * @property-read User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEstate whereYear($value)
 * @mixin \Eloquent
 */
class FormEstate extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'client_name', 'year', 'document', 'incident_id', 'incident_form_id', 'support_id'
    ];

    protected $dates = [
        'created_at'
    ];

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getDateCompleteAttribute()
    {
        $month = $this->getMonthName(($this->created_at->format('n')) - 1);

        return 'San Miguel de Salcedo a, '.$this->created_at->format('d').' de '.$month.' del '.$this->created_at->format('Y');
    }

    public function getMonthName($key)
    {
        $months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

        return $months[$key];
    }
}
