<?php

namespace App\Models\Form;

use App\Models\Incident;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Form\FormCadastralUpdate
 *
 * @property int $id
 * @property string|null $code
 * @property string $client_name
 * @property string $client_document
 * @property int $constancy
 * @property string $year
 * @property string $canton
 * @property string $parish
 * @property string $registration
 * @property string $estate
 * @property string $value
 * @property string $notary
 * @property string $amount
 * @property string $seller_name
 * @property string $seller_document
 * @property string $buyer_name
 * @property string $buyer_document
 * @property string $cadastral_canton
 * @property string $cadastral_parish
 * @property string $sector
 * @property string $property_name
 * @property string $inscription_date
 * @property string $total_area
 * @property string $total_sale
 * @property string $partial_sale
 * @property string $reserved_area
 * @property int $domain_limitation
 * @property string $north_sale
 * @property string $south_sale
 * @property string $west_sale
 * @property string $east_sale
 * @property string $north_reservation
 * @property string $south_reservation
 * @property string $west_reservation
 * @property string $east_reservation
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $date_complete
 * @property-read Incident $incident
 * @property-read User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereBuyerDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereBuyerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereCadastralCanton($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereCadastralParish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereCanton($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereClientDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereConstancy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereDomainLimitation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereEastReservation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereEastSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereEstate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereInscriptionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereNorthReservation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereNorthSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereNotary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereParish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate wherePartialSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate wherePropertyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereRegistration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereReservedArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereSector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereSellerDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereSellerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereSouthReservation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereSouthSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereTotalArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereTotalSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereWestReservation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereWestSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormCadastralUpdate whereYear($value)
 * @mixin \Eloquent
 */
class FormCadastralUpdate extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'client_name', 'client_document', 'constancy', 'year', 'canton', 'parish', 'registration', 'estate', 'value', 'notary',
        'amount', 'seller_name', 'seller_document', 'buyer_name', 'buyer_document', 'cadastral_canton', 'cadastral_parish',
        'sector', 'property_name', 'inscription_date', 'total_area', 'total_sale', 'partial_sale', 'reserved_area',
        'domain_limitation', 'north_sale', 'south_sale', 'west_sale', 'east_sale', 'north_reservation', 'south_reservation',
        'west_reservation', 'east_reservation', 'document', 'incident_id', 'incident_form_id', 'support_id'
    ];

    protected $dates = [
        'created_at'
    ];

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getDateCompleteAttribute()
    {
        $months = ["ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SETIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"];

        $month = $months[($this->created_at->format('n')) - 1];

        return 'Salcedo, ' . $this->created_at->format('d').' de '.$month.' del '.$this->created_at->format('Y');
    }
}
