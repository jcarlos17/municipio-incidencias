<?php

namespace App\Models\Form;

use App\Models\Incident;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Form\FormDomainTransfer
 *
 * @property int $id
 * @property string|null $code
 * @property string $notary
 * @property string $seller
 * @property string $buyer
 * @property string $parish
 * @property string $sector
 * @property string $street
 * @property string $old_key
 * @property string $current_key
 * @property string $old_property
 * @property string $perimeter
 * @property string $acquirer
 * @property string $acquired_date
 * @property string $property_extension
 * @property string $sale_extension
 * @property string $old_price
 * @property string $current_sale
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $client_name
 * @property-read mixed $date_complete
 * @property-read Incident $incident
 * @property-read User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereAcquiredDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereAcquirer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereBuyer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereCurrentKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereCurrentSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereNotary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereOldKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereOldPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereOldProperty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereParish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer wherePerimeter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer wherePropertyExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereSaleExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereSector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereSeller($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormDomainTransfer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FormDomainTransfer extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'notary', 'seller', 'buyer', 'parish', 'sector', 'street', 'old_key', 'current_key', 'old_property',
        'perimeter', 'acquirer', 'acquired_date', 'property_extension', 'sale_extension', 'old_price', 'current_sale',
        'document', 'incident_id', 'incident_form_id', 'support_id'
    ];

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getClientNameAttribute()
    {
        return $this->seller;
    }

    public function getDateCompleteAttribute()
    {
        $month = $this->getMonthName(($this->created_at->format('n')) - 1);

        return 'Salcedo a '.$this->created_at->format('d').' de '.$month.' del '.$this->created_at->format('Y');
    }

    public function getMonthName($key)
    {
        $months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

        return $months[$key];
    }
}
