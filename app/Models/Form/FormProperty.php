<?php

namespace App\Models\Form;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Form\FormProperty
 *
 * @property int $id
 * @property string|null $code
 * @property string $client_name
 * @property string $client_document
 * @property string $owner_name
 * @property string $owner_document
 * @property string $department
 * @property string $reason
 * @property string $street
 * @property string $district
 * @property string $sector
 * @property string $parish
 * @property string $observation
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $date_complete
 * @property-read User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereClientDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereObservation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereOwnerDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereOwnerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereParish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereSector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormProperty whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FormProperty extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'client_name', 'client_document', 'owner_name', 'owner_document', 'department', 'reason', 'street', 'district',
        'sector', 'parish', 'observation', 'document', 'incident_id', 'incident_form_id', 'support_id'
    ];

    protected $dates = [
        'created_at'
    ];

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getDateCompleteAttribute()
    {
        $month = $this->getMonthName(($this->created_at->format('n')) - 1);

        return 'San Miguel de Salcedo a, '.$this->created_at->format('d').' de '.$month.' del '.$this->created_at->format('Y');
    }

    public function getMonthName($key)
    {
        $months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

        return $months[$key];
    }
}
