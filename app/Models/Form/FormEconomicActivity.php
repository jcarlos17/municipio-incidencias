<?php

namespace App\Models\Form;

use App\Models\Incident;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Form\FormEconomicActivity
 *
 * @property int $id
 * @property string|null $code
 * @property string $client_name
 * @property string $client_document
 * @property string $ruc
 * @property string $business_name
 * @property string $initial_date
 * @property string $company_street
 * @property string $company_parish
 * @property string $company_sector
 * @property string $company_floor
 * @property string $legal_street
 * @property string $legal_parish
 * @property string $legal_sector
 * @property string $legal_floor
 * @property string $main
 * @property string $secondary
 * @property string $box
 * @property string $commodity
 * @property string $machinery
 * @property string $furniture
 * @property string $vehicles
 * @property int $local_type
 * @property string $local
 * @property string $other_assets
 * @property string $total_assets
 * @property string $debs_pay
 * @property string $dsts_pay
 * @property string $other_passives
 * @property string $total_passives
 * @property string $asset_passive
 * @property string $total
 * @property string|null $partner11
 * @property string|null $partner21
 * @property string|null $partner12
 * @property string|null $partner22
 * @property string|null $partner13
 * @property string|null $partner23
 * @property string|null $partner14
 * @property string|null $partner24
 * @property string|null $number_partner
 * @property string|null $partner25
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $initial_date_format
 * @property-read Incident $incident
 * @property-read User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereAssetPassive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereBox($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereBusinessName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereClientDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereCommodity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereCompanyFloor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereCompanyParish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereCompanySector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereCompanyStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereDebsPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereDstsPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereFurniture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereInitialDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereLegalFloor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereLegalParish($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereLegalSector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereLegalStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereLocal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereLocalType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereMachinery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereNumberPartner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereOtherAssets($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereOtherPassives($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity wherePartner11($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity wherePartner12($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity wherePartner13($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity wherePartner14($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity wherePartner21($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity wherePartner22($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity wherePartner23($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity wherePartner24($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity wherePartner25($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereRuc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereSecondary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereTotalAssets($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereTotalPassives($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormEconomicActivity whereVehicles($value)
 * @mixin \Eloquent
 */
class FormEconomicActivity extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'client_name', 'client_document', 'ruc', 'business_name', 'initial_date', 'company_street', 'company_parish',
        'company_sector', 'company_floor', 'legal_street', 'legal_parish', 'legal_sector', 'legal_floor', 'main',
        'secondary', 'box', 'commodity', 'machinery', 'furniture', 'vehicles', 'local_type', 'local', 'other_assets',
        'total_assets', 'debs_pay', 'dsts_pay', 'other_passives', 'total_passives', 'asset_passive', 'total',
        'partner11', 'partner21', 'partner12', 'partner22', 'partner13', 'partner23', 'partner14', 'partner24',
        'number_partner', 'partner25', 'document', 'incident_id', 'incident_form_id', 'support_id'
    ];

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getInitialDateFormatAttribute()
    {
        $date = new Carbon($this->initial_date);

        return $date->format('d/m/Y');
    }
}
