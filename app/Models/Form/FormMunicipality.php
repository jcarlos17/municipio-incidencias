<?php

namespace App\Models\Form;

use App\Models\Incident;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Form\FormMunicipality
 *
 * @property int $id
 * @property string|null $code
 * @property string $client_name
 * @property string $client_document
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $process_name
 * @property string|null $code_qr
 * @property string|null $document
 * @property int $incident_id
 * @property int $incident_form_id
 * @property int|null $support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $date_complete
 * @property-read Incident $incident
 * @property-read User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereClientDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereCodeQr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereIncidentFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereProcessName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormMunicipality whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FormMunicipality extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'client_name', 'client_document', 'address', 'email', 'phone', 'process_name', 'code_qr',
        'document', 'incident_id', 'incident_form_id', 'support_id'
    ];

    protected $dates = [
        'created_at'
    ];

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id');
    }

    public function getDateCompleteAttribute()
    {
        $month = $this->getMonthName(($this->created_at->format('n')) - 1);

        return 'Salcedo, a '.$this->created_at->format('d').' de '.$month.' del '.$this->created_at->format('Y');
    }

    public function getMonthName($key)
    {
        $months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

        return $months[$key];
    }
}
