<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transaction
 *
 * @property int $id
 * @property int $contribuyente
 * @property string $transaccion
 * @property string $fechaTx
 * @property string $horaTx
 * @property string $fPago
 * @property float $valorpTx
 * @property string $estadoTx
 * @property string $codigo_respuesta
 * @property string $mensaje_respuesta
 * @property string $fecConsiliacion
 * @property mixed|null $emisiondet
 * @property int $conciliated
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\CollectObligation> $collect_obligations
 * @property-read int|null $collect_obligations_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereCodigoRespuesta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereConciliated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereContribuyente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereEmisiondet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereEstadoTx($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereFPago($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereFecConsiliacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereFechaTx($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereHoraTx($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereMensajeRespuesta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereTransaccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereValorpTx($value)
 * @mixin \Eloquent
 */
class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'contribuyente', 'transaccion', 'fechaTx', 'horaTx', 'fPago', 'valorpTx', 'estadoTx',
        'codigo_respuesta', 'mensaje_respuesta', 'fecConsiliacion', 'emisiondet', 'user_id',
        'conciliated'
    ];

    const PAID = 'P';
    const CANCELED = 'A';
    const REVERSE = 'D';
    const VERIFICATION = 'V';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function collect_obligations()
    {
        return $this->belongsToMany(
            CollectObligation::class,
            'transaction_collect_obligations',
            'transaction_id',
            'collect_obligation_id'
        );
    }
}
