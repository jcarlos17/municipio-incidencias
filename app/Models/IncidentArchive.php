<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\IncidentArchive
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property int|null $archive_id
 * @property int $incident_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Archive|null $archive
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentArchive newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentArchive newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentArchive query()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentArchive whereArchiveId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentArchive whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentArchive whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentArchive whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentArchive whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentArchive whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentArchive whereUserId($value)
 * @mixin \Eloquent
 */
class IncidentArchive extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'user_id', 'archive_id', 'incident_id'
    ];

    public function archive()
    {
        return $this->belongsTo(Archive::class);
    }
}
