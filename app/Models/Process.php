<?php

namespace App\Models;

use App\Models\Admin\Income;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Process
 *
 * @property int $id
 * @property string $name
 * @property string|null $file
 * @property-read int|null $levels_count
 * @property int $department_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Department $department
 * @property-read mixed $first_level_id
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Income> $incomes
 * @property-read int|null $incomes_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Level> $levels
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Requirement> $requirements
 * @property-read int|null $requirements_count
 * @method static \Illuminate\Database\Eloquent\Builder|Process newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Process newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Process onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Process query()
 * @method static \Illuminate\Database\Eloquent\Builder|Process whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Process whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Process whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Process whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Process whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Process whereLevelsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Process whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Process whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Process withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Process withoutTrashed()
 * @mixin \Eloquent
 */
class Process extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name', 'file', 'levels_count', 'department_id'
    ];

    public function department()
    {
    	return $this->belongsTo(Department::class);
    }

    public function levels()
    {
        return $this->hasMany(Level::class);
    }

    public function requirements()
    {
        return $this->hasMany(Requirement::class);
    }

    public function archives()
    {
        return $this->requirements()->where('type', '=',0);
    }

    public function forms()
    {
        return $this->requirements()->where('type', '=', 1);
    }

    public function incomes()
    {
        return $this->hasMany(Income::class);
    }

    // accessors

    public function getFirstLevelIdAttribute()
    {
        return $this->levels()->first()->id;
    }
}
