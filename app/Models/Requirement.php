<?php

namespace App\Models;

use App\Models\Form\FormBlueprint;
use App\Models\Form\FormCadastralUpdate;
use App\Models\Form\FormCadastre;
use App\Models\Form\FormConstruction;
use App\Models\Form\FormCredit;
use App\Models\Form\FormDomainTransfer;
use App\Models\Form\FormEconomicActivity;
use App\Models\Form\FormEstate;
use App\Models\Form\FormMunicipality;
use App\Models\Form\FormParticularRule;
use App\Models\Form\FormProperty;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Requirement
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int $type
 * @property string|null $form_type
 * @property int $process_id
 * @property int $department_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement query()
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement whereFormType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement whereProcessId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Requirement withoutTrashed()
 * @mixin \Eloquent
 */
class Requirement extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name', 'description', 'type', 'form_type', 'process_id', 'department_id'
    ];

    public function form($id)
    {
        if ($this->form_type == 'certificado-no-adeudar-municipio') {

            return FormMunicipality::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();

        } else if ($this->form_type == 'solicitud-todo-tramite') {

            return Form::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();

        } else if ($this->form_type == 'certificado-avaluos-catastros') {

            return FormCadastre::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();

        } else if ($this->form_type == 'certificado-avaluos-actualizacion-catastral') {

            return FormCadastralUpdate::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();

        } else if ($this->form_type == 'normas-particulares') {

            return FormParticularRule::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();

        } else if ($this->form_type == 'permiso-construccion') {

            return FormConstruction::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();

        } else if ($this->form_type == 'solicitud-aprobacion-planos') {

            return FormBlueprint::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();

        } else if ($this->form_type == 'certificado-atencion-propiedad') {

            return FormProperty::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();

        } else if ($this->form_type == 'transferencia-dominio-compra-venta') {

            return FormDomainTransfer::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();

        } else if ($this->form_type == 'certificado-no-poseer-bienes') {

            return FormEstate::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();
        } else if ($this->form_type == 'actividad-economica') {

            return FormEconomicActivity::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();
        } else if ($this->form_type == 'emision-titulo-credito') {

            return FormCredit::where('incident_id', $id)
                ->where('requirement_id', $this->id)
                ->first();
        }
    }
}
