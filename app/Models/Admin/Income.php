<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Admin\Income
 *
 * @property int $id
 * @property string $name
 * @property int $income_group_id
 * @property int|null $process_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin\IncomeGroup $income_group
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Admin\Item> $items
 * @property-read int|null $items_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Admin\Obligation> $obligations
 * @property-read int|null $obligations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Income newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Income newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Income query()
 * @method static \Illuminate\Database\Eloquent\Builder|Income whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Income whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Income whereIncomeGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Income whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Income whereProcessId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Income whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Income extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'income_group_id', 'process_id'
    ];

    public function income_group()
    {
        return $this->belongsTo(IncomeGroup::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function obligations()
    {
        return $this->hasMany(Obligation::class);
    }
}
