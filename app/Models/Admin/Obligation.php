<?php

namespace App\Models\Admin;

use App\Models\CollectObligation;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Admin\Obligation
 *
 * @property int $id
 * @property string $code
 * @property int $income_id
 * @property int $client_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read User $client
 * @property-read \Illuminate\Database\Eloquent\Collection<int, CollectObligation> $collect_obligations
 * @property-read int|null $collect_obligations_count
 * @property-read \App\Models\Admin\Income $income
 * @method static \Illuminate\Database\Eloquent\Builder|Obligation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Obligation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Obligation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Obligation whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Obligation whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Obligation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Obligation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Obligation whereIncomeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Obligation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Obligation extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id', 'income_id', 'code'
    ];

    protected $with = [
        'client'
    ];

    public function client()
    {
        return $this->belongsTo(User::class, 'client_id')->withTrashed();
    }

    public function income()
    {
        return $this->belongsTo(Income::class);
    }

    public function collect_obligations()
    {
        return $this->hasMany(CollectObligation::class);
    }
}
