<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Admin\IncomeGroup
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeGroup whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncomeGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class IncomeGroup extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 'name'
    ];
}
