<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Department
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $start
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Incident> $incidents
 * @property-read int|null $incidents_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Level> $levels
 * @property-read int|null $levels_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Official> $officials
 * @property-read int|null $officials_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Process> $processes
 * @property-read int|null $processes_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Department withoutTrashed()
 * @mixin \Eloquent
 */
class Department extends Model
{
    use SoftDeletes;

    public static $rules = [
        'name' => 'required',
        // 'description' => '',
        'start' => ['nullable', 'date']
    ];

    public static $messages = [
        'name.required' => 'Es necesario ingresar un nombre para el departamento.',
        'start.date' => 'La fecha no tiene un formato adecuado.'
    ];

    protected $fillable = [
        'name', 'description', 'start'
    ];

    // relationships

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function incidents()
    {
        return $this->hasMany(Incident::class);
    }

    public function processes()
    {
        return $this->hasMany(Process::class);
    }

    public function levels()
    {
        return $this->hasMany(Level::class);
    }

    public function officials()
    {
        return $this->hasMany(Official::class);
    }
}
