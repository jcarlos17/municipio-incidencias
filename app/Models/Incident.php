<?php

namespace App\Models;

use App\Models\Admin\Obligation;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Incident
 *
 * @property int $id
 * @property string $description
 * @property string $severity
 * @property int $active
 * @property int|null $process_id
 * @property int|null $department_id
 * @property int|null $level_id
 * @property int|null $obligation_id
 * @property int $client_id
 * @property int|null $support_id
 * @property int $creator_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $date
 * @property int|null $correlative
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\IncidentArchive> $archives
 * @property-read int|null $archives_count
 * @property-read \App\Models\User $client
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\CollectObligation> $collect_obligations
 * @property-read int|null $collect_obligations_count
 * @property-read \App\Models\User $creator
 * @property-read \App\Models\Department|null $department
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\IncidentFile> $files
 * @property-read int|null $files_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Form> $forms
 * @property-read int|null $forms_count
 * @property-read mixed $code
 * @property-read mixed $document_name
 * @property-read mixed $first_level
 * @property-read mixed $last_level
 * @property-read mixed $process_name
 * @property-read mixed $severity_full
 * @property-read mixed $state
 * @property-read mixed $state_html
 * @property-read mixed $support_name
 * @property-read mixed $title_short
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\IncidentChange> $histories
 * @property-read int|null $histories_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\IncidentForm> $incident_forms
 * @property-read int|null $incident_forms_count
 * @property-read \App\Models\Level|null $level
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Message> $messages
 * @property-read int|null $messages_count
 * @property-read Obligation|null $obligation
 * @property-read \App\Models\Process|null $process
 * @property-read \App\Models\User|null $support
 * @method static \Illuminate\Database\Eloquent\Builder|Incident active()
 * @method static \Illuminate\Database\Eloquent\Builder|Incident assignedToMe($departmentId, $userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Incident newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Incident pendingToAttend($levelIds)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident query()
 * @method static \Illuminate\Database\Eloquent\Builder|Incident reportedByMe($userId, $departmentId)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereCorrelative($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereObligationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereProcessId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereSeverity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Incident whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Incident extends Model
{
    protected $appends = ['state'];

    // relationships

    public function process()
    {
    	return $this->belongsTo(Process::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function level()
    {
        return $this->belongsTo(Level::class);
    }

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id')->withTrashed();
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id')->withTrashed();
    }

    public function client()
    {
        return $this->belongsTo(User::class, 'client_id')->withTrashed();
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function files()
    {
        return $this->hasMany(IncidentFile::class);
    }

    public function histories()
    {
        return $this->hasMany(IncidentChange::class);
    }

    public function forms()
    {
        return $this->hasMany(Form::class);
    }

    public function obligation()
    {
        return $this->belongsTo(Obligation::class);
    }

    public function collect_obligations()
    {
        return $this->hasMany(CollectObligation::class);
    }

    public function incident_forms()
    {
        return $this->hasMany(IncidentForm::class);
    }

    public function archives()
    {
        return $this->hasMany(IncidentArchive::class);
    }

    // Scopes

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeAssignedToMe($query, $departmentId, $userId)
    {
        return $query->active()
            ->where('department_id', $departmentId)
            ->where('support_id', $userId);
    }

    public function scopePendingToAttend($query, $levelIds)
    {
        return $query->active()
//            ->where('department_id', $departmentId)
            ->whereNull('support_id')
            ->whereIn('level_id', $levelIds);
    }

    public function scopeReportedByMe($query, $userId, $departmentId)
    {
        return $query->active()
            ->where('creator_id', $userId)
            ->where('department_id', $departmentId);
    }

    // accessors

    public function getCodeAttribute()
    {
        return $this->date.'-'.$this->correlative;
    }

    public function getSeverityFullAttribute()
    {
    	switch ($this->severity) {
    		case 'M':
    			return 'Menor';

    		case 'N':
    			return 'Normal';

    		default:
    			return 'Alta';
    	}
    }

    public function getTitleShortAttribute()
    {
    	return mb_strimwidth($this->title, 0, 20, '...');
    }

    // process_name
    public function getProcessNameAttribute()
    {
        if ($this->process)
            return $this->process->name;

        return '';
    }

    // support_name
    public function getSupportNameAttribute()
    {
        if ($this->support)
            return $this->support->name;

        return 'Sin asignar';
    }

    public function getStateAttribute()
    {
        if ($this->active == 0)
            return 'Resuelto';

        if ($this->support_id)
            return 'Asignado';

        return 'Pendiente';
    }

    public function getStateHtmlAttribute()
    {
        if ($this->active == 0)
            return '<span class="badge badge-success">Resuelto</span>';

        if ($this->support_id)
            return '<span class="badge badge-info">Asignado</span>';

        return '<span class="badge badge-warning">Pendiente</span>';
    }

    public function getDocumentNameAttribute()
    {
        if (!$this->client_id) {
            return '';
        }

        return $this->client->document . ' ' . $this->client->name;
    }

    public function getLastLevelAttribute()
    {
        $process = $this->process;

        $lastLevel = $process->levels()
            ->orderByDesc('id')
            ->first(['id']);

        if (!$lastLevel) {
            return false;
        }

        return $this->level_id == $lastLevel->id;
    }

    public function getFirstLevelAttribute()
    {
        $process = $this->process;

        $firstLevel = $process->levels()
            ->orderBy('id')
            ->first(['id']);

        if (!$firstLevel) {
            return false;
        }

        return $this->level_id == $firstLevel->id;
    }
}
