<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\IncidentFile
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property int $incident_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentFile whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentFile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IncidentFile whereUserId($value)
 * @mixin \Eloquent
 */
class IncidentFile extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'user_id', 'incident_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }
}
