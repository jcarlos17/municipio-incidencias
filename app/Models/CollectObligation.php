<?php

namespace App\Models;

use App\Models\Admin\Income;
use App\Models\Admin\Obligation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CollectObligation
 *
 * @property int $id
 * @property string $issue_date
 * @property string|null $payment_date
 * @property string|null $entry_order
 * @property string|null $period
 * @property string|null $year
 * @property float|null $total
 * @property int $income_id
 * @property int $obligation_id
 * @property int|null $incident_id
 * @property int $user_id
 * @property int|null $cashier_id
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $payment_type
 * @property string $status
 * @property-read \App\Models\User|null $cashier
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\CollectObligationDetail> $details
 * @property-read int|null $details_count
 * @property-read mixed $status_text
 * @property-read \App\Models\Incident|null $incident
 * @property-read Income $income
 * @property-read Obligation $obligation
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation query()
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereCashierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereEntryOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereIncidentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereIncomeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereIssueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereObligationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation wherePaymentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation wherePaymentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CollectObligation whereYear($value)
 * @mixin \Eloquent
 */
class CollectObligation extends Model
{
    use HasFactory;

    protected $fillable = [
        'issue_date', 'payment_date', 'entry_order', 'period', 'year', 'total',
        'income_id', 'obligation_id', 'user_id', 'incident_id', 'cashier_id', 'payment_type',
        'status'
    ];

    // Status
    const DISCHARGED = 'B'; // Dado de baja
    const COLLECTED = 'R'; // Recaudado
    const ISSUED = 'E'; // Emitido
    const REVERSE = 'D'; // Reverso

    // Relationships
    public function obligation()
    {
        return $this->belongsTo(Obligation::class);
    }

    public function income()
    {
        return $this->belongsTo(Income::class);
    }

    public function details()
    {
        return $this->hasMany(CollectObligationDetail::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function cashier()
    {
        return $this->belongsTo(User::class, 'cashier_id')->withTrashed();
    }

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }

    // Attributes
    public function getTotalAttribute()
    {
        return $this->details()->sum('value');
    }

    public function getStatusTextAttribute()
    {
        $arr = [
            self::DISCHARGED => 'Dado de baja',
            self::COLLECTED => 'Recaudado',
            self::ISSUED => 'Emitido',
            self::REVERSE => 'Reverso'
        ];

        return $arr[$this->status];
    }
}
