<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\BoardController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ConsultationController;
use App\Http\Controllers\ArchiveController;
use App\Http\Controllers\Form\AllProcedureController;
use App\Http\Controllers\Form\BlueprintController;
use App\Http\Controllers\Form\CadastralUpdateController;
use App\Http\Controllers\Form\CadastreController;
use App\Http\Controllers\Form\ConstructionController;
use App\Http\Controllers\Form\CreditController;
use App\Http\Controllers\Form\DomainTransferController;
use App\Http\Controllers\Form\EconomicActivityController;
use App\Http\Controllers\Form\EstateController;
use App\Http\Controllers\Form\FormController;
use App\Http\Controllers\Form\MunicipalityController;
use App\Http\Controllers\Form\ParticularRuleController;
use App\Http\Controllers\Form\PropertyController;
use App\Http\Controllers\IncidentObligationController;
use App\Http\Controllers\IncidentPaymentController;
use App\Http\Controllers\TaxController;
use App\Http\Controllers\WebServiceController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/instrucciones', function () {
    return view('instructions');
});

Route::get('tablero', [BoardController::class, 'index']);
Route::get('consulta/tramite', [ConsultationController::class, 'procedure']);
Route::get('consulta/guia-tramite', [ConsultationController::class, 'guide']);
Route::get('download-pdf/{process}', [ConsultationController::class, 'download']);
Route::get('print-requirements-pdf/{process}', [ConsultationController::class, 'requirementPrint']);
Route::get('impuestos-tasas-por-pagar', [TaxController::class, 'payable']);


Route::get('/acerca-de', function () {
    return view('credits');
});

// Store comments
Route::post('comments', [CommentController::class, 'store']);

// Authentication Routes...
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout'])->name('logout');
// Registration Routes...
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');
// Password Reset Routes...
Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::post('password/reset', [ResetPasswordController::class, 'reset']);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/seleccionar/departamento/{id}', 'HomeController@selectDepartment');

Route::group(['middleware' => 'auth', 'namespace' => 'User'], function (){
    Route::post('/profile/image', 'ProfileController@postImage');
});

Route::group(['middleware' => 'support', 'namespace' => 'Support'], function (){
//clients
    Route::get('/cliente', 'ClientController@index');
    Route::get('/cliente/crear', 'ClientController@create');
    Route::post('/cliente/crear', 'ClientController@store');
    Route::get('/cliente/{id}', 'ClientController@edit');
    Route::post('/cliente/{id}', 'ClientController@update');

    Route::get('/cliente/{id}/eliminar', 'ClientController@delete');

    // Assigned incidents
    Route::get('assigned-incidents', 'IncidentController@assigned');
    // Pending incidents
    Route::get('pending-incidents', 'IncidentController@pending');
    // Reported incidents
    Route::get('reported-incidents', 'IncidentController@reported');
    //Incidents
    Route::get('/incidents', 'IncidentController@index');
    Route::get('/incidents/{id}', 'IncidentController@show');
    // Incident charges
    Route::group(['prefix' => 'incidents/{incident}/charges'], function () {
        Route::get('', 'IncidentChargeController@index');
        Route::get('create', 'IncidentChargeController@create');
        Route::post('create', 'IncidentChargeController@store');
        Route::get('{collectObligation}/show', 'IncidentChargeController@show');
        Route::get('{collectObligation}/reverse', 'IncidentChargeController@reverse');
    });

});

// Incident

Route::get('/reportar', 'IncidentController@create');
Route::post('/reportar', 'IncidentController@store');

Route::get('/incidencia/{incident}/editar', 'IncidentController@edit');
Route::post('/incidencia/{incident}/editar', 'IncidentController@update');

Route::get('/ver/{id}', 'IncidentController@show');
Route::get('/guia/{id}', 'IncidentController@guide');

Route::get('/vista/{incident}', 'IncidentController@preview');

Route::group(['prefix' => 'incidencia'], function () {
    Route::group(['prefix' => '{incident}'], function () {
        Route::get('atender', 'IncidentController@take');
        Route::get('resolver', 'IncidentController@solve');
        Route::get('abrir', 'IncidentController@open');
        Route::get('derivar/{option}', 'IncidentController@level');
        // collect obligations
        Route::get('pago', [IncidentObligationController::class, 'index']);
        Route::get('pago/nuevo', [IncidentObligationController::class, 'create']);
        Route::post('pago/nuevo', [IncidentObligationController::class, 'store']);

        // Payment for client
        Route::get('pagar', [IncidentPaymentController::class, 'create']);
        Route::post('pagar', [IncidentPaymentController::class, 'store']);
    });
    Route::group(['prefix' => 'pago'], function () {
        // collect obligations
        Route::get('{collectObligation}/ver', [IncidentObligationController::class, 'show']);
        Route::get('{collectObligation}/editar', [IncidentObligationController::class, 'edit']);
        Route::post('{collectObligation}/editar', [IncidentObligationController::class, 'update']);
        Route::get('{collectObligation}/eliminar', [IncidentObligationController::class, 'delete']);
    });
});

Route::get('download/{incidentFile}', 'IncidentController@download');
Route::post('upload/{id}', 'IncidentController@upload');

// Forms
Route::group(['prefix' => 'incidencia/{incident}/formularios'], function () {
    Route::get('', [FormController::class, 'index'])->name('form.index');
    Route::get('agregar', [FormController::class, 'add']);
    Route::post('agregar', [FormController::class, 'updateAdd']);
});

Route::get('incidencia/{incidentForm}/certificado-no-adeudar-municipio/crear', [MunicipalityController::class, 'create'])->name('certificado-no-adeudar-municipio.create');
Route::post('incidencia/{incidentForm}/certificado-no-adeudar-municipio/crear', [MunicipalityController::class, 'store']);
Route::group(['prefix' => 'certificado-no-adeudar-municipio/{form}'], function () {
    Route::get('editar', [MunicipalityController::class, 'edit'])->name('certificado-no-adeudar-municipio.edit');
    Route::post('editar', [MunicipalityController::class, 'update']);
    Route::get('descargar', [MunicipalityController::class, 'download'])->name('certificado-no-adeudar-municipio.download');
    Route::post('subir', [MunicipalityController::class, 'upload'])->name('certificado-no-adeudar-municipio.upload');
    Route::get('descargar-final', [MunicipalityController::class, 'finalDownload'])->name('certificado-no-adeudar-municipio.final_download');
});

Route::get('incidencia/{incidentForm}/certificado-avaluos-catastros/crear', [CadastreController::class, 'create'])->name('certificado-avaluos-catastros.create');
Route::group(['prefix' => 'certificado-avaluos-catastros/{form}'], function () {
    Route::get('editar', [CadastreController::class, 'edit'])->name('certificado-avaluos-catastros.edit');
    Route::get('descargar', [CadastreController::class, 'download'])->name('certificado-avaluos-catastros.download');
    Route::post('subir', [CadastreController::class, 'upload'])->name('certificado-avaluos-catastros.upload');
    Route::get('descargar-final', [CadastreController::class, 'finalDownload'])->name('certificado-avaluos-catastros.final_download');
});
Route::post('ubicaciones/crear', [CadastreController::class, 'addLocation']);
Route::post('ubicaciones/editar', [CadastreController::class, 'updateLocation']);

Route::get('incidencia/{incidentForm}/certificado-avaluos-actualizacion-catastral/crear', [CadastralUpdateController::class, 'create'])->name('certificado-avaluos-actualizacion-catastral.create');
Route::post('incidencia/{incidentForm}/certificado-avaluos-actualizacion-catastral/crear', [CadastralUpdateController::class, 'store']);
Route::group(['prefix' => 'certificado-avaluos-actualizacion-catastral/{form}'], function () {
    Route::get('editar', [CadastralUpdateController::class, 'edit'])->name('certificado-avaluos-actualizacion-catastral.edit');
    Route::post('editar', [CadastralUpdateController::class, 'update']);
    Route::get('descargar', [CadastralUpdateController::class, 'download'])->name('certificado-avaluos-actualizacion-catastral.download');
    Route::post('subir', [CadastralUpdateController::class, 'upload'])->name('certificado-avaluos-actualizacion-catastral.upload');
    Route::get('descargar-final', [CadastralUpdateController::class, 'finalDownload'])->name('certificado-avaluos-actualizacion-catastral.final_download');
});

Route::get('incidencia/{incidentForm}/normas-particulares/crear', [ParticularRuleController::class, 'create'])->name('normas-particulares.create');
Route::post('incidencia/{incidentForm}/normas-particulares/crear', [ParticularRuleController::class, 'store']);
Route::group(['prefix' => 'normas-particulares/{form}'], function () {
    Route::get('editar', [ParticularRuleController::class, 'edit'])->name('normas-particulares.edit');
    Route::post('editar', [ParticularRuleController::class, 'update']);
    Route::get('descargar', [ParticularRuleController::class, 'download'])->name('normas-particulares.download');
    Route::post('subir', [ParticularRuleController::class, 'upload'])->name('normas-particulares.upload');
    Route::get('descargar-final', [ParticularRuleController::class, 'finalDownload'])->name('normas-particulares.final_download');
});

Route::get('incidencia/{incidentForm}/permiso-construccion/crear', [ConstructionController::class, 'create'])->name('permiso-construccion.create');
Route::post('incidencia/{incidentForm}/permiso-construccion/crear', [ConstructionController::class, 'store']);
Route::group(['prefix' => 'permiso-construccion/{form}'], function () {
    Route::get('editar', [ConstructionController::class, 'edit'])->name('permiso-construccion.edit');
    Route::post('editar', [ConstructionController::class, 'update']);
    Route::get('descargar', [ConstructionController::class, 'download'])->name('permiso-construccion.download');
    Route::post('subir', [ConstructionController::class, 'upload'])->name('permiso-construccion.upload');
    Route::get('descargar-final', [ConstructionController::class, 'finalDownload'])->name('permiso-construccion.final_download');
});

Route::get('incidencia/{incidentForm}/solicitud-aprobacion-planos/crear', [BlueprintController::class, 'create'])->name('solicitud-aprobacion-planos.create');
Route::post('incidencia/{incidentForm}/solicitud-aprobacion-planos/crear', [BlueprintController::class, 'store']);
Route::group(['prefix' => 'solicitud-aprobacion-planos/{form}'], function () {
    Route::get('editar', [BlueprintController::class, 'edit'])->name('solicitud-aprobacion-planos.edit');
    Route::post('editar', [BlueprintController::class, 'update']);
    Route::get('descargar', [BlueprintController::class, 'download'])->name('solicitud-aprobacion-planos.download');
    Route::post('subir', [BlueprintController::class, 'upload'])->name('solicitud-aprobacion-planos.upload');
    Route::get('descargar-final', [BlueprintController::class, 'finalDownload'])->name('solicitud-aprobacion-planos.final_download');
});

Route::get('incidencia/{incidentForm}/certificado-atencion-propiedad/crear', [PropertyController::class, 'create'])->name('certificado-atencion-propiedad.create');
Route::post('incidencia/{incidentForm}/certificado-atencion-propiedad/crear', [PropertyController::class, 'store']);
Route::group(['prefix' => 'certificado-atencion-propiedad'], function () {
    Route::get('{form}/editar', [PropertyController::class, 'edit'])->name('certificado-atencion-propiedad.edit');
    Route::post('{form}/editar', [PropertyController::class, 'update']);
    Route::get('{form}/descargar', [PropertyController::class, 'download'])->name('certificado-atencion-propiedad.download');
    Route::post('{form}/subir', [PropertyController::class, 'upload'])->name('certificado-atencion-propiedad.upload');
    Route::get('{form}/descargar-final', [PropertyController::class, 'finalDownload'])->name('certificado-atencion-propiedad.final_download');
});

Route::get('incidencia/{incidentForm}/transferencia-dominio-compra-venta/crear', [DomainTransferController::class, 'create'])->name('transferencia-dominio-compra-venta.create');
Route::post('incidencia/{incidentForm}/transferencia-dominio-compra-venta/crear', [DomainTransferController::class, 'store']);
Route::group(['prefix' => 'transferencia-dominio-compra-venta'], function () {
    Route::get('{form}/editar', [DomainTransferController::class, 'edit'])->name('transferencia-dominio-compra-venta.edit');
    Route::post('{form}/editar', [DomainTransferController::class, 'update']);
    Route::get('{form}/descargar', [DomainTransferController::class, 'download'])->name('transferencia-dominio-compra-venta.download');
    Route::post('{form}/subir', [DomainTransferController::class, 'upload'])->name('transferencia-dominio-compra-venta.upload');
    Route::get('{form}/descargar-final', [DomainTransferController::class, 'finalDownload'])->name('transferencia-dominio-compra-venta.final_download');
});

Route::get('incidencia/{incidentForm}/certificado-no-poseer-bienes/crear', [EstateController::class, 'create'])->name('certificado-no-poseer-bienes.create');
Route::post('incidencia/{incidentForm}/certificado-no-poseer-bienes/crear', [EstateController::class, 'store']);
Route::group(['prefix' => 'certificado-no-poseer-bienes'], function () {
    Route::get('{form}/editar', [EstateController::class, 'edit'])->name('certificado-no-poseer-bienes.edit');
    Route::post('{form}/editar', [EstateController::class, 'update']);
    Route::get('{form}/descargar', [EstateController::class, 'download'])->name('certificado-no-poseer-bienes.download');
    Route::post('{form}/subir', [EstateController::class, 'upload'])->name('certificado-no-poseer-bienes.upload');
    Route::get('{form}/descargar-final', [EstateController::class, 'finalDownload'])->name('certificado-no-poseer-bienes.final_download');
});

Route::get('incidencia/{incidentForm}/actividad-economica/crear', [EconomicActivityController::class, 'create'])->name('actividad-economica.create');
Route::post('incidencia/{incidentForm}/actividad-economica/crear', [EconomicActivityController::class, 'store']);
Route::group(['prefix' => 'actividad-economica'], function () {
    Route::get('{form}/editar', [EconomicActivityController::class, 'edit'])->name('actividad-economica.edit');
    Route::post('{form}/editar', [EconomicActivityController::class, 'update']);
    Route::get('{form}/descargar', [EconomicActivityController::class, 'download'])->name('actividad-economica.download');
    Route::post('{form}/subir', [EconomicActivityController::class, 'upload'])->name('actividad-economica.upload');
    Route::get('{form}/descargar-final', [EconomicActivityController::class, 'finalDownload'])->name('actividad-economica.final_download');
});

Route::get('incidencia/{incidentForm}/emision-titulo-credito/crear', [CreditController::class, 'create'])->name('emision-titulo-credito.create');
Route::post('incidencia/{incidentForm}/emision-titulo-credito/crear', [CreditController::class, 'store']);
Route::group(['prefix' => 'emision-titulo-credito'], function () {
    Route::get('{form}/editar', [CreditController::class, 'edit'])->name('emision-titulo-credito.edit');
    Route::post('{form}/editar', [CreditController::class, 'update']);
    Route::get('{form}/descargar', [CreditController::class, 'download'])->name('emision-titulo-credito.download');
    Route::post('{form}/subir', [CreditController::class, 'upload'])->name('emision-titulo-credito.upload');
    Route::get('{form}/descargar-final', [CreditController::class, 'finalDownload'])->name('emision-titulo-credito.final_download');
});

// SOL - IOC
Route::get('incidencia/{incidentForm}/{type}/crear', [AllProcedureController::class, 'create'])->name('incident_forms.create');
Route::post('incidencia/{incidentForm}/{type}/crear', [AllProcedureController::class, 'store']);


Route::group(['prefix' => 'solicitud-todo-tramite/{form}'], function () {
    Route::get('editar', [AllProcedureController::class, 'edit'])->name('solicitud-todo-tramite.edit');
    Route::post('editar', [AllProcedureController::class, 'update']);
    Route::get('descargar', [AllProcedureController::class, 'download'])->name('solicitud-todo-tramite.download');
    Route::post('subir', [AllProcedureController::class, 'upload'])->name('solicitud-todo-tramite.upload');
    Route::get('descargar-final', [AllProcedureController::class, 'finalDownload'])->name('solicitud-todo-tramite.final_download');
});

Route::group(['prefix' => 'informe-certificado'], function () {
    Route::get('{form}/editar', [AllProcedureController::class, 'edit'])->name('informe-certificado.edit');
    Route::post('{form}/editar', [AllProcedureController::class, 'update']);
    Route::get('{form}/descargar', [AllProcedureController::class, 'download'])->name('informe-certificado.download');
    Route::post('{form}/subir', [AllProcedureController::class, 'upload'])->name('informe-certificado.upload');
    Route::get('{form}/descargar-final', [AllProcedureController::class, 'finalDownload'])->name('informe-certificado.final_download');
});

// Message
Route::post('/mensajes', 'MessageController@store');

// Files
Route::post('/archivo/cargar', [ArchiveController::class, 'upload']);
Route::get('/archivo/{archive}/descargar', [ArchiveController::class, 'download']);

// Web services
Route::group(['prefix' => 'Transbank/rest/transf'], function () {
    Route::get('consulta/{document}', [WebServiceController::class, 'consultation']);
    Route::post('pago', [WebServiceController::class, 'payment']);
    Route::post('reverso', [WebServiceController::class, 'reverse']);
    Route::post('confirm', [WebServiceController::class, 'confirm']);
});
