<?php

use App\Http\Controllers\Admin\CommentController;
use App\Http\Controllers\Admin\ObligationController;
use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\DepartmentUserController;
use App\Http\Controllers\Admin\IncidentController;
use App\Http\Controllers\Admin\LevelController;
use App\Http\Controllers\Admin\OfficialController;
use App\Http\Controllers\Admin\ChargeController;
use App\Http\Controllers\Admin\ProcessController;
use App\Http\Controllers\Admin\IncomeController;
use App\Http\Controllers\Admin\RequirementController;
use App\Http\Controllers\Admin\ItemController;
use App\Http\Controllers\Admin\CollectObligationController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth', 'admin']], function () {

    //Incidents
    Route::get('/incidencias', [IncidentController::class, 'index']);
    Route::get('/incidencia/{id}', [IncidentController::class, 'show']);
    // User
    Route::group(['prefix' => 'users'], function () {
        Route::get('', [UserController::class, 'index']);
        Route::get('create', [UserController::class, 'create']);
        Route::post('create', [UserController::class, 'store']);
        Route::get('{id}/edit', [UserController::class, 'edit']);
        Route::post('{id}/edit', [UserController::class, 'update']);

        Route::get('{id}/delete', [UserController::class, 'delete']);
    });

    // Income
    Route::group(['prefix' => 'incomes'], function () {
        Route::get('', [IncomeController::class, 'index']);
        Route::get('create', [IncomeController::class, 'create']);
        Route::post('create', [IncomeController::class, 'store']);
        Route::get('{income}/edit', [IncomeController::class, 'edit']);
        Route::post('{income}/edit', [IncomeController::class, 'update']);

        Route::get('{income}/delete', [IncomeController::class, 'delete']);

        // Item
        Route::group(['prefix' => '{income}/items'], function () {
            Route::get('', [ItemController::class, 'index']);
            Route::get('create', [ItemController::class, 'create']);
            Route::post('create', [ItemController::class, 'store']);
        });
    });

    // Item
    Route::group(['prefix' => 'items'], function () {
        Route::get('{item}/edit', [ItemController::class, 'edit']);
        Route::post('{item}/edit', [ItemController::class, 'update']);

        Route::get('{item}/delete', [ItemController::class, 'delete']);
    });

    // Obligation
    Route::group(['prefix' => 'obligations'], function () {
        Route::get('', [ObligationController::class, 'index']);
        Route::get('create', [ObligationController::class, 'create']);
        Route::post('create', [ObligationController::class, 'store']);
        Route::get('{obligation}/edit', [ObligationController::class, 'edit']);
        Route::post('{obligation}/edit', [ObligationController::class, 'update']);

        Route::get('{obligation}/delete', [ObligationController::class, 'delete']);
    });

    // Collect Obligations
    Route::group(['prefix' => 'collect_obligations'], function () {
        Route::get('', [CollectObligationController::class, 'index']);
        Route::get('create', [CollectObligationController::class, 'create']);
        Route::post('create', [CollectObligationController::class, 'store']);
        Route::get('{collectObligation}/edit', [CollectObligationController::class, 'edit']);
        Route::post('{collectObligation}/edit', [CollectObligationController::class, 'update']);

        Route::get('{collectObligation}/delete', [CollectObligationController::class, 'delete']);
        Route::get('{collectObligation}/show', [CollectObligationController::class, 'show']);

        Route::get('download', [CollectObligationController::class, 'download']);
    });

    // Department
    Route::group(['prefix' => 'departments'], function () {
        Route::get('', [DepartmentController::class, 'index']);
        Route::get('create', [DepartmentController::class, 'create']);
        Route::post('create', [DepartmentController::class, 'store']);
        Route::get('{department}/edit', [DepartmentController::class, 'edit']);
        Route::post('{department}/edit', [DepartmentController::class, 'update']);
        Route::get('{department}/delete', [DepartmentController::class, 'delete']);
        Route::get('{department}/restore', [DepartmentController::class, 'restore']);
    });

    // Process
    Route::group(['prefix' => 'processes'], function () {
        Route::get('', [ProcessController::class, 'index']);
        Route::get('create', [ProcessController::class, 'create']);
        Route::post('create', [ProcessController::class, 'store']);
        Route::get('{process}/edit', [ProcessController::class, 'edit']);
        Route::post('{process}/edit', [ProcessController::class, 'update']);
        Route::get('{process}/delete', [ProcessController::class, 'delete']);

        Route::get('{process}/show', [ProcessController::class, 'show']);
    });
    // Level
    Route::post('/niveles', [LevelController::class, 'store']);
    Route::post('/nivel/editar', [LevelController::class, 'update']);
    Route::get('/nivel/{id}/eliminar', [LevelController::class, 'delete']);
    // Requirement
    Route::post('requisitos', [RequirementController::class, 'store']);
    Route::post('/requisito/editar', [RequirementController::class, 'update']);
    Route::get('/requisito/{requirement}/eliminar', [RequirementController::class, 'delete']);

    // Department-User
    Route::post('/departamento-usuario', [DepartmentUserController::class, 'store']);
    Route::get('/departamento-usuario/{id}/eliminar', [DepartmentUserController::class, 'delete']);

    // Department
    Route::group(['prefix' => 'officials'], function () {
        Route::get('', [OfficialController::class, 'index']);
        Route::get('create', [OfficialController::class, 'create']);
        Route::post('create', [OfficialController::class, 'store']);
        Route::get('{official}/edit', [OfficialController::class, 'edit']);
        Route::post('{official}/edit', [OfficialController::class, 'update']);
        Route::get('{official}/delete', [OfficialController::class, 'delete']);
    });

    // Comment
    Route::group(['prefix' => 'comments'], function () {
        Route::get('', [CommentController::class, 'index']);
        Route::get('{comment}/show', [CommentController::class, 'show']);
    });

    // Route::get('/config', 'ConfigController@index');
});

// Charges
Route::group(['middleware' => ['auth', 'cashier'], 'prefix' => 'charges'], function () {
    Route::get('', [ChargeController::class, 'index']);
    Route::get('create', [ChargeController::class, 'create']);
    Route::post('create', [ChargeController::class, 'store']);
    Route::get('{collectObligation}/show', [ChargeController::class, 'show']);
    Route::get('{collectObligation}/reverse', [ChargeController::class, 'reverse']);
});
