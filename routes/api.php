<?php

use App\Http\Controllers\Api\IncomeController;
use App\Http\Controllers\Api\ItemController;
use Illuminate\Support\Facades\Route;

Route::post('/login', 'Api\AuthController@login');

// Android API
Route::get('/departments', 'Api\DepartmentController@all');
Route::get('/department/processes', 'Api\ProcessController@byDepartent');

// Android API - New incident
Route::post('/incidents', 'Api\IncidentController@store');

// Android API - Reports
Route::get('/incidents/state', 'Api\IncidentController@stateCount');
Route::get('/departments/incident', 'Api\DepartmentController@incidentCount');
Route::get('/supports/incident', 'Api\SupportController@incidentCount');

// Web API
Route::get('/processes/{department}', 'Api\DepartmentController@processes');
Route::get('/levels/{process}', 'Api\ProcessController@levels');
Route::get('/archives/{process}', 'Api\ProcessController@archives');
Route::get('clients', 'Api\ClientController@search');
Route::get('obligation/clients', 'Api\ClientController@clients');
Route::get('collect_obligations/{id}', 'Api\ClientController@collectObligation');
Route::get('collect_obligation/{collectObligation}/items', 'Api\ClientController@items');

Route::get('incomes/{user}', [IncomeController::class, 'byClient']);
Route::get('items/{incomeId}', [ItemController::class, 'byIncome']);

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\AuthController@login');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');
    });
});

Route::group([
    'middleware' => 'auth:api'
], function() {
    // Sales Limits
    Route::get('incidencias', 'Api\IncidentController@lists');
    Route::get('incidencias/{id}', 'Api\IncidentController@show');
});
