<div class="card">
    <div class="card-header">
        <h3 class="card-title">Comentarios</h3>
    </div>
    <div class="card-body">
        @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissible alert-alt fade show">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="bootstrap-media">
            <ul class="list-unstyled">
                @foreach($messages as $message)
                <li class="media my-4">
                    <img class="mr-3 rounded" width="60" src="{{ $message->user->avatar_path }}" alt="{{ $message->user->name }}">
                    <div class="media-body">
                        <h5 class="mt-0">{{ $message->user->name }} | {{ $message->created_at }}</h5>
                        <p class="mb-0">{{ $message->message }}</p>
                        @if(!$message->is_sms)
                            <small  class="text-muted">Enviado como SMS</small>
                        @endif
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    @if ($incident->state == 'Asignado')
    <div class="card-footer">
        <form action="/mensajes" method="POST">
            @csrf
            <input type="hidden" name="incident_id" value="{{ $incident->id }}">
            @if(auth()->user()->is(\App\Models\User::SUPPORT))
            <label>
                <input type="checkbox" name="is_sms" @if($incident->client->cellphone == NULL) disabled @endif>
                <small @if($incident->client->cellphone == NULL) class="text-muted" title="No se ha registrado el celular de este cliente" @endif>Enviar por SMS</small>
            </label>
            @endif
            <div class="input-group">
                <input type="text" class="form-control" name="message">
                <span class="input-group-btn">
                    <button class="btn btn-light" type="submit">Enviar</button>
                </span>
            </div>
        </form>
    </div>
    @endif
</div>
