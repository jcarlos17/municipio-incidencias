@extends('layouts.app')

<!-- Main Content -->
@section('content')
    <div class="h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                @if (session('status'))
                                    <div class="alert alert-success alert-dismissible alert-alt fade show">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="auth-form">
                                    <h4 class="text-center mb-4">Restablecer contraseña</h4>
                                    <form role="form" method="POST" action="{{ url('/password/email') }}">
                                        @csrf
                                        <div class="form-group @error('document') input-danger @enderror">
                                            <label class="mb-1" for="email">
                                                <strong>E-mail</strong>
                                            </label>
                                            <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}" required>
                                            @error('email')
                                            <span class="text-danger">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary btn-block">Enviar</button>
                                        </div>
                                    </form>
                                    <div class="new-account mt-3">
                                        <p>¿Recordaste la contraseña? <a class="text-primary" href="{{ route('login') }}">Iniciar sesión</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
