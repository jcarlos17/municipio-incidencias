@extends('layouts.app')

@section('content')
    <div class="h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4">Iniciar sesión</h4>
                                    <form role="form" method="POST" action="{{ url('/login') }}">
                                        @csrf
                                        <div class="form-group @error('document') input-danger @enderror">
                                            <label class="mb-1" for="document">
                                                <strong>Cédula</strong>
                                            </label>
                                            <input type="text" id="document" name="document" class="form-control" value="{{ old('document') }}" required>
                                            @error('document')
                                                <span class="text-danger">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group @error('password') input-danger @enderror">
                                            <label class="mb-1" for="password">
                                                <strong>Contraseña</strong>
                                            </label>
                                            <input type="password" class="form-control" id="password" name="password" required>
                                            @error('password')
                                            <span class="text-danger">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox ml-1">
                                                    <input type="checkbox" class="custom-control-input" id="basic_checkbox_1">
                                                    <label class="custom-control-label" for="basic_checkbox_1">Recordar sesión</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <a href="{{ url('password/reset') }}">Olvidaste tu contraseña?</a>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary btn-block">Iniciar sesión</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
