@extends('layouts.app')

@section('styles')

@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Guía de trámites</h4>
        </div>
        <div class="card-body">
            <div class="basic-form mt-4">
                <form method="GET">

                    <div class="input-group mb-3">
                        <select name="departmentId" class="form-control">
                            <option value="">Seleccionar departmento</option>
                            @foreach ($departments as $department)
                                <option value="{{ $department->id }}"
                                {{ $departmentId == $department->id ? 'selected' : '' }}>
                                    {{ $department->name }}
                                </option>
                            @endforeach
                        </select>
                        <input type="text" class="form-control" name="requestName" placeholder="Buscar por nombre" value="{{ $requestName }}">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="table-responsive mt-4">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>N°</th>
                        <th>Nombre</th>
                        <th class="text-center">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($processes as $process)
                        <tr>
                            <td>{{ $process->id }}</td>
                            <td>{{ $process->name }}</td>
                            <td class="col-sm-2 text-center">
                                @if($process->file)
                                    <a href="{{ url('download-pdf/'.$process->id) }}" class="btn btn-sm btn-danger" title="Descargar PDF">
                                        <span class="fa fa-file-pdf-o"></span>
                                    </a>
                                @else
                                    <button class="btn btn-sm btn-danger" disabled><span class="fa fa-file-pdf-o"></span></button>
                                @endif
                                    <a href="{{ url('print-requirements-pdf/'.$process->id) }}" target="_blank" class="btn btn-sm btn-primary" title="Imprimir requisitos">
                                        <span class="fa fa-print"></span>
                                    </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $processes->appends(Request::except('page'))->links('includes.paginate') }}
            </div>
        </div>
    </div>
@endsection
