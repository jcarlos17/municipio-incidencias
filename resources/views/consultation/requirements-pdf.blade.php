<html>
<head>
    <title>Requisitos-{{ $process->id }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 3em 0 0 0; }
        body {
            font-family: Arial, serif;
            margin: 0;
            font-size: 14px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: 1px solid black;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: center;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: center;
            padding: 4px;
        }

        .col-sm-4 {
            float: left;
            width: 33.3%;
        }
        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 80px;
        }

        .page_break { page-break-before: always; }
    </style>
</head>
<body>
<div style="width: 95%; margin: 0 auto;">
    <table style="border: none !important; width: 90%; margin: 0 auto;">
        <tr style="border: none !important;">
            <td style="border: none !important; text-align: center; position: relative">
                <img style="position: absolute" src="{{ asset('images/home.png') }}" alt="Logo" width="80px">
                <p style="font-weight: bold">G.A.D. MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p>TRÁMITES INSTITUCIONALES</p>
                <p>REQUISITOS</p>
            </td>
        </tr>
    </table>

    <p style="text-transform: uppercase">NOMBRE DEL TRÁMITE: {{ $process->name }}</p>
    <table>
        <thead>
        <tr>
            <th style="width: 5%">#</th>
            <th style="text-align: left !important;">Requisito</th>
        </tr>
        </thead>
        <tbody>
            @foreach($process->archives as $key => $archive)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td style="text-align: left !important;">{{ $archive->name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
