@extends('layouts.app')

@section('styles')

@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Trámite</h4>
        </div>
        <div class="card-body">
            @if($procedureInput && $notification)
                <div class="alert alert-danger alert-dismissible alert-alt fade show">
                   {{ $notification }}
                </div>
            @endif
            <h6>Conozca el estado de sus trámites</h6>
            <p>Para consultar el estado de su trámite siga los siguientes pasos</p>
            <ul>
                <li>Ingrese el número de trámite (se desplegará todos los datos relacionados al trámite)</li>
                <li>Presione consultar</li>
            </ul>
            <div class="basic-form mt-4">
                <form method="GET">

                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="procedureInput" placeholder="Nro trámite" value="{{ $procedureInput }}">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if($incident)
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Incidencia</h4>
            </div>

            <div class="card-body">
                @if (session('notification'))
                    <div class="alert alert-success alert-dismissible alert-alt fade show">
                        {{ session('notification') }}
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Departamento</th>
                            <th>Proceso</th>
                            <th>Fecha de envío</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td id="incident_key">{{ $incident->code }}</td>
                            <td id="incident_department">{{ $incident->department->name }}</td>
                            <td id="incident_process">{{ $incident->process_name }}</td>
                            <td id="incident_created_at">{{ $incident->created_at }}</td>
                        </tr>
                        </tbody>
                        <thead>
                        <tr>
                            <th>Asignada a</th>
                            <th>Nivel</th>
                            <th>Estado</th>
                            <th>Severidad</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td id="incident_responsible">{{ $incident->support_name }}</td>
                            <td>{{ $incident->level->name }}</td>
                            <td id="incident_state">{{ $incident->state }}</td>
                            <td id="incident_severity">{{ $incident->severity_full }}</td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-with-break-word">
                        <thead>
                        <tr>
                            <th class="col-sm-3">Cliente</th>
                            <td id="incident_summary">{{ $incident->client->name }}</td>
                        </tr>
                        <tr>
                            <th>Descripción</th>
                            <td id="incident_description">{{ $incident->description }}</td>
                        </tr>
                        <tr>
                            <th>Archivos</th>
                            <td id="incident_attachment">
                                <a href="{{ route('login') }}">Iniciar sesión</a>
                                para descargar los archivos
                            </td>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Historial</h4>
            </div>
            <div class="card-body">
                <a class="btn btn-info btn-sm" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Ver historial de cambios
                </a>
                <div class="collapse" id="collapseExample">
                    <div class="table-responsive mt-4">
                        <table class="table table-striped">
                            <thead>
                            <tr class="active">
                                <th>Descripción</th>
                                <th>Fecha y Hora</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($incident->histories as $incident_history)
                                <tr>
                                    <td>{{ $incident_history->description }}</td>
                                    <td>{{ $incident_history->created_at }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
