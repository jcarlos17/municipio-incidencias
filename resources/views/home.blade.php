@extends('layouts.app')

@section('content')
    @if(auth()->user()->is(\App\Models\User::CLIENT))
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Incidencias reportadas</h4>
            </div>

            <div class="card-body">
                <form method="GET" role="search">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="process_id">Proceso</label>
                                <select class="form-control" name="process_id" id="process_id">
                                    <option value="">Elegir</option>
                                    @foreach($processes as $process)
                                        <option value="{{ $process->id }}" {{ $processId == $process->id ? 'selected' : '' }}>
                                            {{ $process->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="start_date">Fecha inicio</label>
                                <input type="date" class="form-control" id="start_date" name="start_date" value="{{ $startDate }}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="ending_date">Fecha fin</label>
                                <input type="date" class="form-control" id="ending_date" name="ending_date" value="{{ $endingDate }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-light">
                            <i class="glyphicon glyphicon-search"></i> Buscar
                        </button>
                    </div>
                </form>
                <table class="table table-bordered">
                    <thead>
                    <tr class="active">
                        <th>Código</th>
                        <th>Proceso</th>
                        <th>Severidad</th>
                        <th>Estado</th>
                        <th>Fecha creación</th>
                        <th>Responsable</th>
                        <th>Opción</th>
                    </tr>
                    </thead>
                    <tbody id="dashboard_by_me">
                    @foreach ($incidents_client as $incident)
                        <tr>
                            <td class="text-center">
                                {{ $incident->code }}
                            </td>
                            <td>{{ $incident->process_name }}</td>
                            <td>{{ $incident->severity_full }}</td>
                            <td>{{ $incident->state }}</td>
                            <td>{{ $incident->created_at }}</td>
                            <td>
                                {{ $incident->support_id ? $incident->support->name : 'Sin asignar' }}
                            </td>
                            <td>
                                <a href="/ver/{{ $incident->id }}" class="btn btn-xs btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $incidents_client->appends(Request::except('page'))->links('includes.paginate') }}
            </div>
        </div>
    @endif
@endsection
