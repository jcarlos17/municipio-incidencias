@if ($paginator->lastPage() > 1)
    <nav>
        <ul class="pagination pagination-gutter">
            <li class="page-item page-indicator">
                <a class="page-link" href="{{ $paginator->currentPage() == 1 ? 'javascript:void(0)' : $paginator->url(1) }}">
                    <i class="la la-angle-left"></i></a>
            </li>
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <li class="page-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endfor
            <li class="page-item page-indicator {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                <a class="page-link" href="{{ ($paginator->currentPage() == $paginator->lastPage()) ? 'javascript:void(0)' : $paginator->url($paginator->currentPage()+1) }}">
                    <i class="la la-angle-right"></i></a>
            </li>
        </ul>
    </nav>
@endif
