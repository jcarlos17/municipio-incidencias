<div class="deznav">
    <div class="deznav-scroll">
        @if (auth()->check())
            <form class="basic-form d-block d-sm-none">
                <div class="form-group">
                    <select data-departments id="list-of-departments" class="form-control default-select">
                        @foreach (auth()->user()->list_of_departments as $department)
                            <option value="{{ $department->id }}"
                                    @if($department->id==auth()->user()->selected_department_id) selected @endif>
                                {{ $department->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </form>
            <div class="text-center my-4">
                <div class="user-box">
                    <form action="{{ url('profile/image') }}" id="avatarForm">
                        @csrf
                        <input type="file" accept="image/*" style="display: none" id="avatarInput">
                    </form>
                    <div class="wrap">
                        <div class="user-img">
                            <img src="{{ auth()->user()->imageUrl }}"
                                 alt="user-img" id="avatarImage" title="{{ auth()->user()->name }}"
                                 class="rounded-circle img-responsive">
                        </div>
                        <div class="text_over_image" id="textToEdit">Editar</div>
                    </div>
                    <h6>{{ auth()->user()->name }}</h6>
                </div>
            </div>
        @endif
        <ul class="metismenu" id="menu">
            @guest
                <li class="{{ request()->is('/') ? 'mm-active' : '' }}">
                    <a href="{{ url('') }}" class="ai-icon" aria-expanded="false">
                        <span class="nav-text">Bienvenido</span>
                    </a>
                </li>
                <li class="{{ request()->is('tablero') ? 'mm-active' : '' }}">
                    <a href="{{ url('tablero') }}" class="ai-icon" aria-expanded="false">
                        <span class="nav-text">Tablero</span>
                    </a>
                </li>
                <li>
                    <a class="has-arrow ai-icon" href="javascript:void(0)" aria-expanded="false">
                        <span class="nav-text">Consulta</span>
                    </a>
                    <ul aria-expanded="false" class="metismenu">
                        <li><a href="{{ url('consulta/tramite') }}">Trámite</a></li>
                        <li><a href="{{ url('consulta/guia-tramite') }}">Guía de trámite</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow ai-icon" href="javascript:void(0)" aria-expanded="false">
                        <span class="nav-text">Impuestos</span>
                    </a>
                    <ul aria-expanded="false" class="metismenu">
                        <li><a href="{{ url('impuestos-tasas-por-pagar') }}">Impuestos y tasas por pagar</a></li>
                        <li><a href="javascript:void(0)">Valores pagados</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow ai-icon" href="javascript:void(0)" aria-expanded="false">
                        <span class="nav-text">Exoneraciones</span>
                    </a>
                    <ul aria-expanded="false" class="metismenu">
                        <li><a href="javascript:void(0)">Ley del anciano</a></li>
                        <li><a href="javascript:void(0)">Artesano calificado</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow ai-icon" href="javascript:void(0)" aria-expanded="false">
                        <span class="nav-text">Archivo secretaria general</span>
                    </a>
                    <ul aria-expanded="false" class="metismenu">
                        <li><a href="javascript:void(0)">Ordenanzas</a></li>
                        <li><a href="javascript:void(0)">Actas y Resoluciones de concejo</a></li>
                        <li><a href="javascript:void(0)">Reglamentos</a></li>
                        <li><a href="javascript:void(0)">Convenios</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)" class="ai-icon" aria-expanded="false">
                        <span class="nav-text">Preguntas frecuentes</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="ai-icon" aria-expanded="false">
                        <span class="nav-text">Quejas-sugerencias denuncias</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="ai-icon" aria-expanded="false">
                        <span class="nav-text">Descarga comprobantes electrónicos</span>
                    </a>
                </li>
            @else
                @if(auth()->user()->is(\App\Models\User::CLIENT))
                    <li class="{{ request()->is('/') ? 'mm-active' : '' }}">
                        <a href="{{ url('') }}" class="ai-icon" aria-expanded="false">
                            <span class="nav-text">Bienvenido</span>
                        </a>
                    </li>
                @endif
                @if(!auth()->user()->is(\App\Models\User::ADMIN))
                    @if (auth()->user()->is(\App\Models\User::CLIENT))
                        <li class="{{ request()->is('home*') ? 'mm-active' : '' }}">
                            <a href="{{ url('home') }}" class="ai-icon" aria-expanded="false">
                                <span class="nav-text">Reportadas por mí</span>
                            </a>
                        </li>
                    @endif
                    @if (auth()->user()->is(\App\Models\User::SUPPORT))
                        <li class="{{ request()->is('assigned-incidents*') ? 'mm-active' : '' }}">
                            <a href="{{ url('assigned-incidents') }}" class="ai-icon" aria-expanded="false">
                                <span class="nav-text">Asignadas a mí</span>
                            </a>
                        </li>
                        <li class="{{ request()->is('pending-incidents*') ? 'mm-active' : '' }}">
                            <a href="{{ url('pending-incidents') }}" class="ai-icon" aria-expanded="false">
                                <span class="nav-text">Pendientes por atender</span>
                            </a>
                        </li>
                        <li class="{{ request()->is('reported-incidents*') ? 'mm-active' : '' }}">
                            <a href="{{ url('reported-incidents') }}" class="ai-icon" aria-expanded="false">
                                <span class="nav-text">Reportadas por mí</span>
                            </a>
                        </li>
                        <li class="{{ request()->is('incidents*') ? 'mm-active' : '' }}">
                            <a href="{{ url('incidents') }}" class="ai-icon" aria-expanded="false">
                                <span class="nav-text">Incidencias</span>
                            </a>
                        </li>
                        <li class="{{ request()->is('cliente*') ? 'mm-active' : '' }}">
                            <a href="{{ url('cliente') }}" class="ai-icon" aria-expanded="false">
                                <span class="nav-text">Lista de clientes</span>
                            </a>
                        </li>
                    @endif

                    @if (auth()->user()->is_support_level_one || auth()->user()->is(\App\Models\User::CLIENT))
                        <li class="{{ request()->is('reportar') ? 'mm-active' : '' }}">
                            <a href="{{ url('reportar') }}" class="ai-icon" aria-expanded="false">
                                <span class="nav-text">Reportar incidencia</span>
                            </a>
                        </li>
                    @endif
                    @if(auth()->user()->is(\App\Models\User::CASHIER))
                        <li class="{{ request()->is('charges*') ? 'mm-active' : '' }}">
                            <a href="{{ url('charges') }}" class="ai-icon" aria-expanded="false">
                                <span class="nav-text">Cobros</span>
                            </a>
                        </li>
                    @endif
                @else
                    <li class="{{ request()->is('incidencias*') ? 'mm-active' : '' }}">
                        <a href="{{ url('incidencias') }}" class="ai-icon" aria-expanded="false">
                            <span class="nav-text">Incidencias</span>
                        </a>
                    </li>
                    <li class="{{ request()->is('users*') ? 'mm-active' : '' }}">
                        <a href="{{ url('users') }}" class="ai-icon" aria-expanded="false">
                            <span class="nav-text">Gestionar usuarios</span>
                        </a>
                    </li>
                    <li class="{{ request()->is('incomes*') ? 'mm-active' : '' }}">
                        <a href="{{ url('incomes') }}" class="ai-icon" aria-expanded="false">
                            <span class="nav-text">Ingresos</span>
                        </a>
                    </li>
                    <li class="{{ request()->is('obligations*') ? 'mm-active' : '' }}">
                        <a href="{{ url('obligations') }}" class="ai-icon" aria-expanded="false">
                            <span class="nav-text">Obligaciones</span>
                        </a>
                    </li>
                    <li class="{{ request()->is('collect_obligations*') ? 'mm-active' : '' }}">
                        <a href="{{ url('collect_obligations') }}" class="ai-icon" aria-expanded="false">
                            <span class="nav-text">Obligaciones por recaudar</span>
                        </a>
                    </li>
                    <li class="{{ request()->is('charges*') ? 'mm-active' : '' }}">
                        <a href="{{ url('charges') }}" class="ai-icon" aria-expanded="false">
                            <span class="nav-text">Cobros</span>
                        </a>
                    </li>
                    <li class="{{ request()->is('departments*') ? 'mm-active' : '' }}">
                        <a href="{{ url('departments') }}" class="ai-icon" aria-expanded="false">
                            <span class="nav-text">Departamentos</span>
                        </a>
                    </li>
                    <li class="{{ request()->is('processes*') ? 'mm-active' : '' }}">
                        <a href="{{ url('processes') }}" class="ai-icon" aria-expanded="false">
                            <span class="nav-text">Procesos</span>
                        </a>
                    </li>
                    <li class="{{ request()->is('officials*') ? 'mm-active' : '' }}">
                        <a href="{{ url('officials') }}" class="ai-icon" aria-expanded="false">
                            <span class="nav-text">Funcionarios</span>
                        </a>
                    </li>
                    <li class="{{ request()->is('comments*') ? 'mm-active' : '' }}">
                        <a href="{{ url('comments') }}" class="ai-icon" aria-expanded="false">
                            <span class="nav-text">Comentarios</span>
                        </a>
                    </li>
                @endif
            @endguest
        </ul>
    </div>
</div>
