@extends('layouts.app')

@section('styles')
    <style>
        ul {
            padding-left: 1.5em;
        }
        li {
            list-style: square;
        }
    </style>
@endsection

@section('content')
    @if(auth()->check() && auth()->user()->is(\App\Models\User::CLIENT))
        <div class="card">
            <div class="card-body">
                <div class="text-center mb-5">
                    <img src="{{ asset('images/portal_ciudadano.png') }}" alt="Bienvenido" width="100%">
                </div>

                <h4 class="text-center">BIENVENIDO</h4>
                <p class="text-justify">
                    El GAD Municipal del Cantón Salcedo, pone a disposición el PORTAL WEB CIUDADANO,
                    el cual permitirá simplificar y reducir los tiempos de atención en la gestión de trámites municipales,
                    garantizando una atención, eficaz, transparente y de calidad, optimizando los recursos y
                    dando respuestas oportunas al contribuyente.
                </p>
                <p>En esta sección el ciudadano podra:</p>
                <ul>
                    <li>Ver todas las incidencias reportadas (Trámites reportados).</li>
                    <li>Permite escoger una incidencia en particular y desplegar la situación actual de la incidencia de manera ágil y oportuna.</li>
                    <li>Reportar una Incidencias que será atendida por los funcionarios del GAD Municipal.</li>
                    <li>Permite subir los requisitos de un trámite.</li>
                    <li>Ver y descargar los formularios producto de resolver de manera satisfactoria una incidencia.</li>
                </ul>
            </div>
        </div>
    @else
        <div class="card">
            <div class="card-body p-0">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active">
                        </li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ asset('images/slides/slide-1.png') }}" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('images/slides/slide-2.png') }}" alt="Second slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span> <span
                            class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="text-center">
            <img src="{{ asset('images/welcome.png') }}" alt="Bienvenido" width="100%">
        </div>
    @endif
@endsection
