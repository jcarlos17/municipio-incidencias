@extends('layouts.app')

@section('styles')

@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Impuestos y tasas por pagar</h4>
        </div>
        <div class="card-body">
            <h6>Conozca los valores pendientes por concepto de impuestos, tasas y contribuciones</h6>
            <p>Para consultar los valores adeudados siga los siguientes pasos</p>
            <ul>
                <li>Ingrese el número de cédula/RUC (se listan todos los valores pendientes del contribuyente)</li>
                <li>Presione consultar</li>
            </ul>
            <div class="basic-form mt-4">
                <form method="GET">

                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="documentInput" placeholder="Número de cédula" value="{{ $documentInput }}">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if($pendingValues)
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-responsive-md">
                        <thead>
                        <tr>
                            <th><strong>codarticulo</strong></th>
                            <th><strong>codcliente</strong></th>
                            <th><strong>cuenta</strong></th>
                            <th><strong>nombres</strong></th>
                            <th><strong>direccion</strong></th>
                            <th><strong>monto</strong></th>
                            <th><strong>interes</strong></th>
                            <th><strong>juicio</strong></th>
                            <th><strong>embargo</strong></th>
                            <th><strong>carpeta</strong></th>
                            <th><strong>total</strong></th>
                            <th><strong>cartas</strong></th>
                            <th><strong>desde</strong></th>
                            <th><strong>hasta</strong></th>
                            <th><strong>rubro</strong></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($pendingValues as $pendingValue)
                            <tr>
                                <td>{{ $pendingValue->codarticulo }}</td>
                                <td>{{ $pendingValue->codcliente }}</td>
                                <td>{{ $pendingValue->cuenta }}</td>
                                <td>{{ $pendingValue->nombres }}</td>
                                <td>{{ $pendingValue->direccion }}</td>
                                <td>{{ $pendingValue->monto }}</td>
                                <td>{{ $pendingValue->interes }}</td>
                                <td>{{ $pendingValue->juicio }}</td>
                                <td>{{ $pendingValue->embargo }}</td>
                                <td>{{ $pendingValue->carpeta }}</td>
                                <td>{{ $pendingValue->total }}</td>
                                <td>{{ $pendingValue->cartas }}</td>
                                <td>{{ $pendingValue->desde }}</td>
                                <td>{{ $pendingValue->hasta }}</td>
                                <td>{{ $pendingValue->rubro }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
@endsection
