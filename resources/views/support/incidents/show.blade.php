@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Incidencia</h4>
        </div>
        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-success alert-dismissible alert-alt fade show">
                    {{ session('notification') }}
                </div>
            @endif
            <div class="table-responsive mt-3">
                <table class="table table-bordered table-responsive-md">
                    <thead>
                    <tr class="text-uppercase">
                        <th>Código</th>
                        <th>Departamento</th>
                        <th>Proceso</th>
                        <th>Fecha de envío</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td id="incident_key">{{ $incident->code }}</td>
                        <td id="incident_department">{{ $incident->department->name }}</td>
                        <td id="incident_process">{{ $incident->process_name }}</td>
                        <td id="incident_created_at">{{ $incident->created_at }}</td>
                    </tr>
                    </tbody>
                    <thead>
                    <tr class="text-uppercase">
                        <th>Asignada a</th>
                        <th>Nivel</th>
                        <th>Estado</th>
                        <th>Severidad</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td id="incident_responsible">{{ $incident->support_name }}</td>
                        <td>{{ $incident->level->name }}</td>
                        <td id="incident_state">{{ $incident->state }}</td>
                        <td id="incident_severity">{{ $incident->severity_full }}</td>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-bordered table-responsive-md">
                    <thead>
                        <tr>
                            <th>CLIENTE</th>
                            <td id="incident_summary">{{ $incident->client->name }}</td>
                        </tr>
                        <tr>
                            <th>DESCRIPCIÓN</th>
                            <td id="incident_description">{{ $incident->description }}</td>
                        </tr>
{{--                        @if($incident->files()->count() == 0)--}}
{{--                        <tr>--}}
{{--                            <th>ADJUNTOS</th>--}}
{{--                            <td id="incident_attachment">No se han adjuntado archivos</td>--}}
{{--                        </tr>--}}
{{--                        @endif--}}
                    </thead>
                </table>
            </div>
        </div>
    </div>
    @if($incident->archives()->count() > 0)
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Requisitos</h4>
            </div>
            <div class="card-body">
                <p>
                    <a class="btn btn-outline-info" data-toggle="collapse" href="#collapseArchive" aria-expanded="false" aria-controls="collapseArchive">
                        Ver requisitos
                    </a>
                </p>
                <div class="collapse" id="collapseArchive">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            @foreach($incident->archives as $archive)
                                <tr>
                                    <td>{{ $archive->name }}</td>
                                    <td class="text-right">
                                        <input type="file" data-input-file data-id="{{ $archive->id }}" accept="application/pdf" style="display:none">
                                        <button class="btn btn-sm btn-dark mr-2" type="button" data-archive="{{ $archive->id }}">Archivo</button>
                                        @if($archive->archive_id)
                                            <span><a href="{{ $archive->archive->url }}" target="_blank" class="btn btn-sm btn-dark">Ver</a></span>
                                        @else
                                            <span><button class="btn btn-sm btn-dark" type="button" data-archive="" disabled>Ver</button></span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Historial</h4>
        </div>
        <div class="card-body">
            <p>
                <a class="btn btn-outline-info" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Ver historial de cambios
                </a>
            </p>
            <div class="collapse" id="collapseExample">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="active">
                            <th>Descripción</th>
                            <th>Fecha y Hora</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($incident_histories as $incident_history)
                            <tr>
                                <td>{{ $incident_history->description }}</td>
                                <td>{{ $incident_history->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        const $body = $('#bodyFiles');
        const $processId = $('#process_id');
        const $btnArchive = $('#btnArchive');

        $(document).on('click', '[data-archive]', function () {
            const tr = $(this).closest('tr');
            const inputFile = tr.find('[data-input-file]');

            inputFile.click();
        });

        $(document).on('change', '[data-input-file]', function () {
            const tr = $(this).closest('tr');
            const span = tr.find('span');
            const incident_archive_id = $(this).data('id');

            const file_data = this.files[0];
            const form_data = new FormData();
            form_data.append('archive', file_data);
            form_data.append('_token', $('[name="_token"]').val());
            if (incident_archive_id) {
                form_data.append('incident_archive_id', incident_archive_id);
            }
            $.ajax({
                url: '/archivo/cargar',
                dataType: 'json',
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(archive){
                    this.value = '';
                    span.html('<a href="'+archive.url+'" target="_blank" class="btn btn-sm btn-dark">Ver</a>');
                }
            });
        });
    </script>
@endsection
