@extends('layouts.app')

@section('styles')
    <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Cobros</h4>
        </div>

        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-success alert-dismissible alert-alt fade show">
                    {{ session('notification') }}
                </div>
            @endif
                <div class="row">
                    <div class="col-sm-6">
                        <form>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Buscar..." name="inputSearch" value="{{ $inputSearch }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-6">
                        <a href="{{ url('incidents/'.$incident->id.'/charges/create') }}" class="btn btn-dark float-right">Nuevo cobro</a>
                    </div>
                </div>
                <div class="table-responsive mt-4">
                    <table class="table table-striped">
                        <thead>
                        <tr class="text-uppercase">
                            <th>ID</th>
                            <th>Orden ingreso</th>
                            <th>Cliente</th>
                            <th>Ingreso</th>
                            <th>Total</th>
                            <th>F_Emision</th>
                            <th>F_Pago</th>
                            <th>Forma de pago</th>
                            <th>Usuario</th>
                            <th class="text-center">Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($collectObligations as $collectObligation)
                            <tr>
                                <td>{{ $collectObligation->id }}</td>
                                <td>{{ $collectObligation->entry_order }}</td>
                                <td>{{ $collectObligation->obligation->client->name }}</td>
                                <td>{{ $collectObligation->obligation->income->name }}</td>
                                <td class="text-right">{{ number_format($collectObligation->total, 2) }}</td>
                                <td>{{ $collectObligation->issue_date }}</td>
                                <td>{{ $collectObligation->payment_date }}</td>
                                <td>{{ $collectObligation->payment_type }}</td>
                                <td>{{ $collectObligation->cashier->name }}</td>
                                <td class="text-center">
                                    <a href="{{ url('incidents/'.$incident->id.'/charges/'.$collectObligation->id.'/show') }}" class="btn btn-sm btn-success mb-1" title="Ver">
                                        <span class="fa fa-eye"></span>
                                    </a>
                                    <button type="button" data-reverse="{{ url('incidents/'.$incident->id.'/charges/'.$collectObligation->id.'/reverse') }}" class="btn btn-sm btn-danger mb-1" title="Reversar">
                                        Reversar
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $collectObligations->appends(Request::except('page'))->links('includes.paginate') }}
                </div>
        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ url('ver/'.$incident->id) }}" class="btn btn-light">Volver a la incidencia</a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Sweet alert 2 -->
    <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '[data-reverse]', function () {
            let urlDelete = $(this).data('reverse');

            swal({
                title: '¿Seguro que desea reversar este cobro?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, reversar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        });
    </script>
@endsection
