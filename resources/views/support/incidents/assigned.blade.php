@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Incidencias asignadas a mí</h4>
        </div>

        <div class="card-body">
            <form method="GET" role="search">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="process_id">Proceso</label>
                            <select class="form-control" name="process_id" id="process_id">
                                <option value="">Elegir</option>
                                @foreach($processes as $process)
                                    <option value="{{ $process->id }}" {{ $processId == $process->id ? 'selected' : '' }}>
                                        {{ $process->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="start_date">Fecha inicio</label>
                            <input type="date" class="form-control" id="start_date" name="start_date" value="{{ $startDate }}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="ending_date">Fecha fin</label>
                            <input type="date" class="form-control" id="ending_date" name="ending_date" value="{{ $endingDate }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-light">
                        <i class="fa fa-search"></i> Buscar
                    </button>
                </div>
            </form>
            <div class="table-responsive mt-4">
                <table class="table table-striped">
                    <thead>
                    <tr class="active">
                        <th>Código</th>
                        <th>Proceso</th>
                        <th>Severidad</th>
                        <th>Estado</th>
                        <th>Fecha creación</th>
                        <th>Cliente</th>
                    </tr>
                    </thead>
                    <tbody id="dashboard_my_incidents">
                    @foreach ($myIncidents as $incident)
                        <tr>
                            <td class="text-center">
                                <a href="/ver/{{ $incident->id }}">
                                    {{ $incident->code }}
                                </a>
                            </td>
                            <td>{{ $incident->process_name }}</td>
                            <td>{{ $incident->severity_full }}</td>
                            <td>{{ $incident->state }}</td>
                            <td>{{ $incident->created_at }}</td>
                            <td>{{ $incident->client->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $myIncidents->appends(Request::except('page'))->links('includes.paginate') }}
            </div>
        </div>
    </div>
@endsection

