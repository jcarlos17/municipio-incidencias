@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Lista de clientes</h4>
    </div>

    <div class="card-body">
        @if (session('notification'))
            <div class="alert alert-success alert-dismissible alert-alt fade show">
                {{ session('notification') }}
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissible alert-alt fade show">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-8">
                <a href="/cliente/crear" class="btn btn-primary" title="Nuevo cliente">
                    Nuevo cliente
                </a>
            </div>
            <div class="col-sm-4">
                <form class="" role="search">
                    <div class="input-group mb-3">
                        <input type="text" name="search_client" class="form-control" placeholder="Buscar por cédula o nombre" value="{{ $searchClient }}">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
            <div class="table-responsive mt-4">
                <table class="table table-striped">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th>Cédula</th>
                        <th class="text-center">Estado</th>
                        <th class="text-center">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($clients as $client)
                        <tr>
                            <td>{{ $client->name }}</td>
                            <td>{{ $client->document }}</td>
                            <td class="text-center">{{ $client->statusText }}</td>
                            <td class="text-center">
                                <a href="/cliente/{{ $client->id }}" class="btn btn-sm btn-primary mb-1" title="Editar">
                                    <span class="fa fa-pencil"></span>
                                </a>
                                <a href="/cliente/{{ $client->id }}/eliminar" class="btn btn-sm btn-danger mb-1" title="Dar de baja">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $clients->links('includes.paginate') }}
            </div>
    </div>
</div>
@endsection
