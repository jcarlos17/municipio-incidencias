@php use App\Models\User; @endphp
@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Editar cliente</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                @if (session('notification'))
                    <div class="alert alert-success alert-dismissible alert-alt fade show">
                        {{ session('notification') }}
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" name="name" class="form-control"
                                   value="{{ old('name', $client->name) }}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="document">Cédula</label>
                            <input type="text" name="document" class="form-control"
                                   value="{{ old('document', $client->document) }}" disabled>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="ruc">RUC</label>
                            <input type="text" name="ruc" id="ruc" class="form-control"
                                   value="{{ old('ruc', $client->ruc) }}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="status">
                                <option value="">Seleccionar</option>
                                <option
                                    value="{{ User::LOCKED }}" {{ old('status', $client->status) == User::LOCKED ? 'selected' : '' }}>
                                    Bloqueado
                                </option>
                                <option
                                    value="{{ User::ACTIVE }}" {{ old('status', $client->status) == User::ACTIVE ? 'selected' : '' }}>
                                    Activo
                                </option>
                                <option
                                    value="{{ User::DELETED }}" {{ old('status', $client->status) == User::DELETED ? 'selected' : '' }}>
                                    Eliminado
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="email">Correo</label>
                            <input type="email" name="email" class="form-control"
                                   value="{{ old('email', $client->email) }}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="address">Dirección</label>
                            <input type="text" name="address" class="form-control"
                                   value="{{ old('address', $client->address) }}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="cellphone">Teléfono</label>
                            <input type="number" name="cellphone" class="form-control"
                                   value="{{ old('cellphone', $client->cellphone) }}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="password">Contraseña <small><em>Ingresar solo si se desea modificar</em></small></label>
                            <input type="text" name="password" class="form-control" value="{{ old('password') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <button class="btn btn-success">Guardar usuario</button>
                    <a href="/cliente" class="btn btn-light" title="Volver">Volver</a>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="/js/admin/users/edit.js"></script>
@endsection
