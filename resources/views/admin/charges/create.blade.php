@extends('layouts.app')

@section('styles')
    {{--PagoPlux--}}
    <script src="{{ asset('js/pago_plux/index.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://sandbox-paybox.pagoplux.com/paybox/index.js"></script>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Nuevo cobro</h4>
        </div>

        <div class="card-body">
            <form method="post">
                @csrf
                <div class="form-group row">
                    <label for="client_id" class="col-sm-2 col-form-label">Cliente</label>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#modalSelectClient">
                            Buscar cliente
                        </button>
                    </div>
                </div>
                <div class="row">
                    <input type="hidden" id="client_id" value="">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="client_document" class="col-form-label">Cedula</label>
                            <input type="text" class="form-control" id="client_document" value="" disabled>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="client_name" class="col-form-label">Nombre</label>
                            <input type="text" class="form-control" id="client_name" value="" disabled>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="client_address" class="col-form-label">Dirección</label>
                            <input type="text" class="form-control" id="client_address" value="" disabled>
                        </div>
                    </div>
                    <div class="offset-sm-8 col-sm-4">
                        <div class="form-group">
                            <label for="payment_date" class="col-form-label">Fecha cobro</label>
                            <input type="date" class="form-control" id="payment_date" value="{{ date('Y-m-d') }}" disabled>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <label for="rate_id" class="col-sm-2 col-form-label">ID Ingreso</label>
                    <div class="col-sm-3">
                        <input type="hidden" id="collect_obligation_id" name="collect_obligation_id">
                        <input type="text" class="form-control" id="income_id" disabled>
                    </div>
                    <div class="col-sm-3" id="buttonIncome">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="income_name" class="col-form-label">Ingreso</label>
                            <input type="text" class="form-control" id="income_name" value="" disabled>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="period" class="col-form-label">Periodo</label>
                            <input type="text" class="form-control" id="period" value="" disabled>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="table">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th class="w-75" scope="col">Items</th>
                            <th class="w-25 text-center" scope="col">Total</th>
                        </tr>
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group mt-3" id="formPayment" style="display: none">
                            <label for="payment_type" class="col-form-label">Forma de pago</label>
                            <select class="form-control" name="payment_type" id="payment_type" required>
                                <option value="">Seleccionar</option>
                                <option value="{{ \App\Models\Payment::CASH }}">Dinero en efectivo</option>
                                <option value="{{ \App\Models\Payment::BANK_CHECK }}">Cheque</option>
                                <option value="{{ \App\Models\Payment::PLUX }}">Dinero electrónico</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-sm-10">
                        <div id="ButtonPaybox" style="display:none;"></div>
                        <input type="hidden" id="total">
                        <input type="hidden" id="payboxSendmail">
                        <input type="hidden" id="payboxSendname">
                        <input type="hidden" name="token" id="token">
                        <input type="hidden" name="amount" id="amount">
                        <input type="hidden" name="date" id="date">
                        <input type="hidden" name="response" id="response">
                        <button type="submit" class="btn btn-success" id="btnSubmit" style="display:none;">Aceptar</button>
                        <a href="{{ asset('charges') }}" class="btn btn-light">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modalSelectClient">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Buscar cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible alert-alt fade show" id="alert" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Es necesario completar los campos
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <select name="" id="type" class="form-control">
                                    <option value="">Seleccionar</option>
                                    <option value="document">Buscar por cédula</option>
                                    <option value="name">Buscar por nombre</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="inputSearch">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <button class="btn btn-light" id="btnSearch">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Cédula</th>
                            <th>Nombre</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="content-search">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modalCollectObligation">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Buscar obligaciones por recaudar</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id Ingreso</th>
                            <th>Ingreso</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="content-collection-search">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
{{----}}
    <script>
        const $btnSearch = $('#btnSearch');
        const $option = $('#type');
        const $input = $('#inputSearch');
        const $alert = $('#alert');
        const $content = $('#content-search');
        const $contentCollection = $('#content-collection-search');
        const $processId = $('#process_id');
        const $tbody = $('#tbody');
        const $btnSubmit = $('#btnSubmit');
        const $btnPaybox = $('#ButtonPaybox');
        const $formPayment = $('#formPayment');

        $(document).ready(function() {
            $btnSearch.click(function () {
                $(this).attr('disabled', true);
                $alert.hide();

                if ($option.val() && $input.val()) {
                    // AJAX
                    $.get('/api/obligation/clients?type='+$option.val()+'&input_search='+$input.val(), function (clients) {
                        let html = '';
                        Array.prototype.forEach.call(clients, function(client) {
                            html += '<tr>' +
                                '<td>'+client.document+'</td>' +
                                '<td>'+client.name+'</td>' +
                                '<td class="text-center"><button class="btn btn-light" type="button" data-select="'+client.id+'" data-document="'+client.document+'" data-name="'+client.name+'" data-address="'+client.address+'" data-email="'+client.email+'">Seleccionar</button></td>' +
                                '</tr>';
                        });
                        $content.html(html);
                    });
                    $(this).attr('disabled', false);
                } else {
                    $(this).attr('disabled', false);
                    $alert.show();
                }
            });
        });

        $(document).on('click', '[data-select]', function () {
            const id = $(this).data('select');
            const document = $(this).data('document');
            const name = $(this).data('name');
            const email = $(this).data('email');
            const address = $(this).data('address');
            $('#client_id').val(id);
            $('#client_document').val(document);
            $('#client_name').val(name);
            $('#client_address').val(address);

            $('#payboxSendmail').val(email);
            $('#payboxSendname').val(name);

            $content.html('');
            $input.val('');
            $option.val('');
            $('#buttonIncome').html('<button type="button" class="btn btn-dark" data-client="'+id+'">Buscar obligaciones por recaudar</button>');

            $tbody.html('');
            $('#collect_obligation_id').val('');
            $('#income_id').val('');
            $('#income_name').val('');
            $('#period').val('');
            $formPayment.hide();
            $btnSubmit.hide();
            $btnPaybox.hide();

            $('#modalSelectClient').modal('hide');
        });

        $(document).on('click', '[data-client]', function () {
            const id = $(this).data('client');

            $.get('/api/collect_obligations/'+id, function (collectObligations) {
                let html = '';
                Array.prototype.forEach.call(collectObligations, function(collectObligation) {
                    html += '<tr>' +
                        '<td>'+collectObligation.income_id+'</td>' +
                        '<td>'+collectObligation.income_name+'</td>' +
                        '<td>'+collectObligation.total+'</td>' +
                        '<td class="text-center"><button class="btn btn-light" type="button" data-collect="'+collectObligation.id+'">Agregar</button></td>' +
                        '</tr>';
                });
                $contentCollection.html(html);
            });

            $('#modalCollectObligation').modal('show');
        });

        $(document).on('click', '[data-collect]', function () {
            const id = $(this).data('collect');

            $.get('/api/collect_obligation/'+id+'/items', function (collectObligation) {
                let html = '';
                const details = collectObligation.details;
                Array.prototype.forEach.call(details, function(detail, key) {
                    html += '<tr>' +
                        '<th>'+(key+1)+'</th>' +
                        '<td>'+detail.item.description+'</td>' +
                        '<td class="text-right">'+addZeroes(detail.value)+'</td>' +
                        '</tr>';
                });
                html += '<tr>' +
                    '<th class="text-right" colspan="2">Total</th>' +
                    '<td class="text-right">'+addZeroes(collectObligation.total)+'</td>' +
                    '</tr>';

                $('#total').val(addZeroes(collectObligation.total));

                $tbody.html(html);
                $('#collect_obligation_id').val(collectObligation.id);
                $('#income_id').val(collectObligation.income_id);
                $('#income_name').val(collectObligation.income_name);
                $('#period').val(collectObligation.period);
            });

            $contentCollection.html('');
            $formPayment.show();
            $btnSubmit.show();
            $btnPaybox.show();

            $('#modalCollectObligation').modal('hide');
        });

        function addZeroes(num) {
            return num.toLocaleString("en", {useGrouping: false, minimumFractionDigits: 2})
        }
    </script>
@endsection
