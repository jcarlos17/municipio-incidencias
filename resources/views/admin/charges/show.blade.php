@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Ver cobro</h4>
        </div>

        <div class="card-body">
            <form method="post">
                @csrf
                <h5>Cliente</h5>
                <div class="row">
                    <input type="hidden" id="client_id" value="">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="client_document" class="col-form-label">Cedula</label>
                            <input type="text" class="form-control" id="client_document" value="{{ $obligation->client->document }}" disabled>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="client_name" class="col-form-label">Nombre</label>
                            <input type="text" class="form-control" id="client_name" value="{{ $obligation->client->name }}" disabled>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="client_address" class="col-form-label">Dirección</label>
                            <input type="text" class="form-control" id="client_address" value="{{ $obligation->client->address }}" disabled>
                        </div>
                    </div>
                    <div class="offset-sm-8 col-sm-4">
                        <div class="form-group">
                            <label for="payment_date" class="col-form-label">Fecha cobro</label>
                            <input type="text" class="form-control" id="payment_date" value="{{ $collectObligation->payment_date }}" disabled>
                        </div>
                    </div>
                </div>
                <hr>
                <h5>Ingreso</h5>
                <div class="form-group row">
                    <label for="rate_id" class="col-sm-2 col-form-label">ID Ingreso</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="income_id" value="{{ $collectObligation->income_id }}" disabled>
                    </div>
                    <div class="col-sm-3" id="buttonIncome">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="income_name" class="col-form-label">Ingreso</label>
                            <input type="text" class="form-control" id="income_name" value="{{ $collectObligation->income->name }}" disabled>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="period" class="col-form-label">Periodo</label>
                            <input type="text" class="form-control" id="period" value="{{ $collectObligation->period }}">
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="table">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th class="w-75" scope="col">Items</th>
                            <th class="w-25 text-center" scope="col">Total</th>
                        </tr>
                        </thead>
                        <tbody id="tbody">
                            @foreach($collectObligation->details as $key => $detail)
                                <tr>
                                    <th>{{ $key+1 }}</th>
                                    <td>{{ $detail->item->description }}</td>
                                    <td class="text-right">{{ $detail->value }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th class="text-right" colspan="2">Total</th>
                                <td class="text-right">{{ $collectObligation->total }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-sm-10">
                        <a href="{{ asset('charges') }}" class="btn btn-light">Volver</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
@endsection
