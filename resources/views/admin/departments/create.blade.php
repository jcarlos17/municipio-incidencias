@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Registrar departamento</h4>
    </div>
    <form action="" method="POST">
        @csrf
        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-success alert-dismissible alert-alt fade show">
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible alert-alt fade show">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
            </div>
            <div class="form-group">
                <label for="description">Descripción</label>
                <input type="text" name="description" class="form-control" value="{{ old('description') }}">
            </div>
            <div class="form-group">
                <label for="start">Fecha de inicio</label>
                <input type="date" name="start" class="form-control" value="{{ old('start', date('Y-m-d')) }}">
            </div>

        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ url('departments') }}" class="btn btn-light">Volver a la lista de departamentos</a>
                <button type="submit" class="btn btn-primary">Registrar departamento</button>
            </div>
        </div>
    </form>

</div>

@endsection

@section('scripts')
@endsection
