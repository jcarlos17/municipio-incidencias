@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Departamentos</h4>
    </div>

    <div class="card-body">
        @if (session('notification'))
            <div class="alert alert-success alert-dismissible alert-alt fade show">
                {{ session('notification') }}
            </div>
        @endif

        <div class="form-group">
            <a href="{{ url('departments/create') }}" class="btn btn-primary">Registrar departamentos</a>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Fecha de inicio</th>
                    <th class="text-center">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($departments as $department)
                <tr>
                    <td>{{ $department->name }}</td>
                    <td>{{ $department->description }}</td>
                    <td>{{ $department->start ?: 'No se ha indicado' }}</td>
                    <td class="text-center">
                        @if ($department->trashed())
                        <a href="{{ url('departments/'.$department->id.'/restore') }}" class="btn btn-sm btn-success mb-1" title="Restaurar">
                            <span class="fa fa-repeat"></span>
                        </a>
                        @else
                        <a href="{{ url('departments/'.$department->id.'/edit') }}" class="btn btn-sm btn-primary mb-1" title="Editar">
                            <span class="fa fa-pencil"></span>
                        </a>
                        <a href="{{ url('departments/'.$department->id.'/delete') }}" class="btn btn-sm btn-danger mb-1" title="Dar de baja">
                            <span class="fa fa-trash"></span>
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $departments->links('includes.paginate') }}
    </div>
</div>
@endsection
