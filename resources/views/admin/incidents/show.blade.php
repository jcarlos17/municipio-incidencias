@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Incidencia: {{ $incident->title }}</h4>
        </div>
        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-success alert-dismissible alert-alt fade show">
                    {{ session('notification') }}
                </div>
            @endif
            <div class="table-responsive mt-3">
                <table class="table table-bordered table-responsive-md">
                    <thead>
                    <tr class="text-uppercase">
                        <th>Código</th>
                        <th>Departamento</th>
                        <th>Proceso</th>
                        <th>Fecha de envío</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td id="incident_key">{{ $incident->code }}</td>
                        <td id="incident_department">{{ $incident->department->name }}</td>
                        <td id="incident_process">{{ $incident->process_name }}</td>
                        <td id="incident_created_at">{{ $incident->created_at }}</td>
                    </tr>
                    </tbody>
                    <thead>
                    <tr class="text-uppercase">
                        <th>Asignada a</th>
                        <th>Nivel</th>
                        <th>Estado</th>
                        <th>Severidad</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td id="incident_responsible">{{ $incident->support_name }}</td>
                        <td>{{ $incident->level->name }}</td>
                        <td id="incident_state">{{ $incident->state }}</td>
                        <td id="incident_severity">{{ $incident->severity_full }}</td>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-bordered table-responsive-md">
                    <thead>
                        <tr>
                            <th>CLIENTE</th>
                            <td id="incident_summary">{{ $incident->client->name }}</td>
                        </tr>
                        <tr>
                            <th>DESCRIPCIÓN</th>
                            <td id="incident_description">{{ $incident->description }}</td>
                        </tr>
                        @if($incident->archives()->count() == 0)
                        <tr>
                            <th>ADJUNTOS</th>
                            <td id="incident_attachment">No se han adjuntado archivos</td>
                        </tr>
                        @endif
                    </thead>
                </table>
                @if($incident->archives()->count() > 0)
                    <a class="btn btn-outline-info" data-toggle="collapse" href="#collapseArchives" aria-expanded="false" aria-controls="collapseArchives">
                        Requisitos
                    </a>
                    <div class="collapse" id="collapseArchives">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                @foreach($incident->archives as $archive)
                                    <tr>
                                        <td>{{ $archive->name }}</td>
                                        <td class="text-right">
                                            @if($archive->archive_id)
                                                <span><a href="{{ $archive->archive->url }}" target="_blank" class="btn btn-sm btn-dark">Ver</a></span>
                                            @else
                                                <span><button class="btn btn-sm btn-dark" type="button" data-archive="" disabled>Ver</button></span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Historial</h4>
    </div>
    <div class="card-body">
        <p>
            <a class="btn btn-outline-info" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Ver historial de cambios
            </a>
        </p>
        <div class="collapse" id="collapseExample">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr class="active">
                        <th>Descripción</th>
                        <th>Fecha y Hora</th>
                        <th>Tiempo en espera</th>
                        <th>Tiempo de atención</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($incident_histories as $incident_history)
                        <tr>
                            <td>{{ $incident_history->description }}</td>
                            <td>{{ $incident_history->created_at }}</td>
                            @if($incident_history->standbyTime)
                                <td rowspan="2">{{ $incident_history->standbyTime }}</td>
                            @elseif($incident_history->type == 'resolved')
                                <td></td>
                            @endif
                            @if($incident_history->attentionTime)
                                <td rowspan="2">{{ $incident_history->attentionTime }}</td>
                            @elseif($incident_history->type == 'registry')
                                <td></td>
                            @endif
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2"></td>
                        <th>{{ $totalStandbyTime }}</th>
                        <th>{{ $totalAttentionTime }}</th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
