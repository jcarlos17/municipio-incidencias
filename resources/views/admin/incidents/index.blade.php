@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Incidencias</h4>
        </div>
        <div class="card-body">
            <div class="basic-form">
                <form>
                    <div class="form-row align-items-center">
                        <div class="col-auto">
                            <div class="form-group mb-2">
                                <select class="form-control default-select" name="state">
                                    <option value="" >Buscar por estado</option>
                                    <option value="Resuelto" @if($searchState == 'Resuelto') selected @endif>Resuelto</option>
                                    <option value="Asignado" @if($searchState == 'Asignado') selected @endif>Asignado</option>
                                    <option value="Pendiente" @if($searchState == 'Pendiente') selected @endif>Pendiente</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="form-group mb-2">
                                <input type="text" name="document" class="form-control" placeholder="Buscar por cédula" value="{{ $searchIncident }}">
                            </div>
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-primary mb-2">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="table-responsive mt-4">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="w-10"><strong>CÓDIGO</strong></th>
                        <th><strong>PROCESO</strong></th>
                        <th><strong>CIENTE</strong></th>
                        <th><strong>CÉDULA</strong></th>
                        <th class="text-center"><strong>ESTADO</strong></th>
                        <th class="text-center"><strong>OPCIÓN</strong></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($incidents as $incident)
                        <tr>
                            <td>{{ $incident->code }}</td>
                            <td>{{ $incident->process->name }}</td>
                            <td>{{ $incident->client->name }}</td>
                            <td>{{ $incident->client->document }}</td>
                            <td class="text-center">{!! $incident->stateHtml !!}</td>
                            <td class="text-center">
                                <a href="{{ url('incidencia/'.$incident->id) }}" class="btn btn-info shadow sharp mr-1">
                                    <i class="fa fa-search-plus"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $incidents->links('includes.paginate') }}
            </div>
        </div>
    </div>
@endsection
