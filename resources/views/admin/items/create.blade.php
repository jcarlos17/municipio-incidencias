@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Nuevo item</h4>
    </div>
    <form action="" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="code">Cta. Contable</label>
                <input type="text" name="code" class="form-control @error('code') is-invalid @enderror" value="{{ old('code') }}">
                @error('code')
                    <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="description">Descripción</label>
                <input type="text" name="description" class="form-control @error('description') is-invalid @enderror" value="{{ old('description') }}">
                @error('description')
                <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ url('incomes/'.$income->id.'/items') }}" class="btn btn-light">Volver a la lista de items</a>
                <button class="btn btn-primary">Registrar item</button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
@endsection
