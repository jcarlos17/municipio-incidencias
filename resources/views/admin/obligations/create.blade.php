@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Nueva obligación</h4>
    </div>
    <form action="" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="client_id">Cliente</label>
                <div class="input-group">
                    <input type="hidden" name="client_id" id="client_id" value="{{ old('client_id') }}">
                    <input type="text" name="document" id="document" class="form-control @error('client_id') is-invalid @enderror"
                           value="{{ old('document') }}">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#modalSelectClient">
                            Seleccionar cliente
                        </button>
                    </div>
                </div>
                @error('client_id')
                <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="income_id">Ingreso</label>
                <select name="income_id" class="form-control @error('income_id') is-invalid @enderror">
                    <option value="">Seleccionar</option>
                    @foreach ($incomes as $income)
                        <option value="{{ $income->id }}" {{ old('income_id') == $income->id ? 'selected' : '' }}>
                            {{ $income->name }}
                        </option>
                    @endforeach
                </select>
                @error('income_id')
                    <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="code">Código</label>
                <input type="text" name="code" class="form-control @error('code') is-invalid @enderror" value="{{ old('code', $code) }}">
                @error('code')
                <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ url('obligations') }}" class="btn btn-light">Volver a la lista de obligaciones</a>
                <button class="btn btn-primary">Registrar obligación</button>
            </div>
        </div>
    </form>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalSelectClient">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Seleccionar cliente</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible alert-alt fade show" id="alert" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    Es necesario completar los campos y seleccionar un proceso para buscar
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <select name="" id="type" class="form-control">
                                <option value="">Seleccionar</option>
                                <option value="document">Buscar por cédula</option>
                                <option value="name">Buscar por nombre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <input type="text" class="form-control" id="inputSearch">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <button class="btn btn-light" id="btnSearch">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Cédula</th>
                        <th>Nombre</th>
                        <th>Código</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="content-search">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        const $btnSearch = $('#btnSearch');
        const $option = $('#type');
        const $input = $('#inputSearch');
        const $alert = $('#alert');
        const $content = $('#content-search');
        const $processId = $('#process_id');

        $(document).ready(function() {
            $btnSearch.click(function () {
                $(this).attr('disabled', true);
                $alert.hide();

                if ($option.val()) {
                    // AJAX
                    $.get('/api/clients?type='+$option.val()+'&input_search='+$input.val()+'&process_id='+$processId.val(), function (clients) {
                        let html = '';
                        Array.prototype.forEach.call(clients, function(client) {
                            html += '<tr>' +
                                '<td>'+client.document+'</td>' +
                                '<td>'+client.name+'</td>' +
                                '<td>'+client.id+'</td>' +
                                '<td class="text-center"><button class="btn btn-light" type="button" data-obligation="'+client.id+'" data-value="'+client.document+' '+client.name+'" data-select="'+client.id+'">Seleccionar</button></td>' +
                                '</tr>';
                        });
                        $content.html(html);
                    });
                    $(this).attr('disabled', false);
                } else {
                    $(this).attr('disabled', false);
                    $alert.show();
                }
            });
        });

        $(document).on('click', '[data-select]', function () {
            const id = $(this).data('select');
            const value = $(this).data('value');

            $('#client_id').val(id);
            $('#document').val(value);
            $content.html('');
            $input.val('');
            $option.val('');

            $('#modalSelectClient').modal('hide');
        });
    </script>
@endsection
