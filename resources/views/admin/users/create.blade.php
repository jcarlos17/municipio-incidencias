@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Registrar usuario</h4>
    </div>
    <form action="" method="POST">
        @csrf
        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-success alert-dismissible alert-alt fade show">
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible alert-alt fade show">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h5>Datos personales</h5>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="cellphone">Celular</label>
                        <input type="number" name="cellphone" class="form-control" value="{{ old('cellphone') }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="email" name="email" class="form-control" value="{{ old('email') }}">
            </div>
            <div class="form-group">
                <label for="address">Dirección</label>
                <input type="text" name="address" class="form-control" value="{{ old('address') }}">
            </div>
            <h5>Permisos en la incidencia</h5>
            <div class="custom-control custom-checkbox checkbox-info">
                <input type="checkbox" class="custom-control-input" id="incident_cashier" name="incident_cashier">
                <label class="custom-control-label" for="incident_cashier">COBROS</label>
            </div>
            <div class="custom-control custom-checkbox mb-5 checkbox-info">
                <input type="checkbox" class="custom-control-input" id="incident_obligation" name="incident_obligation">
                <label class="custom-control-label" for="incident_obligation">OBLIGACIONES POR RECAUDAR</label>
            </div>
            <h5>Datos de la cuenta</h5>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="document">Cédula</label>
                        <input type="text" name="document" class="form-control" value="{{ old('document') }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input type="text" name="password" class="form-control" value="{{ old('password', str_random(8)) }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ url('users') }}" class="btn btn-light">Volver a la lista de usuarios</a>
                <button class="btn btn-primary">Registrar usuario</button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
@endsection
