@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Editar usuario</h4>
    </div>
    <form action="" method="POST">
        @csrf
        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-success alert-dismissible alert-alt fade show">
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible alert-alt fade show">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h5>Datos personales</h5>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" class="form-control" value="{{ old('name', $user->name) }}">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="cellphone">Celular</label>
                        <input type="number" name="cellphone" class="form-control" value="{{ old('cellphone', $user->cellphone) }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="email" name="email" class="form-control" value="{{ old('email', $user->email) }}">
            </div>
            <div class="form-group">
                <label for="address">Dirección</label>
                <input type="text" name="address" class="form-control" value="{{ old('address', $user->address) }}">
            </div>
            <h5>Permisos en la incidencia</h5>
            <div class="custom-control custom-checkbox checkbox-info">
                <input type="checkbox" class="custom-control-input"
                       {{ $user->incident_cashier ? 'checked' : '' }}
                       id="incident_cashier" name="incident_cashier">
                <label class="custom-control-label" for="incident_cashier">COBROS</label>
            </div>
            <div class="custom-control custom-checkbox mb-5 checkbox-info">
                <input type="checkbox" class="custom-control-input"
                       {{ $user->incident_obligation ? 'checked' : '' }}
                       id="incident_obligation" name="incident_obligation">
                <label class="custom-control-label" for="incident_obligation">OBLIGACIONES POR RECAUDAR</label>
            </div>
            <h5>Datos de la cuenta</h5>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="document">Cédula</label>
                        <input type="text" name="document" class="form-control" value="{{ old('document', $user->document) }}" disabled>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password">Contraseña <em>Ingresar solo si se desea modificar</em></label>
                        <input type="text" name="password" class="form-control" value="{{ old('password') }}">
                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ url('users') }}" class="btn btn-light">Volver a la lista de usuarios</a>
                <button class="btn btn-primary">Guardar usuario</button>
            </div>
        </div>
    </form>

</div>
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Departamentos asignados</h4>
    </div>
    <div class="card-body">
        <form action="{{ url('departamento-usuario') }}" method="POST">
            @csrf
            <input type="hidden" name="user_id" value="{{ $user->id }}">
            <div class="row">
                <div class="col-md-3">
                    <select name="department_id" class="form-control" id="select-department">
                        <option value="">Seleccione departamento</option>
                        @foreach ($departments as $department)
                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="process_id" class="form-control" id="select-process">
                        <option value="">Seleccione proceso</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="level_id" class="form-control" id="select-level">
                        <option value="">Seleccione nivel</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-primary btn-block">Asignar permisos</button>
                </div>
            </div>
        </form>

        <div class="table-responsive mt-4">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Departamento</th>
                    <th>Proceso</th>
                    <th>Nivel</th>
                    <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($departments_user as $department_user)
                    <tr>
                        <td>{{ $department_user->department->name }}</td>
                        <td>{{ $department_user->process ? $department_user->process->name : '' }}</td>
                        <td>{{ $department_user->level ? $department_user->level->name : '' }}</td>
                        <td>
                            <a href="{{ url('/departamento-usuario/'.$department_user->id.'/eliminar') }}" class="btn btn-sm btn-danger" title="Dar de baja">
                                <span class="fa fa-remove"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/admin/users/edit.js') }}"></script>
@endsection
