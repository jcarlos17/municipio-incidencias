@extends('layouts.app')

@section('styles')
    <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Usuarios</h4>
    </div>

    <div class="card-body">
        @if (session('notification'))
            <div class="alert alert-success alert-dismissible alert-alt fade show">
                {{ session('notification') }}
            </div>
        @endif

            <div class="row">
                <div class="col-sm-6">
                    <form>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Buscar..." name="inputSearch" value="{{ $inputSearch }}">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Buscar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-6">
                    <div class="form-group text-right">
                        <a href="{{ url('users/create') }}" class="btn btn-primary">Registrar usuarios</a>
                    </div>
                </div>
            </div>
        <div class="table-responsive mt-4">
            <table class="table table-striped">
                <thead>
                <tr class="text-uppercase">
                    <th>Cédula</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Actualizado</th>
                    <th class="text-center">Opciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->document }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->address }}</td>
                        <td>{{ $user->updatedFormat }}</td>
                        <td class="text-center">
                            <a href="{{ url('users/'.$user->id.'/edit') }}" class="btn btn-sm btn-primary mb-1" title="Editar">
                                <span class="fa fa-pencil"></span>
                            </a>
                            <button type="button" data-delete="{{ url('users/'.$user->id.'/delete') }}" class="btn btn-sm btn-danger mb-1" title="Dar de baja">
                                <span class="fa fa-trash"></span>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $users->links('includes.paginate') !!}
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <!-- Sweet alert 2 -->
    <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '[data-delete]', function () {
            let urlDelete = $(this).data('delete');

            swal({
                title: '¿Seguro que desea dar de baja a este usuario?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        });
    </script>
@endsection
