@extends('layouts.app')

@section('styles')
    <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Descripción del proceso: {{ $process->name }}</h4>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr class="active">
                    <th>#</th>
                    <th>Nivel</th>
                    <th>Responsable</th>
                    <th>Tiempo estimado / horas</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($levels as $key => $level)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $level->name }}</td>
                        <td>
                            @foreach($level->users as $user)
                                {{ "- $user->name" }} <br>
                            @endforeach
                        </td>
                        <td class="text-right">
                            {{ number_format($level->totalHours, 2) }}
                        </td>
                    </tr>
                @endforeach
                    <tr>
                        <td></td>
                        <td>
                            TOTAL ESTIMADO EN HORAS
                        </td>
                        <td></td>
                        <td class="text-right">
                            {{ $sumTotalHours }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer">
        <div class="form-group">
            <a href="{{ url('processes') }}" class="btn btn-light">Volver a la lista de procesos</a>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
