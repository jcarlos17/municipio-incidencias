@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Registrar proceso</h4>
    </div>
    <form action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-success alert-dismissible alert-alt fade show">
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible alert-alt fade show">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" name="name" class="form-control" id="name"
                       value="{{ old('name') }}">
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="department_id">Departamento</label>
                        <select class="form-control" name="department_id" id="department_id">
                            <option value="">Seleccionar</option>
                            @foreach($departments as $department)
                                <option value="{{ $department->id }}"
                                    {{ $department->id == old('department_id') ? 'selected' : '' }}>
                                    {{ $department->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Archivo</label>
                        <div class="custom-file">
                            <input type="file" name="file" class="custom-file-input"
                                   accept="application/pdf">
                            <label class="custom-file-label">Ningun archivo seleccionado</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ url('processes') }}" class="btn btn-light">Volver a la lista de procesos</a>
                <button class="btn btn-primary">Registrar proceso</button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
@endsection
