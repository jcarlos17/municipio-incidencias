@extends('layouts.app')

@section('styles')
    <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Editar proceso</h4>
    </div>
    <form action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-success alert-dismissible alert-alt fade show">
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible alert-alt fade show">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" name="name" class="form-control" value="{{ old('name', $process->name) }}">
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="department_id">Departamento</label>
                        <select class="form-control" name="department_id" id="department_id">
                            <option value="">Seleccionar</option>
                            @foreach($departments as $department)
                                <option value="{{ $department->id }}"
                                    {{ $department->id == old('department_id', $process->department_id) ? 'selected' : '' }}>
                                    {{ $department->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Archivo</label>
                        <div class="custom-file">
                            <input type="file" name="file" class="custom-file-input"
                                   accept="application/pdf">
                            <label class="custom-file-label">
                                {{ $process->file ?? 'Ningun archivo seleccionado' }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ url('processes') }}" class="btn btn-light">Volver a la lista de procesos</a>
                <button class="btn btn-primary">Guardar proceso</button>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Niveles</h4>
            </div>
            <div class="card-body">
                <button class="btn btn-sm btn-success waves-effect waves-light" data-toggle="modal" data-target="#modalNewLevel">
                    <i class="fa fa-plus"></i> Añadir nivel
                </button>

                <div class="table-responsive mt-4">
                    <table class="table table-striped">
                        <thead>
                        <tr class="active">
                            <th>#</th>
                            <th>Nivel</th>
                            <th>Días</th>
                            <th>Horas</th>
                            <th>Min</th>
                            <th class="text-center">Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($levels as $key => $level)
                            <tr>
                                <td>N{{ $key+1 }}</td>
                                <td>{{ $level->name }}</td>
                                <td class="text-center">{{ $level->days }}</td>
                                <td class="text-center">{{ $level->hours }}</td>
                                <td class="text-center">{{ $level->minutes }}</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-sm btn-primary" title="Editar" data-level="{{ $level->id }}">
                                        <span class="fa fa-pencil"></span>
                                    </button>
                                    <a href="{{ url('nivel/'.$level->id.'/eliminar') }}" class="btn btn-sm btn-danger" title="Dar de baja">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Requisitos</h4>
            </div>
            <div class="card-body">
                <button class="btn btn-sm btn-info waves-effect waves-light" data-toggle="modal" data-target="#modalNewRequirement">
                    <i class="glyphicon glyphicon-plus"></i> Añadir requisito
                </button>
                <div class="table-responsive mt-4">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="active">
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th class="text-center">Tipo</th>
                            <th class="text-center">Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($requirements as $key => $requirement)
                            <tr>
                                <td>{{ $requirement->name }}</td>
                                <td>{{ $requirement->description }}</td>
                                <td class="text-center">{{ $requirement->type ? 'formulario' : 'archivo' }}</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-sm btn-primary" title="Editar"
                                            data-form-type="{{ $requirement->form_type }}" data-requirement="{{ $requirement->id }}">
                                        <span class="fa fa-pencil"></span>
                                    </button>
                                    <button type="button" data-delete="{{ url('requisito/'.$requirement->id.'/eliminar') }}" class="btn btn-sm btn-danger" title="Eliminar">
                                        <span class="fa fa-trash"></span>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalEditLevel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Editar nivel</h4>
            </div>
            <form action="{{ url('nivel/editar') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="level_id" id="level_id" value="">
                    <div class="form-group">
                        <label for="name">Nombre del nivel</label>
                        <input type="text" required class="form-control" name="name" id="level_name">
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="days">Días</label>
                                <input type="number" min="0" class="form-control" name="days" id="level_day">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="hours">Horas</label>
                                <input type="number" min="0" max="23" class="form-control" name="hours" id="level_hour">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="minutes">Minutos</label>
                                <input type="number" min="0" max="59" class="form-control" name="minutes" id="level_minute">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar cambios</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalNewLevel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo nivel</h4>
            </div>
            <form action="{{ url('niveles') }}" method="POST" class="form-group">
                @csrf
                <input type="hidden" name="department_id" value="{{ $process->department_id }}">
                <input type="hidden" name="process_id" value="{{ $process->id }}">
                <div class="modal-body">
                    <div class="form-group">
                        <p>Niveles</p>
                        <input type="text" required name="name" placeholder="Ingrese nombre" class="form-control">
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <p>Días</p>
                                <input type="number" min="0" name="days" placeholder="" class="form-control" value="0">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <p>Horas</p>
                                <input type="number" min="0" max="23" name="hours" placeholder="" class="form-control" value="0">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <p>Minutos</p>
                                <input type="number" min="0" max="59" name="minutes" placeholder="" class="form-control" value="0">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar cambios</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalEditRequirement">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Editar requisito</h4>
            </div>
            <form action="{{ url('requisito/editar') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="requirement_id" id="requirement_id" value="">
                    <div class="form-group">
                        <p>Nombre</p>
                        <input type="text" name="name" id="requirement_name" placeholder="Ingrese nombre" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <p>Descripción</p>
                        <textarea name="description" id="requirement_description" class="form-control" rows="4" placeholder="Ingrese descripción"></textarea>
                    </div>
                    <div class="form-group">
                        <p>Tipo</p>
                        <div class="radio">
                            <label>
                                <input type="radio" name="type" id="archivo" value="0" required>
                                archivo
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="type" id="formulario" value="1">
                                formulario
                            </label>
                        </div>
                    </div>
                    <div class="form-group" id="select-form-type" style="display:none;">
                        <label for="form_type">Tipo formulario</label>
                        <select name="form_type" id="form_type" class="form-control">
                            <option value="">Seleccione</option>
                            <option value="certificado-no-adeudar-municipio">Formulario certificado de no adeudar al municipio</option>
                            <option value="solicitud-todo-tramite">Formulario solicitud todo trámite</option>
                            <option value="certificado-avaluos-catastros">Formularios certificado de avaluos y catastros (poseer bienes)</option>
                            <option value="certificado-avaluos-actualizacion-catastral">Formulario certificado de avaluos y actualización catastral</option>
                            <option value="normas-particulares">Formulario de Normas Particulares-línea de fábrica</option>
                            <option value="certificado-atencion-propiedad">Formulario certificado de atencion de la propiedad</option>
                            <option value="transferencia-dominio-compra-venta">Formularios para transferencia de dominio-compra venta</option>
                            <option value="permiso-construccion">Formulario de Permiso de construcción</option>
                            <option value="solicitud-aprobacion-planos">Formulario de solicitud de aprobación de planos</option>
                            <option value="certificado-no-poseer-bienes">Formulario de certificado de no poseer bienes</option>
                            <option value="actividad-economica">Formulario declaracion inicial de Actividad Económica</option>
                            <option value="emision-titulo-credito">Formulario emision titulo de credito</option>
                            <option value="informe-certificado">Formulario para informe o certificado</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar cambios</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalNewRequirement">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo requisito</h4>
            </div>
            <form action="{{ url('requisitos') }}" method="POST" class="form-group">
                @csrf
                <input type="hidden" name="department_id" value="{{ $process->department_id }}">
                <input type="hidden" name="process_id" value="{{ $process->id }}">
                <div class="modal-body">
                    <div class="form-group">
                        <p>Nombre</p>
                        <input type="text" name="name" placeholder="Ingrese nombre" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <p>Descripción</p>
                        <textarea name="description" id="description" class="form-control" rows="4" placeholder="Ingrese descripción"></textarea>
                    </div>
                    <div class="form-group">
                        <p>Tipo</p>
                        <div class="radio">
                            <label>
                                <input type="radio" name="type" id="archivo" value="0" required>
                                archivo
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="type" id="formulario" value="1">
                                formulario
                            </label>
                        </div>
                    </div>
                    <div class="form-group" id="select-form-type" style="display:none;">
                        <label for="form_type">Tipo formulario</label>
                        <select name="form_type" id="form_type" class="form-control">
                            <option value="">Seleccione</option>
                            <option value="certificado-no-adeudar-municipio">Formulario certificado de no adeudar al municipio</option>
                            <option value="solicitud-todo-tramite">Formulario solicitud todo trámite</option>
                            <option value="certificado-avaluos-catastros">Formularios certificado de avaluos y catastros (poseer bienes)</option>
                            <option value="certificado-avaluos-actualizacion-catastral">Formulario certificado de avaluos y actualización catastral</option>
                            <option value="normas-particulares">Formulario de Normas Particulares-línea de fábrica</option>
                            <option value="certificado-atencion-propiedad">Formulario certificado de atencion de la propiedad</option>
                            <option value="transferencia-dominio-compra-venta">Formularios para transferencia de dominio-compra venta</option>
                            <option value="permiso-construccion">Formulario de Permiso de construcción</option>
                            <option value="solicitud-aprobacion-planos">Formulario de solicitud de aprobación de planos</option>
                            <option value="certificado-no-poseer-bienes">Formulario de certificado de no poseer bienes</option>
                            <option value="actividad-economica">Formulario declaracion inicial de Actividad Económica</option>
                            <option value="emision-titulo-credito">Formulario emision titulo de credito</option>
                            <option value="informe-certificado">Formulario para informe o certificado</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar cambios</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <!-- Sweet alert 2 -->
    <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '[data-delete]', function () {
            let urlDelete = $(this).data('delete');

            swal({
                title: '¿Seguro que desea eliminar este requisito?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        });
    </script>
    <script src="{{ asset('/js/admin/process/edit.js?V=1.1') }}"></script>
    <script>
        $('[name="type"]').on('change', function () {
            const form = $(this).closest('form');
            const val = parseInt($(this).val());
            const $content = form.find('#select-form-type');
            const $select = form.find('#form_type');

            $select.attr('required', val === 1);
            if (val === 1){
                $content.show();
            } else {
                $content.hide();
                $select.val('');
            }
        });
    </script>
@endsection
