@extends('layouts.app')

@section('styles')
    <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Procesos</h4>
    </div>

    <div class="card-body">
        @if (session('notification'))
            <div class="alert alert-success alert-dismissible alert-alt fade show">
                {{ session('notification') }}
            </div>
        @endif

        <div class="form-group">
            <a href="{{ url('processes/create') }}" class="btn btn-primary">Registrar procesos</a>
        </div>

        <div class="table-responsive mt-4">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th class="text-center">Departamento</th>
                    <th class="text-center">Cantidad de Niveles</th>
                    <th class="text-center">Opciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($processes as $process)
                    <tr>
                        <td>{{ $process->name }}</td>
                        <td class="text-center">{{ $process->department->name }}</td>
                        <td class="text-center">{{ $process->levels_count }}</td>
                        <td class="col-sm-2 text-center">
                            <a href="{{ url('processes/'.$process->id.'/show') }}" class="btn btn-sm btn-dark mb-1" title="Ver">
                                <span class="fa fa-eye"></span>
                            </a>
                            <a href="{{ url('processes/'.$process->id.'/edit') }}" class="btn btn-sm btn-primary mb-1" title="Editar">
                                <span class="fa fa-pencil"></span>
                            </a>
                            <button type="button" data-delete="{{ url('processes/'.$process->id.'/delete') }}" class="btn btn-sm btn-danger mb-1" title="Dar de baja">
                                <span class="fa fa-trash"></span>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $processes->links('includes.paginate') !!}
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <!-- Sweet alert 2 -->
    <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '[data-delete]', function () {
            let urlDelete = $(this).data('delete');

            swal({
                title: '¿Seguro que desea dar de baja a este proceso?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        });
    </script>
@endsection
