@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Registrar funcionario</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                @if (session('notification'))
                    <div class="alert alert-success alert-dismissible alert-alt fade show">
                        {{ session('notification') }}
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name', $official->name) }}">
                </div>
                <div class="form-group">
                    <label for="position">Cargo</label>
                    <input type="text" name="position" class="form-control" value="{{ old('position', $official->position) }}">
                </div>
                <div class="form-group">
                    <label for="department_id">Departamento</label>
                    <select name="department_id" id="department_id" class="form-control">
                        <option value="">Seleccionar departmento</option>
                        @foreach ($departments as $department)
                            <option value="{{ $department->id }}"
                                {{ old('department_id', $official->department_id) == $department->id ? 'selected' : '' }}>
                                {{ $department->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ url('officials') }}" class="btn btn-light">Volver a la lista de funcionarios</a>
                    <button type="submit" class="btn btn-primary">Guardar cambios</button>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
@endsection
