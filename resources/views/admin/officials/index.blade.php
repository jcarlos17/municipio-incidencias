@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Funcionarios de la institución</h4>
    </div>

    <div class="card-body">
        @if (session('notification'))
            <div class="alert alert-success alert-dismissible alert-alt fade show">
                {{ session('notification') }}
            </div>
        @endif

        <div class="form-group">
            <a href="{{ url('officials/create') }}" class="btn btn-primary">Registrar funcionario</a>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Cargo</th>
                    <th>Departamento</th>
                    <th class="text-center">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($officials as $official)
                <tr>
                    <td>{{ $official->name }}</td>
                    <td>{{ $official->position }}</td>
                    <td>{{ $official->department->name }}</td>
                    <td class="text-center">
                        <a href="{{ url('officials/'.$official->id.'/edit') }}" class="btn btn-sm btn-primary mb-1" title="Editar">
                            <span class="fa fa-pencil"></span>
                        </a>
                        <a href="{{ url('officials/'.$official->id.'/delete') }}" class="btn btn-sm btn-danger mb-1" title="Eliminar">
                            <span class="fa fa-trash"></span>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $officials->links('includes.paginate') }}
    </div>
</div>
@endsection
