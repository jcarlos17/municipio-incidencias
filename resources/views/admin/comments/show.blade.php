@extends('layouts.app')

@section('styles')
    <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Comentario</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" class="form-control" value="{{ $comment->name }}" disabled>
                    </div>
                    <div class="form-group">
                        <label>Cédula</label>
                        <input type="text" class="form-control" value="{{ $comment->document }}" disabled>
                    </div>
                    <div class="form-group">
                        <label>Correo</label>
                        <input type="text" class="form-control" value="{{ $comment->email }}" disabled>
                    </div>
                    <div class="form-group">
                        <label>Celular</label>
                        <input type="text" class="form-control" value="{{ $comment->cellphone }}" disabled>
                    </div>
                    <div class="form-group">
                        <label>Comentarios y sugerencia</label>
                        <textarea class="form-control" rows="5">{{ $comment->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox ml-1">
                            <input type="checkbox" class="custom-control-input" id="permission" disabled
                            {{ $comment->permission ? 'checked' : '' }}>
                            <label class="custom-control-label" for="permission">Permitir que se contacten conmigo.</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox ml-1">
                            <input type="checkbox" class="custom-control-input" id="responsibility" disabled
                            {{ $comment->responsibility ? 'checked' : '' }}>
                            <label class="custom-control-label" for="responsibility">La información aquí ingresada es de mi absoluta responsabilidad.</label>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="form-group">
                        <a href="{{ url('comments') }}" class="btn btn-light">Volver a la lista de comentarios</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
