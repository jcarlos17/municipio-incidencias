@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Comentarios</h4>
    </div>

    <div class="card-body">
        <div class="table-responsive mt-4">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>N°</th>
                    <th>Nombre</th>
                    <th>Cédula</th>
                    <th>Correo</th>
                    <th class="text-center">Celular</th>
                    <th class="text-center">Fecha creación</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($comments as $comment)
                    <tr>
                        <td>{{ ($comments ->currentpage()-1) * $comments ->perpage() + $loop->index + 1 }}</td>
                        <td>{{ $comment->name }}</td>
                        <td>{{ $comment->document }}</td>
                        <td>{{ $comment->email }}</td>
                        <td class="text-center">{{ $comment->cellphone }}</td>
                        <td class="text-center">{{ $comment->created_at }}</td>
                        <td class="col-sm-2 text-center">
                            <a href="{{ url('comments/'.$comment->id.'/show') }}" class="btn btn-sm btn-primary mb-1" title="Ver">
                                <span class="fa fa-eye"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $comments->links('includes.paginate') !!}
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
