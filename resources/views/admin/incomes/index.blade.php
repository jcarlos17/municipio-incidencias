@extends('layouts.app')

@section('styles')
    <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Ingresos</h4>
    </div>

    <div class="card-body">
        @if (session('notification'))
            <div class="alert alert-success alert-dismissible alert-alt fade show">
                {{ session('notification') }}
            </div>
        @endif
        <div class="row">
            <div class="col-sm-6">
                <form>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Buscar..." name="inputSearch" value="{{ $inputSearch }}">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-6">
                <div class="form-group text-right">
                    <a href="{{ url('incomes/create') }}" class="btn btn-primary">Nuevo ingreso</a>
                </div>
            </div>
        </div>

        <div class="table-responsive mt-4">
            <table class="table table-striped">
                <thead>
                <tr class="text-uppercase">
                    <th>ID</th>
                    <th>Ingreso</th>
                    <th>Grupo</th>
                    <th class="text-center">Opciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($incomes as $income)
                    <tr>
                        <td>{{ $income->id }}</td>
                        <td>{{ $income->name }}</td>
                        <td>{{ $income->income_group->name }}</td>
                        <td class="text-center">
                            <a href="{{ url('incomes/'.$income->id.'/items') }}" class="btn btn-sm btn-success mb-1" title="Items">
                                Items
                            </a>
                            <a href="{{ url('incomes/'.$income->id.'/edit') }}" class="btn btn-sm btn-primary mb-1" title="Editar">
                                <span class="fa fa-pencil"></span>
                            </a>
                            <button type="button" data-delete="{{ url('incomes/'.$income->id.'/delete') }}" class="btn btn-sm btn-danger mb-1" title="Eliminar">
                                <span class="fa fa-trash"></span>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $incomes->appends(Request::except('page'))->links('includes.paginate') }}
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <!-- Sweet alert 2 -->
    <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '[data-delete]', function () {
            let urlDelete = $(this).data('delete');

            swal({
                title: '¿Seguro que desea eliminar este ingreso?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        });
    </script>
@endsection
