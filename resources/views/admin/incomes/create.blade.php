@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Nuevo ingreso</h4>
    </div>
    <form action="" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="name">Ingreso</label>
                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                @error('name')
                <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="income_group_id">Grupo</label>
                <select name="income_group_id" class="form-control @error('income_group_id') is-invalid @enderror">
                    <option value="">Seleccionar</option>
                    @foreach ($groups as $group)
                        <option value="{{ $group->id }}" {{ old('income_group_id') == $group->id ? 'selected' : '' }}>
                            {{ $group->name }}
                        </option>
                    @endforeach
                </select>
                @error('income_group_id')
                    <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                @enderror
            </div>
                <div class="form-group">
                    <label for="process_id">Proceso</label>
                    <select name="process_id" class="form-control @error('process_id') is-invalid @enderror">
                        <option value="">Seleccionar</option>
                        @foreach ($processes as $process)
                            <option value="{{ $process->id }}" {{ old('process_id') == $process->id ? 'selected' : '' }}>
                                {{ $process->name }}
                            </option>
                        @endforeach
                    </select>
                    @error('process_id')
                    <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                    @enderror
                </div>
        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ url('incomes') }}" class="btn btn-light">Volver a la lista de ingresos</a>
                <button class="btn btn-primary">Registrar ingreso</button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
@endsection
