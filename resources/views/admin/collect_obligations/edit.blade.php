@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Editar obligación por recaudar</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="client_id">Cliente</label>
                    <select name="client_id" id="client_id" class="form-control @error('client_id') is-invalid @enderror">
                        <option value="">Seleccionar</option>
                        @foreach ($clients as $client)
                            <option value="{{ $client->id }}" {{ old('client_id', $collectObligation->obligation->client_id) == $client->id ? 'selected' : '' }}>
                                {{ $client->name }}
                            </option>
                        @endforeach
                    </select>
                    @error('client_id')
                    <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="income_id">Ingreso</label>
                    <select name="income_id" id="income_id" class="form-control @error('income_id') is-invalid @enderror">
                        <option value="">Seleccionar</option>
                        @foreach ($collectObligation->obligation->client->incomes as $income)
                            <option value="{{ $income->id }}" {{ old('income_id', $collectObligation->obligation->income_id) == $income->id ? 'selected' : '' }}>
                                {{ $income->name }}
                            </option>
                        @endforeach
                    </select>
                    @error('income_id')
                    <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="code">Emisión</label>
                            <input type="date" name="issue_date" class="form-control" disabled
                                   value="{{ old('issue_date', $collectObligation->issue_date) }}">
                        </div>
                        <div class="form-group">
                            <label for="period">Periodo</label>
                            <input type="text" id="period" class="form-control" value="{{ $collectObligation->period }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="entry_order">Orden Ingreso</label>
                            <input type="text" id="entry_order" class="form-control" value="{{ $collectObligation->entry_order }}" disabled>
                        </div>
                    </div>
                </div>
                <h4><span class="d-block py-2 badge badge-dark">Items</span></h4>
                <div class="table-responsive" id="table">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th>ID</th>
                            <th class="w-75">Item</th>
                            <th class="w-25 text-center">Valor</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($collectObligation->details as $detail)
                            <tr>
                                <td><b>{{ $detail->item_id }}</b></td>
                                <td class="w-50">{{ $detail->item->description }}</td>
                                <td class="text-center w-25">
                                    <input type="hidden" name="item_ids[]" value="{{ $detail->item_id }}">
                                    <input type="number" step="0.01" class="form form-control form-control-sm" data-value name="values[]" required value="{{ $detail->value }}">
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="2" class="text-right"><b>Total</b></td>
                            <td class="text-center w-25">
                                <div class="form-group">
                                    <input type="number" class="form form-control form-control-sm" id="total" name="total" value="{{ $collectObligation->total }}" readonly>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ url('collect_obligations') }}" class="btn btn-light">Volver a la lista de obligaciones por recaudar</a>
                    <button type="submit" class="btn btn-primary">Guardar cambios</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/collect_obligations.js') }}"></script>
@endsection
