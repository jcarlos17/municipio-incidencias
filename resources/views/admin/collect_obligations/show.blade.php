@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Ver tributo</h4>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <label for="client_id" class="col-sm-2 col-form-label">Cliente</label>
                <div class="col-sm-6">
                    <select name="client_id" id="client_id" class="form-control" disabled>
                        <option selected>{{ $collectObligation->obligation->client->name }}</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="income_id" class="col-sm-2 col-form-label">Ingreso</label>
                <div class="col-sm-6">
                    <select name="income_id" id="income_id" class="form-control" disabled>
                        <option selected>{{ $collectObligation->obligation->income->name }}</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="issue_date" class="col-sm-2 col-form-label">Fecha emisión</label>
                <div class="col-sm-3">
                    <input type="date" class="form-control" id="issue_date" name="issue_date" value="{{ $collectObligation->issue_date }}" disabled>
                </div>
            </div>
            <h4><span class="d-block py-2 badge badge-dark">Impuestos</span></h4>
            <div class="table-responsive" id="table">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th>ID</th>
                        <th class="w-75">Impuesto</th>
                        <th class="w-25 text-center">Valor</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($collectObligation->details as $detail)
                        <tr>
                            <th scope="row">{{ $detail->item_id }}</th>
                            <td class="w-50">{{ $detail->item->name }}</td>
                            <td class="text-center w-25">
                                <input type="number" step="0.01" class="form form-control form-control-sm" value="{{ $detail->value }}" disabled>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="2" class="text-right">Total</th>
                        <td class="text-center w-25">
                            <div class="form-group">
                                <input type="number" class="form form-control form-control-sm" value="{{ $collectObligation->total }}" disabled>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <div class="form-group row mt-3">
        <div class="col-sm-10">
            <a href="{{ asset('collect_obligations') }}" class="btn btn-default">Volver</a>
        </div>
    </div>
@endsection
