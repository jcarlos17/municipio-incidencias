@extends('layouts.app')

@section('styles')
    <link href="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Obligaciones por recaudar</h4>
    </div>

    <div class="card-body">
        @if (session('notification'))
            <div class="alert alert-success alert-dismissible alert-alt fade show">
                {{ session('notification') }}
            </div>
        @endif
        <div class="row">
            <div class="col-sm-6">
                <div class="basic-form">
                    <form>
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <div class="form-group mb-2">
                                    <select class="form-control default-select" name="optionSearch" id="optionSearch">
                                        <option value="" >Buscar</option>
                                        <option value="OI" {{ $optionSearch == 'OI' ? 'selected' : '' }}>Orden de ingreso</option>
                                        <option value="I" {{ $optionSearch == 'I' ? 'selected' : '' }}>Ingreso</option>
                                        <option value="FE" {{ $optionSearch == 'FE' ? 'selected' : '' }}>Fecha de emisión</option>
                                        <option value="U" {{ $optionSearch == 'U' ? 'selected' : '' }}>Usuario</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="form-group mb-2">
                                    <input type="{{ $optionSearch == 'FE' ? 'date' : 'text' }}" name="inputSearch" id="inputSearch"
                                           class="form-control" placeholder="Buscar..." value="{{ $inputSearch }}">
                                </div>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary mb-2">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group text-right">
                    <a href="{{ url('collect_obligations/create') }}" class="btn btn-primary">Nueva oligación por recaudar</a>
                </div>
            </div>
        </div>

        <div class="table-responsive mt-4">
            <table class="table table-striped">
                <thead>
                <tr class="text-uppercase">
                    <th>ID</th>
                    <th>Orden ingreso</th>
                    <th>Cliente</th>
                    <th>Ingreso</th>
                    <th>Total</th>
                    <th>F_Emision</th>
                    <th>F_Pago</th>
                    <th>Usuario</th>
                    <th class="text-center">Opciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($collectObligations as $collectObligation)
                    <tr>
                        <td>{{ $collectObligation->id }}</td>
                        <td>{{ $collectObligation->entry_order }}</td>
                        <td>{{ $collectObligation->obligation->client->name }}</td>
                        <td>{{ $collectObligation->obligation->income->name }}</td>
                        <td>{{ $collectObligation->total }}</td>
                        <td>{{ $collectObligation->issue_date }}</td>
                        <td></td>
                        <td>{{ $collectObligation->user->name }}</td>
                        <td class="text-center">
                            <a href="{{ url('collect_obligations/'.$collectObligation->id.'/show') }}" class="btn btn-sm btn-success mb-1" title="Ver">
                                <span class="fa fa-eye"></span>
                            </a>
                            <a href="{{ url('collect_obligations/'.$collectObligation->id.'/edit') }}" class="btn btn-sm btn-primary mb-1" title="Editar">
                                <span class="fa fa-pencil"></span>
                            </a>
                            <button type="button" data-delete="{{ url('collect_obligations/'.$collectObligation->id.'/delete') }}" class="btn btn-sm btn-danger mb-1" title="Eliminar">
                                <span class="fa fa-trash"></span>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $collectObligations->appends(Request::except('page'))->links('includes.paginate') }}
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        const select = document.getElementById('optionSearch');
        const input = document.getElementById('inputSearch');

        select.addEventListener('change', function () {
            input.value = '';
            if (this.value === 'FE') {
                input.type = 'date';
            } else {
                input.type = 'text';
            }
        });
    </script>
    <!-- Sweet alert 2 -->
    <script src="{{ asset('vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '[data-delete]', function () {
            let urlDelete = $(this).data('delete');

            swal({
                title: '¿Seguro que desea eliminar esta Obligación por recaudar?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',

                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.value) {
                    location.href = urlDelete;
                }
            });
        });
    </script>
@endsection
