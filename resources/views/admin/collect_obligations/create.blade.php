@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Nueva obigación por recaudar</h4>
    </div>
    <form action="" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="client_id">Cliente</label>
                <select name="client_id" id="client_id" class="form-control @error('client_id') is-invalid @enderror" required>
                    <option value="">Seleccionar</option>
                    @foreach ($clients as $client)
                        <option value="{{ $client->id }}" {{ old('client_id') == $client->id ? 'selected' : '' }}>
                            {{ $client->name }}
                        </option>
                    @endforeach
                </select>
                @error('client_id')
                    <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="income_id">Ingreso</label>
                <select name="income_id" id="income_id" class="form-control @error('income_id') is-invalid @enderror" required>
                    <option value="">Seleccionar</option>
                </select>
                @error('income_id')
                    <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="code">Emisión</label>
                        <input type="date" name="issue_date" class="form-control @error('issue_date') is-invalid @enderror"
                               value="{{ old('issue_date') }}" data-id="{{ $futureId }}" required>
                        @error('issue_date')
                        <div class="invalid-feedback animated fadeInUp d-block">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="period">Periodo</label>
                        <input type="text" id="period" class="form-control" disabled>
                    </div>
                    <div class="form-group">
                        <label for="entry_order">Orden Ingreso</label>
                        <input type="text" id="entry_order" class="form-control" disabled>
                    </div>
                </div>
            </div>
            <h4><span class="d-block py-2 badge badge-dark">Items</span></h4>
            <div class="table-responsive" id="table">
            </div>
        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ url('collect_obligations') }}" class="btn btn-light">Volver a la lista de obligaciones por recaudar</a>
                <button type="submit" class="btn btn-primary">Registrar obligación por recaudar</button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/collect_obligations.js') }}"></script>
    <script>
        const period = $('#period');
        const order = $('#entry_order');

        $('[name="issue_date"]').on('change', function () {
            const value = this.value;
            const id = this.dataset.id;

            if (value) {
                const $date = value.split('-');
                const year = $date[0];
                const month = $date[1];
                const day = $date[2];

                period.val(year+month);
                order.val(year+month+day+'-'+id)
            }
        });
    </script>
@endsection
