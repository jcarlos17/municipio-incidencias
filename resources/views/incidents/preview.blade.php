@extends('layouts.app')

@section('content')
<div id="printableArea">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Incidencia generada</h4>
        </div>

        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-success alert-dismissible alert-alt fade show">
                    {{ session('notification') }}
                </div>
            @endif
            <div class="table-responsive mt-4">
                <table class="table table-bordered">
                    <thead>
                    <tr class="active">
                        <th>Código</th>
                        <th>Nombre del usuario</th>
                        <th>Tiempo estimado</th>
                        <th>Tipo de incidencia</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td id="incident_key">{{ $incident->code }}</td>
                        <td id="incident_client">{{ $incident->client->name }}</td>
                        <td id="incident_category">
                            @if ($count_days > 0)
                                {{ $count_days }}
                                {{ $count_days == 1 ? 'día' : 'días' }}
                            @endif
                            @if ($count_hours > 0)
                                {{ $count_days > 0 ? ',' : '' }}
                                {{ $count_hours }}
                                {{ $count_hours == 1 ? 'hora' : 'horas' }}
                            @endif
                            @if ($count_minutes > 0)
                                {{ $count_minutes > 0 ? ',' : '' }}
                                {{ $count_minutes }}
                                {{ $count_minutes == 1 ? 'minuto' : 'minutos' }}
                            @endif
                        </td>
                        <td id="incident_created_at">{{ $incident->department->name }}</td>
                    </tr>
                    </tbody>
                    <thead>
                    <tr class="active">
                        <th>Fecha de registro</th>
                        <th colspan="2"># de incidencias sin atender en la cola</th>
                        <th>Nivel de atención</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td id="incident_responsible">{{ $incident->created_at }}</td>
                        <td colspan="2">{{ $incident_count }}</td>
                        <td>{{ $incident->level->name }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="pull-right">
    <a href="/ver/{{ $incident->id }}" class="btn btn-light">Volver</a>
    @if(!auth()->user()->is(\App\Models\User::CLIENT))
        <a href="{{ url('guia/'.$incident->id) }}" target="_blank" class="btn btn-primary">Imprimir guía</a>
    @endif
    <button onclick="printDiv('printableArea')" class="btn btn-success waves-effect waves-light"><i class="glyphicon glyphicon-print"></i> Imprimir</button>
</div>
@endsection

@section('scripts')
    <script>
        function printDiv(divName) {
            let printContents = document.getElementById(divName).innerHTML;
            let originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
@endsection
