@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Incidencia</h4>
            @if($existsCollectObligation)
                <span class="badge badge-{{ $pending ? 'warning' : 'success' }}">
                    {{ $pending ? 'PENDIENTE DE PAGO' : 'INCIDENCIA PAGADA' }}
                </span>
            @endif
        </div>

        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-success alert-dismissible alert-alt fade show">
                    {{ session('notification') }}
                </div>
            @endif
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Departamento</th>
                        <th>Proceso</th>
                        <th>Fecha de envío</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td id="incident_key">{{ $incident->code }}</td>
                        <td id="incident_department">{{ $incident->department->name }}</td>
                        <td id="incident_process">{{ $incident->process_name }}</td>
                        <td id="incident_created_at">{{ $incident->created_at }}</td>
                    </tr>
                    </tbody>
                    <thead>
                    <tr>
                        <th>Asignada a</th>
                        <th>Nivel</th>
                        <th>Estado</th>
                        <th>Severidad</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td id="incident_responsible">{{ $incident->support_name }}</td>
                        <td>{{ $incident->level->name }}</td>
                        <td id="incident_state">{{ $incident->state }}</td>
                        <td id="incident_severity">{{ $incident->severity_full }}</td>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-bordered table-with-break-word">
                    <thead>
                    <tr>
                        <th class="col-sm-3">Cliente</th>
                        <td id="incident_summary">{{ $incident->client->name }}</td>
                    </tr>
                    <tr>
                        <th>Descripción</th>
                        <td id="incident_description">{{ $incident->description }}</td>
                    </tr>
                    @if($incident->archives()->count() == 0)
                        <tr>
                            <th>Archivos</th>
                            <td id="incident_attachment">No se han adjuntado archivos</td>
                        </tr>
                    @endif
                    </thead>
                </table>
            </div>
            @if ($incident->support_id == null && $incident->active && auth()->user()->canTake($incident))
            <a href="{{ url('incidencia/'.$incident->id.'/atender') }}" class="btn btn-primary btn-sm" id="incident_btn_apply">
                Atender incidencia
            </a>
            @endif
            @if ($incident->active)
                @if($take_incident)
                    <a href="{{ url('incidencia/'.$incident->id.'/resolver') }}" class="btn btn-info btn-sm" id="incident_btn_solve">
                        Marcar como resuelto
                    </a>
                @endif
                @if (auth()->id() == $incident->creator_id && $incident->level_id == $first->id && $incident->support_id == NULL)
                    <a href="{{ url('incidencia/'.$incident->id.'/editar') }}" class="btn btn-success btn-sm" id="incident_btn_edit">
                        Editar incidencia
                    </a>
                @endif
            @endif

            @if (auth()->id() == $incident->support_id && $incident->active && !$incident->firstLevel)
                <a href="{{ url('incidencia/'.$incident->id.'/derivar/anterior') }}" class="btn btn-info btn-sm">
                    Derivar al nivel anterior
                </a>
            @endif
            @if (auth()->id() == $incident->support_id && $incident->active && !$incident->last_level)
            <a href="{{ url('incidencia/'.$incident->id.'/derivar/siguiente') }}" class="btn btn-danger btn-sm">
                Derivar al siguiente nivel
            </a>
            @endif
            @if (auth()->id() == $incident->support_id && $incident->active)
                @if(auth()->user()->incident_obligation)
                    <a href="{{ url('incidencia/'.$incident->id.'/pago') }}" class="btn btn-dark btn-sm">
                        Obligaciones por recaudar
                    </a>
                @endif
                @if ($pending && auth()->user()->incident_cashier)
                    <a href="{{ url('incidents/'.$incident->id.'/charges') }}" class="btn btn-success btn-sm">
                        Cobros
                    </a>
                @endif
            @endif
            @if (auth()->id() == $incident->client_id && $pending)
                <a href="{{ url('incidencia/'.$incident->id.'/pagar') }}" class="btn btn-dark btn-sm">
                    Realizar pago
                </a>
            @endif
            @if (auth()->id() == $incident->support_id || (auth()->id() == $incident->client_id && (!$existsCollectObligation || !$pending)))
                <a href="{{ url('incidencia/'.$incident->id.'/formularios') }}" class="btn btn-warning btn-sm" id="incident_btn_form">
                    Formularios
                </a>
            @endif

            @if (auth()->id() == $incident->creator_id && $incident->active)
                <a href="{{ url('vista/'.$incident->id) }}" class="btn btn-light btn-sm pull-right">
                    Vista de impresión
                </a>
            @endif
        </div>
    </div>

    @if($incident->archives()->count() > 0 && auth()->user()->is(\App\Models\User::SUPPORT))
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Requisitos</h4>
            </div>
            <div class="card-body">
                <p>
                    <a class="btn btn-outline-info" data-toggle="collapse" href="#collapseArchive" aria-expanded="false" aria-controls="collapseArchive">
                        Ver requisitos
                    </a>
                </p>
                <div class="collapse" id="collapseArchive">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            @foreach($incident->archives as $archive)
                                <tr>
                                    <td>{{ $archive->name }}</td>
                                    <td class="text-right">
                                        <input type="file" data-input-file data-id="{{ $archive->id }}" accept="application/pdf" style="display:none">
                                        <button class="btn btn-sm btn-dark mr-2" type="button" data-archive="{{ $archive->id }}">Archivo</button>
                                        @if($archive->archive_id)
                                            <span><a href="{{ $archive->archive->url }}" target="_blank" class="btn btn-sm btn-dark">Ver</a></span>
                                        @else
                                            <span><button class="btn btn-sm btn-dark" type="button" data-archive="" disabled>Ver</button></span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @include('layouts.chat')

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Historial</h4>
        </div>
        <div class="card-body">
            <a class="btn btn-info btn-sm" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Ver historial de cambios
            </a>
            <div class="collapse" id="collapseExample">
                <div class="table-responsive mt-4">
                    <table class="table table-striped">
                        <thead>
                        <tr class="active">
                            <th>Descripción</th>
                            <th>Fecha y Hora</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($incident_histories as $incident_history)
                            <tr>
                                <td>{{ $incident_history->description }}</td>
                                <td>{{ $incident_history->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        const $body = $('#bodyFiles');
        const $processId = $('#process_id');
        const $btnArchive = $('#btnArchive');

        $(document).on('click', '[data-archive]', function () {
            const tr = $(this).closest('tr');
            const inputFile = tr.find('[data-input-file]');

            inputFile.click();
        });

        $(document).on('change', '[data-input-file]', function () {
            const tr = $(this).closest('tr');
            const span = tr.find('span');
            const incident_archive_id = $(this).data('id');

            const file_data = this.files[0];
            const form_data = new FormData();
            form_data.append('archive', file_data);
            form_data.append('_token', $('[name="_token"]').val());
            if (incident_archive_id) {
                form_data.append('incident_archive_id', incident_archive_id);
            }
            $.ajax({
                url: '/archivo/cargar',
                dataType: 'json',
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(archive){
                    this.value = '';
                    span.html('<a href="'+archive.url+'" target="_blank" class="btn btn-sm btn-dark">Ver</a>');
                }
            });
        });
    </script>
@endsection
