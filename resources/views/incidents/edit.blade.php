@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Editar incidencia</h4>
    </div>
    <form action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible alert-alt fade show">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if($user->id == $incident->creator_id && $incident->level_id == $firstLevelId && $incident->support_id == NULL)
                <div class="form-group">
                    <label for="process_id">Proceso</label>
                    <select name="process_id" class="form-control">
                        <option value="">Seleccionar</option>
                        @foreach ($processes as $process)
                            <option value="{{ $process->id }}" @if(old('process_id', $incident->process_id) == $process->id) selected @endif>
                                {{ $process->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="severity">Severidad</label>
                    <select name="severity" class="form-control">
                        <option value="M" @if(old('severity', $incident->severity)=='M') selected @endif>Menor</option>
                        <option value="N" @if(old('severity', $incident->severity)=='N') selected @endif>Normal</option>
                        <option value="A" @if(old('severity', $incident->severity)=='A') selected @endif>Alta</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Cliente</label>
                    <div class="input-group">
                        @if($user->is(\App\Models\User::CLIENT))
                            <input type="hidden" name="client_id" id="client_id" value="{{ $user->id }}">
                            <input type="text" name="document" id="document" class="form-control" value="{{ $incident->document_name }}" readonly>
                        @else
                            <input type="hidden" name="client_id" id="client_id" value="{{ old('client_id', $incident->client_id) }}">
                            <input type="text" name="document" id="document" class="form-control"
                                   value="{{ old('document', $incident->document_name) }}" readonly>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#modalSelectClient">
                                    Seleccionar cliente
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Descripción</label>
                    <textarea name="description" id="description" class="form-control">{{ old('description', $incident->description) }}</textarea>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        @foreach($incident->archives as $archive)
                            <tr>
                                <td>{{ $archive->name }}</td>
                                <td class="text-right">
                                    <input type="file" data-input-file data-id="{{ $archive->id }}" accept="application/pdf" style="display:none">
                                    <button class="btn btn-sm btn-dark mr-2" type="button" data-archive="{{ $archive->id }}">Archivo</button>
                                    @if($archive->archive_id)
                                        <span><a href="{{ $archive->archive->url }}" target="_blank" class="btn btn-sm btn-dark">Ver</a></span>
                                    @else
                                        <span><button class="btn btn-sm btn-dark" type="button" data-archive="" disabled>Ver</button></span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-dismissible alert-info">
                    La incidencia está siendo <strong>atendida</strong>, por lo que no podrá ser editada.
                </div>
            @endif
        </div>
        @if($user->id == $incident->creator_id && $incident->level_id == $firstLevelId && $incident->support_id == NULL)
            <div class="card-footer">
                <div class="form-group">
                    <button class="btn btn-primary">Guardar cambios</button>
                </div>
            </div>
        @endif
    </form>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalSelectClient">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Seleccionar cliente</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible alert-alt fade show" id="alert" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    Es necesario completar los campos para buscar
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <select name="" id="type" class="form-control">
                                <option value="">Seleccionar</option>
                                <option value="document">Buscar por cédula</option>
                                <option value="name">Buscar por nombre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <input type="text" class="form-control" id="inputSearch">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <button class="btn btn-light" id="btnSearch">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Cédula</th>
                        <th>Nombre</th>
                        <th>Código</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="content-search">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        const $btnSearch = $('#btnSearch');
        const $option = $('#type');
        const $input = $('#inputSearch');
        const $alert = $('#alert');
        const $content = $('#content-search');
        const $processId = $('#process_id');

        const $body = $('#bodyFiles');
        const $btnArchive = $('#btnArchive');

        $(document).on('click', '[data-archive]', function () {
            const tr = $(this).closest('tr');
            const inputFile = tr.find('[data-input-file]');

            inputFile.click();
        });

        $(document).on('change', '[data-input-file]', function () {
            const tr = $(this).closest('tr');
            const span = tr.find('span');
            const incident_archive_id = $(this).data('id');

            const file_data = this.files[0];
            const form_data = new FormData();
            form_data.append('archive', file_data);
            form_data.append('_token', $('[name="_token"]').val());
            if (incident_archive_id) {
                form_data.append('incident_archive_id', incident_archive_id);
            }
            $.ajax({
                url: '/archivo/cargar',
                dataType: 'json',
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(archive){
                    this.value = '';
                    span.html('<a href="'+archive.url+'" target="_blank" class="btn btn-sm btn-dark">Ver</a>');
                }
            });
        });

        $(document).ready(function() {
            $btnSearch.click(function () {
                $(this).attr('disabled', true);
                $alert.hide();

                if ($option.val() && $processId.val()) {
                    // AJAX
                    $.get('/api/clients?type='+$option.val()+'&input_search='+$input.val()+'&process_id='+$processId.val(), function (obligations) {
                        let html = '';
                        Array.prototype.forEach.call(obligations, function(obligation) {
                            html += '<tr>' +
                                '<td>'+obligation.client.document+'</td>' +
                                '<td>'+obligation.client.name+'</td>' +
                                '<td>'+obligation.code+'</td>' +
                                '<td class="text-center"><button class="btn btn-light" type="button" data-obligation="'+obligation.id+'" data-value="'+obligation.client.document+' '+obligation.client.name+'" data-select="'+obligation.client_id+'">Seleccionar</button></td>' +
                                '</tr>';
                        });
                        $content.html(html);
                    });
                    $(this).attr('disabled', false);
                } else {
                    $(this).attr('disabled', false);
                    $alert.show();
                }
            });
        });

        $(document).on('click', '[data-select]', function () {
            const id = $(this).data('select');
            const value = $(this).data('value');
            const obligation = $(this).data('obligation');

            $('#obligation_id').val(obligation);
            $('#client_id').val(id);
            $('#document').val(value);
            $content.html('');
            $input.val('');
            $option.val('');

            $('#modalSelectClient').modal('hide');
        });
    </script>
@endsection
