@extends('layouts.app')

@section('styles')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Registrar formulario</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group row">
                    <label for="code" class="col-sm-3 col-form-label">Documento</label>
                    <div class="col-sm-3">
                        <input type="text" id="code" class="form-control" value="{{ $code }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-3 col-form-label">Fecha</label>
                    <div class="col-sm-3">
                        <input type="date" id="date" class="form-control" value="{{ date('Y-m-d') }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="from_official_name" class="col-sm-3 col-form-label">De</label>
                    <div class="col-sm-6">
                        <select class="form-control" name="from_official_name" id="from_official_name">
                            <option value="">Elegir</option>
                            @foreach($officials as $official)
                                <option value="{{ $official->name }}" {{ old('from_official_name') == $official->name ? 'selected' : '' }}>
                                    {{ $official->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="to_official_name" class="col-sm-3 col-form-label">Para</label>
                    <div class="col-sm-6">
                        <select class="form-control" name="to_official_name" id="to_official_name">
                            <option value="">Elegir</option>
                            @foreach($officials as $official)
                                <option value="{{ $official->name }}" {{ old('to_official_name') == $official->name ? 'selected' : '' }}>
                                    {{ $official->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <h5>Detalle</h5>
                <div class="form-group row">
                    <label for="client" class="col-sm-3 col-form-label">Agradecere  emitir un título de crédito a nombre del Sr.(a).</label>
                    <div class="col-sm-6">
                        <input type="text" id="client" class="form-control" value="{{ $incident->client->name }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="document" class="col-sm-3 col-form-label">con cedula de indentidad N°:</label>
                    <div class="col-sm-3">
                        <input type="text" id="document" class="form-control" value="{{ $incident->client->document }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="quantity" class="col-sm-3 col-form-label">Por la cantidad de:</label>
                    <div class="col-sm-3">
                        <input type="text" id="quantity" name="quantity" class="form-control" value="{{ old('quantity') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="concept" class="col-sm-3 col-form-label">Por concepto de:</label>
                    <div class="col-sm-3">
                        <input type="text" id="concept" name="concept" class="form-control" value="{{ old('concept') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="street" class="col-sm-3 col-form-label">Calle:</label>
                    <div class="col-sm-3">
                        <input type="text" id="street" name="street" class="form-control" value="{{ old('street') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="district" class="col-sm-3 col-form-label">Barrio:</label>
                    <div class="col-sm-3">
                        <input type="text" id="district" name="district" class="form-control" value="{{ old('district') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="parish" class="col-sm-3 col-form-label">Parroquia:</label>
                    <div class="col-sm-3">
                        <input type="text" id="parish" name="parish" class="form-control" value="{{ old('parish') }}">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ route('form.index', ['incident' => $incident->id]) }}" class="btn btn-light" title="Volver">Volver</a>
                    <button type="submit" class="btn btn-success">Registrar formulario</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')

@endsection
