<html>
<head>
    <title>{{ $form->code }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 2.5cm; }
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: none;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 120px;
        }
    </style>
</head>
<body>
<div style="margin: 0 auto;">
    <table style="margin: 0;">
        <tr style="">
            <td style="width: 53%">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
            <td style="text-align: right">
                <p style="font-weight: bold; margin: 0; font-size: 16px">Documento: <b>{{ $form->code }}</b></p>
            </td>
        </tr>
    </table>
    <table style="margin: 0 auto;">
        <tr style="">
            <td style=" text-align: center">
                <p style="font-weight: bold; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; font-size: 14px">MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p style="background: black; color: white; font-weight: bold; padding: 7px !important; margin: 0 19em !important;">ORDEN DE PAGO</p>
            </td>
        </tr>
    </table>
    <table style="margin: 0">
        <tr>
            <td style="width: 53%"></td>
            <td style="text-align: right">
                <p style="padding: 7px; margin: 0"></p>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style=" font-weight: bold;">
                <span>Para</span>
            </td>
            <td style="width: 90%">
                <p style="margin: 0">{{ $form->from_official_name }}</p>
                <p style="margin: 3px 0">{{ $form->from_official_position }}</p>
            </td>
        </tr>
        <tr>
            <td style=" font-weight: bold;">
                <span>De</span>
            </td>
            <td style="width: 90%">
                <p style="margin: 0">{{ $form->to_official_name }}</p>
                <p style="margin: 3px 0">{{ $form->to_official_position }}</p>
            </td>
        </tr>
    </table>
    <p style="text-align: right !important; margin-bottom: 10px">{{ $form->dateComplete }}</p>
    <table>
        <tr>
            <td style="font-weight: bold;">
                <p>Detalle</p>
            </td>
            <td style="width: 90%"></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <p>Agradecere  emitir un título de crédito a nombre del Sr.(a). <b>{{ $form->client_name }}</b></p>
                <p>con cedula de indentidad N°: <b>{{ $form->client_document }}</b></p>
                <p>Por la cantidad de: <b>{{ $form->quantity }}</b></p>
                <p>Por concepto de: <b>{{ $form->concept }}</b></p>
                <p>Calle: <b>{{ $form->street }}</b></p>
                <p>Barrio: <b>{{ $form->district }}</b></p>
                <p>Parroquia: <b>{{ $form->parish }}</b></p>
            </td>
        </tr>
    </table>
    <footer>
        <table>
            <tr>
                <td style="text-align: center">
                    <p>Atentamente</p>
                    <p style="margin-top: 4em">________________________________________</p>
                    <p>Solicitante</p>
                </td>
            </tr>
        </table>
    </footer>
</div>
</body>
</html>
