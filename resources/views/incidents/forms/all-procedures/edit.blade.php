@extends('layouts.app')

@section('styles')
    <!-- Summernote -->
    <link href="{{ asset('vendor/summernote/summernote.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Editar formulario</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group row">
                    <label id="$code" class="col-sm-3 col-form-label">Documento</label>
                    <div class="col-sm-3">
                        <input type="text" id="$code" class="form-control" value="{{ $form->code }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label id="date" class="col-sm-3 col-form-label">Fecha</label>
                    <div class="col-sm-3">
                        <input type="datetime-local" id="date" class="form-control" value="{{ date('Y-m-d').'T'.date('H:i') }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="client" class="col-sm-3 col-form-label">{{ $form->type == 'SOL' ? 'De' : 'Para' }}</label>
                    <div class="col-sm-3">
                        <input type="hidden" id="client_id" name="client_id" value="{{ old('client_id', $client->id) }}">
                        <input type="text" name="client_name" id="client_name" class="form-control" value="{{ old('client_name', $form->client_name) }}" readonly>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#modalSelectClient">
                                Seleccionar cliente
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="official_name" class="col-sm-3 col-form-label">{{ $form->type == 'SOL' ? 'Para' : 'De' }}</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="official_name" id="official_name">
                            <option value="">Elegir</option>
                            @foreach($officials as $official)
                                <option value="{{ $official->name }}" {{ old('official_name', $form->official_name) == $official->name ? 'selected' : '' }}>
                                    {{ $official->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="detail" class="col-sm-3 col-form-label">Detalle</label>
                    <div class="col-sm-9">
                        <textarea class="summernote" id="detail" name="detail">{{ old('detail', $form->detail) }}</textarea>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ route('form.index', ['incident' => $form->incident_id]) }}" class="btn btn-light" title="Volver">Volver</a>
                    <button type="submit" class="btn btn-success">Guardar cambios</button>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modalSelectClient">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Seleccionar cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible alert-alt fade show" id="alert" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Es necesario completar los campos y seleccionar un proceso para buscar
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <select name="" id="type" class="form-control">
                                    <option value="">Seleccionar</option>
                                    <option value="document">Buscar por cédula</option>
                                    <option value="name">Buscar por nombre</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="inputSearch">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <button class="btn btn-light" id="btnSearch">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Cédula</th>
                            <th>Nombre</th>
                            <th>Código</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="content-search">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Summernote -->
    <script src="{{ asset('vendor/summernote/js/summernote.min.js') }}"></script>
    <!-- Summernote init -->
    <script src="{{ asset('js/plugins-init/summernote-init.js') }}"></script>

    <script src="{{ asset('js/select_client.js') }}"></script>
@endsection
