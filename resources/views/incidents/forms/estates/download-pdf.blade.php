<html>
<head>
    <title>{{ $form->code }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 2.5cm; }
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: none;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 60px;
        }
    </style>
</head>
<body>
<div style="margin: 0 auto;">
    <table style="margin: 0;">
        <tr style="">
            <td style="width: 53%">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
            <td style="text-align: right">
                <p style="font-weight: bold; margin: 0; font-size: 16px">Documento: {{ $form->code }}</p>
            </td>
        </tr>
    </table>
    <table style="margin: 0 auto;">
        <tr style="">
            <td style=" text-align: center; position: relative">
                <p style="font-weight: bold; margin-top: 0; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; font-size: 14px">MUNICIPAL DEL CANTÓN SALCEDO</p>
            </td>
        </tr>
    </table>
    <table style="">
        <tr style="">
            <td style=" text-align: right">
                <p style="font-weight: bold;">SECCIÓN:  AVALÚOS Y CATASTROS</p>
            </td>
        </tr>
    </table>
    <p style="text-align: left !important; margin-bottom: 2em">Fecha: <span style="font-weight: bold">{{ $form->dateComplete }}</span></p>
    <p style="text-align: center !important; margin-bottom: 2em; font-weight: bold">
        EL JEFE DE AVALÚOS Y CATASTROS MUNICIPALES
    </p>
    <table>
        <tr>
            <td style="text-align: justify; line-height: 2em">
                <span style="font-weight: bold">CERTIFICA:</span>
                Que revisado los catastros de contribuyentes del Impuesto predial
                tanto Urbano como Rural,
                del año <u style="font-weight: bold">{{ $form->year }}</u>
                no se encuentra registrado el nombre del Sr.
                <u style="font-weight: bold">{{ $form->client_name }}</u>
                por lo tanto, NO POSEE BIENES raíces de ninguna clase
                en esta jurisdicción cantonal.
            </td>
        </tr>
    </table>
    <footer>
        <table style="width: 100%">
            <tr>
                <td style="text-align: center">
                    <p>________________________________________</p>
                    <p style="font-weight: bold">EL JEFE DE AVALÚOS Y CATASTROS </p>
                </td>
            </tr>
        </table>
    </footer>
</div>
</body>
</html>
