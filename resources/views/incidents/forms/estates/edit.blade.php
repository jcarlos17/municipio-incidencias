@extends('layouts.app')

@section('styles')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Editar formulario</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group row">
                    <label for="date" class="col-sm-3 col-form-label">Fecha</label>
                    <div class="col-sm-3">
                        <input type="datetime-local" id="date" class="form-control" value="{{ date('Y-m-d').'T'.date('H:i') }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="client" class="col-sm-3 col-form-label">Cliente</label>
                    <div class="col-sm-3">
                        <input type="text" id="client" class="form-control" value="{{ $form->client_name }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="year" class="col-sm-3 col-form-label">Año</label>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" id="year" name="year" min="1900" max="2099" step="1" value="{{ old('year', $form->year) }}">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ route('form.index', ['incident' => $form->incident_id]) }}" class="btn btn-light" title="Volver">Volver</a>
                    <button type="submit" class="btn btn-success">Registrar formulario</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')

@endsection
