<html>
<head>
    <title>{{ $form->code }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 2.5cm; }
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: none;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 112px   ;
        }
    </style>
</head>
<body>
<div style="margin: 0 auto;">
    <table>
        <tr style="">
            <td style="width: 53%">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
            <td style="text-align: right">
                <p style="font-weight: bold; margin: 0; font-size: 16px">Documento: {{ $form->code }}</p>
            </td>
        </tr>
    </table>
    <table style="margin: 0;">
        <tr style="">
            <td style=" text-align: center; position: relative">
                <p style="font-weight: bold; margin-top: 0; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; font-size: 14px">MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p style="font-weight: bold; font-size: 12px">DEPARTAMENTO FINANCIERO</p>
                <p style="background: black; color: white; font-weight: bold; padding: 7px !important; margin: 0 21em !important;">CERTIFICADO</p>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="font-weight: bold">
                <span>Sección:</span>
            </td>
            <td style="width: 90%; font-weight: bold">
                <span>AVALÚOS Y CATASTROS</span>
            </td>
        </tr>
        <tr>
            <td style=" font-weight: bold">
                <p style="margin-top: 0">Fecha:</p>
            </td>
            <td style="width: 90%">
                <p style="text-decoration: underline; margin-top: 0; margin-bottom: 2em">___{{ $form->createdAtFormat }}___</p>
            </td>
        </tr>
        <tr>
            <td colspan="2" style=" font-weight: bold;">
                <span>EL SUSCRITO JEFE DE AVALÚOS Y CATASTROS MUNICIPALES</span>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <span style="font-weight: bold;">CERTIFICA:</span>
            </td>
            <td style="border-bottom: 1px solid; width: 90%; text-align: justify-all;">
                Que revisado el catastro si se encuentra registrado el nombre de:
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-bottom: 1px solid; width: 90%">
                <span style="text-transform: uppercase">{{ $form->client_name }}</span>
                con el siguiente detalle predial.
            </td>
        </tr>
    </table>
    <table>
        <tbody>
            <tr>
                <td colspan="5" style="border-bottom: 1px solid; height: 15px"></td>
            </tr>
            <tr>
                <td colspan="5" style="border-bottom: 1px solid; height: 15px"></td>
            </tr>
            <tr style="font-size: 12px">
                <td style="border-bottom: 1px solid; font-weight: bold">PREDIO</td>
                <td style="border-bottom: 1px solid; font-weight: bold">PARROQUIA</td>
                <td style="border-bottom: 1px solid; font-weight: bold">UBICACIÓN</td>
                <td style="border-bottom: 1px solid; text-align: center; font-weight: bold">CLAVE CATASTRAL</td>
                <td style="border-bottom: 1px solid; text-align: center; font-weight: bold">AVALUO COMERCIAL</td>
            </tr>
            @foreach($form->locations as $location)
                <tr style="font-size: 12px">
                    <td style="border-bottom: 1px solid">{{ $location->state }}</td>
                    <td style="border-bottom: 1px solid">{{ $location->parish }}</td>
                    <td style="border-bottom: 1px solid">{{ $location->location }}</td>
                    <td style="border-bottom: 1px solid; text-align: center">{{ $location->key }}</td>
                    <td style="border-bottom: 1px solid; text-align: right">
                        {{ $location->commercial }}
                    </td>
                </tr>
            @endforeach
            <tr>
                <td colspan="5" style="border-bottom: 1px solid; height: 15px"></td>
            </tr>
            <tr>
                <td colspan="5" style="border-bottom: 1px solid; height: 15px"></td>
            </tr>
            <tr>
                <td colspan="5" style="border-bottom: 1px solid">Es todo cuanto puedo certificar</td>
            </tr>
            <tr>
                <td colspan="5" style="border-bottom: 1px solid; height: 15px"></td>
            </tr>
            <tr>
                <td colspan="5" style="border-bottom: 1px solid; height: 15px"></td>
            </tr>
        </tbody>
    </table>
    <footer>
        <table>
            <tr>
                <td style="text-align: center">
                    <p style="margin-bottom: 0">RODRIGUEZ FRANCISCO ING.</p>
                    <p style="margin-bottom: 0">JEFE DE AVALÚOS Y CATASTROS</p>
                    <p style="margin-bottom: 0">________________________________________</p>
                    <p style="font-weight: bold">JEFE DE AVALÚOS Y CATASTROS</p>
                </td>
            </tr>
        </table>
    </footer>
</div>
</body>
</html>
