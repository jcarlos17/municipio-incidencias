@extends('layouts.app')

@section('styles')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Certificado de avalúos y catastros</h4>
        </div>
        <div class="card-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible alert-alt fade show">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group row">
                <label id="date" class="col-sm-3 col-form-label">Fecha</label>
                <div class="col-sm-3">
                    <input type="datetime-local" id="date" class="form-control" value="{{ date('Y-m-d').'T'.date('H:i') }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="client" class="col-sm-3 col-form-label">Cliente</label>
                <div class="col-sm-3">
                    <input type="text" id="client" class="form-control" value="{{ $form->client_name }}" disabled>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Ubicaciones</h4>
                </div>
                <div class="card-body">
                    <button type="button" class="btn btn-sm btn-info waves-effect waves-light" data-toggle="modal" data-target="#modalNewLocation">
                        <i class="glyphicon glyphicon-plus"></i> Añadir ubicación
                    </button>
                    <div class="table-responsive mt-4">
                        <table class="table table-bordered">
                            <thead>
                            <tr class="active">
                                <th>Predio</th>
                                <th>Parroquia</th>
                                <th>Ubicación</th>
                                <th>Clave</th>
                                <th>Avalúo comercial</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($form->locations as $key => $location)
                                <tr>
                                    <td>{{ $location->state }}</td>
                                    <td>{{ $location->parish }}</td>
                                    <td>{{ $location->location }}</td>
                                    <td>{{ $location->key }}</td>
                                    <td>{{ $location->commercial }}</td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-sm btn-primary" title="Editar" data-id="{{ $location->id }}">
                                            <span class="fa fa-pencil"></span>
                                        </button>
                                        <a href="{{ url('ubicaciones/'.$location->id.'/eliminar') }}" class="btn btn-sm btn-danger" title="Dar de baja">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ route('form.index', ['incident' => $form->incident_id]) }}" class="btn btn-light" title="Volver">Volver</a>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modalEditLocation">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar ubicación</h4>
                </div>
                <form action="{{ url('ubicaciones/editar') }}" method="POST" class="form-group">
                    @csrf
                    <input type="hidden" name="location_id">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="state">Predio:</label>
                            <select name="state" id="state" class="form-control">
                                <option value="">Seleccione</option>
                                <option value="URBANO">URBANO</option>
                                <option value="RURAL">RURAL</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="parish">Parroquia:</label>
                            <select name="parish" id="parish" class="form-control">
                                <option value="">Seleccione</option>
                                <option value="SAN MIGUEL">SAN MIGUEL</option>
                                <option value="ANTONIO JOSE HOLGUIN">ANTONIO JOSE HOLGUIN</option>
                                <option value="CUSUBAMBA">CUSUBAMBA</option>
                                <option value="MULALILLO">MULALILLO</option>
                                <option value="MULLIQUINDIL">MULLIQUINDIL</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="location">Ubicación</label>
                            <input type="text" class="form-control" id="location" name="location" placeholder="Ingresar ubicación">
                        </div>
                        <div class="form-group">
                            <label for="key">Clave</label>
                            <input type="text" class="form-control" id="key" name="key" placeholder="Clave catastral">
                        </div>
                        <div class="form-group">
                            <label for="commercial">Avalúo comercial</label>
                            <input type="text" class="form-control" id="commercial" name="commercial" placeholder="Ingresar avalúo comercial">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modalNewLocation">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Añadir ubicación</h4>
                </div>
                <form action="{{ url('ubicaciones/crear') }}" method="POST" class="form-group">
                    @csrf
                    <input type="hidden" name="form_id" value="{{ $form->id }}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="state">Predio:</label>
                            <select name="state" id="state" class="form-control" required>
                                <option value="">Seleccione</option>
                                <option value="URBANO">URBANO</option>
                                <option value="RURAL">RURAL</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="parish">Parroquia:</label>
                            <select name="parish" id="parish" class="form-control" required>
                                <option value="">Seleccione</option>
                                <option value="SAN MIGUEL">SAN MIGUEL</option>
                                <option value="ANTONIO JOSE HOLGUIN">ANTONIO JOSE HOLGUIN</option>
                                <option value="CUSUBAMBA">CUSUBAMBA</option>
                                <option value="MULALILLO">MULALILLO</option>
                                <option value="MULLIQUINDIL">MULLIQUINDIL</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="location">Ubicación</label>
                            <input type="text" class="form-control" id="location" name="location" placeholder="Ingresar ubicación" required>
                        </div>
                        <div class="form-group">
                            <label for="key">Clave</label>
                            <input type="text" class="form-control" id="key" name="key" placeholder="Clave catastral" required>
                        </div>
                        <div class="form-group">
                            <label for="commercial">Avalúo comercial</label>
                            <input type="text" class="form-control" id="commercial" name="commercial" placeholder="Ingresar avalúo comercial" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('[data-id]').on('click', function (){

            const id = $(this).data('id');
            const modal = $('#modalEditLocation');
            const location_id = modal.find('[name="location_id"]')
            const state = modal.find('#state');
            const parish = modal.find('#parish');
            const location = modal.find('#location');
            const key = modal.find('#key');
            const commercial = modal.find('#commercial');
            const tr = $(this).closest('tr');
            // id
            location_id.val(id);

            let val_state = tr.find('td:eq(0)').text();
            state.val(val_state);
            // parish
            let val_parish = tr.find('td:eq(1)').text();
            parish.val(val_parish);
            // location
            let val_location = tr.find('td:eq(2)').text();
            location.val(val_location);
            // key
            let val_key = tr.find('td:eq(3)').text();
            key.val(val_key);
            // commercial
            let val_commercial = tr.find('td:eq(4)').text();
            commercial.val(val_commercial);
            // show
            modal.modal('show');
        });
    </script>
@endsection
