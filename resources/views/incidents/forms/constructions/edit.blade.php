@extends('layouts.app')

@section('styles')
    <!-- Summernote -->
    <link href="{{ asset('vendor/summernote/summernote.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Editar formulario</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group row">
                    <label id="date" class="col-sm-3 col-form-label">Fecha</label>
                    <div class="col-sm-3">
                        <input type="datetime-local" id="date" class="form-control" value="{{ date('Y-m-d').'T'.date('H:i') }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="client" class="col-sm-3 col-form-label">Cliente:</label>
                    <div class="col-sm-3">
                        <input type="text" id="client" class="form-control" value="{{ $form->client_name }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-3 col-form-label">Descripción</label>
                    <div class="col-sm-9">
                        <textarea class="summernote" id="description" name="description">{{ old('description', $form->description) }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="observation" class="col-sm-3 col-form-label">Observaciones</label>
                    <div class="col-sm-9">
                        <textarea class="summernote" id="observation" name="observation">{{ old('observation', $form->observation) }}</textarea>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ route('form.index', ['incident' => $form->incident_id]) }}" class="btn btn-light" title="Volver">Volver</a>
                    <button type="submit" class="btn btn-success">Guardar cambios</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <!-- Summernote -->
    <script src="{{ asset('vendor/summernote/js/summernote.min.js') }}"></script>
    <!-- Summernote init -->
    <script src="{{ asset('js/plugins-init/summernote-init.js') }}"></script>
@endsection
