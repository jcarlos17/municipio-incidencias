<html>
<head>
    <title>{{ $form->code }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 2.5cm; }
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: none;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 120px;
        }
    </style>
</head>
<body>
<div style="margin: 0 auto;">
    <table style="margin: 0;">
        <tr style="">
            <td style="width: 53%">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
            <td style="text-align: right">
                <p style="font-weight: bold; margin: 0; font-size: 16px">Documento: {{ $form->code }}</p>
            </td>
        </tr>
    </table>
    <table style="margin: 0 auto;">
        <tr style="">
            <td style=" text-align: center">
                <p style="font-weight: bold; margin-top: 0; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; margin-bottom: 0; font-size: 14px">MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p style="font-weight: bold; margin-bottom: 0">DIRECCIÓN DE PLANIFICACIÓN</p>
                <p STYLE="font-weight: bold;">PERMISO DE CONSTRUCCIÓN</p>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <p style="font-weight: bold">{{ $form->dateComplete }}</p>
                <p>La Dirección de Obras Públicas Municipales, concede permiso definitivo al Señor: </p>
                <p style="font-weight: bold">{{ $form->client_name }}</p>
                <p>Para que continúe la construcción de: </p>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="border: 1px solid; padding: 1em">
                {!! $form->description !!}
            </td>
        </tr>
        <tr>
            <td>
                <p>OBSERVACIONES:</p>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid; padding: 1em">
                {!! $form->observation !!}
            </td>
        </tr>
    </table>
    <p>Nota</p>
    <ol style="margin: 5px">
        <li>El propietario se sujetará estrictamente a solo lo que consta en la autorización que antecede, en el caso de
            extralimitarse en cualquier otra obra, la Dirección de Obras ordenará el derrocamiento a costa del interesado.
        </li>
        <li>
            Este permiso no significa título legal alguno que pueda hacerse valer contra terceros, ni que vaya en su perjuicio,
            y caduca en el plazo de DOS AÑOS, a contarse desde la fecha de su otorgamiento.
        </li>
    </ol>
    <footer>
        <table>
            <tr>
                <td style="text-align: center">
                    <p style="margin-bottom: 0">_________________________________</p>
                    <p style="font-weight: bold">Director de Planificación</p>
                </td>
                <td style="text-align: center">
                    <p style="margin-bottom: 0">__________________________________</p>
                    <p style="font-weight: bold">Director de Obras Públicas</p>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight: bold; text-align: center">
                    <span>DIREC:  BOLIVAR Y SUCRE ESQ. SALCEDO / FONOS : 03-2726001- 03-2726143 - 30- 2726255</span>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <span>EMAIL: IMSALCEDO@ANDINANET.NET</span>
                </td>
            </tr>
        </table>
    </footer>
</div>
</body>
</html>
