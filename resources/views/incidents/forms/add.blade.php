@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Agregar formulario</h4>
    </div>

        <div class="card-body">
            <form action="" method="POST">
                @csrf
                <div class="form-group">
                    <label for="form_id">Formulario</label>
                    <div class="row">
                        <div class="col-sm-6">
                            <select name="form_id" class="form-control" required="">
                                <option value="">Seleccionar</option>
                                @foreach ($incidentForms as $incidentForm)
                                    <option value="{{ $incidentForm->form_id }}" {{ old('form_id', $incident->form_id) == $incidentForm->form_id ? 'selected' : '' }}>
                                        {{ $incidentForm->requirement->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Agregar formulario</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <div class="form-group">
                <a href="{{ url('incidencia/'.$incident->id.'/formularios') }}" class="btn btn-light">Volver a la lista de formularios</a>

            </div>
        </div>
</div>
@endsection
@section('scripts')

@endsection
