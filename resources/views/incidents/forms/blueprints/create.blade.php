@extends('layouts.app')

@section('styles')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Registrar formulario</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group row">
                    <label for="code" class="col-sm-2 col-form-label">Documento:</label>
                    <div class="col-sm-4">
                        <input type="text" id="code" class="form-control" value="{{ $code }}" disabled>
                    </div>
                    <label for="date" class="col-sm-2 col-form-label">Fecha:</label>
                    <div class="col-sm-4">
                        <input type="date" id="date" class="form-control" value="{{ date('Y-m-d') }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="client" class="col-sm-2 col-form-label">Cliente:</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="client" value="{{ $incident->client->name }}" disabled>
                    </div>
                    <label for="location" class="col-sm-2 col-form-label">Ubicada en:</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="location" name="location" value="{{ old('location') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="key" class="col-sm-2 col-form-label">Clave catastral</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="key" name="key" value="{{ old('key') }}">
                    </div>
                    <label for="street" class="col-sm-2 col-form-label">Calle (N° - Entre)</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="street" name="street" value="{{ old('street') }}">
                    </div>
                </div>
                <h5>CARACTERÍSTICAS DE LA EDIFICAIÓN</h5>
                <div class="form-group row">
                    <label for="ground_surface" class="col-sm-2 col-form-label">Superficie del terreno (m<sup>2</sup>)</label>
                    <div class="col-sm-4">
                        <input type="number" min="0" step="0.01" class="form-control" id="ground_surface" name="ground_surface" value="{{ old('ground_surface') }}">
                    </div>
                    <label for="ground_floor_surface" class="col-sm-2 col-form-label">Superficie en planta baja (m<sup>2</sup>)</label>
                    <div class="col-sm-4">
                        <input type="number" min="0" step="0.01" class="form-control" id="ground_floor_surface" name="ground_floor_surface" value="{{ old('ground_floor_surface') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="total_construction" class="col-sm-2 col-form-label">Superficie total de la construcción (m<sup>2</sup>)</label>
                    <div class="col-sm-4">
                        <input type="number" min="0" step="0.01" class="form-control" id="total_construction" name="total_construction" value="{{ old('total_construction') }}">
                    </div>
                    <label for="rooms" class="col-sm-2 col-form-label">Número de cuartos</label>
                    <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="rooms" name="rooms" value="{{ old('rooms') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="floors" class="col-sm-2 col-form-label">Número de pisos</label>
                    <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" id="floors" name="floors" value="{{ old('floors') }}">
                    </div>
                    <label for="retreat" class="col-sm-2 col-form-label">RETIROS</label>
                    <div class="col-sm-4">
                        <select name="retreat" id="retreat" class="form-control">
                            <option value="1" {{ old('retreat') == '1' ? 'selected' : '' }}>SÍ</option>
                            <option value="0" {{ old('retreat') == '0' ? 'selected' : '' }}>NO</option>
                        </select>
                    </div>
                </div>

                <h5>MATERIALES DE LA EDIFICACIÓN</h5>
                <div class="form-group row">
                    <label for="foundation" class="col-sm-2 col-form-label">CIMIENTOS</label>
                    <div class="col-sm-4">
                        <select name="foundation" id="foundation" class="form-control">
                            <option value="">Seleccionar</option>
                            <option value="1" {{ old('foundation') == '1' ? 'selected' : '' }}>Piedra</option>
                            <option value="2" {{ old('foundation') == '2' ? 'selected' : '' }}>Hormigón</option>
                            <option value="3" {{ old('foundation') == '3' ? 'selected' : '' }}>Otros</option>
                        </select>
                    </div>
                    <label for="structure" class="col-sm-2 col-form-label">ESTRUCTURA</label>
                    <div class="col-sm-4">
                        <select name="structure" id="structure" class="form-control">
                            <option value="">Seleccionar</option>
                            <option value="1" {{ old('structure') == '1' ? 'selected' : '' }}>Hormigón</option>
                            <option value="2" {{ old('structure') == '2' ? 'selected' : '' }}>Muro Soporte</option>
                            <option value="3" {{ old('structure') == '3' ? 'selected' : '' }}>Madera</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="wall" class="col-sm-2 col-form-label">PAREDES</label>
                    <div class="col-sm-4">
                        <select name="wall" id="wall" class="form-control">
                            <option value="">Seleccionar</option>
                            <option value="1" {{ old('wall') == '1' ? 'selected' : '' }}>Ladrillo o Bloque</option>
                            <option value="2" {{ old('wall') == '2' ? 'selected' : '' }}>Tapial o adobe</option>
                            <option value="3" {{ old('wall') == '3' ? 'selected' : '' }}>Madera o Bahareque</option>
                        </select>
                    </div>
                    <label for="cover" class="col-sm-2 col-form-label">CUBIERTAS</label>
                    <div class="col-sm-4">
                        <select name="cover" id="cover" class="form-control">
                            <option value="">Seleccionar</option>
                            <option value="1" {{ old('cover') == '1' ? 'selected' : '' }}>Hormigón</option>
                            <option value="2" {{ old('cover') == '2' ? 'selected' : '' }}>Teja o asbesto cemento</option>
                            <option value="3" {{ old('cover') == '3' ? 'selected' : '' }}>Zinc o Paja</option>
                        </select>
                    </div>
                </div>
                <h5>COSTO Y FINANCIAMIENTO</h5>
                <div class="form-group row">
                    <label for="unit_cost" class="col-sm-2 col-form-label">Costo unitario de construcción ($)</label>
                    <div class="col-sm-4">
                        <input type="number" min="0" step="0.01" class="form-control" id="unit_cost" name="unit_cost" value="{{ old('unit_cost') }}">
                    </div>
                    <label for="total_cost" class="col-sm-2 col-form-label">Costo total ($)</label>
                    <div class="col-sm-4">
                        <input type="number" min="0" step="0.01" class="form-control" id="total_cost" name="total_cost" value="{{ old('total_cost') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="financing" class="col-sm-2 col-form-label">FINANCIAMIENTO CON:</label>
                    <div class="col-sm-4">
                        <select name="financing" id="financing" class="form-control">
                            <option value="">Seleccionar</option>
                            <option value="1" {{ old('financing') == '1' ? 'selected' : '' }}>Recursos propios</option>
                            <option value="2" {{ old('financing') == '2' ? 'selected' : '' }}>Préstamos</option>
                            <option value="3" {{ old('financing') == '3' ? 'selected' : '' }}>Otros</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ route('form.index', ['incident' => $incident->id]) }}" class="btn btn-light" title="Volver">Volver</a>
                    <button type="submit" class="btn btn-success">Registrar formulario</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
@endsection
