<html>
<head>
    <title>{{ $form->code }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 2.5cm }
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: none;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 60px;
        }
    </style>
</head>
<body>
<div>
    <table>
        <tr style="">
            <td style="width: 53%; margin: 0">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
            <td style="text-align: right">
                <p style="font-weight: bold; margin: 0; font-size: 16px">Documento: {{ $form->code }}</p>
            </td>
        </tr>
    </table>
    <table style="">
        <tr style="">
            <td style=" text-align: center">
                <p style="font-weight: bold; margin-top: 0; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; font-size: 14px">MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p style="font-weight: bold">DIRECCIÓN DE PLANIFICACIÓN</p>
                <p style="background: black; color: white; font-weight: bold; padding: 7px !important; margin: 0 13.5em !important;">SOLICITUD DE APROBACIÓN DE PLANOS</p>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="text-align: right">
                <p style="margin-bottom: 0">{{ $form->dateComplete }}</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="line-height: 1.7em; text-align: justify; margin: 0">
                    <span>Señor Director de Obras Públicas Municipales</span> <br>
                    <span>Presente.</span> <br>
                    <span>Yo, <u style="font-weight: bold">{{ $form->name }}</u></span> <br>
                    Solicito se conceda la aprobación de Planos, de la construcción a edificarse; ubicada en:
                    <u style="font-weight: bold">{{ $form->location }}</u> Clave catastral
                    <u style="font-weight: bold">{{ $form->key }}</u>
                    <u style="font-weight: bold">{{ $form->street }}</u>
                </p>
            </td>
        </tr>
    </table>
    <p style="font-weight: bold">CARACTERÍSTICAS DE LA EDIFICACIÓN</p>
    <table>
        <tr>
            <td style="width: 40%; border-top: 2px solid; border-left: 2px solid; padding: 0.6em 1em">
                Superficie del Terreno:
            </td>
            <td style="width: 20%; text-align: right; border-right: 2px solid; border-top: 2px solid; padding: 0.6em 1em">
                {{ $form->ground_surface }} m<sup>2</sup>
            </td>
            <td style="border-top: 2px solid; padding: 0.6em 1em">
                Número de cuartos:
            </td>
            <td style="border-top: 2px solid; padding: 0.6em 1em; border-right: 2px solid">
                {{ $form->rooms }}
            </td>
        </tr>
        <tr>
            <td style="padding: 0.6em 1em; border-left: 2px solid;">
                Superficie en Planta Baja:
            </td>
            <td style="text-align: right; border-right: 2px solid; padding: 0.6em 1em">
                {{ $form->ground_floor_surface }} m<sup>2</sup>
            </td>
            <td style="padding: 0.6em 1em">
                Número de pisos:
            </td>
            <td style="padding: 0.6em 1em; border-right: 2px solid">
                {{ $form->floors }}
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 2px solid; padding: 0.6em 1em; border-left: 2px solid;">
                Superficie total de la Construcción:
            </td>
            <td style="text-align: right; border-right: 2px solid; border-bottom: 2px solid; padding: 0.6em 1em">
                {{ $form->total_construction }} m<sup>2</sup>
            </td>
            <td style="border-bottom: 2px solid; padding: 0.6em 1em">
                RETIROS:
            </td>
            <td style="border-bottom: 2px solid; padding: 0.6em 1em; border-right: 2px solid">
                 SÍ
                @if($form->retreat)
                (X)
                @else
                (<span style="color: white">X</span>)
                @endif
                NO
                @if($form->retreat)
                (<span style="color: white">X</span>)
                @else
                (X)
                @endif
            </td>
        </tr>
    </table>
    <p style="font-weight: bold">MATERIALES DE LA EDIFICACIÓN </p>
    <table>
        <tr>
            <td colspan="2" style="text-align: center; font-weight: bold; border-top: 2px solid; border-left: 2px solid; border-bottom: 1px solid">
                CIMIENTOS
            </td>
            <td colspan="2" style="text-align: center; font-weight: bold; border-left: 2px solid; border-right: 2px solid; border-top: 2px solid; border-bottom: 1px solid">
                ESTRUCTURA
            </td>
            <td colspan="2" style="text-align: center; font-weight: bold; border-top: 2px solid; border-bottom: 1px solid">
                PAREDES
            </td>
            <td colspan="2" style="text-align: center; font-weight: bold; border-left: 2px solid; border-right: 2px solid; border-top: 2px solid; border-bottom: 1px solid">
                CUBIERTAS
            </td>
        </tr>
        <tr>
            <td style="width: 11%; border-left: 2px solid;">
                Piedra
            </td>
            <td style="text-align: center; width: 4.5%; border-right: 2px solid;">
                @if($form->foundation == 1)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
            <td style="width: 17%;">
                Hormigón
            </td>
            <td style="text-align: center; border-right: 2px solid; width: 5% !important;">
                @if($form->structure == 1)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
            <td style="width: 22%">
                Ladrillo o Bloque
            </td>
            <td style="text-align: center; border-right: 2px solid; width: 5% !important;">
                @if($form->wall == 1)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
            <td style="">
                Hormigón
            </td>
            <td style="text-align: center; border-right: 2px solid; width: 5% !important;">
                @if($form->cover == 1)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
        </tr>
        <tr>
            <td style="border-left: 2px solid;">
                Hormigón
            </td>
            <td style="text-align: center; border-right: 2px solid;">
                @if($form->foundation == 2)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
            <td style="">
                Muro Soporte
            </td>
            <td style="text-align: center; border-right: 2px solid;">
                @if($form->structure == 2)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
            <td style="">
                Tapial o adobe
            </td>
            <td style="text-align: center; border-right: 2px solid;">
                @if($form->wall == 2)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
            <td style="">
                Teja o asbesto cemento
            </td>
            <td style="text-align: center; border-right: 2px solid;">
                @if($form->cover == 2)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
        </tr>
        <tr>
            <td style="border-left: 2px solid; border-bottom: 2px solid">
                Otros
            </td>
            <td style="text-align: center; border-right: 2px solid; border-bottom: 2px solid;">
                @if($form->foundation == 3)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
            <td style="border-bottom: 2px solid">
                Madera
            </td>
            <td style="text-align: center; border-right: 2px solid; border-bottom: 2px solid;">
                @if($form->structure == 3)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
            <td style="border-bottom: 2px solid">
                Madera o Bahareque
            </td>
            <td style="text-align: center; border-right: 2px solid; border-bottom: 2px solid;">
                @if($form->wall == 3)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
            <td style="border-bottom: 2px solid">
                Zinc o Paja
            </td>
            <td style="text-align: center; border-right: 2px solid; border-bottom: 2px solid;">
                @if($form->cover == 3)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
        </tr>
    </table>
    <p style="font-weight: bold">COSTO Y FINANCIAMIENTO</p>
    <table>
        <tr>
            <td style="width: 35%; border-top: 2px solid; border-left: 2px solid; padding: 0.6em 1em">
                Costo unitario de construcción:
            </td>
            <td style="width: 17%; text-align: right; border-right: 2px solid; border-top: 2px solid; padding: 0.6em 1em">
                $ {{ $form->unit_cost }}
            </td>
            <td colspan="4" style="border-top: 2px solid; padding: 0.6em 1em; border-right: 2px solid">
                FINANCIAMIENTO CON:
            </td>
        </tr>
        <tr>
            <td style="padding: 0.6em 1em; border-left: 2px solid;">
                Costo total:
            </td>
            <td style="text-align: right; border-right: 2px solid; padding: 0.6em 1em">
                $ {{ $form->total_cost }}
            </td>
            <td style="padding: 0.6em 1em">
                Recursos Propios
            </td>
            <td style="width: 5%">
                @if($form->financing == 1)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
            <td style="padding: 0.6em 1em">
                Préstamos
            </td>
            <td style="border-right: 2px solid">
                @if($form->financing == 2)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 2px solid; padding: 0.6em 1em; border-left: 2px solid;">
            </td>
            <td style="text-align: right; border-bottom: 2px solid; padding: 0.6em 1em">
            </td>
            <td style="text-align: right; border-bottom: 2px solid; padding: 0.6em 1em; border-left: 2px solid;">
            </td>
            <td style="border-bottom: 2px solid; padding: 0.6em 1em">
            </td>
            <td style="border-bottom: 2px solid; padding: 0.6em 1em; width: 15%">
                Otros
            </td>
            <td style="border-bottom: 2px solid; border-right: 2px solid; width: 5%">
                @if($form->financing == 3)
                    (x)
                @else
                    (<span style="color: white">x</span>)
                @endif
            </td>
        </tr>
    </table>
    <footer>
        <table style="width: 100%">
            <tr>
                <td style="text-align: center">
                    <p style="margin-bottom: 0">________________________________________</p>
                    <p style="font-weight: bold">Firma Propietario o Solicitante</p>
                </td>
            </tr>
        </table>
    </footer>
</div>
</body>
</html>
