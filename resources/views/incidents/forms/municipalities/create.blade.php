@extends('layouts.app')

@section('styles')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Registrar formulario</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group row">
                    <label id="$code" class="col-sm-3 col-form-label">Documento:</label>
                    <div class="col-sm-3">
                        <input type="text" id="$code" class="form-control" value="{{ $code }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="client" class="col-sm-3 col-form-label">Nombres:</label>
                    <div class="col-sm-3">
                        <input type="text" name="client_name" id="client_name" class="form-control" value="{{ old('client_name', $incident->client->name) }}" readonly>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#modalSelectClient">
                                Seleccionar cliente
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="client_document" class="col-sm-3 col-form-label">Céd. Identidad:</label>
                    <div class="col-sm-3">
                        <input type="text" id="client_document" name="client_document" class="form-control" value="{{ old('client_document', $incident->client->document) }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-3 col-form-label">Dirección:</label>
                    <div class="col-sm-3">
                        <input type="text" id="address" name="address" class="form-control" value="{{ old('address', $incident->client->address) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-3 col-form-label">Correo:</label>
                    <div class="col-sm-3">
                        <input type="text" id="email" name="email" class="form-control" value="{{ old('email', $incident->client->email) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone" class="col-sm-3 col-form-label">Teléfono:</label>
                    <div class="col-sm-3">
                        <input type="text" id="phone" name="phone" class="form-control" value="{{ old('phone', $incident->client->cellphone) }}">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ route('form.index', ['incident' => $incident->id]) }}" class="btn btn-light" title="Volver">Volver</a>
                    <button type="submit" class="btn btn-success">Registrar formulario</button>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modalSelectClient">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Seleccionar cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible alert-alt fade show" id="alert" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Es necesario completar los campos y seleccionar un proceso para buscar
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <select name="" id="type" class="form-control">
                                    <option value="">Seleccionar</option>
                                    <option value="document">Buscar por cédula</option>
                                    <option value="name">Buscar por nombre</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="inputSearch">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <button class="btn btn-light" id="btnSearch">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Cédula</th>
                            <th>Nombre</th>
                            <th>Código</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="content-search">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        const $btnSearch = $('#btnSearch');
        const $option = $('#type');
        const $input = $('#inputSearch');
        const $alert = $('#alert');
        const $content = $('#content-search');
        const $processId = $('#process_id');

        $btnSearch.click(function () {
            $(this).attr('disabled', true);
            $alert.hide();

            if ($option.val()) {
                // AJAX
                $.get('/api/clients?type='+$option.val()+'&input_search='+$input.val()+'&process_id='+$processId.val(), function (clients) {
                    let html = '';
                    Array.prototype.forEach.call(clients, function(client) {
                        html += '<tr>' +
                            '<td>'+client.document+'</td>' +
                            '<td>'+client.name+'</td>' +
                            '<td>'+client.id+'</td>' +
                            '<td class="text-center"><button class="btn btn-light" type="button" ' +
                            'data-select="'+client.id+'" data-name="'+client.name+'" ' +
                            'data-document="'+client.document+'" data-address="'+client.address+'" data-phone="'+client.cellphone+'" data-email="'+client.email+'">Seleccionar</button></td>' +
                            '</tr>';
                    });
                    $content.html(html);
                });
                $(this).attr('disabled', false);
            } else {
                $(this).attr('disabled', false);
                $alert.show();
            }
        });

        $(document).on('click', '[data-select]', function () {
            const id = $(this).data('select');
            const name = $(this).data('name');
            const document = $(this).data('document');
            const phone = $(this).data('phone');
            const address = $(this).data('address');
            const email = $(this).data('email');

            $('#client_id').val(id);
            $('#client_name').val(name);
            $('#client_document').val(document);
            $('#phone').val(phone);
            $('#address').val(address);
            $('#email').val(email);
            $content.html('');
            $input.val('');
            $option.val('');

            $('#modalSelectClient').modal('hide');
        });

    </script>
@endsection
