<html>
<head>
    <title>{{ $form->code }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 2.5cm; }
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: none;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 32em;
        }
    </style>
</head>
<body>
<div style="margin: 0 auto;">
    <table style="margin: 0 auto;">
        <tr style="">
            <td style="width: 50%">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
            <td style="text-align: right; position: relative">
                <div style="position: absolute">
                    <p style="margin: 0 !important; font-weight: bold; font-size: 16px">Documento: {{ $form->code }}</p>
                    <div style="height: 7em; width: 7em;"></div>
                    <p style="margin: 0 !important; font-weight: bold; padding: 7px 0 !important;">Código: {{ $form->code_qr }}</p>
                </div>
            </td>
        </tr>
    </table>
    <table style="margin-right: 7em">
        <tr style="">
            <td style=" text-align: center">
                <p style="font-weight: bold; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; font-size: 14px">MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p style="font-weight: bold">TESORERÍA MUNICIPAL</p>
                <p style="background: black; color: white; font-weight: bold; padding: 7px !important; margin: 0 9em !important;">CERTIFICADO DE NO ADEUDAR AL MUNICIPIO</p>
            </td>
        </tr>
    </table>
    <p style="text-align: center !important; margin-bottom: 2px; margin-right: 7em; font-weight: bold">DATOS DEL PETICIONARIO</p>
    <table style="margin-right: 7em">
        <tr>
            <td style=" font-weight: bold">Nombres</td>
            <td style="width: 75%">{{ $form->client_name }}</td>
        </tr>
        <tr>
            <td style=" font-weight: bold">Céd. Identidad:</td>
            <td>{{ $form->client_document }}</td>
        </tr>
        <tr>
            <td style=" font-weight: bold">Dirección:</td>
            <td>{{ $form->address }}</td>
        </tr>
        <tr>
            <td style=" font-weight: bold">Correo</td>
            <td>{{ $form->email }}</td>
        </tr>
        <tr>
            <td style=" font-weight: bold">Teléfono</td>
            <td>{{ $form->phone }}</td>
        </tr>
    </table>
    <table style="margin-right: 7em">
        <tr>
            <td style=" font-weight: bold;">MOTIVO DE LA SOLICITUD:</td>
            <td style="width: 65%">
                {{ $form->process_name }}
            </td>
        </tr>
    </table>
    <footer>
        <table>
            <tr>
                <td style="width: 80%"></td>
                <td style="text-align: center">
                    <p>_____________________________________</p>
                    <p>Firma del Peticionario</p>
                </td>
            </tr>
        </table>
        <p style="text-align: justify">
            VISTA DE LA PETICIÓN DEL INTERESADO, EL TESORERO MUNICIPAL, CERTIFICA: Que revisado los Archivos
            de esta Dependencia NO ADEUDA a la presente fecha, por ningún concepto, al municipio de Salcedo.
        </p>
        <table style="width: 100%; margin-top: 8em">
            <tr>
                <td style="width: 80%; padding-top: 2em">
                    {{ $form->dateComplete }}
                </td>
                <td style="text-align: center">
                    <p>_____________________________________</p>
                    <p>Tesorero Municipal</p>
                </td>
            </tr>
        </table>
        <p style="text-align: justify">
            EL PRESENTE DOCUMENTO FUE GENERADO CON EL PORTAL CIUDADANO DEL GAD MUNICIPAL DE SALCEDO EL {{ $form->created_at->format('d-m-Y') }}
        </p>
        <p style="text-align: justify">
            EL PRESENTE CERTIFICADO ES VALIDO POR QUINCE DIAS, UD PUEDE VERIFICAR LA VALIDEZ DE ESTE DOCUMENTO EN:
            tramites.salcedo.gob.ec/salcedoenlinea "VALIDAR CERTIFICADO" CON EL CODIGO {{ $form->code_qr }}
        </p>
    </footer>
</div>
</body>
</html>
