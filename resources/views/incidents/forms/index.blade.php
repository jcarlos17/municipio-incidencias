@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <div class="col-sm-6">
            <h4 class="card-title">Lista de formularios</h4>
        </div>
        @if($existsForms)
            <div class="col-sm-6 text-right">
                <a href="{{ url('incidencia/'.$incident->id.'/formularios/agregar') }}" class="btn btn-primary">Nuevo formulario</a>
            </div>
        @endif
    </div>

    <div class="card-body">
        @if (session('notification'))
            <div class="alert alert-success alert-dismissible alert-alt fade show">
                {{ session('notification') }}
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissible alert-alt fade show">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <div class="table-responsive mt-4">
                <table class="table table-striped">
                    <thead>
                    <tr class="active">
                        <th>Documento</th>
                        <th>Cliente</th>
                        <th>Formulario</th>
                        <th>Usuario</th>
                        <th class="text-center">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($incidentForms as $incidentForm)
                        <tr>
                            <td>{{ $incidentForm->form ? $incidentForm->form->code : '' }}</td>
                            <td>{{ $incidentForm->form ? $incidentForm->form->client_name : '' }}</td>
                            <td>{{ $incidentForm->requirement->name }}</td>
                            <td>{{ $incidentForm->form ? $incidentForm->form->support->name : '' }}</td>
                            <td class="text-center">
                                @if(auth()->user()->is(\App\Models\User::SUPPORT))
                                    @if($incidentForm->form)
                                        <a href="{{ route($incidentForm->requirement->form_type.'.edit', ['form' => $incidentForm->form->id]) }}" class="btn btn-xs btn-success mb-1" title="Editar formulario">
                                            Editar
                                        </a>
                                        <a href="{{ route($incidentForm->requirement->form_type.'.download', ['form' => $incidentForm->form->id]) }}" class="btn btn-xs btn-danger mb-1" target="_blank" title="Descargar PDF">
                                            PDF
                                        </a>
                                        <form method="POST" action="{{ route($incidentForm->requirement->form_type.'.upload', ['form' => $incidentForm->form->id]) }}" enctype="multipart/form-data" style="display: inline">
                                            @csrf
                                            <input type="file" name="document" accept="application/pdf" style="display: none">
                                            <button type="submit" style="display: none"></button>
                                            <button type="button" data-upload="{{ $incidentForm->form->document }}" class="btn btn-xs btn-primary mb-1" title="Subir PDF legalizado">
                                                Subir
                                            </button>
                                        </form>
                                        @if($incidentForm->form->document)
                                            <a href="{{ route($incidentForm->requirement->form_type.'.final_download', ['form' => $incidentForm->form->id]) }}" class="btn btn-xs btn-dark mb-1"
                                               title="Descargar documento final">
                                                Descargar
                                            </a>
                                        @else
                                            <button type="button" class="btn btn-xs btn-dark mb-1" title="Descargar documento final" disabled>Descargar</button>
                                        @endif
                                    @elseif($incidentForm->requirement->form_type == 'solicitud-todo-tramite' || $incidentForm->requirement->form_type == 'informe-certificado')
                                        <a href="{{ route('incident_forms.create', ['incidentForm' => $incidentForm->id, 'type' => $incidentForm->requirement->form_type]) }}" class="btn btn-xs btn-success mb-1" title="Completar formulario">
                                            Completar formulario
                                        </a>
                                    @else
                                        <a href="{{ route($incidentForm->requirement->form_type.'.create', ['incidentForm' => $incidentForm->id]) }}" class="btn btn-xs btn-success mb-1" title="Completar formulario">
                                            Completar formulario
                                        </a>
                                    @endif
                                @elseif(auth()->user()->is(\App\Models\User::CLIENT))
                                    @if($incidentForm->form && $incidentForm->form->document)
                                        <a href="{{ route($incidentForm->requirement->form_type.'.final_download', ['form' => $incidentForm->form->id]) }}" class="btn btn-xs btn-dark mb-1"
                                           title="Descargar documento final">
                                            Descargar
                                        </a>
                                    @else
                                        <button type="button" class="btn btn-xs btn-dark mb-1" title="Descargar documento final" disabled>Descargar</button>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
    </div>
    <div class="card-footer">
        <div class="form-group">
            <a href="{{ url('ver/'.$incident->id) }}" class="btn btn-light">Volver a la incidencia</a>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $('[data-upload]').on('click', function () {
            const exists = $(this).data('upload');
            const form = $(this).closest('form');
            const document = form.find('[name="document"]');
            const submit = form.find('[type="submit"]');

            if (exists) {
                let r = confirm("¿Está seguro que desea reemplazar el documento legalizado que ya ha subido anteriormente?");
                if (r === true) {
                    document.click();

                    document.on('change', function () {
                        submit.click();
                        submit.attr('disabled', true)
                    });
                }
            } else {
                document.click();

                document.on('change', function () {
                    submit.click();
                    submit.attr('disabled', true)
                });
            }
        });
    </script>
@endsection
