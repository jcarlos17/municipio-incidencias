<html>
<head>
    <title>{{ $form->code }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 2.5cm; }
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: none;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 24em;
        }
    </style>
</head>
<body>
<div style="margin: 0 auto;">
    <table style="margin: 0;">
        <tr style="">
            <td style="width: 53%">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
            <td style="text-align: right">
                <p style="font-weight: bold; margin: 0; font-size: 16px">Documento: {{ $form->code }}</p>
            </td>
        </tr>
    </table>
    <table style="margin: 0 auto;">
        <tr style="">
            <td style=" text-align: center; position: relative">
                <p style="font-weight: bold; margin-top: 0; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; font-size: 14px">MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p style="background: black; color: white; font-weight: bold; padding: 7px !important; margin: 0 13em 2em 13em!important;">
                    FORMULARIO DE NORMAS PARTICULARES
                </p>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="font-weight: bold">
                Para:
            </td>
            <td style="width: 90%">
                <span>{{ $form->official_name }}</span><br>
                <span>{{ $form->official_position }}</span>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">
                De:
            </td>
            <td style="width: 90%">
                <span>{{ $form->client_name }}</span>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="line-height: 1.5em">
                Solicito se confiiera el certificado de LINEA DE FABRICA, para: <u style="font-weight: bold">{{ $form->reason }}</u>
                a mi propiedad ubicada en: <u style="font-weight: bold">{{ $form->address_location }}</u>
                entre <u style="font-weight: bold">{{ $form->address_between }}</u>
                Intersección: <u style="font-weight: bold">{{ $form->intersection }}</u>
                Clave catastral: <u style="font-weight: bold">{{ $form->key }}</u>
                Sector: <u style="font-weight: bold">{{ $form->sector }}</u>
                Barrio o parcelación: <u style="font-weight: bold">{{ $form->district }}</u>
                Lote N°: <u style="font-weight: bold">{{ $form->lot }}</u>
                Manzana N°: <u style="font-weight: bold">{{ $form->square }}</u> <br>
                Solicitante: <u style="font-weight: bold">{{ $form->applicant }}</u>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="font-weight: bold; width: 50%; text-align: center">
                <p>CROQUIS DE UBICACIÓN</p>
                <div style="border: 2px solid; padding: 1em">
                    <img src="{{ $form->sketchUrl }}" alt="Logo" width="300px">
                </div>
            </td>
            <td style="vertical-align: middle; text-align: justify; padding: 1em">
                NOTA: Para el croquis debe hacerse constar las manzanas ubicando calles, parques o edificaciones
                importantes que sirvan de  referencia para su rápida localización. Puede hacerse a mano alzada, no
                es necesario usar escala.
            </td>

        </tr>
    </table>
    <footer>
        <table>
            <tr>
                <td style="text-align: center">
                    <p style="margin-bottom: 0; margin-top: 4em">__________________________________</p>
                    <p style="font-weight: bold">
                        Propietario o Solicitante
                    </p>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="text-align: center">
                    <p style="border: 2px solid; padding: 1em">
                        <b>ESPACIO PARA SELLOS DE</b>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <span style="float: left; text-align: left">
                            <b>AGUA POTABLE</b> <br>
                            Fecha de recepción: {{ $form->created_at->format('d/m/Y') }}
                        </span>
                        <span style="float: right">
                            <b>ALCANTARILLADO</b>
                        </span>
                        <br>
                        <br>
                    </p>
                </td>
            </tr>
        </table>
    </footer>
</div>
<div style="margin: 0 auto; page-break-before: always">
    <table style="margin: 0;">
        <tr style="">
            <td style="width: 53%">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
        </tr>
    </table>
    <table style="margin: 0 auto;">
        <tr style="">
            <td style=" text-align: center; position: relative">
                <p style="font-weight: bold; margin-top: 0; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; font-size: 14px">MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p style="background: black; color: white; font-weight: bold; padding: 7px !important; margin: 0 13em 2em 13em!important;">
                    FORMULARIO DE NORMAS PARTICULARES
                </p>
            </td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.5em 0">DATOS DE LAS VIAS</p>
    <table style="margin-left: 1em">
        <tr>
            <td style="font-weight: bold; width: 60%">
                Nombre de la calle o Avda.
            </td>
            <td style="font-weight: bold">
                Ancho(Mt)
            </td>
        </tr>
        <tr>
            <td>
                {{ $form->via_address }}
            </td>
            <td>
                {{ $form->via_width }}
            </td>
        </tr>
        <tr>
            <td>
                {{ $form->via_address2 }}
            </td>
            <td>
                {{ $form->via_width2 }}
            </td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.5em 0">RETIROS</p>
    <table style="margin-left: 1em">
        <tr>
            <td>
                <b>Frontal:</b> {{ $form->frontal }}
            </td>
            <td>
                <b>Lateral:</b> {{ $form->side }}
            </td>
            <td>
                <b>Fondo:</b> {{ $form->bottom }}
            </td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.5em 0">ADOSAMIENTO:</p>
    <table style="margin-left: 1em">
        <tr>
            <td>
                A las medianeras laterales: {{ $form->wall_side }}
            </td>
            <td>
                A la medianera posterior: {{ $form->wall_later }}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                A la medianera: {{ $form->wall }}
            </td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.5em 0">ZONIFICACIÓN:</p>
    <table style="margin-left: 1em">
        <tr>
            <td rowspan="2" style="vertical-align: middle">
                TIPO: {{ $form->type }}
            </td>
            <td>
                C.O.S. {{ $form->cos }}
            </td>
            <td>
                Otros pisos: {{ $form->other_floor }}
            </td>
        </tr>
        <tr>
            <td>
                C.U.S. {{ $form->cus }}
            </td>
            <td></td>
        </tr>
    </table>
    <p style="margin: 0.5em 0"><b>USO DE SUELO:</b> {{ $form->land_use }}</p>
    <table style="margin-left: 1em">
        <tr>
            <td>
                N° de pisos: {{ $form->number_floor }}
            </td>
            <td>
                Altura máxima metros: {{ $form->max_height }}
            </td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.5em 0">DISPONIBILIDAD DE SERVICIOS</p>
    <table style="margin-left: 1em">
        <tr>
            <td>
                Agua Potable: {{ $form->water }}
            </td>
            <td>
                Calzada: {{ $form->road }}
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                Alcantarillado: {{ $form->sewerage }}
            </td>
            <td>
                Bordillos: {{ $form->curb }}
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                Luz Eléctrica: {{ $form->electric }}
            </td>
            <td>
                Aceras: {{ $form->pavement }}
            </td>
            <td>
                Telefono: {{ $form->phone }}
            </td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.5em 0">SITUACIÓN DE LA PROPIEDAD:</p>
    <table style="margin-left: 1em">
        <tr>
            <td>
                A) Afectada totalmente: {{ $form->situation_total }}
            </td>
            <td>
                Parcialmente: {{ $form->situation_partial }}
            </td>
        </tr>
        <tr>
            <td>
                B) Tiene cerramiento: {{ $form->enclosure }}
            </td>
            <td>
                En Línea de Fábrica: {{ $form->factory_line }}
            </td>
            <td></td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.5em 0">INFORMES ADICIONALES:</p>
    <table style="margin-left: 1em">
        <tr>
            <td>
                {!! $form->add_report !!}
            </td>
        </tr>
    </table>
    <footer style="height: 130px !important;">
        <table>
            <tr>
                <td style="text-align: center">
                    <p style="margin-bottom: 0; margin-top: 4em">__________________________________</p>
                    <p style="font-weight: bold">
                        Director de Planificación
                    </p>
                </td>
                <td style="text-align: center">
                    <p style="margin-bottom: 0; margin-top: 4em">__________________________________</p>
                    <p style="font-weight: bold">
                        Responsable Técnico
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    {{ $form->dateComplete }}
                </td>
            </tr>
        </table>
    </footer>
</div>
</body>
</html>
