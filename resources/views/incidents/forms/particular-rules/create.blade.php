@extends('layouts.app')

@section('styles')
    <!-- Summernote -->
    <link href="{{ asset('vendor/summernote/summernote.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Registrar formulario</h4>
        </div>
        <form action="" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <div class="form-group row">
                        <label for="code" class="col-sm-3 col-form-label">Documento:</label>
                        <div class="col-sm-3">
                            <input type="text" id="code" class="form-control" value="{{ $code }}" disabled>
                        </div>
                        <label for="date" class="col-sm-3 col-form-label">Fecha:</label>
                        <div class="col-sm-3">
                            <input type="date" id="date" class="form-control" value="{{ date('Y-m-d') }}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="official_name" class="col-sm-3 col-form-label">Para:</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="official_name" id="official_name">
                                <option value="">Elegir</option>
                                @foreach($officials as $official)
                                    <option value="{{ $official->name }}" {{ old('official_name') == $official->name ? 'selected' : '' }}>
                                        {{ $official->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="client" class="col-sm-3 col-form-label">De:</label>
                        <div class="col-sm-6">
                            <input type="text" id="client" class="form-control" value="{{ $incident->client->name }}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="reason" class="col-sm-3 col-form-label">Motivo:</label>
                        <div class="col-sm-6">
                            <select name="reason" id="wall_side" class="form-control">
                                <option value="">Seleccionar</option>
                                <option value="Vender" {{ old('reason') == 'Vender' ? 'selected' : '' }}>Vender</option>
                                <option value="Edificar" {{ old('reason') == 'Edificar' ? 'selected' : '' }}>Edificar</option>
                                <option value="Cerramiento" {{ old('reason') == 'Cerramiento' ? 'selected' : '' }}>Cerramiento</option>
                                <option value="Otros fines" {{ old('reason') == 'Otros fines' ? 'selected' : '' }}>Otros fines</option>
                                <option value="Revisión de planes" {{ old('reason') == 'Revisión de planes' ? 'selected' : '' }}>Revisión de planes</option>
                                <option value="Urbanizar" {{ old('reason') == 'Urbanizar' ? 'selected' : '' }}>Urbanizar</option>
                                <option value="Lotizar" {{ old('reason') == 'Lotizar' ? 'selected' : '' }}>Lotizar</option>
                                <option value="Propiedad horizontal" {{ old('reason') == 'Propiedad horizontal' ? 'selected' : '' }}>Propiedad horizontal</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address_location" class="col-sm-3 col-form-label">Propiedad ubicada en:</label>
                        <div class="col-sm-6">
                            <input type="text" id="address_location" class="form-control" name="address_location" value="{{ old('address_location') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address_between" class="col-sm-3 col-form-label">Entre:</label>
                        <div class="col-sm-6">
                            <input type="text" id="address_between" class="form-control" name="address_between" value="{{ old('address_between') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="intersection" class="col-sm-3 col-form-label">Intersección:</label>
                        <div class="col-sm-6">
                            <input type="text" id="intersection" class="form-control" name="intersection" value="{{ old('intersection') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="key" class="col-sm-3 col-form-label">Clave catastral:</label>
                        <div class="col-sm-3">
                            <input type="text" id="key" class="form-control" name="key" value="{{ old('key') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="sector" class="col-sm-3 col-form-label">Sector:</label>
                        <div class="col-sm-6">
                            <input type="text" id="sector" class="form-control" name="sector" value="{{ old('sector') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="district" class="col-sm-3 col-form-label">Barrio o parcelación:</label>
                        <div class="col-sm-6">
                            <input type="text" id="district" class="form-control" name="district" value="{{ old('district') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lot" class="col-sm-3 col-form-label">Lote N°:</label>
                        <div class="col-sm-3">
                            <input type="text" id="lot" class="form-control" name="lot" value="{{ old('lot') }}">
                        </div>
                        <label for="square" class="col-sm-3 col-form-label">Manzana N°:</label>
                        <div class="col-sm-3">
                            <input type="text" id="square" class="form-control" name="square" value="{{ old('square') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="applicant" class="col-sm-3 col-form-label">Solicitante:</label>
                        <div class="col-sm-6">
                            <input type="text" id="applicant" class="form-control" name="applicant" value="{{ old('applicant') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="sketch" class="col-sm-3 col-form-label">Croquis de la ubicación:</label>
                        <div class="col-sm-4">
                            <div class="custom-file">
                                <input type="file" id="sketch" class="custom-file-input" name="sketch" accept="image/*" required>
                                <label class="custom-file-label">Seleccionar archivo</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h5 class="">Datos de las vias</h5>
                    <div class="form-group row">
                        <label for="via_address" class="col-sm-3 col-form-label">Nombre de la calle o avenida (1):</label>
                        <div class="col-sm-6">
                            <input type="text" id="via_address" class="form-control" name="via_address" value="{{ old('via_address') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="via_width" class="col-sm-3 col-form-label">Ancho Mt. (1):</label>
                        <div class="col-sm-3">
                            <input type="text" id="via_width" class="form-control" name="via_width" value="{{ old('via_width') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="via_address2" class="col-sm-3 col-form-label">Nombre de la calle o avenida (2):</label>
                        <div class="col-sm-6">
                            <input type="text" id="via_address2" class="form-control" name="via_address2" value="{{ old('via_address2') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="via_width2" class="col-sm-3 col-form-label">Ancho Mt. (2):</label>
                        <div class="col-sm-3">
                            <input type="text" id="via_width2" class="form-control" name="via_width2" value="{{ old('via_width2') }}">
                        </div>
                    </div>
                    <h5 class="">Retiros</h5>
                    <div class="form-group row">
                        <label for="frontal" class="col-sm-3 col-form-label">Frontal:</label>
                        <div class="col-sm-3">
                            <input type="text" id="frontal" class="form-control" name="frontal" value="{{ old('frontal') }}">
                        </div>
                        <label for="side" class="col-sm-3 col-form-label">Lateral:</label>
                        <div class="col-sm-3">
                            <input type="text" id="side" class="form-control" name="side" value="{{ old('side') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bottom" class="col-sm-3 col-form-label">Fondo:</label>
                        <div class="col-sm-3">
                            <input type="text" id="bottom" class="form-control" name="bottom" value="{{ old('bottom') }}">
                        </div>
                    </div>
                    <h5 class="">Adosamiento</h5>
                    <div class="form-group row">
                        <label for="wall_side" class="col-sm-3 col-form-label">A las medianeras laterales:</label>
                        <div class="col-sm-2">
                            <select name="wall_side" id="wall_side" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('wall_side') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('wall_side') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                        <label for="wall_later" class="col-sm-3 col-form-label">A las medianera posterior:</label>
                        <div class="col-sm-2">
                            <select name="wall_later" id="wall_later" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('wall_later') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('wall_side') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="wall" class="col-sm-3 col-form-label">A la medianera:</label>
                        <div class="col-sm-6">
                            <input type="text" id="wall" class="form-control" name="wall" value="{{ old('wall') }}">
                        </div>
                    </div>
                    <h5 class="">Zonificación</h5>
                    <div class="form-group row">
                        <label for="type" class="col-sm-3 col-form-label">Tipo:</label>
                        <div class="col-sm-3">
                            <input type="text" id="type" class="form-control" name="type" value="{{ old('type') }}">
                        </div>
                        <label for="cos" class="col-sm-3 col-form-label">C.O.S.:</label>
                        <div class="col-sm-3">
                            <input type="text" id="cos" class="form-control" name="cos" value="{{ old('cos') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cus" class="col-sm-3 col-form-label">C.U.S.:</label>
                        <div class="col-sm-3">
                            <input type="text" id="cus" class="form-control" name="cus" value="{{ old('cus') }}">
                        </div>
                        <label for="other_floor" class="col-sm-3 col-form-label">Otros pisos:</label>
                        <div class="col-sm-3">
                            <input type="text" id="other_floor" class="form-control" name="other_floor" value="{{ old('other_floor') }}">
                        </div>
                    </div>
                    <h5 class="">Uso de suelo</h5>
                    <div class="form-group row">
                        <label for="land_use" class="col-sm-3 col-form-label">Uso de suelo:</label>
                        <div class="col-sm-3">
                            <input type="text" id="land_use" class="form-control" name="land_use" value="{{ old('land_use') }}">
                        </div>
                        <label for="number_floor" class="col-sm-3 col-form-label">N° de pisos:</label>
                        <div class="col-sm-3">
                            <input type="text" id="number_floor" class="form-control" name="number_floor" value="{{ old('number_floor') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="max_height" class="col-sm-3 col-form-label">Altura máxima mt:</label>
                        <div class="col-sm-3">
                            <input type="text" id="max_height" class="form-control" name="max_height" value="{{ old('max_height') }}">
                        </div>
                    </div>
                    <h5 class="">Disponibilidad de servicios</h5>
                    <div class="form-group row">
                        <label for="water" class="col-sm-2 col-form-label">Agua potable:</label>
                        <div class="col-sm-2">
                            <select name="water" id="water" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('water') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('water') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                        <label for="road" class="col-sm-2 col-form-label">Calzada:</label>
                        <div class="col-sm-2">
                            <select name="road" id="road" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('road') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('road') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                        <label for="sewerage" class="col-sm-2 col-form-label">Alcantarillado:</label>
                        <div class="col-sm-2">
                            <select name="sewerage" id="sewerage" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('sewerage') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('sewerage') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="curb" class="col-sm-2 col-form-label">Bordillos:</label>
                        <div class="col-sm-2">
                            <select name="curb" id="curb" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('curb') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('curb') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                        <label for="electric" class="col-sm-2 col-form-label">Luz eléctrica:</label>
                        <div class="col-sm-2">
                            <select name="electric" id="electric" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('electric') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('electric') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                        <label for="pavement" class="col-sm-2 col-form-label">Aceras:</label>
                        <div class="col-sm-2">
                            <select name="pavement" id="pavement" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('pavement') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('pavement') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class="col-sm-2 col-form-label">Teléfono:</label>
                        <div class="col-sm-2">
                            <select name="phone" id="phone" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('phone') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('phone') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                    </div>
                    <h5 class="">Situación de la propiedad</h5>
                    <div class="form-group row">
                        <label for="situation_total" class="col-sm-2 col-form-label">Afectada totalmente:</label>
                        <div class="col-sm-2">
                            <select name="situation_total" id="situation_total" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('situation_total') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('situation_total') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                        <label for="situation_partial" class="col-sm-2 col-form-label">Parcialmente:</label>
                        <div class="col-sm-2">
                            <select name="situation_partial" id="situation_partial" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('situation_partial') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('situation_partial') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                        <label for="enclosure" class="col-sm-2 col-form-label">Tiene cerramiento:</label>
                        <div class="col-sm-2">
                            <select name="enclosure" id="enclosure" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('enclosure') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('enclosure') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="factory_line" class="col-sm-2 col-form-label">En línea de fábrica:</label>
                        <div class="col-sm-2">
                            <select name="factory_line" id="factory_line" class="form-control">
                                <option value=""></option>
                                <option value="SI" {{ old('factory_line') == 'SI' ? 'selected' : '' }}>SI</option>
                                <option value="NO" {{ old('factory_line') == 'NO' ? 'selected' : '' }}>NO</option>
                            </select>
                        </div>
                    </div>
                    <h5>Informes adicionales</h5>
                    <div class="form-group row">
                        <label for="add_report" class="col-sm-3 col-form-label">Informes adicionales</label>
                        <div class="col-sm-9">
                            <textarea class="summernote" id="add_report" name="add_report">{{ old('add_report') }}</textarea>
                        </div>
                    </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ route('form.index', ['incident' => $incident->id]) }}" class="btn btn-light" title="Volver">Volver</a>
                    <button type="submit" class="btn btn-success">Registrar formulario</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <!-- Summernote -->
    <script src="{{ asset('vendor/summernote/js/summernote.min.js') }}"></script>
    <!-- Summernote init -->
    <script src="{{ asset('js/plugins-init/summernote-init.js') }}"></script>
@endsection
