<html>
<head>
    <title>{{ $form->code }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 2.5cm; }
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: none;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 95px;
        }
    </style>
</head>
<body>
<div style="margin: 0 auto;">
    <table style="margin: 0;">
        <tr style="">
            <td style="width: 53%">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
            <td style="text-align: right">
                <p style="font-weight: bold; margin: 0; font-size: 16px">Documento: {{ $form->code }}</p>
            </td>
        </tr>
    </table>
    <table style="margin: 0 auto;">
        <tr style="">
            <td style=" text-align: center; position: relative">
                <p style="font-weight: bold; margin: 5px; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; font-size: 14px; margin: 5px">MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p style="background: black; color: white; font-weight: bold; padding: 7px !important; margin: 0 11em 0.5em 11em!important;">
                    DECLARACIÓN INICIAL DE ACTIVIDAD ECONÓMICA
                </p>
            </td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.3em 0">I.-DATOS GENERALES:</p>
    <table style="margin-left: 1em">
        <tr>
            <td colspan="2">
                <b>Nombres y Apellidos:</b> {{ $form->client_name }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Cedula de ciudadanía:</b> {{ $form->client_document }}
            </td>
            <td>
                <b>R.U.C</b> {{ $form->ruc }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Razón social:</b> {{ $form->business_name }}
            </td>
            <td>
                <b>Fecha de inicio:</b> {{ $form->initialDateFormat }}
            </td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.3em 0">II.-DIRECCION EMPRESA /O NEGOCIO</p>
    <table style="margin-left: 1em">
        <tr>
            <td>
                <b>Calle(s) y No:</b> {{ $form->company_street }}
            </td>
            <td>
                <b>Parroquia:</b> {{ $form->company_parish }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Sector:</b> {{ $form->company_sector }}
            </td>
            <td>
                <b>Piso:</b> {{ $form->company_floor }}
            </td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.3em 0">III.- DIRECCION DEL REPRESENTANTE LEGAL</p>
    <table style="margin-left: 1em">
        <tr>
            <td>
                <b>Calle(s) y No:</b> {{ $form->legal_street }}
            </td>
            <td>
                <b>Parroquia:</b> {{ $form->legal_parish }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Sector:</b> {{ $form->legal_sector }}
            </td>
            <td>
                <b>Piso:</b> {{ $form->legal_floor }}
            </td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.3em 0">IV.- ACTIVIDAD ECONÓMICA</p>
    <table style="margin-left: 1em">
        <tr>
            <td>
                Principal: {{ $form->main }}
            </td>
            <td>
                Secundaria: {{ $form->secondary }}
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td colspan="2" style="text-align: center; font-weight: bold; border-bottom: 1px solid">
                            DESGLOSE DE ACTIVOS
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; font-weight: bold; border-bottom: 1px solid; border-left: 1px solid">
                            CONCEPTO
                        </td>
                        <td style="text-align: center; font-weight: bold; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            VALOR
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            Caja-bancos
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->box }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            Mercaderías
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->commodity }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            Maquinaria y equipos
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->machinery }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            Muebles y Enseres
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->furniture }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            Vehículos
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->vehicles }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            <table>
                                <tr>
                                    <td style="vertical-align: middle">
                                        Local
                                    </td>
                                    <td>
                                        <span style="border: 1px solid;">
                                            <span style="color: {{ !$form->local_type ? 'black' : 'white' }}">X</span>
                                        </span>
                                        <span style="margin-left: 5px; margin-bottom: 2px">Propio</span>
                                        <br>
                                        <span style="border: 1px solid;">
                                            <span style="color: {{ $form->local_type ? 'black' : 'white' }}">X</span>
                                        </span>
                                        <span style="margin-left: 5px">Arendado</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; vertical-align: middle">
                            ${{ $form->local }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            Otros activos
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->other_assets }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            <b>TOTAL DE ACTIVOS</b>
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->total_assets }}
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td colspan="2" style="text-align: center; font-weight: bold; border-bottom: 1px solid">
                            PASIVOS Y PATRIMONIO
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; font-weight: bold; border-bottom: 1px solid; border-left: 1px solid">
                            CONCEPTO
                        </td>
                        <td style="text-align: center; font-weight: bold; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            VALOR
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; font-weight: bold; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            PASIVOS
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            Cuentas por pagar
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->debs_pay }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            Dsts. por pagar
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->dsts_pay }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            Otros pasivos
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->other_passives }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            TOTAL PASIVOS
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->total_passives }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding: 1.25em 0; text-align: center; font-weight: bold; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; vertical-align: middle">
                            PATRIMONIO
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            Activo-pasivo
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->asset_passive }}
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; border-bottom: 1px solid; border-left: 1px solid">
                            <b>TOTAL</b>
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid">
                            ${{ $form->total }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <p style="font-weight: bold; margin: 0.3em 0">v.- ESTRUCTURA DE SOCIOS (solo companías)Nombres, apellidos y direcciónL</p>
    <table style="margin-left: 1em">
        <tr>
            <td>
                <b>1.-</b> {{ $form->partner11 }}
            </td>
            <td>
                {{ $form->partner21 }}
            </td>
        </tr>
        <tr>
            <td>
                <b>2.-</b> {{ $form->partner12 }}
            </td>
            <td>
                {{ $form->partner22 }}
            </td>
        </tr>
        <tr>
            <td>
                <b>3.-</b> {{ $form->partner13 }}
            </td>
            <td>
                {{ $form->partner23 }}
            </td>
        </tr>
        <tr>
            <td>
                <b>4.-</b> {{ $form->partner14 }}
            </td>
            <td>
                {{ $form->partner24 }}
            </td>
        </tr>
        <tr>
            <td>
                <b>5.- NÚMERO DE SOCIOS</b> {{ $form->number_partner }}
            </td>
            <td>
                {{ $form->partner25 }}
            </td>
        </tr>
    </table>
    <p style="margin: 0.5em 0; text-align: justify">
        Declaro que toda información contenida en este formulario es verídica, correcta y completa. Me comprometo a
        las penas que establece la ley por ocultamiento o falsedad de informacion.
    </p>
    <footer>
        <table>
            <tr>
                <td style="text-align: center">
                    <p style="margin-bottom: 0; margin-top: 4em">____________________________</p>
                    <p style="font-weight: bold">
                        Contribuyente
                    </p>
                </td>
                <td style="text-align: center">
                    <p style="margin-bottom: 0; margin-top: 4em">____________________________</p>
                    <p style="font-weight: bold">
                        Fecha
                    </p>
                </td>
                <td style="text-align: center">
                    <p style="margin-bottom: 0; margin-top: 4em">____________________________</p>
                    <p style="font-weight: bold">
                        Recibido por.
                    </p>
                </td>
            </tr>
        </table>
    </footer>
</div>
</body>
</html>
