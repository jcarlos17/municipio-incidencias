@extends('layouts.app')

@section('styles')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Registrar formulario</h4>
        </div>
        <form action="" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <div class="form-group row">
                        <label for="code" class="col-sm-3 col-form-label">Documento:</label>
                        <div class="col-sm-3">
                            <input type="text" id="code" class="form-control" value="{{ $code }}" disabled>
                        </div>
                        <label for="date" class="col-sm-3 col-form-label">Fecha:</label>
                        <div class="col-sm-3">
                            <input type="date" id="date" class="form-control" value="{{ date('Y-m-d') }}" disabled>
                        </div>
                    </div>
                    <h5>I.-DATOS GENERALES</h5>
                    <div class="form-group row">
                        <label for="client" class="col-sm-3 col-form-label">Nombres y Apellidos:</label>
                        <div class="col-sm-9">
                            <input type="text" id="client" class="form-control" value="{{ $incident->client->name }}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="document" class="col-sm-3 col-form-label">Cédula ciudadana:</label>
                        <div class="col-sm-3">
                            <input type="text" id="document" class="form-control" value="{{ $incident->client->document }}" disabled>
                        </div>
                        <label for="ruc" class="col-sm-3 col-form-label">R.U.C.:</label>
                        <div class="col-sm-3">
                            <input type="text" id="ruc" class="form-control" name="ruc" value="{{ old('ruc') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="business_name" class="col-sm-3 col-form-label">Razón social:</label>
                        <div class="col-sm-3">
                            <input type="text" id="business_name" class="form-control" name="business_name" value="{{ old('business_name') }}">
                        </div>
                        <label for="initial_date" class="col-sm-3 col-form-label">Fecha de inicio:</label>
                        <div class="col-sm-3">
                            <input type="date" id="initial_date" class="form-control" name="initial_date" value="{{ old('initial_date') }}">
                        </div>
                    </div>
                    <h5>II.-DIRECCION EMPRESA /O NEGOCIO</h5>
                    <div class="form-group row">
                        <label for="company_street" class="col-sm-3 col-form-label">Calle(s) y N°:</label>
                        <div class="col-sm-6">
                            <input type="text" id="company_street" class="form-control" name="company_street" value="{{ old('company_street') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="company_parish" class="col-sm-3 col-form-label">Parroquia:</label>
                        <div class="col-sm-6">
                            <input type="text" id="company_parish" class="form-control" name="company_parish" value="{{ old('company_parish') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="company_sector" class="col-sm-3 col-form-label">Sector:</label>
                        <div class="col-sm-6">
                            <input type="text" id="company_sector" class="form-control" name="company_sector" value="{{ old('company_sector') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="company_floor" class="col-sm-3 col-form-label">Piso:</label>
                        <div class="col-sm-6">
                            <input type="text" id="company_floor" class="form-control" name="company_floor" value="{{ old('company_floor') }}">
                        </div>
                    </div>
                    <h5>III.- DIRECCION DEL REPRESENTANTE LEGAL</h5>
                    <div class="form-group row">
                        <label for="legal_street" class="col-sm-3 col-form-label">Calle(s) y N°:</label>
                        <div class="col-sm-6">
                            <input type="text" id="legal_street" class="form-control" name="legal_street" value="{{ old('legal_street') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="legal_parish" class="col-sm-3 col-form-label">Parroquia:</label>
                        <div class="col-sm-6">
                            <input type="text" id="legal_parish" class="form-control" name="legal_parish" value="{{ old('legal_parish') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="legal_sector" class="col-sm-3 col-form-label">Sector:</label>
                        <div class="col-sm-6">
                            <input type="text" id="legal_sector" class="form-control" name="legal_sector" value="{{ old('legal_sector') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="legal_floor" class="col-sm-3 col-form-label">Piso:</label>
                        <div class="col-sm-6">
                            <input type="text" id="legal_floor" class="form-control" name="legal_floor" value="{{ old('legal_floor') }}">
                        </div>
                    </div>
                    <h5>IV.- ACTIVIDAD ECONÓMICA</h5>
                    <div class="form-group row">
                        <label for="main" class="col-sm-3 col-form-label">Principal:</label>
                        <div class="col-sm-3">
                            <input type="text" id="main" class="form-control" name="main" value="{{ old('main') }}">
                        </div>
                        <label for="secondary" class="col-sm-3 col-form-label">Secundaria:</label>
                        <div class="col-sm-3">
                            <input type="text" id="secondary" class="form-control" name="secondary" value="{{ old('secondary') }}">
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-center">
                                <th colspan="2">DESGLOSE DE ACTIVOS</th>
                                <th colspan="2">PASIVOS Y PATRIMONIO</th>
                            </tr>
                            <tr class="text-center">
                                <th>CONCEPTO</th>
                                <th>VALOR</th>
                                <th>CONCEPTO</th>
                                <th>VALOR</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Caja-bancos</td>
                                <td>
                                    <input type="text" id="box" class="form-control" name="box" value="{{ old('box') }}">
                                </td>
                                <td colspan="2" class="text-center">
                                    PASIVOS
                                </td>
                            </tr>
                            <tr>
                                <td>Mercaderia</td>
                                <td>
                                    <input type="text" id="commodity" class="form-control" name="commodity" value="{{ old('commodity') }}">
                                </td>
                                <td>Cuentas por pagar</td>
                                <td>
                                    <input type="text" id="debs_pay" class="form-control" name="debs_pay" value="{{ old('debs_pay') }}">
                                </td>
                            </tr>
                            <tr>
                                <td>Maquinaria y equipo</td>
                                <td>
                                    <input type="text" id="machinery" class="form-control" name="machinery" value="{{ old('machinery') }}">
                                </td>
                                <td>Dsts por pagar</td>
                                <td>
                                    <input type="text" id="dsts_pay" class="form-control" name="dsts_pay" value="{{ old('dsts_pay') }}">
                                </td>
                            </tr>
                            <tr>
                                <td>Muebles y enseres</td>
                                <td>
                                    <input type="text" id="furniture" class="form-control" name="furniture" value="{{ old('furniture') }}">
                                </td>
                                <td>Otros pasivos</td>
                                <td>
                                    <input type="text" id="other_passives" class="form-control" name="other_passives" value="{{ old('other_passives') }}">
                                </td>
                            </tr>
                            <tr>
                                <td>Vehiculos</td>
                                <td>
                                    <input type="text" id="vehicles" class="form-control" name="vehicles" value="{{ old('vehicles') }}">
                                </td>
                                <td>TOTAL PASIVOS</td>
                                <td>
                                    <input type="text" id="total_passives" class="form-control" name="total_passives" value="{{ old('total_passives') }}">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Local
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="local_type" id="type0" value="0" {{ old('local_type') == '0' ? 'checked' : '' }}>
                                        <label class="form-check-label" for="type0">
                                            Propio
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="local_type" id="type1" value="1" {{ old('local_type') == '1' ? 'checked' : '' }}>
                                        <label class="form-check-label" for="type1">
                                            Arendado
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" id="local" class="form-control" name="local" value="{{ old('local') }}">
                                </td>
                                <td colspan="2" class="text-center">PATRIMONIO</td>
                            </tr>
                            <tr>
                                <td>otros activos</td>
                                <td>
                                    <input type="text" id="other_assets" class="form-control" name="other_assets" value="{{ old('other_assets') }}">
                                </td>
                                <td>Activos-pasivos</td>
                                <td>
                                    <input type="text" id="asset_passive" class="form-control" name="asset_passive" value="{{ old('asset_passive') }}">
                                </td>
                            </tr>
                            <tr>
                                <td>TOTAL ACTIVOS</td>
                                <td>
                                    <input type="text" id="total_assets" class="form-control" name="total_assets" value="{{ old('total_assets') }}">
                                </td>
                                <td>TOTAL</td>
                                <td>
                                    <input type="text" id="total" class="form-control" name="total" value="{{ old('total') }}">
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <h5 class="">V.- ESTRUCTURA DE SOCIOS (solo companías)Nombres, apellidos y dirección</h5>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <input type="text" id="partner11" class="form-control" name="partner11" placeholder="Estructura socios 01" value="{{ old('partner11') }}">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" id="partner21" class="form-control" name="partner21" placeholder="Estructura socios 021" value="{{ old('partner21') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <input type="text" id="partner12" class="form-control" name="partner12" placeholder="Estructura socios 02" value="{{ old('partner12') }}">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" id="partner22" class="form-control" name="partner22" placeholder="Estructura socios 022" value="{{ old('partner22') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <input type="text" id="partner13" class="form-control" name="partner13" placeholder="Estructura socios 03" value="{{ old('partner13') }}">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" id="partner23" class="form-control" name="partner23" placeholder="Estructura socios 023" value="{{ old('partner23') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <input type="text" id="partner14" class="form-control" name="partner14" placeholder="Estructura socios 04" value="{{ old('partner14') }}">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" id="partner24" class="form-control" name="partner24" placeholder="Estructura socios 024" value="{{ old('partner24') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <input type="text" id="number_partner" class="form-control" name="number_partner" placeholder="Número de socios" value="{{ old('number_partner') }}">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" id="partner25" class="form-control" name="partner25" placeholder="Estructura socios 025" value="{{ old('partner25') }}">
                        </div>
                    </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ route('form.index', ['incident' => $incident->id]) }}" class="btn btn-light" title="Volver">Volver</a>
                    <button type="submit" class="btn btn-success">Registrar formulario</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
@endsection
