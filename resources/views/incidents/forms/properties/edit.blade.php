@extends('layouts.app')

@section('styles')
    <!-- Summernote -->
    <link href="{{ asset('vendor/summernote/summernote.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Editar formulario</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group row">
                    <label for="date" class="col-sm-3 col-form-label">Fecha</label>
                    <div class="col-sm-3">
                        <input type="datetime-local" id="date" class="form-control" value="{{ date('Y-m-d').'T'.date('H:i') }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="client" class="col-sm-3 col-form-label">Nombre Solicitante</label>
                    <div class="col-sm-3">
                        <input type="text" id="client" class="form-control" value="{{ $form->client_name }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="document" class="col-sm-3 col-form-label">Cédula Solicitante</label>
                    <div class="col-sm-3">
                        <input type="text" id="document" class="form-control" value="{{ $form->client_document }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="owner_name" class="col-sm-3 col-form-label">Nombre Propietario</label>
                    <div class="col-sm-3">
                        <input type="text" id="owner_name" class="form-control" name="owner_name" value="{{ old('owner_name', $form->owner_name) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="owner_document" class="col-sm-3 col-form-label">Cédula Propietario</label>
                    <div class="col-sm-3">
                        <input type="text" id="owner_document" class="form-control" name="owner_document" value="{{ old('owner_document', $form->owner_document) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="department" class="col-sm-3 col-form-label">Departamentos</label>
                    <div class="col-sm-3">
                        <select name="department" id="department" class="form-control">
                            <option value="">Seleccionar</option>
                            <option value="PRESIDENCIA" {{ old('department', $form->department) == 'PRESIDENCIA' ? 'selected' : '' }}>PRESIDENCIA</option>
                            <option value="AVALUOS Y CATASTROS" {{ old('department', $form->department) == 'AVALUOS Y CATASTROS' ? 'selected' : '' }}>AVALUOS Y CATASTROS</option>
                            <option value="OBRAS PUBLICAS" {{ old('department', $form->department) == 'OBRAS PUBLICAS' ? 'selected' : '' }}>OBRAS PUBLICAS</option>
                            <option value="COMISARIA MUNICIPAL" {{ old('department', $form->department) == 'COMISARIA MUNICIPAL' ? 'selected' : '' }}>COMISARIA MUNICIPAL</option>
                            <option value="OTROS DEPARTAMENTOS" {{ old('department', $form->department) == 'OTROS DEPARTAMENTOS' ? 'selected' : '' }}>OTROS DEPARTAMENTOS</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="reason" class="col-sm-3 col-form-label">Motivos</label>
                    <div class="col-sm-3">
                        <select name="reason" id="reason" class="form-control">
                            <option value="">Seleccionar</option>
                            <option value="ZONA URBANA" {{ old('reason', $form->reason) == 'ZONA URBANA' ? 'selected' : '' }}>ZONA URBANA</option>
                            <option value="ZONA RURAL" {{ old('reason', $form->reason) == 'ZONA RURAL' ? 'selected' : '' }}>ZONA RURAL</option>
                            <option value="AVALUO Y REAVALUO" {{ old('reason', $form->reason) == 'AVALUO Y REAVALUO' ? 'selected' : '' }}>AVALUO Y REAVALUO</option>
                            <option value="DESAPROPIOS" {{ old('reason', $form->reason) == 'DESAPROPIOS' ? 'selected' : '' }}>DESAPROPIOS</option>
                            <option value="LINEA DE FABRICA" {{ old('reason', $form->reason) == 'LINEA DE FABRICA' ? 'selected' : '' }}>LINEA DE FABRICA</option>
                            <option value="OTROS" {{ old('reason', $form->reason) == 'OTROS' ? 'selected' : '' }}>OTROS</option>
                        </select>
                    </div>
                </div>
                <hr>
                <h5 class="">Ubicación de los predios</h5>
                <div class="form-group row">
                    <label for="street" class="col-sm-3 col-form-label">Calle</label>
                    <div class="col-sm-4">
                        <input type="text" id="street" class="form-control" name="street" value="{{ old('street', $form->street) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="district" class="col-sm-3 col-form-label">Barrio</label>
                    <div class="col-sm-4">
                        <input type="text" id="district" class="form-control" name="district" value="{{ old('district', $form->district) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sector" class="col-sm-3 col-form-label">Sector</label>
                    <div class="col-sm-4">
                        <input type="text" id="sector" class="form-control" name="sector" value="{{ old('sector', $form->sector) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="parish" class="col-sm-3 col-form-label">Parroquia</label>
                    <div class="col-sm-4">
                        <input type="text" id="parish" class="form-control" name="parish" value="{{ old('parish', $form->parish) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="observation" class="col-sm-3 col-form-label">Observaciones</label>
                    <div class="col-sm-9">
                        <textarea class="summernote" id="observation" name="observation">{{ old('observation', $form->observation) }}</textarea>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ route('form.index', ['incident' => $form->incident_id]) }}" class="btn btn-light" title="Volver">Volver</a>
                    <button type="submit" class="btn btn-success">Registrar formulario</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <!-- Summernote -->
    <script src="{{ asset('vendor/summernote/js/summernote.min.js') }}"></script>
    <!-- Summernote init -->
    <script src="{{ asset('js/plugins-init/summernote-init.js') }}"></script>
@endsection
