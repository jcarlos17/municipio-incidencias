<html>
<head>
    <title>{{ $form->code }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 2.5cm; }
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: none;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 60px;
        }
    </style>
</head>
<body>
<div style="margin: 0 auto;">
    <table style="margin: 0;">
        <tr style="">
            <td style="width: 53%">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
            <td style="text-align: right">
                <p style="font-weight: bold; margin: 0; font-size: 16px">Documento: {{ $form->code }}</p>
            </td>
        </tr>
    </table>
    <table style="margin: 0 auto;">
        <tr style="">
            <td style=" text-align: center; position: relative">
                <p style="font-weight: bold; margin-top: 0; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; margin-bottom: 0; font-size: 14px">MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p style="font-weight: bold; margin-bottom: 4px">DEPARTAMENTO FINANCIERO</p>
                <p style="background: black; color: white; font-weight: bold; padding: 7px !important; margin: 0 12em 2em 12em!important;">
                    CERTIFICADO DE ATENCION DE LA PROPIEDAD
                </p>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="font-weight: bold">
                FECHA:
            </td>
            <td style="width: 90%">
                <span>{{ $form->dateComplete }}</span>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="font-weight: bold; width: 31%">
                NOMBRE DEL PROPIETARIO:
            </td>
            <td>
                <span>{{ $form->owner_name }}</span>
            </td>
            <td style="font-weight: bold; width: 5%">
                CÉDULA:
            </td>
            <td>
                <span>{{ $form->owner_document }}</span>
            </td>
        </tr>
    </table>
    <hr>
    <table style="margin-bottom: 2em">
        <tr>
            <td style="font-weight: bold; width: 31%">
                NOMBRE DEL SOLICITANTE:
            </td>
            <td>
                <span>{{ $form->client_name }}</span>
            </td>
            <td style="font-weight: bold; width: 5%">
                CÉDULA:
            </td>
            <td>
                <span>{{ $form->client_document }}</span>
            </td>
        </tr>
    </table>
    <table style="margin-bottom: 2em">
        <tr>
            <td colspan="2" style="font-weight: bold">
                DEPARTAMENTOS
            </td>
            <td colspan="2" style="font-weight: bold">
                MOTIVOS
            </td>
        </tr>
        <tr>
            <td>PRESIDENCIA</td>
            <td>
                @if($form->department == 'PRESIDENCIA')
                    (X)
                @else
                    (<span style="color: white">X</span>)
                @endif
            </td>
            <td>ZONA URBANA</td>
            <td>
                @if($form->reason == 'ZONA URBANA')
                    (X)
                @else
                    (<span style="color: white">X</span>)
                @endif
            </td>
        </tr>
        <tr>
            <td>AVALUOS Y CATASTROS</td>
            <td>
                @if($form->department == 'AVALUOS Y CATASTROS')
                    (X)
                @else
                    (<span style="color: white">X</span>)
                @endif
            </td>
            <td>ZONA RURAL</td>
            <td>
                @if($form->reason == 'ZONA RURAL')
                    (X)
                @else
                    (<span style="color: white">X</span>)
                @endif
            </td>
        </tr>
        <tr>
            <td>OBRAS PUBLICAS</td>
            <td>
                @if($form->department == 'OBRAS PUBLICAS')
                    (X)
                @else
                    (<span style="color: white">X</span>)
                @endif
            </td>
            <td>AVALUO Y REAVALUO</td>
            <td>
                @if($form->reason == 'AVALUO Y REAVALUO')
                    (X)
                @else
                    (<span style="color: white">X</span>)
                @endif
            </td>
        </tr>
        <tr>
            <td>COMISARIA MUNICIPAL</td>
            <td>
                @if($form->department == 'COMISARIA MUNICIPAL')
                    (X)
                @else
                    (<span style="color: white">X</span>)
                @endif
            </td>
            <td>DESAPROPIOS</td>
            <td>
                @if($form->reason == 'DESAPROPIOS')
                    (X)
                @else
                    (<span style="color: white">X</span>)
                @endif
            </td>
        </tr>
        <tr>
            <td>OTROS DEPARTAMENTOS</td>
            <td>
                 @if($form->department == 'OTROS DEPARTAMENTOS')
                    (X)
                @else
                    (<span style="color: white">X</span>)
                @endif
            </td>
            <td>LINEA DE FABRICA</td>
            <td>
                 @if($form->reason == 'LINEA DE FABRICA')
                    (X)
                @else
                    (<span style="color: white">X</span>)
                @endif
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>OTROS</td>
            <td>
                @if($form->reason == 'OTROS')
                    (X)
                @else
                    (<span style="color: white">X</span>)
                @endif
            </td>
        </tr>
    </table>
    <hr>
    <table style="margin-bottom: 2em">
        <tr>
            <td colspan="4" style="font-weight: bold; text-align: center">
                UBICACION DE LOS PREDIOS
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; width: 10%">CALLE:</td>
            <td colspan="3">{{ $form->street }}</td>
        </tr>
        <tr>
            <td style="font-weight: bold; width: 10%">BARRIO:</td>
            <td>{{ $form->district }}</td>
            <td style="font-weight: bold; width: 10%">SECTOR:</td>
            <td>{{ $form->sector }}</td>
        </tr>
        <tr>
            <td style="font-weight: bold; width: 10%">PARROQUIA:</td>
            <td colspan="3">{{ $form->parish }}</td>
        </tr>
        <tr>
            <td style="font-weight: bold; width: 10%">OBSERVACIONES:</td>
            <td colspan="3">{!! $form->observation !!}</td>
        </tr>
    </table>
    <footer>
        <table>
            <tr>
                <td style="text-align: center; width: 60%">
                    <p style="margin-bottom: 0">Autorización de la inspección</p>
                    <p>VSTO. BNO. PRESIDENTE DEL CONSEJO</p>
                </td>
                <td style="text-align: center">
                    <p style="margin-bottom: 0">__________________________________</p>
                    <p style="font-weight: bold">EL INSPECTOR</p>
                </td>
            </tr>
        </table>
    </footer>
</div>
</body>
</html>
