@extends('layouts.app')

@section('styles')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Editar formulario</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group row">
                    <label id="$code" class="col-sm-2 col-form-label">Formulario</label>
                    <div class="col-sm-4">
                        <input type="text" id="$code" class="form-control" value="{{ $form->code }}" disabled>
                    </div>
                    <label id="date" class="col-sm-2 col-form-label">Fecha</label>
                    <div class="col-sm-4">
                        <input type="date" id="date" class="form-control" value="{{ $form->created_at->format('Y-m-d') }}" disabled>
                    </div>
                </div>
                <div class="form-group row">

                </div>
                <div class="form-group row">
                    <label for="client" class="col-sm-2 col-form-label">Nombre</label>
                    <div class="col-sm-4">
                        <input type="text" id="client" class="form-control" value="{{ $form->client_name }}" disabled>
                    </div>
                    <label for="document" class="col-sm-2 col-form-label">Cédula</label>
                    <div class="col-sm-4">
                        <input type="text" id="document" class="form-control" value="{{ $form->client_document }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="constancy" class="col-sm-2 col-form-label">Consta en el catastro</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="constancy" id="constancy">
                            <option value="1" {{ old('constancy', $form->constancy) == '1' ? 'selected' : '' }}>SI</option>
                            <option value="0" {{ old('constancy', $form->constancy) == '0' ? 'selected' : '' }}>NO</option>
                        </select>
                    </div>
                    <label for="year" class="col-sm-2 col-form-label">Consta en el catastro (año)</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" id="year" name="year" min="1900" max="2099" step="1" value="{{ old('year', $form->year) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="canton" class="col-sm-2 col-form-label">Cantón</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="canton" name="canton" value="{{ old('canton', $form->canton) }}">
                    </div>
                    <label for="parish" class="col-sm-2 col-form-label">Parroquia</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parish" name="parish" value="{{ old('parish', $form->parish) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="registration" class="col-sm-2 col-form-label">Registro</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="registration" name="registration" value="{{ old('registration', $form->registration) }}">
                    </div>
                    <label for="estate" class="col-sm-2 col-form-label">Predio</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="estate" name="estate" value="{{ old('estate', $form->estate) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="value" class="col-sm-2 col-form-label">Avaluo Catastral</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="value" name="value" value="{{ old('value', $form->value) }}">
                    </div>
                </div>

                <h5>INFORMACIÓN PARA ACTUALIZACIÓN CATASTRAL</h5>
                <div class="form-group row">
                    <label for="notary" class="col-sm-2 col-form-label">Notaría</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="notary" name="notary" value="{{ old('notary', $form->notary) }}">
                    </div>
                    <label for="amount" class="col-sm-2 col-form-label">Cuantía</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="amount" name="amount" value="{{ old('amount', $form->amount) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="seller_name" class="col-sm-2 col-form-label">Vendedor</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="seller_name" name="seller_name" value="{{ old('seller_name', $form->seller_name) }}">
                    </div>
                    <label for="seller_document" class="col-sm-2 col-form-label">Cédula N°</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="seller_document" name="seller_document" value="{{ old('seller_document', $form->seller_document) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="buyer_name" class="col-sm-2 col-form-label">Comprador</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="buyer_name" name="buyer_name" value="{{ old('buyer_name', $form->buyer_name) }}">
                    </div>
                    <label for="buyer_document" class="col-sm-2 col-form-label">Cédula N°</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="buyer_document" name="buyer_document" value="{{ old('buyer_document', $form->buyer_document) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cadastral_canton" class="col-sm-2 col-form-label">Cantón</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="cadastral_canton" name="cadastral_canton" value="{{ old('cadastral_canton', $form->cadastral_canton) }}">
                    </div>
                    <label for="cadastral_parish" class="col-sm-2 col-form-label">Parroquia</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="cadastral_parish" name="cadastral_parish" value="{{ old('cadastral_parish', $form->cadastral_parish) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sector" class="col-sm-2 col-form-label">Sector o barrio</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="sector" name="sector" value="{{ old('sector', $form->sector) }}">
                    </div>
                    <label for="property_name" class="col-sm-2 col-form-label">Nombre del predio</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="property_name" name="property_name" value="{{ old('property_name', $form->property_name) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inscription_date" class="col-sm-2 col-form-label">Fecha de la inscripción de la escritura</label>
                    <div class="col-sm-4">
                        <input type="date" class="form-control" id="inscription_date" name="inscription_date" value="{{ old('inscription_date', $form->inscription_date) }}">
                    </div>
                    <label for="total_area" class="col-sm-2 col-form-label">Superficie total</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="total_area" name="total_area" value="{{ old('total_area', $form->total_area) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="total_sale" class="col-sm-2 col-form-label">Venta total (Has.)</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="total_sale" name="total_sale" value="{{ old('total_sale', $form->total_sale) }}">
                    </div>
                    <label for="partial_sale" class="col-sm-2 col-form-label">Venta parcial (Has.)</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="partial_sale" name="partial_sale" value="{{ old('partial_sale', $form->partial_sale) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="reserved_area" class="col-sm-2 col-form-label">Superficie reservada</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="reserved_area" name="reserved_area" value="{{ old('reserved_area', $form->reserved_area) }}">
                    </div>
                    <label for="domain_limitation" class="col-sm-2 col-form-label">Limitación de dominio (USUFRUCTO)</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="domain_limitation" id="domain_limitation">
                            <option value="1" {{ old('domain_limitation', $form->domain_limitation) == '1' ? 'selected' : '' }}>SI</option>
                            <option value="0" {{ old('domain_limitation', $form->domain_limitation) == '0' ? 'selected' : '' }}>NO</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <h5>LINDEROS DEL LOTE QUE VENDE</h5>
                        <div class="form-group row">
                            <label for="north_sale" class="col-sm-4 col-form-label">Norte</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="north_sale" name="north_sale" value="{{ old('north_sale', $form->north_sale) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="south_sale" class="col-sm-4 col-form-label">Sur</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="south_sale" name="south_sale" value="{{ old('south_sale', $form->south_sale) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="west_sale" class="col-sm-4 col-form-label">Oeste</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="west_sale" name="west_sale" value="{{ old('west_sale', $form->west_sale) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="east_sale" class="col-sm-4 col-form-label">Este</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="east_sale" name="east_sale" value="{{ old('east_sale', $form->east_sale) }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h5>LINDEROS DEL LOTE QUE RESERVA</h5>
                        <div class="form-group row">
                            <label for="north_reservation" class="col-sm-4 col-form-label">Norte</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="north_reservation" name="north_reservation" value="{{ old('north_reservation', $form->north_reservation) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="south_reservation" class="col-sm-4 col-form-label">Sur</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="south_reservation" name="south_reservation" value="{{ old('south_reservation', $form->south_reservation) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="west_reservation" class="col-sm-4 col-form-label">Oeste</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="west_reservation" name="west_reservation" value="{{ old('west_reservation', $form->west_reservation) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="east_reservation" class="col-sm-4 col-form-label">Este</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="east_reservation" name="east_reservation" value="{{ old('east_reservation', $form->east_reservation) }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ route('form.index', ['incident' => $form->incident_id]) }}" class="btn btn-light" title="Volver">Volver</a>
                    <button type="submit" class="btn btn-success">Guardar cambios</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
@endsection
