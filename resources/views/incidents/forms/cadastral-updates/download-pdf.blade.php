<html>
<head>
    <title>{{ $form->code }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 2.5cm; }
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: none;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 46.5em;
        }
    </style>
</head>
<body>
<div style="width: 100%; margin: 0 auto;">
    <table style="margin: 0;">
        <tr style="">
            <td style="width: 50%">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
            <td style="text-align: right">
                <p style="margin: 0 !important; font-weight: bold; font-size: 18px">Documento: {{ $form->code }}</p>
            </td>
        </tr>
    </table>
    <table style="margin: 0;">
        <tr>
            <td style=" text-align: center">
                <p style="font-weight: bold; margin-top: 0; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; font-size: 14px">MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p style="background: black; color: white; font-weight: bold; padding: 7px !important; margin: 0 9em !important;">CERTIFICADO DE AVALÚOS Y ACTUALIZACIÓN CATASTRAL </p>
            </td>
        </tr>
    </table>
    <p style="text-align: right !important; margin-bottom: 2px; margin-right: 7em; font-weight: bold">TRAMITE - NÚMERO</p>
    <table>
        <tr>
            <td style="border: 2px solid; padding: 1em">
                <p style="text-align: justify; line-height: 2em">
                    El Jefe de Avalúos del Cantón Salcedo certifica que revisado los registros catastrales el (la) señor (a)
                    <u style="font-weight: bold">{{ $form->client_name }}</u> con cédula o RUC. N°.
                    <u style="font-weight: bold">{{ $form->client_document }}</u>
                    SÍ
                    @if($form->constancy)
                        (X)
                    @else
                    (<span style="color: white">X</span>)
                    @endif
                    NO
                    @if($form->constancy)
                        (<span style="color: white">X</span>)
                    @else
                    (X)
                    @endif
                    consta en el catastro del año del <u style="font-weight: bold">{{ $form->year }}</u>
                    cantón <u style="font-weight: bold">{{ $form->canton }}</u>
                    parroquia <u style="font-weight: bold">{{ $form->parish }}</u>
                    Registro N°. <u style="font-weight: bold">{{ $form->registration }}</u>
                    con el predio <u style="font-weight: bold">{{ $form->estate }}</u>
                    un avalúo catastral de USD. <u style="font-weight: bold">{{ $form->value }}</u>
                </p>
            </td>
        </tr>
    </table>
    <p style="text-align: center !important; font-weight: bold">INFORMACIÓN PARA ACTUALIZACIÓN CATASTRAL</p>
    <table>
        <tr>
            <td style="border: 2px solid; padding: 1em">
                <table>
                    <tr>
                        <td style="min-width: 50%">
                            NOTARÍA: {{ $form->notary }}
                        </td>
                        <td style="padding-left: 2em">
                            CUANTÍA: {{ $form->amount }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            VENDEDOR: {{ $form->seller_name }}
                        </td>
                        <td style="padding-left: 2em">
                            Cédula N°: {{ $form->seller_document }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            COMPRADOR: {{ $form->buyer_name }}
                        </td>
                        <td style="padding-left: 2em">
                            Cédula N°: {{ $form->buyer_document }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            CANTÓN: {{ $form->cadastral_canton }}
                        </td>
                        <td style="padding-left: 2em">
                            PARROQUIA: {{ $form->cadastral_parish }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            SECTOR O BARRIO: {{ $form->cadastral_canton }}
                        </td>
                        <td style="padding-left: 2em">
                            NOMBRE DEL PREDIO: {{ $form->cadastral_parish }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            FECHA DE LA INSCRIPCIÓN DE LA ESCRITURA: {{ $form->created_at->format('d-m-Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            SUPERFICIE TOTAL: {{ $form->total_area }}
                        </td>
                        <td style="padding-left: 2em">
                            VENTA TOTAL: ({{ $form->total_sale }} Has.)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            VENTA PARCIAL: ({{ $form->partial_sale }} Has.)
                        </td>
                        <td style="padding-left: 2em">
                            SUPERFICIE RESERVADA: {{ $form->reserved_area }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            LIMITACIÓN DE DOMINIO (USUFRUCTO): SI ({{ $form->domain_limitation ? 'X' : ' ' }}) NO({{ $form->domain_limitation ? ' ' : 'X' }})
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 50%">
                            <p style="font-weight: bold">LINDEROS DEL LOTE QUE VENDE </p>
                            <table>
                                <tr>
                                    <td style="border-bottom: 1px solid">
                                        NORTE: {{ $form->north_sale }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid">
                                        SUR: {{ $form->south_sale }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid">
                                        OESTE: {{ $form->west_sale }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid">
                                        ESTE: {{ $form->east_sale }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <p style="font-weight: bold">LINDEROS DEL LOTE QUE RESERVA</p>
                            <table>
                                <tr>
                                    <td style="border-bottom: 1px solid">
                                        NORTE: {{ $form->north_reservation }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid">
                                        SUR: {{ $form->south_reservation }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid">
                                        OESTE: {{ $form->west_reservation }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid">
                                        ESTE: {{ $form->east_reservation }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%; margin-top: 4em">
                    <tr>
                        <td style="text-align: center">
                            <p style="margin-bottom: 0; text-decoration: underline">f.) {{ $form->dateComplete }}</p>
                            <p style="font-weight: bold">Lugar y Fecha de Protocolización</p>
                        </td>
                        <td style="text-align: center">
                            <p style="margin-bottom: 0">f.) ________________________________</p>
                            <p style="font-weight: bold">Firma y sello del Notario</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <p style="text-align: center !important; margin-bottom: 2px; font-weight: bold">CERTIFICADO DEL REGISTRADOR DE LA PROPIEDAD</p>
</div>
</body>
</html>
