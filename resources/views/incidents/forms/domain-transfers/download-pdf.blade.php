<html>
<head>
    <title>{{ $form->code }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 2.5cm; }
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: none;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: left;
            padding: 4px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 120px;
        }
    </style>
</head>
<body>
<div style="margin: 0 auto;">
    <table style="margin: 0;">
        <tr style="">
            <td style="width: 50%">
                <img src="{{ asset('images/horizontal1.png') }}" alt="Logo" width="120px">
            </td>
            <td style="text-align: right">
                <p style="margin: 0 !important; font-weight: bold; font-size: 16px">Documento: {{ $form->code }}</p>
            </td>
        </tr>
    </table>
    <table style="margin: 0;">
        <tr>
            <td style=" text-align: center">
                <p style="font-weight: bold; margin-top: 0; font-size: 14px">GOBIERNO AUTÓNOMO DESCENTRALIZADO</p>
                <p style="font-weight: bold; font-size: 14px">MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p style="font-weight: bold; font-size: 12px">SECCIÓN:  AVALÚOS Y CATASTROS</p>
                <p style="font-weight: bold; font-size: 12px">PERMISO PARA OTORGAMIENTO DE ESCRITURA DE COMPRA-VENTA</p>
            </td>
        </tr>
    </table>
    <table style="margin-top: 2em;">
        <tr>
            <td style="min-width: 50%">
                {{ $form->dateComplete }}
            </td>
            <td style="padding-left: 2em">
                NOTARÍA: {{ $form->notary }}
            </td>
        </tr>
        <tr>
            <td>
                VENDEDOR: {{ $form->seller }}
            </td>
            <td style="padding-left: 2em">
                COMPRADOR: {{ $form->buyer }}
            </td>
        </tr>
        <tr>
            <td>
                PARROQUIA: {{ $form->parish }}
            </td>
            <td style="padding-left: 2em">
                SECTOR: {{ $form->sector }}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                CALLE: {{ $form->street }}
            </td>
        </tr>
        <tr>
            <td>
                CLAVE CATASTRAL ANTERIOR: {{ $form->old_key }}
            </td>
            <td style="padding-left: 2em">
                CLAVE CATASTRAL ACTUAL: {{ $form->current_key }}
            </td>
        </tr>
        <tr>
            <td>
                DUEÑO ANTERIOR: {{ $form->old_property }}
            </td>
            <td style="padding-left: 2em">
                PERIMETRO: {{ $form->perimeter }}
            </td>
        </tr>
        <tr>
            <td>
                ADQUIRIDO POR: {{ $form->acquirer }}
            </td>
            <td style="padding-left: 2em">
                FECHA: {{ $form->acquired_date }}
            </td>
        </tr>
        <tr>
            <td>
                EXTENSIÓN DEL PRED: {{ $form->property_extension }}
            </td>
            <td style="padding-left: 2em">
                VENTA TOTAL: {{ $form->sale_extension }}
            </td>
        </tr>
        <tr>
            <td>
                PRECIO COMPRA ANTERIOR: {{ $form->old_price }}
            </td>
            <td style="padding-left: 2em">
                PRECIO VENTA ACTUAL:  {{ $form->current_sale }}
            </td>
        </tr>
    </table>
    <p style="margin-top: 3em; line-height: 2em; text-align: justify">
        Por tanto usted Sr. Notario se servirá hacer efectivos los Impuestos de Ley sobre el indicado avalúo,
        tambien necesariamente debe hacer constar en la escritura de enajenación la cabida del terreno para
        efectos legales
    </p>
    <footer>
        <table>
            <tr>
                <td style="text-align: center">
                    <p>Atentamente</p>
                    <p style="margin-top: 4em">________________________________________</p>
                    <p>Jefe de Avalúos y Catastros</p>
                </td>
            </tr>
        </table>
    </footer>
</div>
</body>
</html>
