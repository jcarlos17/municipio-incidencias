@extends('layouts.app')

@section('styles')
    <!-- Summernote -->
    <link href="{{ asset('vendor/summernote/summernote.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Editar formulario</h4>
        </div>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible alert-alt fade show">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group row">
                    <label for="code" class="col-sm-2 col-form-label">Formulario</label>
                    <div class="col-sm-4">
                        <input type="text" id="code" class="form-control" value="{{ $form->code }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label">Fecha</label>
                    <div class="col-sm-4">
                        <input type="date" id="date" class="form-control" value="{{ $form->created_at->format('Y-m-d') }}" disabled>
                    </div>
                    <label for="notary" class="col-sm-2 col-form-label">Notaría</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="notary" name="notary" value="{{ old('notary', $form->notary) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="seller" class="col-sm-2 col-form-label">Vendedor</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="seller" name="seller" value="{{ old('seller', $form->seller) }}">
                    </div>
                    <label for="buyer" class="col-sm-2 col-form-label">Comprador</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="buyer" name="buyer" value="{{ old('buyer', $form->buyer) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="parish" class="col-sm-2 col-form-label">Parroquia</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parish" name="parish" value="{{ old('parish', $form->parish) }}">
                    </div>
                    <label for="sector" class="col-sm-2 col-form-label">Sector</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="sector" name="sector" value="{{ old('sector', $form->sector) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="street" class="col-sm-2 col-form-label">Calle</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="street" name="street" value="{{ old('street', $form->street) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="old_key" class="col-sm-2 col-form-label">Clave catastral anterior</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="old_key" name="old_key" value="{{ old('old_key', $form->old_key) }}">
                    </div>
                    <label for="current_key" class="col-sm-2 col-form-label">Clave catastral actual</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="current_key" name="current_key" value="{{ old('current_key', $form->current_key) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="old_property" class="col-sm-2 col-form-label">Dueño anterior</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="old_property" name="old_property" value="{{ old('old_property', $form->old_property) }}">
                    </div>
                    <label for="perimeter" class="col-sm-2 col-form-label">Perímetro</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="perimeter" name="perimeter" value="{{ old('perimeter', $form->perimeter) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="acquirer" class="col-sm-2 col-form-label">Adquirido por</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="acquirer" name="acquirer" value="{{ old('acquirer', $form->acquirer) }}">
                    </div>
                    <label for="acquired_date" class="col-sm-2 col-form-label">Fecha adquirida</label>
                    <div class="col-sm-4">
                        <input type="date" class="form-control" id="acquired_date" name="acquired_date" value="{{ old('acquired_date', $form->acquired_date) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="property_extension" class="col-sm-2 col-form-label">Extensión del predio</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="property_extension" name="property_extension" value="{{ old('property_extension', $form->property_extension) }}">
                    </div>
                    <label for="sale_extension" class="col-sm-2 col-form-label">Venta total</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="sale_extension" name="sale_extension" value="{{ old('sale_extension', $form->sale_extension) }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="old_price" class="col-sm-2 col-form-label">Precio compra anterior</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="old_price" name="old_price" value="{{ old('old_price', $form->old_price) }}">
                    </div>
                    <label for="current_sale" class="col-sm-2 col-form-label">Precio venta actual</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="current_sale" name="current_sale" value="{{ old('current_sale', $form->current_sale) }}">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <a href="{{ route('form.index', ['incident' => $form->incident_id]) }}" class="btn btn-light" title="Volver">Volver</a>
                    <button type="submit" class="btn btn-success">Guardar cambios</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <!-- Summernote -->
    <script src="{{ asset('vendor/summernote/js/summernote.min.js') }}"></script>
    <!-- Summernote init -->
    <script src="{{ asset('js/plugins-init/summernote-init.js') }}"></script>
@endsection
