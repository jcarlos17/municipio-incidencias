@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Reportar incidencia</h4>
    </div>
    <form action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible alert-alt fade show">
                    <ul>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if($user->is_support_level_one || $user->is(\App\Models\User::CLIENT))
                <div class="form-group">
                    <label for="process_id">Proceso</label>
                    <select name="process_id" class="form-control" id="process_id" required>
                        <option value="">Seleccionar</option>
                        @foreach ($processes as $process)
                            <option value="{{ $process->id }}" {{ old('process_id') == $process->id ? 'selected' : '' }}>
                                {{ $process->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="severity">Severidad</label>
                    <select name="severity" class="form-control">
                        <option value="M">Menor</option>
                        <option value="N">Normal</option>
                        <option value="A">Alta</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Cliente</label>
                    <div class="input-group">
                        @if($user->is(\App\Models\User::CLIENT))
                            <input type="hidden" name="client_id" id="client_id" value="{{ $user->id }}">
                            <input type="text" name="document" id="document" class="form-control" value="{{ $user->document. ' '. $user->name }}" readonly>
                        @else
                            <input type="hidden" name="client_id" id="client_id" value="{{ old('client_id') }}">
                            <input type="text" name="document" id="document" class="form-control" value="{{ old('document') }}" readonly>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#modalSelectClient">
                                    Seleccionar cliente
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Descripción</label>
                    <textarea name="description" class="form-control" rows="4" required>{{ old('description') }}</textarea>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-dark" id="btnArchive">Requisitos</button>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <tbody id="bodyFiles">
                            @if($oldNames)
                                @foreach($oldNames as $key => $oldName)
                                    <tr>
                                        <td>{{ $oldName }}</td>
                                        <td class="text-right">
                                            <input type="hidden" name="names[]" value="{{ $oldName }}">
                                            <input type="hidden" data-archive name="archive_ids[]" value="{{ $oldArchiveIds[$key] }}">
                                            <input type="file" data-input-file accept="application/pdf" style="display:none">
                                            <button class="btn btn-sm btn-dark mr-2" type="button" data-archive="{{ $oldArchiveIds[$key] }}">Archivo</button>
                                            @if($oldArchiveIds[$key] > 0)
                                                <a href="{{ $archiveUrls[$key] }}" target="_blank" class="btn btn-sm btn-dark">Ver</a>
                                            @else
                                                <span><button class="btn btn-sm btn-dark" type="button" data-archive="{{ $oldArchiveIds[$key] }}" disabled>Ver</button></span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                @if(auth()->user()->is(\App\Models\User::SUPPORT))
                    <div class="form-group">
                        <div class="custom-control custom-checkbox ml-1">
                            <input type="checkbox" class="custom-control-input" id="verified" required>
                            <label class="custom-control-label" for="verified">Requisitos verificados y revisados</label>
                        </div>
                    </div>
                @endif
            @else
                <div class="alert alert-dismissible alert-info">
                    Solo pueden <strong>registrar</strong> usuarios de soporte de nivel 1.
                </div>
            @endif
        </div>
        @if($user->is_support_level_one || $user->is(\App\Models\User::CLIENT))
            <div class="card-footer">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Registrar incidencia</button>
                    @if ($errors->first('document'))
                        <a href="{{ url('cliente/crear') }}" class="btn btn-light pull-right" target="_blank" title="¿Se trata de un cliente nuevo?">
                            Nuevo cliente
                        </a>
                    @endif
                </div>
            </div>
        @endif
    </form>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalSelectClient">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Seleccionar cliente</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible alert-alt fade show" id="alert" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    Es necesario completar los campos y seleccionar un proceso para buscar
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <select name="" id="type" class="form-control">
                                <option value="">Seleccionar</option>
                                <option value="document">Buscar por cédula</option>
                                <option value="name">Buscar por nombre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <input type="text" class="form-control" id="inputSearch">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <button class="btn btn-light" id="btnSearch">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Cédula</th>
                        <th>Nombre</th>
                        <th>Código</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="content-search">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        const $btnSearch = $('#btnSearch');
        const $option = $('#type');
        const $input = $('#inputSearch');
        const $alert = $('#alert');
        const $content = $('#content-search');

        const $body = $('#bodyFiles');
        const $processId = $('#process_id');
        const $btnArchive = $('#btnArchive');

        $(document).ready(function() {
            $btnArchive.click(function () {
                if ($processId.val()) {
                    // AJAX
                    $.get('/api/archives/'+$processId.val(), function (archives) {
                        let html = '';
                        Array.prototype.forEach.call(archives, function(archive) {
                            html += '<tr>' +
                                '<td>'+archive.name+'</td>' +
                                '<td class="text-right">' +
                                '<input type="hidden" name="names[]" value="'+archive.name+'">' +
                                '<input type="hidden" data-archive name="archive_ids[]" value="0">' +
                                '<input type="file" data-input-file accept="application/pdf" style="display:none">' +
                                '<button class="btn btn-sm btn-dark mr-2" type="button" data-archive="'+archive.id+'">Archivo</button>' +
                                '<span><button class="btn btn-sm btn-dark" type="button" data-archive="'+archive.id+'" disabled>Ver</button></span>' +
                                '</td>' +
                                '</tr>';
                        });
                        $body.html(html);
                    });
                }
            });
        });

        $(document).on('click', '[data-archive]', function () {
            const tr = $(this).closest('tr');
            const inputFile = tr.find('[data-input-file]');

            inputFile.click();
        });

        $(document).on('change', '[data-input-file]', function () {
            const tr = $(this).closest('tr');
            const span = tr.find('span');
            const inputArchive = tr.find('[data-archive]');

            const file_data = this.files[0];
            const form_data = new FormData();
            form_data.append('archive', file_data);
            form_data.append('_token', $('[name="_token"]').val());
            $.ajax({
                url: '/archivo/cargar',
                dataType: 'json',
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(archive){
                    this.value = '';
                    inputArchive.val(archive.id);
                    span.html('<a href="'+archive.url+'" target="_blank" class="btn btn-sm btn-dark">Ver</a>');
                }
            });
        });

        $(document).ready(function() {
            $btnSearch.click(function () {
                $(this).attr('disabled', true);
                $alert.hide();

                if ($option.val()) {
                    // AJAX
                    $.get('/api/clients?type='+$option.val()+'&input_search='+$input.val()+'&process_id='+$processId.val(), function (clients) {
                        let html = '';
                        Array.prototype.forEach.call(clients, function(client) {
                            html += '<tr>' +
                                '<td>'+client.document+'</td>' +
                                '<td>'+client.name+'</td>' +
                                '<td>'+client.id+'</td>' +
                                '<td class="text-center"><button class="btn btn-light" type="button" data-obligation="'+client.id+'" data-value="'+client.document+' '+client.name+'" data-select="'+client.id+'">Seleccionar</button></td>' +
                                '</tr>';
                        });
                        $content.html(html);
                    });
                    $(this).attr('disabled', false);
                } else {
                    $(this).attr('disabled', false);
                    $alert.show();
                }
            });
        });

        $(document).on('click', '[data-select]', function () {
            const id = $(this).data('select');
            const value = $(this).data('value');
            const obligation = $(this).data('obligation');

            // $('#obligation_id').val(obligation);
            $('#client_id').val(id);
            $('#document').val(value);
            $content.html('');
            $input.val('');
            $option.val('');

            $('#modalSelectClient').modal('hide');
        });
    </script>
    <script>
        $(document).on('change', '[data-file]', function () {
            let fileData = $(this).prop("files");
            const maxSize = @json(env('MAX_SIZE_FILE'));

            Array.from(fileData).forEach(fileData => {
                const size = fileData.size/1024;

                if (size > maxSize) {
                    $(this).val('');
                    alert('Algunas imágenes exceden el peso máximo (4MB)');
                }
            });
        });
    </script>
@endsection
