<html>
<head>
    <title>Guia-{{ $incident->id }}.pdf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @page { margin: 3em 0 0 0; }
        body {
            font-family: Arial, serif;
            margin: 0;
            font-size: 14px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: 1px solid black;
            vertical-align: center;
        }

        th {
            vertical-align: center;
            text-align: center;
            padding: 4px;
        }
        td {
            height: 5px;
            vertical-align: center;
            text-align: center;
            padding: 4px;
        }

        .col-sm-4 {
            float: left;
            width: 33.3%;
        }
        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 80px;
        }

        .page_break { page-break-before: always; }
    </style>
</head>
<body>
<div style="width: 95%; margin: 0 auto;">
    <table style="border: none !important; width: 90%; margin: 0 auto;">
        <tr style="border: none !important;">
            <td style="border: none !important; text-align: center; position: relative">
                <img style="position: absolute" src="{{ asset('images/home.png') }}" alt="Logo" width="80px">
                <p style="font-weight: bold">G.A.D. MUNICIPAL DEL CANTÓN SALCEDO</p>
                <p>CONTROL DE COMUNICACIONES </p>
                <p>INGRESO</p>
                <p>OFICINA DE RECEPCIÓN DE DOCUMENTACIÓN</p>
            </td>
        </tr>
    </table>

    <p style="text-align: right !important; text-transform: uppercase">Trámite N° {{ $incident->code }}</p>
    <p style="text-transform: uppercase">Procedencia: {{ $incident->client->name }}</p>
    <table style="border: none !important;margin: 0 auto;">
        <tr style="border: none !important;">
            <td style="border: none !important;">
                <table>
                    <tbody>
                    <tr>
                        <td style="border: none !important;"></td>
                        <td style="border: none !important;">DOCUMENTO:</td>
                        <td style="border: none !important;">FECHA DOCUMENTO:</td>
                        <td style="border: none !important;">FECHA RECEPCIÓN:</td>
                        <td style="border: none !important;">HORA:</td>
                    </tr>
                    </tbody>
                    <thead>
                        <tr>
                            <td style="border: none !important;">OFICIO</td>
                            <td style="border: none !important;">0042475</td>
                            <td style="border: none !important;">04-08-2021</td>
                            <td style="border: none !important;">04-08-2021</td>
                            <td style="border: none !important;">14:53:13</td>
                        </tr>
                    </thead>
                </table>
            </td>
        </tr>
    </table>
    <table style="border: none !important; margin-bottom: 3rem">
        <tbody style="border: none !important;">
        <tr>
            <td style="text-align: left !important;border: none !important;">Asunto:</td>
            <td style="text-align: left !important;border: none !important;">{{ $incident->process->name }}</td>
        </tr>
        <tr>
            <td style="text-align: left !important;border: none !important;">Descripción:</td>
            <td style="text-align: left !important;border: none !important;">{{ $incident->description }}</td>
        </tr>
        </tbody>
    </table>
    <table>
        <thead>
        <tr>
            <th colspan="5">TRÁMITE INTERNO</th>
        </tr>
        <tr>
            <th>ENVIADO A</th>
            <th>FECHA</th>
            <th>INSTRUCCIÓN</th>
            <th>DISPUESTO POR</th>
            <th>HORA</th>

        </tr>
        </thead>
        <tbody>
        @for($i = 1; $i <= 10 ; $i++)
            <tr>
                <td style="height: 20px;"></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endfor
        <tr>
            <td style="text-align: left !important;height: 40px;" colspan="5">OBSERVACIONES: </td>
        </tr>
        </tbody>
    </table>
    <table style="border: none !important;">
        <tbody style="border: none !important;">
        <tr>
            <td style="text-align: left !important;border: none !important;">NOTA:</td>
            <td style="text-align: left !important;border: none !important;">
                <p>UNA VEZ FINALIZADO EL TRÁMITE SE DEBEN REMITIR LOS ANEXOS DE LA COMUNICACIÓN. SEÑALADOS EN EL ACÁPITE DE OBSERVACIONES PARA CUSTODIA DE LA DIRECCIÓN DE ARCHIVO CENTRAL, O EN SU DEFECTO COMUNICAR AL FUNCIONARIO RESPONSABLE DE LA MISMA.</p>
            </td>
        </tr>
        <tr>
            <td style="text-align: left !important;border: none !important;"></td>
            <td style="text-align: left !important;border: none !important;">
                CASO CONTRARIO, SE ENTENDERÁ QUE EL CUSTODIO DE LA DOCUMENTACIÓN Y LOS ANEXOS ES LA ÚLTIMA PERSONA QUIEN SE HA DISPUESTO UNA INSTRUCCIÓN MEDIANTE LA RESPECTIVA SUMILLA, EN ESTA HOJA DE CONTROL
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
