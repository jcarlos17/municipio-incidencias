@extends('layouts.app')

@section('styles')

@endsection

@section('content')
    <div class="row">
        <div class="col-sm-3">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Número de incidencias según su estado</h4>
                </div>
                <div class="card-body">
                    <canvas id="pie_chart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Número de incidencias según el departamento al que pertenecen</h4>
                </div>
                <div class="card-body">
                    <canvas id="barChart_1"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Chart ChartJS plugin files -->
    <script src="{{ asset('vendor/chart.js/Chart.bundle.min.js') }}"></script>
{{--    <script src="{{ asset('js/plugins-init/chartjs-init.js') }}"></script>--}}
    <script>
        (function($) {


            var dzSparkLine = function(){
                let draw = Chart.controllers.line.__super__.draw; //draw shadow

                var screenWidth = $(window).width();

                var barChart1 = function(){
                    if(jQuery('#barChart_1').length > 0 ){
                        const barChart_1 = document.getElementById("barChart_1").getContext('2d');

                        barChart_1.height = 100;

                        new Chart(barChart_1, {
                            type: 'bar',
                            data: {
                                defaultFontFamily: 'Poppins',
                                labels: @json($departments),
                                datasets: [
                                    {
                                        label: "# incidencias",
                                        data: @json($incidentsCount),
                                        borderColor: 'rgba(34, 47, 185, 1)',
                                        borderWidth: "0",
                                        backgroundColor: 'rgba(34, 47, 185, 1)'
                                    }
                                ]
                            },
                            options: {
                                legend: false,
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                    xAxes: [{
                                        // Change here
                                        barPercentage: 0.5
                                    }]
                                }
                            }
                        });
                    }
                }

                var pieChart = function(){
                    //pie chart
                    if(jQuery('#pie_chart').length > 0 ){
                        //pie chart
                        const pie_chart = document.getElementById("pie_chart").getContext('2d');
                        // pie_chart.height = 100;
                        new Chart(pie_chart, {
                            type: 'pie',
                            data: {
                                defaultFontFamily: 'Poppins',
                                datasets: [{
                                    data: @json($state),
                                    borderWidth: 0,
                                    backgroundColor: [
                                        "rgba(34, 47, 185, .9)",
                                        "rgba(34, 47, 185, .7)",
                                        "rgba(34, 47, 185, .5)"
                                    ],
                                    hoverBackgroundColor: [
                                        "rgba(34, 47, 185, .9)",
                                        "rgba(34, 47, 185, .7)",
                                        "rgba(34, 47, 185, .5)"
                                    ]

                                }],
                                labels: [
                                    "Resuelto",
                                    "Asignado",
                                    "Pendiente"
                                ]
                            },
                            options: {
                                responsive: true,
                                legend: false,
                                maintainAspectRatio: false
                            }
                        });
                    }
                }



                /* Function ============ */
                return {
                    init:function(){
                    },


                    load:function(){
                        barChart1();
                        pieChart();
                    },

                    resize:function(){
                        barChart1();
                        pieChart();
                    }
                }

            }();

            jQuery(document).ready(function(){
            });

            jQuery(window).on('load',function(){
                dzSparkLine.load();
            });

            jQuery(window).on('resize',function(){
                dzSparkLine.resize();
                setTimeout(function(){ dzSparkLine.resize(); }, 1000);
            });

        })(jQuery);
    </script>
@endsection
