const $btnSearch = $('#btnSearch');
const $option = $('#type');
const $input = $('#inputSearch');
const $alert = $('#alert');
const $content = $('#content-search');
const $processId = $('#process_id');

$btnSearch.click(function () {
    $(this).attr('disabled', true);
    $alert.hide();

    if ($option.val()) {
        // AJAX
        $.get('/api/clients?type='+$option.val()+'&input_search='+$input.val()+'&process_id='+$processId.val(), function (clients) {
            let html = '';
            Array.prototype.forEach.call(clients, function(client) {
                html += '<tr>' +
                    '<td>'+client.document+'</td>' +
                    '<td>'+client.name+'</td>' +
                    '<td>'+client.id+'</td>' +
                    '<td class="text-center"><button class="btn btn-light" type="button" data-select="'+client.id+'" data-name="'+client.name+'" data-document="'+client.document+'">Seleccionar</button></td>' +
                    '</tr>';
            });
            $content.html(html);
        });
        $(this).attr('disabled', false);
    } else {
        $(this).attr('disabled', false);
        $alert.show();
    }
});

$(document).on('click', '[data-select]', function () {
    const id = $(this).data('select');
    const name = $(this).data('name');
    const document = $(this).data('document');

    $('#client_id').val(id);
    $('#client_name').val(name);
    $('#client_document').val(document);
    $content.html('');
    $input.val('');
    $option.val('');

    $('#modalSelectClient').modal('hide');
});
