var data = {
    /* Requerido. Email de la cuenta PagoPlux del Establecimiento o Id/Class
    del elemento html que posee el valor */
    PayboxRemail: "arcos_1969@hotmail.com",
    /* Requerido. Email del usuario que realiza el pago o Id/Class del
    elemento html que posee el valor */
    PayboxSendmail: "#payboxSendmail",
    /* Requerido. Nombre del establecimiento en PagoPlux o Id/Class del
    elemento html que posee el valor */
    PayboxRename: "SALCEDO",
    /* Requerido. Nombre del usuario que realiza el pago o Id/Class del
    elemento html que posee el valor */
    PayboxSendname: "#payboxSendname",
    /* Requerido. Ejemplo: 100.00, 10.00, 1.00 o Id/Class del elemento html
    que posee el valor de los productos sin impuestos */
    PayboxBase0: "#total",
    /* Requerido. Ejemplo: 100.00, 10.00, 1.00 o Id/Class del elemento html
    que posee el valor de los productos con su impuesto incluido */
    PayboxBase12: "0.00",
    /* Requerido. Descripción del pago o Id/Class del elemento html que posee
    el valor */
    PayboxDescription: "Nuevo cobro",
    /* Requerido Tipo de Ejecución
    * Production: true (Modo Producción, Se procesarán cobros y se
    cargarán al sistema, afectará a la tdc)
    * Production: false (Modo Prueba, se realizarán cobros de prueba y no
    se guardará ni afectará al sistema)
    */
    PayboxProduction: false,
    /* Requerido Ambiente de ejecución
    * prod: Modo Producción, Se procesarán cobros y se cargarán al sistema,
    afectará a la tdc.
    * sandbox: Modo Prueba, se realizarán cobros de prueba
    */
    PayboxEnvironment: "sandbox",
    /* Requerido. Lenguaje del Paybox
    * Español: es | (string) (Paybox en español)
    */
    PayboxLanguage: "es",
    /* Opcional Valores HTML que son requeridos por la web que implementa
    el botón de pago.
    * Se permiten utilizar los identificadores de # y . que describen los
    Id y Class de los Elementos HTML
    * Array de identificadores de elementos HTML |
    Ejemplo: PayboxRequired: ["#nombre", "#correo", "#monto"]
    */
    PayboxRequired: [],
    /*
    * Requerido. dirección del tarjeta habiente o Id/Class del elemento
    * html que posee el valor
    */
    PayboxDirection: "Psje San Martin" ,
    /*
    * Requerido. Teléfono del tarjeta habiente o Id/Class del elemento
    * html que posee el valor
    */
    PayBoxClientPhone: "98962656",
    /* Opcionales
    * Solo aplica para comercios que tengan habilitado pagos
    internacionales
    */
    PayBoxClientName: 'Nombre Cliente',
    PayBoxClientIdentification: '1010011111',
    /* Opcional
    * true ->
    Se usa en TRUE cuando se necesita enlazar el paybox a un botón ya existen
    te en el sitio del cliente, caso contrario. NOTA: Valor defecto false
    */
    PayboxPagoPlux: false,
    /* Opcional
    * Es requerido solo en el caso de tener PayboxPagoPlux en true se debe
    especificar el elemento HTML al cual se anclará el click para levantar el
    Paybox
    */
    PayboxIdElement: 'idHtml'
};

var onAuthorize = function(response)
{
    // La variable response posee un Objeto con la respuesta de PagoPlux.
    if (response.status == 'succeeded') {
        // Pago exitoso response contiene la información del pago la cual puede
        // usarse para validaciones que depende del tipo de pago realizado
        $('#token').val(response.detail.token);
        $('#amount').val(response.detail.amount);
        $('#date').val(response.detail.fecha);
        $('#response').val(JSON.stringify(response));
        const $paymentType = $('#payment_type');

        if ($paymentType) {
            $paymentType.val('DE')  // Dinero electrónico, pagoplux
        }

        $('#btnSubmit').click();
    }
};
