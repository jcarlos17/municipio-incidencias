const $selectClient = $('#client_id');
const $selectIncome = $('#income_id');
const $table = $('#table');

$selectClient.on('change', onSelectRates);
$selectIncome.on('change', onShowItems);

function onSelectRates() {
    let clientId = $selectClient.val();

    if (clientId) {
        // AJAX
        $.get('/api/incomes/'+clientId, function (incomes) {
            let html = '<option value="">Seleccionar</option>';
            $.each(incomes, function(i, income) {
                html += '<option value="'+income.id+'">'+income.name+'</option>';
            });

            $selectIncome.html(html);
            $table.html('');
        });
    } else {
        $selectIncome.html('<option value="">Seleccionar</option>');
        $table.html('');
    }
}

function onShowItems() {
    let incomeId = $selectIncome.val();

    if (incomeId) {
        // AJAX
        $.get('/api/items/'+incomeId, function (items) {
            $table.html('');

            let list = '';
            $.each(items, function(i, item) {
                list += '<tr>'+
                    '<th scope="row">'+item.id+'</th>'+
                    '<td class="w-50">'+item.description+'</td>'+
                    '<td class="text-center w-25">'+
                    '<input type="hidden" name="item_names[]" value="'+item.description+'">'+
                    '<input type="hidden" name="item_ids[]" value="'+item.id+'">'+
                    '<input type="number" step="0.01" class="form form-control form-control-sm" data-value name="values[]" required value="0.00">'+
                    '</td>'+
                    '</tr>';
            });

            let tableHtml = '<table class="table">\n' +
                '<thead class="thead-light">\n' +
                '<tr>\n' +
                '<th>ID</th>\n' +
                '<th>Item</th>\n' +
                '<th class="text-center">Valor</th>\n' +
                '</tr>\n' +
                '</thead>\n' +
                '<tbody>\n' +
                list +
                '<tr>\n' +
                '<th colspan="2" class="text-right">Total</th>\n' +
                '<td class="text-center w-25">\n' +
                '<div class="form-group">\n' +
                '<input type="number" class="form form-control form-control-sm" id="total" name="total" readonly>\n' +
                '</div>\n' +
                '</td>\n' +
                '</tr>\n' +
                '</tbody>\n' +
                '</table>';

            $table.html(tableHtml);

            let $values = $('[data-value]');

            $values.keyup(function() {
                let total = 0;
                $values.each(function(){
                    total += parseFloat($(this).val());
                });
                $('#total').val(total.toFixed(2));
            });
        });
    } else {
        $table.html('');
    }
}

let $values = $('[data-value]');

$values.keyup(function() {
    let total = 0;
    $values.each(function(){
        total += parseFloat($(this).val());
    });
    $('#total').val(total.toFixed(2));
});
