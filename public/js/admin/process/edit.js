$(function() {
	$('[data-level]').on('click', editLevelModal);
    $('[data-requirement]').on('click', editRequirementModal);
});

function editLevelModal() {
	// id
	let level_id = $(this).data('level');
	$('#level_id').val(level_id);
    // name
    let level_name = $(this).parent().prev().prev().prev().prev().text();
    $('#level_name').val(level_name);
    // time
    let level_day = $(this).parent().prev().prev().prev().text();
    $('#level_day').val(level_day);
    // time
    let level_hour = $(this).parent().prev().prev().text();
    $('#level_hour').val(level_hour);
    // time
    let level_minute = $(this).parent().prev().text();
    $('#level_minute').val(level_minute);
	// show
	$('#modalEditLevel').modal('show');
}

function editRequirementModal() {
    // id
    const requirement_id = $(this).data('requirement');
    const form_type = $(this).data('form-type');
    const modal = $('#modalEditRequirement');
    const requirementId = modal.find('#requirement_id');
    const requirementName = modal.find('#requirement_name');
    const requirementDescription = modal.find('#requirement_description');
    const content = modal.find('#select-form-type');
    const select = modal.find('#form_type');

    requirementId.val(requirement_id);

    // Name
    let name = $(this).parent().prev().prev().prev().text();
    requirementName.val(name);

    // Description
    let description = $(this).parent().prev().prev().text();
    requirementDescription.val(description);

    // Type
    let type = $(this).parent().prev().text();

    const $formulario = $('#formulario');
    const $archivo = $('#archivo');
    if (type === 'formulario') {
        $formulario.prop('checked', 'checked');
        $archivo.removeProp('checked');
        content.show();
        select.attr('required', true);
        select.find('option[value="'+form_type+'"]').attr("selected", "selected");
    } else {
        $archivo.prop('checked', 'checked');
        $formulario.removeProp('checked');
        content.hide();
        select.attr('required', false);
        select.val('');
    }

    // show
    modal.modal('show');
}
