$(function() {
	$('#select-department').on('change', onSelectDepartmentChange);
    $('#select-process').on('change', onSelectProcessChange);
});

function onSelectDepartmentChange() {
	const department_id = $(this).val();

	if (! department_id) {
		$('#select-process').html('<option value="">Seleccione proceso</option>');
		return;
	}

	// AJAX
	$.get('/api/processes/'+department_id, function (processes) {
		let html_select = '<option value="">Seleccione proceso</option>';
        Array.prototype.forEach.call(processes, function(process) {
            html_select += '<option value="'+process.id+'">'+process.name+'</option>';
        });
		$('#select-process').html(html_select);
	});
}

function onSelectProcessChange() {
    const process_id = $(this).val();

    if (! process_id) {
        $('#select-level').html('<option value="">Seleccione nivel</option>');
        return;
    }

    // AJAX
    $.get('/api/levels/'+process_id, function (levels) {
        let html_select = '<option value="">Seleccione nivel</option>';
        Array.prototype.forEach.call(levels, function(level) {
            html_select += '<option value="'+level.id+'">'+level.name+'</option>';
        });
        $('#select-level').html(html_select);
    });
}
