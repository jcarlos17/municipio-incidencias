$(function () {
    $('[data-departments]').on('change', onNewDepartmentSelected);
});

function onNewDepartmentSelected() {
    const department_id = $(this).val();
    location.href = '/seleccionar/departamento/'+department_id;
}
