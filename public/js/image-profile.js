let $avatarInput, $avatarImage, $avatarForm, $textToEdit, $avatarSmallImage;
let avatarUrl;
$(function () {
    $avatarInput = $('#avatarInput');
    $avatarImage = $('#avatarImage');
    $avatarForm = $('#avatarForm');
    $textToEdit = $('#textToEdit');
    $avatarSmallImage = $('#avatarSmallImage');

    $avatarImage.on('click', function () {
       $avatarInput.click();
    });
    $textToEdit.on('click', function () {
        $avatarInput.click();
    });

    avatarUrl = $avatarForm.attr('action');

    $avatarInput.on('change', function () {
        //AJAX
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let formData = new FormData();
        formData.append('image', $avatarInput[0].files[0]);

        $.ajax({
            url: avatarUrl,
            method: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false
        })
        .done(function (data) {
            if (data.success) {
                $avatarImage.attr('src', '/images/users/'+data.file_name+'?'+ new Date().getTime());
                $avatarSmallImage.attr('src', '/images/users/'+data.file_name+'?'+ new Date().getTime());
            }
        })
        .fail(function () {
            alert('La imagen subida no tiene el formato correcto')
        });
    })
});
