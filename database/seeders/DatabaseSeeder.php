<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(ProcessesTableSeeder::class);
        $this->call(LevelsTableSeeder::class);
        $this->call(IncomeGroupsTableSeeder::class);

        $this->call(SupportsTableSeeder::class);
        $this->call(DepartmentsUserTableSeeder::class);
//        $this->call(IncidentsTableSeeder::class);

        Artisan::call('passport:install');
    }
}
