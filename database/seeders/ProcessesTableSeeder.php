<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Process;

class ProcessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Process::create([
			'name' => '2831.- APERTURA DE PATENTE MUNICIPAL NO OBLIGADOS A LLEVAR CONTABILIDAD',
			'department_id' => 7,
        ]);

        Process::create([
			'name' => '2832.- APERTURA DE PATENTE MUNICIPAL OBLIGADOS A LLEVAR CONTABILIDAD',
			'department_id' => 7,
        ]);

        Process::create([
			'name' => '2837.- CIERRE DE ACTIVIDAD ECONOMICA PATENTE Y 1.5 POR MIL NO OBLIGADOSA LLEVAR CONTABILIDAD',
			'department_id' => 7,
        ]);

        Process::create([
			'name' => '2838.- CIERRE DE ACTIVIDAD ECONOMICA PATENTE Y 1.5 POR MIL OBLIGADOS A LLEVAR CONTABILIDAD',
			'department_id' => 7,
        ]);

        Process::create([
            'name' => '2854.- DETERMINACION DE PATENTE Y 1.5 POR MIL SOBRE LOS ACTIVOS TOTALES',
            'department_id' => 7,
        ]);

        Process::create([
            'name' => '2855.- EXONERACION DEL IMPUESTO A LA PATENTE MUNICIPAL POR TERCERA EDAD',
            'department_id' => 7,
        ]);

        Process::create([
            'name' => '2856.- EXONERACION DEL IMPUESTO A LA PATENTE MUNICIPAL ARTESANO CALIFICADO',
            'department_id' => 7,
        ]);

        // 8 : DIRECCIÓN FINANCIERA
        Process::create([
            'name' => 'BAJA DE IMPUESTO PREDIAL POR TERCERA EDAD',
            'department_id' => 8,
        ]);
        Process::create([
            'name' => 'BAJA DE TÍTULOS DE AGUA POTABLE  POR TERCERA EDAD',
            'department_id' => 8,
        ]);
        Process::create([
            'name' => 'BAJA DE TÍTULOS POR PATENTE E IMPUESTO DEL 1.5 POR MIL SOBRE ACTIVOS TOTALES.',
            'department_id' => 8,
        ]);
        Process::create([
            'name' => 'BAJA DE TÍTULOS POR IMPUESTO PREDIAL ERRORES VARIOS (VALORACIÓN DEL BIEN, DUPLICIDAD, ÁREAS, ETC)',
            'department_id' => 8,
        ]);
        Process::create([
            'name' => 'DEVOLUCIÓN DE ALCABALAS (RESCICIÓN DE CONTRATOS)',
            'department_id' => 8,
        ]);
        Process::create([
            'name' => 'BAJA DE TÍTULOS DE ARRIENDOS LOCALES COMERCIALES',
            'department_id' => 8,
        ]);
        Process::create([
            'name' => 'BAJA POR PRESCRIPCIÓN',
            'department_id' => 8,
        ]);
        Process::create([
            'name' => 'DEVOLUCIÓN DE GARANTÍAS',
            'department_id' => 8,
        ]);
        Process::create([
            'name' => 'PAGO DE PLIEGOS',
            'department_id' => 8,
        ]);

        // 9: UNIDAD DE GESTION URBANAY RURAL
        Process::create([
            'name' => 'INSCRIPCION PROFESIONAL',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'DETERMINACION DE LINEA DE FABRICA',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'PERMISO DE TRABAJOS VARIOS',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'PERMISO DE COMPRA-VENTA',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'CONSULTA ANTEPROYECTO',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'APROBACION DE PLANOS PARA EDIFICACIONES',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'EMISION DEL INFORME FINAL PERMISO DE HABITABILIDAD Y/O DEVOLUCION DE FONDOS DE GARANTIA',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'LEGALIZACION DE CONSTRUCCIONES INFORMALES',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'DECLARACION DE PROPIEDAD HORIZONTAL',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'FACTIBILIDAD PARA URBANIZAR UN PREDIO',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'APROBACION DE ANTEPROYECTO DE URBANIZACIONES',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'APROBACION DE URBANIZACIONES',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'APROBACION DE SUBDIVISIONES Y REESTRUCTURACIONES PARCELARIAS',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'ACTUALIZACION DE PERMISO DE CONSTRUCCION',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'CAMBIO Y OCUPACION DE RETIROS DE CONSTRUCCION',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'CERTIFICACIÓN DE COMPATIBILIDAD DE USO DEL SUELO Y ZONIFICACIÓN',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'REGULACION DE EXCEDENTES O DIFERENCIAS DE AREAS DE TERRENOS',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'COPIAS CERTIFICADAS DE PLANOS APROBADOS',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'PARTICION EXTRAJUDICIAL',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'UNIFICACION DE LOTES',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'PERMISO PARA INSTALACION DE ROTULOS Y PUBLICIDAD EXTERIOR',
            'department_id' => 9,
        ]);
        Process::create([
            'name' => 'MOVILIDAD',
            'department_id' => 9,
        ]);

        // 10: UNIDAD DE PLANEAMIENTO DEL SUELO
        Process::create([
            'name' => 'DECLARATORIA DE UTILIDAD PÚBLICA O INTERÉS SOCIAL',
            'department_id' => 10,
        ]);
        Process::create([
            'name' => 'SUELOEXPROPIACIÓN Y OCUPACIÓN INMEDIATA',
            'department_id' => 10,
        ]);
        Process::create([
            'name' => 'DONACIONES Y COMODATOS',
            'department_id' => 10,
        ]);
        Process::create([
            'name' => 'PERMUTA',
            'department_id' => 10,
        ]);
    }
}
