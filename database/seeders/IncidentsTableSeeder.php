<?php

namespace Database\Seeders;

use App\Models\IncidentChange;
use Illuminate\Database\Seeder;
use App\Models\Incident;

class IncidentsTableSeeder extends Seeder
{

    public function run()
    {
        $incident = Incident::create([
        	'description' => 'Lo que ocurre es que se encontró un problema en la página y esta se cerró.',
        	'severity' => 'N',
        	'process_id' => 1,
        	'department_id' => 1,
        	'level_id' => 1,
        	'client_id' => 2,
            'creator_id' => 3
    	]);
        IncidentChange::create([
            'type' => 'registry',
            'incident_id' => $incident->id,
            'user_id' => 3
        ]);
    }
}
