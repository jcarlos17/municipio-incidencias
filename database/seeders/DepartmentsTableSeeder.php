<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Department;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create([
        	'name' => 'Tecnologías de la información'
        ]);

        Department::create([
        	'name' => 'Tesorería'
        ]);

        Department::create([
            'name' => 'Contabilidad'
        ]);

        Department::create([
            'name' => 'Avalúos'
        ]);

        Department::create([
            'name' => 'Obras públicas'
        ]);

        Department::create([
            'name' => 'Dirección de planificación'
        ]);

        Department::create([
            'name' => 'Rentas'
        ]);

        Department::create([
            'name' => 'DIRECCIÓN FINANCIERA'
        ]);

        Department::create([
            'name' => 'UNIDAD DE GESTION URBANAY RURAL'
        ]);

        Department::create([
            'name' => 'UNIDAD DE PLANEAMIENTO DEL SUELO'
        ]);
    }
}
