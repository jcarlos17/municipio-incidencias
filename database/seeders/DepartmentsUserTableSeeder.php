<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\DepartmentUser;

class DepartmentsUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // user 3
        DepartmentUser::create([
        	'department_id' => 8,
        	'process_id' => 8,
        	'user_id' => 3,
        	'level_id' => 1
		]);

        // user 4
        DepartmentUser::create([
            'department_id' => 8,
            'process_id' => 8,
            'user_id' => 4,
            'level_id' => 1
        ]);
		DepartmentUser::create([
        	'department_id' => 8,
        	'process_id' => 8,
        	'user_id' => 4,
        	'level_id' => 2
		]);

		// user 5
        DepartmentUser::create([
            'department_id' => 8,
            'process_id' => 8,
            'user_id' => 5,
            'level_id' => 3
        ]);
    }
}
