<?php

namespace Database\Seeders;

use App\Models\Admin\IncomeGroup;
use Illuminate\Database\Seeder;

class IncomeGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        IncomeGroup::create([
            'code' => 'T',
            'name' => 'Tasa'
        ]);

        IncomeGroup::create([
            'code' => 'I',
            'name' => 'Impuesto'
        ]);

        IncomeGroup::create([
            'code' => 'C',
            'name' => 'Contribución'
        ]);
    }
}
