<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Level;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Level::create([ // 1
        	'name' => 'Atención por teléfono',
        	'days' => 1,
            'hours' => 10,
            'minutes' => 30,
        	'process_id' => 8,
            'department_id' => 8
    	]);
    	Level::create([ // 2
        	'name' => 'Envío de técnico',
            'days' => 1,
            'hours' => 10,
            'minutes' => 30,
        	'process_id' => 8,
            'department_id' => 8
    	]);
        Level::create([ // 3
            'name' => 'Visita a la central',
            'days' => 1,
            'hours' => 10,
            'minutes' => 30,
            'process_id' => 8,
            'department_id' => 8
        ]);
    }
}
