<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectObligationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collect_obligations', function (Blueprint $table) {
            $table->id();

            $table->date('issue_date');
            $table->dateTime('payment_date')->nullable();
            $table->string('entry_order')->nullable();
            $table->string('period', 6)->nullable();
            $table->string('year', 4)->nullable();

            $table->float('total')->nullable();

            $table->unsignedBigInteger('income_id');
            $table->foreign('income_id')
                ->references('id')->on('incomes');

            $table->unsignedBigInteger('obligation_id');
            $table->foreign('obligation_id')
                ->references('id')->on('obligations');

            $table->unsignedInteger('incident_id')->nullable();
            $table->foreign('incident_id')
                ->references('id')->on('incidents');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users');

            $table->unsignedInteger('cashier_id')->nullable();
            $table->foreign('cashier_id')
                ->references('id')->on('users');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collect_obligations');
    }
}
