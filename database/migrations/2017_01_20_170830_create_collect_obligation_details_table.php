<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectObligationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collect_obligation_details', function (Blueprint $table) {
            $table->id();

            $table->float('value');

            $table->unsignedBigInteger('item_id');
            $table->foreign('item_id')
                ->references('id')->on('items');

            $table->unsignedBigInteger('income_id');
            $table->foreign('income_id')
                ->references('id')->on('incomes');

            $table->unsignedBigInteger('collect_obligation_id');
            $table->foreign('collect_obligation_id')
                ->references('id')->on('collect_obligations');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collect_obligation_details');
    }
}
