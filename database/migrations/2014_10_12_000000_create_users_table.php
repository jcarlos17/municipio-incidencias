<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('email')->nullable();
            $table->string('password');

            $table->string('document')->unique(); // cédula
            $table->string('cellphone')->nullable();
            $table->string('address')->nullable();

            $table->smallInteger('role')->default(User::CLIENT); // 0: Admin | 1: Support | 2: Client
            $table->string('image')->nullable(); // profile photo

            $table->unsignedInteger('selected_department_id')->nullable();
            $table->foreign('selected_department_id')
                ->references('id')->on('departments');

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
