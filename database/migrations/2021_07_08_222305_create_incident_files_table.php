<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidentFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incident_files', function (Blueprint $table) {
            $table->id();

            $table->string('name');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users');

            $table->unsignedInteger('incident_id');
            $table->foreign('incident_id')
                ->references('id')->on('incidents');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_files');
    }
}
