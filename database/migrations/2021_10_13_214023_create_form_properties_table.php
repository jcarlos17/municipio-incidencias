<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_properties', function (Blueprint $table) {
            $table->id();

            $table->string('code')->nullable();
            $table->string('client_name');
            $table->string('client_document');
            $table->string('owner_name');
            $table->string('owner_document');
            $table->string('department');
            $table->string('reason');
            $table->string('street');
            $table->string('district');
            $table->string('sector');
            $table->string('parish');
            $table->text('observation');

            $table->string('document')->nullable();

            $table->unsignedInteger('incident_id');
            $table->foreign('incident_id')
                ->references('id')->on('incidents');

            $table->unsignedInteger('incident_form_id');
            $table->foreign('incident_form_id')
                ->references('id')->on('incident_forms');

            $table->unsignedInteger('support_id')->nullable();
            $table->foreign('support_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_properties');
    }
}
