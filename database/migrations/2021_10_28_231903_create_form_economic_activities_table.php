<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormEconomicActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_economic_activities', function (Blueprint $table) {
            $table->id();

            $table->string('code')->nullable();
            $table->string('client_name');
            $table->string('client_document');
            $table->string('ruc');
            $table->string('business_name');
            $table->date('initial_date');

            $table->string('company_street');
            $table->string('company_parish');
            $table->string('company_sector');
            $table->string('company_floor');
            $table->string('legal_street');
            $table->string('legal_parish');
            $table->string('legal_sector');
            $table->string('legal_floor');

            $table->string('main');
            $table->string('secondary');
            $table->string('box');
            $table->string('commodity');
            $table->string('machinery');
            $table->string('furniture');
            $table->string('vehicles');
            $table->boolean('local_type');
            $table->string('local');
            $table->string('other_assets');
            $table->string('total_assets');
            $table->string('debs_pay');
            $table->string('dsts_pay');
            $table->string('other_passives');
            $table->string('total_passives');
            $table->string('asset_passive');
            $table->string('total');
            $table->string('partner11')->nullable();
            $table->string('partner21')->nullable();
            $table->string('partner12')->nullable();
            $table->string('partner22')->nullable();
            $table->string('partner13')->nullable();
            $table->string('partner23')->nullable();
            $table->string('partner14')->nullable();
            $table->string('partner24')->nullable();
            $table->string('number_partner')->nullable();
            $table->string('partner25')->nullable();

            $table->string('document')->nullable();

            $table->unsignedInteger('incident_id');
            $table->foreign('incident_id')
                ->references('id')->on('incidents');

            $table->unsignedInteger('incident_form_id');
            $table->foreign('incident_form_id')
                ->references('id')->on('incident_forms');

            $table->unsignedInteger('support_id')->nullable();
            $table->foreign('support_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_economic_activities');
    }
}
