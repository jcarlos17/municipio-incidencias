<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_user', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('department_id');
            $table->foreign('department_id')
                ->references('id')->on('departments');

            $table->unsignedInteger('process_id');
            $table->foreign('process_id')
                ->references('id')->on('processes');

            $table->unsignedInteger('level_id');
            $table->foreign('level_id')
                ->references('id')->on('levels');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_user');
    }
}
