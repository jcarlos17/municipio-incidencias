<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidents', function (Blueprint $table) {
            $table->increments('id');

            $table->string('description');
            $table->string('severity', 1);

            $table->boolean('active')->default(1);

            $table->unsignedInteger('process_id')->nullable();
            $table->foreign('process_id')
                ->references('id')->on('processes');

            $table->unsignedInteger('department_id')->nullable();
            $table->foreign('department_id')
                ->references('id')->on('departments');

            $table->unsignedInteger('level_id')->nullable();
            $table->foreign('level_id')
                ->references('id')->on('levels');

            $table->unsignedBigInteger('obligation_id')->nullable();
            $table->foreign('obligation_id')
                ->references('id')->on('obligations');

            $table->unsignedInteger('client_id');
            $table->foreign('client_id')
                ->references('id')->on('users');

            $table->unsignedInteger('support_id')->nullable();
            $table->foreign('support_id')
                ->references('id')->on('users');

            $table->unsignedInteger('creator_id');
            $table->foreign('creator_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidents');
    }
}
