<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormMunicipalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_municipalities', function (Blueprint $table) {
            $table->id();

            $table->string('code')->nullable();
            $table->string('client_name');
            $table->string('client_document');
            $table->string('address');
            $table->string('email');
            $table->string('phone');
            $table->string('process_name');
            $table->string('code_qr')->nullable();

            $table->string('document')->nullable();

            $table->unsignedInteger('incident_id');
            $table->foreign('incident_id')
                ->references('id')->on('incidents');

            $table->unsignedInteger('incident_form_id');
            $table->foreign('incident_form_id')
                ->references('id')->on('incident_forms');

            $table->unsignedInteger('support_id')->nullable();
            $table->foreign('support_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_municipalities');
    }
}
