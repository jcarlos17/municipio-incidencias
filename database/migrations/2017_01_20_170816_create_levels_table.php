<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->integer('days');
            $table->integer('hours');
            $table->integer('minutes');

            $table->unsignedInteger('process_id');
            $table->foreign('process_id')
                ->references('id')->on('processes');

            $table->unsignedInteger('department_id');
            $table->foreign('department_id')
                ->references('id')->on('departments');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('levels');
    }
}
