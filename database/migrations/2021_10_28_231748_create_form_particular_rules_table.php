<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormParticularRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_particular_rules', function (Blueprint $table) {
            $table->id();

            $table->string('code')->nullable();
            $table->string('official_name');
            $table->string('official_position');
            $table->string('client_name');
            $table->string('reason');
            $table->string('address_location');
            $table->string('address_between');
            $table->string('intersection');
            $table->string('key');
            $table->string('sector');
            $table->string('district');
            $table->string('lot');
            $table->string('square');
            $table->string('applicant');
            $table->string('sketch');

            $table->string('via_address');
            $table->string('via_width');
            $table->string('via_address2');
            $table->string('via_width2');
            $table->string('frontal');
            $table->string('side');
            $table->string('bottom');
            $table->string('wall_side', 2)->nullable();
            $table->string('wall_later', 2)->nullable();
            $table->string('wall');
            $table->string('type');
            $table->string('cos');
            $table->string('cus');
            $table->string('other_floor');
            $table->string('land_use');
            $table->string('number_floor');
            $table->string('max_height');

            $table->string('water', 2)->nullable();
            $table->string('road', 2)->nullable();
            $table->string('sewerage', 2)->nullable();
            $table->string('curb', 2)->nullable();
            $table->string('electric', 2)->nullable();
            $table->string('pavement', 2)->nullable();
            $table->string('phone', 2)->nullable();
            $table->string('situation_total', 2)->nullable();
            $table->string('situation_partial', 2)->nullable();
            $table->string('enclosure', 2)->nullable();
            $table->string('factory_line', 2)->nullable();
            $table->text('add_report');

            $table->string('document')->nullable();

            $table->unsignedInteger('incident_id');
            $table->foreign('incident_id')
                ->references('id')->on('incidents');

            $table->unsignedInteger('incident_form_id');
            $table->foreign('incident_form_id')
                ->references('id')->on('incident_forms');

            $table->unsignedInteger('support_id')->nullable();
            $table->foreign('support_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_particular_rules');
    }
}
