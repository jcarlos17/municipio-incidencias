<?php

use App\Models\CollectObligation;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToCollectObligationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collect_obligations', function (Blueprint $table) {
            $table->string('status', 1)->default(CollectObligation::ISSUED);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collect_obligations', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
