<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormBlueprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_blueprints', function (Blueprint $table) {
            $table->id();

            $table->string('code')->nullable();
            $table->string('name');
            $table->string('location');
            $table->string('key');
            $table->string('street');
            $table->float('ground_surface');
            $table->float('ground_floor_surface');
            $table->float('total_construction');
            $table->smallInteger('rooms');
            $table->smallInteger('floors');
            $table->boolean('retreat');

            $table->smallInteger('foundation');
            $table->smallInteger('structure');
            $table->smallInteger('wall');
            $table->smallInteger('cover');

            $table->float('unit_cost');
            $table->float('total_cost');
            $table->smallInteger('financing');

            $table->string('document')->nullable();

            $table->unsignedInteger('incident_id');
            $table->foreign('incident_id')
                ->references('id')->on('incidents');

            $table->unsignedInteger('incident_form_id');
            $table->foreign('incident_form_id')
                ->references('id')->on('incident_forms');

            $table->unsignedInteger('support_id')->nullable();
            $table->foreign('support_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_blueprints');
    }
}
