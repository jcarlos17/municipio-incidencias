<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormCadastreLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_cadastre_locations', function (Blueprint $table) {
            $table->id();

            $table->string('state', 50);
            $table->string('parish', 50);
            $table->string('location');
            $table->string('key');
            $table->string('commercial');

            $table->unsignedBigInteger('form_cadastre_id');
            $table->foreign('form_cadastre_id', 'fc_foreign_id')
                ->references('id')->on('form_cadastres');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_cadastre_locations');
    }
}
