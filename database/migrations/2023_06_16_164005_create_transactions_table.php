<?php

use App\Models\Transaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();

            $table->integer('contribuyente');
            $table->string('transaccion');
            $table->string('fechaTx', 8);
            $table->string('horaTx', 5);
            $table->string('fPago', 2);
            $table->float('valorpTx');
            $table->string('estadoTx', 1);
            $table->string('codigo_respuesta', 2);
            $table->string('mensaje_respuesta');
            $table->string('fecConsiliacion', 8);
            $table->json('emisiondet')->nullable();

            $table->boolean('conciliated')->default(false);

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
