<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_estates', function (Blueprint $table) {
            $table->id();

            $table->string('code')->nullable();
            $table->string('client_name');
            $table->string('year');

            $table->string('document')->nullable();

            $table->unsignedInteger('incident_id');
            $table->foreign('incident_id')
                ->references('id')->on('incidents');

            $table->unsignedInteger('incident_form_id');
            $table->foreign('incident_form_id')
                ->references('id')->on('incident_forms');

            $table->unsignedInteger('support_id')->nullable();
            $table->foreign('support_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_states');
    }
}
