<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormCadastralUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_cadastral_updates', function (Blueprint $table) {
            $table->id();

            $table->string('code')->nullable();
            $table->string('client_name');
            $table->string('client_document');
            $table->boolean('constancy');
            $table->string('year', 4);
            $table->string('canton');
            $table->string('parish');
            $table->string('registration');
            $table->string('estate');
            $table->string('value');

            $table->string('notary');
            $table->string('amount');
            $table->string('seller_name');
            $table->string('seller_document');
            $table->string('buyer_name');
            $table->string('buyer_document');
            $table->string('cadastral_canton');
            $table->string('cadastral_parish');
            $table->string('sector');
            $table->string('property_name');
            $table->date('inscription_date');
            $table->string('total_area');
            $table->string('total_sale');
            $table->string('partial_sale');
            $table->string('reserved_area');
            $table->boolean('domain_limitation');

            $table->string('north_sale');
            $table->string('south_sale');
            $table->string('west_sale');
            $table->string('east_sale');
            $table->string('north_reservation');
            $table->string('south_reservation');
            $table->string('west_reservation');
            $table->string('east_reservation');

            $table->string('document')->nullable();

            $table->unsignedInteger('incident_id');
            $table->foreign('incident_id')
                ->references('id')->on('incidents');

            $table->unsignedInteger('incident_form_id');
            $table->foreign('incident_form_id')
                ->references('id')->on('incident_forms');

            $table->unsignedInteger('support_id')->nullable();
            $table->foreign('support_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_cadastral_updates');
    }
}
