<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();

            $table->string('id_transaccion');
            $table->string('token');
            $table->string('amount');
            $table->string('cardType');
            $table->string('cardIssuer');
            $table->string('cardInfo');
            $table->string('clientID');
            $table->string('clientName');
            $table->string('state');
            $table->string('fecha');
            $table->string('acquirer');
            $table->string('deferred');
            $table->string('interests');
            $table->string('interestValue');
            $table->string('amountWoTaxes');
            $table->string('amountWTaxes');
            $table->string('taxesValue');
            $table->string('tipoPago');

            $table->text('response');

            $table->boolean('succeeded')->default(false);

            $table->unsignedBigInteger('collect_obligation_id');
            $table->foreign('collect_obligation_id')
                ->references('id')->on('collect_obligations');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
