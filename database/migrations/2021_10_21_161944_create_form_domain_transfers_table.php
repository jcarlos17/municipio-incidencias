<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormDomainTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_domain_transfers', function (Blueprint $table) {
            $table->id();

            $table->string('code')->nullable();
            $table->string('notary');
            $table->string('seller');
            $table->string('buyer');
            $table->string('parish');
            $table->string('sector');
            $table->string('street');
            $table->string('old_key');
            $table->string('current_key');
            $table->string('old_property');
            $table->string('perimeter');
            $table->string('acquirer');
            $table->date('acquired_date');
            $table->string('property_extension');
            $table->string('sale_extension');
            $table->string('old_price');
            $table->string('current_sale');

            $table->string('document')->nullable();

            $table->unsignedInteger('incident_id');
            $table->foreign('incident_id')
                ->references('id')->on('incidents');

            $table->unsignedInteger('incident_form_id');
            $table->foreign('incident_form_id')
                ->references('id')->on('incident_forms');

            $table->unsignedInteger('support_id')->nullable();
            $table->foreign('support_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_domain_transfers');
    }
}
